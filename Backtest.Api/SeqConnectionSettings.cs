namespace Backtest.Api;

public class SeqConnectionSettings
{
    public string Url { get; set; }
    public string Token { get; set; }
}