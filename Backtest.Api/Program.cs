using System.Text.Json;
using System.Text.Json.Serialization;
using Backtest.Domain;
using Backtest.Domain.Backtest;
using Backtest.Domain.Job;
using Backtest.Domain.Simulation;
using Backtest.Domain.States;
using Backtest.Domain.States.Settings;
using Backtest.Api;
using Backtest.Api.Subscriptions;
using Backtest.Common;
using Backtest.Persistence;
using FastEndpoints;
using FastEndpoints.Swagger;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using Seq.Api;
using Serilog;
using Serilog.Events;

const string spaSrcPath = "ClientApp";
var bootstrapLogger = new LoggerConfiguration()
    .MinimumLevel.Debug()
    .WriteTo.Console()
    .CreateBootstrapLogger();
BsonSerializer.RegisterSerializer(new GuidSerializer(BsonType.String));
var webPort = int.TryParse(Environment.GetEnvironmentVariable("PORT"), out var port) ? port : 5000;
var builder = WebApplication.CreateBuilder(args);
builder.Configuration
    .AddConsulConfiguration("backtest-api")
    .AddConsulConfiguration("logger-configuration")
    .AddConsulConfiguration("database-settings")
    .AddConsulConfiguration("kafka-streams-configuration");

builder.Services
    .AddFastEndpoints(options =>
    {
        options.IncludeAbstractValidators = true;
    })
    .AddSwaggerDoc(settings =>
    {
        settings.Title = "Backtest API";
        settings.Version = "v1";
    })
    .AddResponseCaching();
builder.Services
    .AddMediatR(cfg => cfg.RegisterServicesFromAssemblyContaining<MongoDbContext>())
    .AddStateByIdQuery<BacktestState>()
    .AddStateByIdQuery<BacktestSettings>()
    .AddStateByIdQuery<StrategySettings>()
    .AddStateByIdQuery<SimulationState>()
    .AddStateByIdQuery<UserParams>()
    .Configure<DatabaseSettings>(builder.Configuration.GetSection(nameof(DatabaseSettings)))
    .AddSingleton<IMongoDbContext, MongoDbContext>()
    .AddHostedService(provider => provider.GetRequiredService<IMongoDbContext>())
    .AddSignalR(hubOptions =>
    {
        hubOptions.EnableDetailedErrors = true;
    })
    .AddJsonProtocol(options =>
    {
        options.PayloadSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
        options.PayloadSerializerOptions.Converters.Add(new JsonStringEnumConverter(allowIntegerValues: false));
    })
    .Services
    .AddSingleton<SubscriptionsManager>()
    .AddCors()
    .AddSingleton(_ =>
    {
        var seqSettings = builder.Configuration.GetSection(nameof(SeqConnectionSettings)).Get<SeqConnectionSettings>();
        var seqConnection = new SeqConnection(seqSettings.Url, seqSettings.Token);
        return seqConnection;
    })
    // .AddSpaStaticFiles(opt => opt.RootPath = $"{spaSrcPath}/build")
    ;

builder.Services.AddOrleansClient(clientBuilder =>
{
    clientBuilder.ConfigureBacktestClient(builder.Configuration, "api", false);
});

builder.Host.UseSerilog((hostContext, loggingConfiguration) => loggingConfiguration
    .ReadFrom.Configuration(hostContext.Configuration)
    .Enrich.WithProperty(Constants.APPLICATION_NAME_PROP, Constants.APPLICATION_NAME_VAL)
    .Enrich.WithProperty(Constants.INSTANCE_NAME_PROP, "Api")
);
builder.WebHost.ConfigureKestrel(x => x.ListenAnyIP(webPort));
try
{
    await using var app = builder.Build();
    app.UseCors(cors => cors
        .SetIsOriginAllowed(_ => true)
        .AllowAnyHeader()
        .AllowAnyMethod()
        .AllowCredentials()
    );
    app.UseMiddleware<CorsMiddleware>();
    app.MapHub<StateUpdatesHub<IBacktest>>("/backtests", options =>
    {
        options.Transports = HttpTransportType.WebSockets;
    });
    app.MapHub<StateUpdatesHub<ISimulation>>("/simulations", options =>
    {
        options.Transports = HttpTransportType.WebSockets;
    });
    app.MapHub<StateUpdatesHub<IJob>>("/jobs", options =>
    {
        options.Transports = HttpTransportType.WebSockets;
    });
    app.UseResponseCaching();
    app.UseFastEndpoints(config =>
    {
        config.Errors.StatusCode = 400;
        config.Endpoints.RoutePrefix = "api";
        config.Errors.ResponseBuilder = (failures, ctx, statusCode) =>
        {
            return new ValidationProblemDetails(
                failures.GroupBy(f => f.PropertyName)
                    .ToDictionary(
                        keySelector: e => e.Key,
                        elementSelector: e => e.Select(m => m.ErrorMessage).ToArray()))
            {
                Type = "https://tools.ietf.org/html/rfc7231#section-6.5.1",
                Title = "One or more validation errors occurred.",
                Status = statusCode,
                Instance = ctx.Request.Path,
                Extensions = { { "traceId", ctx.TraceIdentifier } }
            };
        };
        config.Serializer.Options.Converters.Add(new JsonStringEnumConverter());
    });
    app.UseSwaggerGen();
    app.UseSwaggerUi3(settings => settings.Path = "/swagger");
    // app.UseStaticFiles();
    // app.UseSpa(spa =>
    // {
    //     spa.Options.SourcePath = spaSrcPath;
    //     if (builder.Environment.IsDevelopment())
    //         spa.UseReactDevelopmentServer("start");
    // });
    await app.RunAsync();
}
catch (Exception exception)
{
    bootstrapLogger.Write(LogEventLevel.Error, exception, "Error launching host");
    throw;
}