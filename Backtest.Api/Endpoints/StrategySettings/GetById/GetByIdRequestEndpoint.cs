using Backtest.Api.Abstract;
using Backtest.Domain.Simulation;
using FastEndpoints;

namespace Backtest.Api.Endpoints.StrategySettings.GetById;

public class GetByIdRequestEndpoint : Endpoint<RequestById, ViewModel<Domain.States.Settings.StrategySettings>>
{
    private readonly IClusterClient _clusterClient;

    public GetByIdRequestEndpoint(IClusterClient clusterClient)
    {
        _clusterClient = clusterClient;
    }

    public override void Configure()
    {
        Get("/{Id}/strategy-settings");
        ResponseCache(3600);
        AllowAnonymous();
    }

    public override async Task HandleAsync(RequestById request, CancellationToken cancellationToken)
    {
        var state = await _clusterClient.GetGrain<ISimulation>(request.Id).GetStrategySettings();
        var view = new ViewModel<Domain.States.Settings.StrategySettings>(request.Id, state);
        await SendAsync(view, cancellation: cancellationToken);
    }
}