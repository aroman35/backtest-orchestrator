﻿using Backtest.Api.Abstract;
using Backtest.Domain.Agent;
using Backtest.Domain.States;
using FastEndpoints;

namespace Backtest.Api.Endpoints.Agent.RunningJobs;

public class RunningJobsRequest
{
    public string AgentName { get; set; }
}

public class RunningJobsEndpoint : Endpoint<RunningJobsRequest, ICollection<ViewModel<JobState>>>
{
    private readonly IClusterClient _clusterClient;

    public RunningJobsEndpoint(IClusterClient clusterClient)
    {
        _clusterClient = clusterClient;
    }
    
    public override void Configure()
    {
        Get("/agent/{AgentName}/running-jobs");
        AllowAnonymous();
    }

    public override async Task HandleAsync(RunningJobsRequest request, CancellationToken cancellationToken)
    {
        var agent = _clusterClient.GetGrain<IAgent>(request.AgentName);
        var jobs = await agent.GetRunningJobs();
        var jobStates = await Task.WhenAll(jobs.Select(x => x.GetState()));
        var response = new ViewModel<JobState>[jobs.Length];
        for (var i = 0; i < jobs.Length; i++)
        {
            response[i] = new ViewModel<JobState>(jobs[i].GetPrimaryKey(), jobStates[i]);
        }
        await SendAsync(response, cancellation: cancellationToken);
    }
}