using Backtest.Api.Abstract;
using Backtest.Domain.Job;
using Backtest.Domain.States;
using FastEndpoints;

namespace Backtest.Api.Endpoints.Jobs.GetById;

public class GetByIdRequestEndpoint : Endpoint<RequestById, ViewModel<JobState>>
{
    private readonly IClusterClient _clusterClient;

    public GetByIdRequestEndpoint(IClusterClient clusterClient)
    {
        _clusterClient = clusterClient;
    }
    
    public override void Configure()
    {
        Get("/job/{Id}");
        AllowAnonymous();
    }

    public override async Task HandleAsync(RequestById request, CancellationToken cancellationToken)
    {
        var state = await _clusterClient.GetGrain<IJob>(request.Id).GetState();
        var view = new ViewModel<JobState>(request.Id, state);
        await SendAsync(view, cancellation: cancellationToken);
    }
}