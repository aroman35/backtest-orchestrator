using Backtest.Api.Abstract;
using Backtest.Domain.Job;
using Backtest.Domain.States;
using FastEndpoints;

namespace Backtest.Api.Endpoints.Jobs.GetResult;

public class GetJobResultEndpoint : Endpoint<RequestById, ViewModel<JobResult>>
{
    private readonly IClusterClient _clusterClient;

    public GetJobResultEndpoint(IClusterClient clusterClient)
    {
        _clusterClient = clusterClient;
    }
    
    public override void Configure()
    {
        Get("/job/{Id}/result");
        AllowAnonymous();
    }

    public override async Task HandleAsync(RequestById request, CancellationToken cancellationToken)
    {
        var state = await _clusterClient.GetGrain<IJob>(request.Id).GetResult();
        var view = new ViewModel<JobResult>(request.Id, state);
        await SendAsync(view, cancellation: cancellationToken);
    }
}