using System.Text.RegularExpressions;
using FastEndpoints;
using Seq.Api;
using Seq.Api.Model.Events;
using Seq.Api.Model.LogEvents;

namespace Backtest.Api.Endpoints.Jobs.JobLogs;

public class JobLogsEndpoint : Endpoint<JobLogsRequest, ICollection<JobLogEvent>>
{
    private readonly SeqConnection _seqConnection;

    public JobLogsEndpoint(SeqConnection seqConnection)
    {
        _seqConnection = seqConnection;
    }
    
    public override void Configure()
    {
        AllowAnonymous();
        Get("/job/logs");
    }

    public override async Task HandleAsync(JobLogsRequest request, CancellationToken cancellationToken)
    {
        var filter = $"JobId='{request.JobId}' AND Application='Backtest'";
        var jobEvents = await _seqConnection.Events.ListAsync(
            filter: filter,
            count: request.LogsCount,
            render: true, cancellationToken: cancellationToken);

        await SendAsync(jobEvents.Select(x => new JobLogEvent(x)).ToArray(), cancellation: cancellationToken);
    }
}

public class JobLogsRequest
{
    public Guid JobId { get; set; }
    public int LogsCount { get; set; } = 50;
}

public class JobLogEvent
{
    public JobLogEvent()
    {
    }

    public JobLogEvent(EventEntity eventEntity)
    {
        if (!Enum.TryParse(eventEntity.Level, true, out LogEventLevel level))
            level = LogEventLevel.Information;
        if (!DateTime.TryParse(eventEntity.Timestamp, out var timestamp))
            timestamp = DateTime.UtcNow;
        
        Id = eventEntity.Id;
        Timestamp = timestamp;
        Level = level;
        Message = Regex.Replace(eventEntity.RenderedMessage, @"^Job \[.*\]: ", "");
        Instance = eventEntity.Properties.FirstOrDefault(x => x.Name == nameof(Instance))?.Value.ToString();
        SourceContext = eventEntity.Properties.FirstOrDefault(x => x.Name == nameof(SourceContext))?.Value.ToString();
        Error = Level == LogEventLevel.Warning ? Message : eventEntity.Exception;
    }
    
    public string Id { get; set; }
    public DateTime Timestamp { get; set; }
    public LogEventLevel Level { get; set; }
    public string Message { get; set; }
    public string Instance { get; set; }
    public string SourceContext { get; set; }
    public string Error { get; set; }
}