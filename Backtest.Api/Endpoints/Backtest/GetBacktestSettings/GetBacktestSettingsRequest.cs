using Backtest.Domain.States.Settings;
using Backtest.Api.Abstract;
using Backtest.Domain.Backtest;
using FastEndpoints;

namespace Backtest.Api.Endpoints.Backtest.GetBacktestSettings;

public class GetBacktestSettingsRequestEndpoint : Endpoint<RequestById, ViewModel<BacktestSettings>>
{
    private readonly IClusterClient _clusterClient;

    public GetBacktestSettingsRequestEndpoint(IClusterClient clusterClient)
    {
        _clusterClient = clusterClient;
    }

    public override void Configure()
    {
        Get("/backtest/{Id}/backtest-settings");
        ResponseCache(3600);
        AllowAnonymous();
    }

    public override async Task HandleAsync(RequestById request, CancellationToken cancellationToken)
    {
        var settings = await _clusterClient.GetGrain<IBacktest>(request.Id).GetBacktestSettings();
        var response = new ViewModel<BacktestSettings>(request.Id, settings);
        await SendAsync(response, cancellation: cancellationToken);
    }
}