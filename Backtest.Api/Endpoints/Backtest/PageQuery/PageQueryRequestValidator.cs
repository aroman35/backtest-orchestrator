using Backtest.Persistence.Queries.Backtest;
using FastEndpoints;
using FluentValidation;

namespace Backtest.Api.Endpoints.Backtest.PageQuery;

public class PageQueryRequestValidator : Validator<BacktestsPageQuery>
{
    public PageQueryRequestValidator()
    {
        RuleFor(x => x.Page).GreaterThanOrEqualTo(1).WithMessage("Page number must be a positive number");
        RuleFor(x => x.ItemsPerPage).GreaterThan(0).WithMessage("Items per page must be a positive number");
    }
}