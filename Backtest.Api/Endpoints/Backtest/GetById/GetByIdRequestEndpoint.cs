using Backtest.Domain.States;
using Backtest.Api.Abstract;
using Backtest.Domain.Backtest;
using FastEndpoints;

namespace Backtest.Api.Endpoints.Backtest.GetById;

public class GetByIdRequestEndpoint : Endpoint<RequestById, ViewModel<BacktestState>>
{
    private readonly IClusterClient _clusterClient;

    public GetByIdRequestEndpoint(IClusterClient clusterClient)
    {
        _clusterClient = clusterClient;
    }

    public override void Configure()
    {
        Get("/backtest/{Id}");
        ResponseCache(15);
        AllowAnonymous();
    }

    public override async Task HandleAsync(RequestById request, CancellationToken cancellationToken)
    {
        var state = await _clusterClient.GetGrain<IBacktest>(request.Id).GetState();
        var view = new ViewModel<BacktestState>(request.Id, state);
        await SendAsync(view, cancellation: cancellationToken);
    }
}