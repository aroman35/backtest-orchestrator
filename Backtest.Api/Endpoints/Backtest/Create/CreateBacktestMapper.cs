﻿using Backtest.Domain.Backtest;
using FastEndpoints;
using Newtonsoft.Json;

namespace Backtest.Api.Endpoints.Backtest.Create;

public class CreateBacktestMapper : Mapper<CreateBacktestRequest, CreateBacktestResponse, CreateBacktestCommand>
{
    public override CreateBacktestCommand ToEntity(CreateBacktestRequest request)
    {
        return new CreateBacktestCommand(
            request.Name,
            request.BacktestSettings,
            request.StrategySettings,
            string.IsNullOrEmpty(request.UserParams)
                ? new Dictionary<string, string[]>()
                : JsonConvert.DeserializeObject<string[][]>(request.UserParams)
                    .Select(x => Utils.RepeatParams(x).ToArray())
                    .ToDictionary(x => x[0], x => x.Skip(1).ToArray()),
            request.MdStart,
            request.MdEnd,
            request.MdDates ?? Array.Empty<DateTime>(),
            request.SplitDays,
            request.BinaryName,
            request.EnableLogs,
            request.EnableTelemetry,
            request.RetryCount,
            request.ReportingIntervalSec,
            request.DesiredAgents ?? new List<string>(),
            request.Labels ?? new List<string>(),
            request.User,
            request.Namespace
        );
    }
}

public class CreateBacktestWithPredefinedSettingsMapper
    : Mapper<CreateBacktestWithPredefinedSettingsRequest, CreateBacktestResponse, CreateBacktestWithPredefinedSettingsCommand>
{
    public override CreateBacktestWithPredefinedSettingsCommand ToEntity(CreateBacktestWithPredefinedSettingsRequest request)
    {
        return new CreateBacktestWithPredefinedSettingsCommand(
            request.Name,
            request.BacktestSettings,
            request.StrategySettings,
            request.MdStart,
            request.MdEnd,
            request.MdDates ?? Array.Empty<DateTime>(),
            request.SplitDays,
            request.BinaryName,
            request.EnableLogs,
            request.EnableTelemetry,
            request.RetryCount,
            request.ReportingIntervalSec,
            request.DesiredAgents ?? new List<string>(),
            request.Labels ?? new List<string>(),
            request.User,
            request.Namespace
        );
    }
}