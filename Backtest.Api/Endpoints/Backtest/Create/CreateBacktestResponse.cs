﻿namespace Backtest.Api.Endpoints.Backtest.Create;

public class CreateBacktestResponse
{
    public Guid BacktestId { get; set; }
}