﻿using FastEndpoints;
using FluentValidation;

namespace Backtest.Api.Endpoints.Backtest.Create;

public class CreateBacktestRequestValidator : Validator<CreateBacktestRequest>
{
    public CreateBacktestRequestValidator()
    {
        RuleFor(x => x.Name).NotEmpty().WithMessage("Name is required");
        RuleFor(x => x.BacktestSettings).NotEmpty().WithMessage("Backtest settings are required");
        RuleFor(x => x.StrategySettings).NotEmpty().WithMessage("Strategy settings are required");
        When(x => x.MdDates is null || !x.MdDates.Any(), () =>
        {
            RuleFor(x => x.MdStart).NotNull().WithMessage("No md start was provided");
            RuleFor(x => x.MdEnd).NotNull().WithMessage("No md end was provided");
            RuleFor(x => x.MdStart).Must((request, mdStart) => mdStart < request.MdEnd);
        });
        RuleFor(x => x.BinaryName).NotEmpty().WithMessage("No binary name was provided");
        RuleFor(x => x.RetryCount).GreaterThanOrEqualTo(0).WithMessage("Retry count must be a positive or zero");
        RuleFor(x => x.ReportingIntervalSec).GreaterThanOrEqualTo(60).WithMessage("Reporting interval must me greater than 60 sec");
    }
}

public class CreateBacktestWithPredefinedSettingsRequestValidator : Validator<CreateBacktestWithPredefinedSettingsRequest>
{
    public CreateBacktestWithPredefinedSettingsRequestValidator()
    {
        RuleFor(x => x.Name).NotEmpty().WithMessage("Name is required");
        RuleFor(x => x.BacktestSettings).NotEmpty().WithMessage("Backtest settings are required");
        RuleFor(x => x.StrategySettings).NotNull().Must(x => x.Length > 0).WithMessage("An empty settings array");
        When(x => x.MdDates is null || !x.MdDates.Any(), () =>
        {
            RuleFor(x => x.MdStart).NotNull().WithMessage("No md start was provided");
            RuleFor(x => x.MdEnd).NotNull().WithMessage("No md end was provided");
            RuleFor(x => x.MdStart).Must((request, mdStart) => mdStart < request.MdEnd);
        });
        RuleFor(x => x.BinaryName).NotEmpty().WithMessage("No binary name was provided");
        RuleFor(x => x.RetryCount).GreaterThanOrEqualTo(0).WithMessage("Retry count must be a positive or zero");
        RuleFor(x => x.ReportingIntervalSec).GreaterThanOrEqualTo(60).WithMessage("Reporting interval must me greater than 60 sec");
    }
}