﻿namespace Backtest.Api.Endpoints.Backtest.Create;

public class CreateBacktestRequest
{
    public string Name { get; set; }
    public string BacktestSettings { get; set; }
    public string StrategySettings { get; set; }
    public string UserParams { get; set; }
    public DateTime? MdStart { get; set; }
    public DateTime? MdEnd { get; set; }
    public DateTime[] MdDates { get; set; }
    public bool SplitDays { get; set; }
    public string BinaryName { get; set; }
    public bool EnableLogs { get; set; }
    public bool EnableTelemetry { get; set; }
    public int RetryCount { get; set; }
    public double ReportingIntervalSec { get; set; }
    public List<string> DesiredAgents { get; set; }
    public List<string> Labels { get; set; }
    public string User { get; set; }
    public string Namespace { get; set; }
}

public class CreateBacktestWithPredefinedSettingsRequest
{
    public string Name { get; set; }
    public string BacktestSettings { get; set; }
    public string[] StrategySettings { get; set; }
    public DateTime? MdStart { get; set; }
    public DateTime? MdEnd { get; set; }
    public DateTime[] MdDates { get; set; }
    public bool SplitDays { get; set; }
    public string BinaryName { get; set; }
    public bool EnableLogs { get; set; }
    public bool EnableTelemetry { get; set; }
    public int RetryCount { get; set; }
    public double ReportingIntervalSec { get; set; }
    public List<string> DesiredAgents { get; set; }
    public List<string> Labels { get; set; }
    public string User { get; set; }
    public string Namespace { get; set; }
}