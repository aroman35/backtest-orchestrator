﻿using Backtest.Domain.Backtest;
using Backtest.Domain.Scheduler;
using FastEndpoints;

namespace Backtest.Api.Endpoints.Backtest.Create;

public class CreateBacktestEndpoint : Endpoint<CreateBacktestRequest, CreateBacktestResponse, CreateBacktestMapper>
{
    private readonly IClusterClient _clusterClient;

    public CreateBacktestEndpoint(IClusterClient clusterClient)
    {
        _clusterClient = clusterClient;
    }

    public override void Configure()
    {
        Post("/backtest");
        AllowAnonymous();
        AllowFormData();
    }

    public override async Task HandleAsync(CreateBacktestRequest request, CancellationToken cancellationToken)
    {
        var scheduler = _clusterClient.GetGrain<ISchedulerConnector>(Guid.Empty);
        var agentNames = await scheduler.GetAgents();
        if (request.DesiredAgents is not null && request.DesiredAgents.Any() && !request.DesiredAgents.All(x => agentNames.Contains(x)))
        {
            var notExistingAgents = string.Join(';', request.DesiredAgents.Where(x => !agentNames.Contains(x)));
            ThrowError(x => x.DesiredAgents, $"We don't have this agents: {notExistingAgents}");
        }
        var createCommand = Map.ToEntity(request);
        var backtestId = Guid.NewGuid();
        var backtest = _clusterClient.GetGrain<IBacktest>(backtestId);
        await backtest.Create(createCommand);
        await SendAsync(new CreateBacktestResponse
        {
            BacktestId = backtestId
        }, StatusCodes.Status201Created, cancellationToken);
    }
}

public class CreateBacktestWithPredefinedSettingsEndpoint
    : Endpoint<CreateBacktestWithPredefinedSettingsRequest, CreateBacktestResponse, CreateBacktestWithPredefinedSettingsMapper>
{
    private readonly IClusterClient _clusterClient;

    public CreateBacktestWithPredefinedSettingsEndpoint(IClusterClient clusterClient)
    {
        _clusterClient = clusterClient;
    }
    
    public override void Configure()
    {
        Post("/backtest-with-settings");
        AllowAnonymous();
        // AllowFormData();
    }

    public override async Task HandleAsync(CreateBacktestWithPredefinedSettingsRequest request, CancellationToken cancellationToken)
    {
        var scheduler = _clusterClient.GetGrain<ISchedulerConnector>(Guid.Empty);
        var agentNames = await scheduler.GetAgents();
        if (request.DesiredAgents is not null && request.DesiredAgents.Any() && !request.DesiredAgents.All(x => agentNames.Contains(x)))
        {
            var notExistingAgents = string.Join(';', request.DesiredAgents.Where(x => !agentNames.Contains(x)));
            ThrowError(x => x.DesiredAgents, $"We don't have this agents: {notExistingAgents}");
        }
        var createCommand = Map.ToEntity(request);
        var backtestId = Guid.NewGuid();
        var backtest = _clusterClient.GetGrain<IBacktest>(backtestId);
        await backtest.Create(createCommand);
        await SendAsync(new CreateBacktestResponse
        {
            BacktestId = backtestId
        }, StatusCodes.Status201Created, cancellationToken);
    }
}