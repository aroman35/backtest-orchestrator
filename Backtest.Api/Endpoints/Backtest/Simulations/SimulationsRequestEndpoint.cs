using Backtest.Domain.States;
using Backtest.Api.Abstract;
using Backtest.Domain.Backtest;
using Backtest.Persistence;
using FastEndpoints;

namespace Backtest.Api.Endpoints.Backtest.Simulations;

public class SimulationsRequestEndpoint : Endpoint<RequestById, ICollection<PersistenceStateModel<SimulationState>>>
{
    private readonly IClusterClient _clusterClient;

    public SimulationsRequestEndpoint(IClusterClient clusterClient)
    {
        _clusterClient = clusterClient;
    }

    public override void Configure()
    {
        Get("/backtest/{Id}/simulations");
        ResponseCache(60);
        AllowAnonymous();
    }

    public override async Task HandleAsync(RequestById request, CancellationToken cancellationToken)
    {
        var backtestGrain = _clusterClient.GetGrain<IBacktest>(request.Id);
        var simulationGrains = await backtestGrain.GetSimulations();
        var simulationStates = await Task.WhenAll(simulationGrains.Select(x => x.GetState()));
        var response = new PersistenceStateModel<SimulationState>[simulationGrains.Length];
        for (var i = 0; i < simulationGrains.Length; i++)
        {
            response[i] = new PersistenceStateModel<SimulationState>
            {
                State = simulationStates[i],
                Id = simulationGrains[i].GetPrimaryKey()
            };
        }
        await SendAsync(response, cancellation: cancellationToken);
    }
}