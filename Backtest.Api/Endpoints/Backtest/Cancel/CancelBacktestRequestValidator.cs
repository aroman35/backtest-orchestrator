using FastEndpoints;
using FluentValidation;

namespace Backtest.Api.Endpoints.Backtest.Cancel;

public class CancelBacktestRequestValidator : Validator<CancelBacktestRequest>
{
    public CancelBacktestRequestValidator()
    {
        RuleFor(x => x.BacktestId).NotEqual(Guid.Empty).WithMessage("No backtest id was provided");
    }
}