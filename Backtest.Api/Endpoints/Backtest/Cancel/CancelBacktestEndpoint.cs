﻿using Backtest.Domain.Backtest;
using FastEndpoints;

namespace Backtest.Api.Endpoints.Backtest.Cancel;

public class CancelBacktestEndpoint : Endpoint<CancelBacktestRequest,EmptyResponse>
{
    private readonly IClusterClient _clusterClient;

    public override void Configure()
    {
        Post("/backtest/cancel");
        AllowAnonymous();
    }
    
    public CancelBacktestEndpoint(IClusterClient clusterClient)
    {
        _clusterClient = clusterClient;
    }

    public override async Task HandleAsync(CancelBacktestRequest request, CancellationToken cancellationToken)
    {
        var backtest = _clusterClient.GetGrain<IBacktest>(request.BacktestId);
        await backtest.Cancel(request.Force);
        await SendAsync(new EmptyResponse(), 200, cancellationToken);
    }
}