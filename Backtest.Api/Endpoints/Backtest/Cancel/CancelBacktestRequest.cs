﻿namespace Backtest.Api.Endpoints.Backtest.Cancel;

public class CancelBacktestRequest
{
    public Guid BacktestId { get; set; }
    public bool Force { get; set; } = true;
}