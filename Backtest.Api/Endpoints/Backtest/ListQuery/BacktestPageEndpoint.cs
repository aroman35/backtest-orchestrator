﻿using Backtest.Domain.States;
using Backtest.Persistence;
using Backtest.Persistence.Queries.Backtest;
using FastEndpoints;
using MediatR;

namespace Backtest.Api.Endpoints.Backtest.ListQuery;

public class BacktestPageEndpoint : Endpoint<BacktestsPageQuery, PageResult<BacktestState>>
{
    private readonly IMediator _mediator;

    public BacktestPageEndpoint(IMediator mediator)
    {
        _mediator = mediator;
    }

    public override void Configure()
    {
        AllowAnonymous();
        Get("/backtest/list");
    }
    
    public override async Task HandleAsync(BacktestsPageQuery request, CancellationToken cancellationToken)
    {
        var backtestsPage = await _mediator.Send(request, cancellationToken);
        await SendAsync(backtestsPage, cancellation: cancellationToken);
    }
}