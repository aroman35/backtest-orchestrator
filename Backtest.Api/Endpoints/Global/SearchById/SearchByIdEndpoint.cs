using Backtest.Api.Abstract;
using Backtest.Persistence.Queries;
using FastEndpoints;
using MediatR;

namespace Backtest.Api.Endpoints.Global.SearchById;

public class SearchByIdEndpoint : Endpoint<RequestById, GlobalIdSearchResult>
{
    private readonly IMediator _mediator;

    public SearchByIdEndpoint(IMediator mediator)
    {
        _mediator = mediator;
    }

    public override void Configure()
    {
        Get("/global/{Id}");
        ResponseCache(15);
        AllowAnonymous();
    }

    public override async Task HandleAsync(RequestById request, CancellationToken cancellationToken)
    {
        var searchResult = await _mediator.Send(new GlobalIdSearchQuery(request.Id), cancellationToken);
        await SendAsync(searchResult, cancellation: cancellationToken);
    }
}