using Backtest.Domain.States;
using Backtest.Api.Abstract;
using Backtest.Domain.Simulation;
using FastEndpoints;

namespace Backtest.Api.Endpoints.Simulation.Results;

public class SimulationResultsEndpoint : Endpoint<RequestById, ICollection<ViewModel<JobResult>>>
{
    private readonly IClusterClient _clusterClient;

    public SimulationResultsEndpoint(IClusterClient clusterClient)
    {
        _clusterClient = clusterClient;
    }


    public override void Configure()
    {
        Get("/simulation/{Id}/results");
        ResponseCache(15);
        AllowAnonymous();
        
    }
    
    public override async Task HandleAsync(RequestById request, CancellationToken cancellationToken)
    {
        var simulation = _clusterClient.GetGrain<ISimulation>(request.Id);
        var jobs = await simulation.GetJobs();
        var jobResults = await Task.WhenAll(jobs.Select(x => x.GetResult()));
        var response = new ViewModel<JobResult>[jobs.Length];
        for (var i = 0; i < jobs.Length; i++)
        {
            response[i] = new ViewModel<JobResult>(jobs[i].GetPrimaryKey(), jobResults[i]);
        }
        await SendAsync(response, cancellation: cancellationToken);
    }
}