﻿using Backtest.Api.Abstract;
using Backtest.Persistence.Queries.Simulation;
using FastEndpoints;
using MediatR;

namespace Backtest.Api.Endpoints.Simulation.AppliedParams;

public class GetAppliedUserParamsEndpoint : Endpoint<RequestById, SimulationParams[]>
{
    private readonly IMediator _mediator;

    public GetAppliedUserParamsEndpoint(IMediator mediator)
    {
        _mediator = mediator;
    }

    public override void Configure()
    {
        Get("/backtest/{Id}/simulations-applied-params");
        ResponseCache(60);
        AllowAnonymous();
    }

    public override async Task HandleAsync(RequestById request, CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(new SimulationParamsByBacktestIdQuery(request.Id), cancellationToken);
        await SendAsync(response.Select(x => new SimulationParams(x.Key, x.Value)).ToArray(), cancellation: cancellationToken);
    }
}

public record SimulationParams(Guid SimulationId, Dictionary<string, string> AppliedParam);