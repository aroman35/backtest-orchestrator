using Backtest.Domain.States;
using Backtest.Api.Abstract;
using Backtest.Domain.Simulation;
using FastEndpoints;

namespace Backtest.Api.Endpoints.Simulation.Jobs;

public class JobsRequestEndpoint : Endpoint<RequestById, ICollection<ViewModel<JobState>>>
{
    private readonly IClusterClient _clusterClient;

    public JobsRequestEndpoint(IClusterClient clusterClient)
    {
        _clusterClient = clusterClient;
    }

    public override void Configure()
    {
        Get("/simulation/{Id}/jobs");
        ResponseCache(60);
        AllowAnonymous();
    }

    public override async Task HandleAsync(RequestById request, CancellationToken cancellationToken)
    {
        var simulation = _clusterClient.GetGrain<ISimulation>(request.Id);
        var jobs = await simulation.GetJobs();
        var jobStates = await Task.WhenAll(jobs.Select(x => x.GetState()));
        var response = new ViewModel<JobState>[jobs.Length];
        for (var i = 0; i < jobs.Length; i++)
        {
            response[i] = new ViewModel<JobState>(jobs[i].GetPrimaryKey(), jobStates[i]);
        }
        await SendAsync(response, cancellation: cancellationToken);
    }
}