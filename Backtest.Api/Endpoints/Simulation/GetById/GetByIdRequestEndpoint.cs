using Backtest.Domain.States;
using Backtest.Api.Abstract;
using Backtest.Domain.Simulation;
using FastEndpoints;

namespace Backtest.Api.Endpoints.Simulation.GetById;

public class GetByIdRequestEndpoint : Endpoint<RequestById, ViewModel<SimulationState>>
{
    private readonly IClusterClient _clusterClient;

    public GetByIdRequestEndpoint(IClusterClient clusterClient)
    {
        _clusterClient = clusterClient;
    }
    
    public override void Configure()
    {
        Get("/simulation/{Id}");
        ResponseCache(15);
        AllowAnonymous();
    }
    
    public override async Task HandleAsync(RequestById request, CancellationToken cancellationToken)
    {
        var state = await _clusterClient.GetGrain<ISimulation>(request.Id).GetState();
        var view = new ViewModel<SimulationState>(request.Id, state);
        await SendAsync(view, cancellation: cancellationToken);
    }
}