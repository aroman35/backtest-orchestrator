using Backtest.Persistence;
using Backtest.Persistence.Queries;
using FastEndpoints;
using FluentValidation;
using MediatR;

namespace Backtest.Api.Abstract;

public class RequestById
{
    public Guid Id { get; set; }
}

public class RequestByIdValidator : Validator<RequestById>
{
    public RequestByIdValidator()
    {
        RuleFor(x => x.Id).NotEqual(Guid.Empty).WithMessage("No id was provided");
    }
}

public class PersistentToViewMapper<TState> : Mapper<RequestById, ViewModel<TState>, PersistenceStateModel<TState>>
{
    public override ViewModel<TState> FromEntity(PersistenceStateModel<TState> entity)
    {
        return new ViewModel<TState>(entity.Id, entity.State);
    }
}


public abstract class RequestByIdEndpoint<TState> : Endpoint<RequestById, ViewModel<TState>, PersistentToViewMapper<TState>>
{
    private protected abstract string Route { get; }
    private protected virtual int ResponseCachingTimeout { get; } = -1;

    public override void Configure()
    {
        Get(Route);
        AllowAnonymous();
        if (ResponseCachingTimeout > 0)
            this.ResponseCache(ResponseCachingTimeout);
    }

    public override async Task HandleAsync(RequestById request, CancellationToken cancellationToken)
    {
        var mediator = HttpContext.RequestServices.GetRequiredService<IMediator>();
        var item = await mediator.Send(new GenericStateByIdQuery<TState>(request.Id), cancellationToken);
        if (item is null)
        {
            await SendNotFoundAsync(cancellationToken);
            return;
        }
        await SendAsync(Map.FromEntity(item), cancellation: cancellationToken);
    }
}

public class ViewModel<TState>
{
    public ViewModel(Guid id, TState state)
    {
        Id = id;
        State = state;
    }
    public Guid Id { get; set; }
    public TState State { get; set; }
}