import {IViewModel} from "../../types/IViewModel";
import {ISimulation} from "../../types/ISimulation";
import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/dist/query/react";
import * as signalR from "@microsoft/signalr";
import {JsonHubProtocol} from "@microsoft/signalr";
import {ItemSummary} from "../../types/ItemSummary";
import {IStrategySettings} from "../../types/IStrategySettings";
import {ISimulationParams} from "../../types/ISimulationParams";

export const simulationsApi = createApi({
    reducerPath: 'simulations/api',
    baseQuery: fetchBaseQuery({
        baseUrl: process.env.REACT_APP_BACKTEST_API_URL + '/api'
    }),
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
    refetchOnReconnect: true,
    endpoints: (builder) => ({
        getSimulations: builder.query<IViewModel<ISimulation>[], string>({
            query: (backtestId) => ({
                url: `/backtest/${backtestId}/simulations`
            }),
            async onCacheEntryAdded(
                arg,
                { updateCachedData, cacheDataLoaded, cacheEntryRemoved, dispatch }
            ) {
                const cacheData = await cacheDataLoaded
                const simulationIds = cacheData.data.map(simulation => simulation.id)

                const connection = new signalR.HubConnectionBuilder()
                    .withUrl(process.env.REACT_APP_BACKTEST_API_URL + "/simulations")
                    .withHubProtocol(new JsonHubProtocol())
                    .withAutomaticReconnect()
                    .build();

                connection.on("updateReceived", (message: ItemSummary) => {
                    updateCachedData((draft) => {
                        const existingSimulation = draft.find(
                            (simulation) => simulation.id === message.itemId
                        )
                        if (existingSimulation) {
                            existingSimulation.state.summary = {
                                ...existingSimulation.state.summary,
                                progress: message.progress,
                                status: message.status,
                                totalJobs: message.totalJobs,
                                finishedJobs: message.finishedJobs,
                                failedJobs: message.failedJobs,
                            }
                        }
                    })
                })
                await connection.start()
                await connection.send("subscribe", simulationIds)

                await cacheEntryRemoved
                await connection.stop()
            }
        }),
        getSimulationsAppliedParams: builder.query<ISimulationParams[], string>({
            query: (backtestId) => ({
                url: `/backtest/${backtestId}/simulations-applied-params`
            })
        }),
        getSimulationDetails: builder.query<IViewModel<ISimulation>, string>({
            query: (simulationId) => ({
                url: `/simulation/${simulationId}`
            }),
            async onCacheEntryAdded(
                arg,
                { updateCachedData, cacheDataLoaded, cacheEntryRemoved, dispatch }
            ) {
                const cacheData = await cacheDataLoaded
                const simulationId = cacheData.data.id

                const connection = new signalR.HubConnectionBuilder()
                    .withUrl(process.env.REACT_APP_BACKTEST_API_URL + "/simulations")
                    .withHubProtocol(new JsonHubProtocol())
                    .withAutomaticReconnect()
                    .build();

                connection.on("updateReceived", (message: ItemSummary) => {
                    updateCachedData(cachedValue => {
                        cachedValue.state.summary.progress = message.progress
                        cachedValue.state.summary.failedJobs = message.failedJobs
                        cachedValue.state.summary.totalJobs = message.totalJobs
                        cachedValue.state.summary.finishedJobs = message.finishedJobs
                        cachedValue.state.summary.status = message.status
                    })
                })
                await connection.start()
                await connection.send("subscribe", [simulationId])

                await cacheEntryRemoved
                await connection.stop()
            }
        }),
        getStrategySettings: builder.query<IViewModel<IStrategySettings>, string>({
            query: (simulationId) => ({
                url: `${simulationId}/strategy-settings`
            })
        })
    })
})

export const {
    useGetSimulationsQuery,
    useGetSimulationDetailsQuery,
    useGetStrategySettingsQuery,
    useGetSimulationsAppliedParamsQuery
} = simulationsApi