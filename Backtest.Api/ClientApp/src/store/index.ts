import { configureStore } from '@reduxjs/toolkit'
import {setupListeners} from "@reduxjs/toolkit/query";
import {backtestsSlice} from "./backtests/backtestsSlice";
import {backtestApi} from "./backtests/backtest.api";
import {backtestsTabsSlice} from "./backtests/backtestTabsSlice";
import {simulationsApi} from "./simulations/simulations.api";
import {jobsApi} from "./jobs/jobs.api";
import {loadState, saveState} from "./localStorage";

const persistedState = loadState();

export const store = configureStore({
    reducer: {
        backtests: backtestsSlice.reducer,
        backtestsTabs: backtestsTabsSlice.reducer,
        [backtestApi.reducerPath]: backtestApi.reducer,
        [simulationsApi.reducerPath]: simulationsApi.reducer,
        [jobsApi.reducerPath]: jobsApi.reducer
    },
    preloadedState: persistedState,
    devTools: process.env.NODE_ENV !== 'production',
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(backtestApi.middleware, simulationsApi.middleware, jobsApi.middleware)
})

store.subscribe(() => {
    saveState(store.getState());
});

setupListeners(store.dispatch)

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
