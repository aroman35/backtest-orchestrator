import {createEntityAdapter, createSlice, PayloadAction} from "@reduxjs/toolkit";
import {backtestApi} from "./backtest.api";
import {simulationsApi} from "../simulations/simulations.api";

export interface BacktestsTabContent {
    name?: string
    id: string
    type: BacktestsTabType
}

export enum BacktestsTabType {
    list,
    backtest,
    simulation,
    job
}

interface BacktestTabState {
    activeTab: string
    tabContents: BacktestsTabContent[]
}

export const backtestsTabsAdapter = createEntityAdapter<BacktestsTabContent>({
    selectId: (tab) => tab.id
})

const initialState: BacktestTabState = {
    activeTab: "backtests-list",
    tabContents: [
        {
            name: '',
            id: 'backtests-list',
            type: BacktestsTabType.list
        }
    ]
}

export const backtestsTabsSlice = createSlice({
    name: 'backtest.tabs',
    initialState: backtestsTabsAdapter.getInitialState(initialState),
    reducers: {
        setActiveTab (state, action: PayloadAction<string>) {
            state.activeTab = action.payload
        },
        addTab (state, action: PayloadAction<BacktestsTabContent>) {
            backtestsTabsAdapter.addOne(state, {
                type: action.payload.type,
                id: action.payload.id
            })
        },
        addBacktestTab (state, action: PayloadAction<BacktestsTabContent>) {
            backtestsTabsAdapter.addOne(state, {
                type: BacktestsTabType.backtest,
                id: action.payload.id
            })
        },
        addSimulationTab (state, action: PayloadAction<BacktestsTabContent>) {
            backtestsTabsAdapter.addOne(state, {
                type: BacktestsTabType.simulation,
                id: action.payload.id
            })
        },
        removeBacktestTab (state, action: PayloadAction<string>) {
            backtestsTabsAdapter.removeOne(state, action.payload)

            if (action.payload === state.activeTab) {
                state.activeTab = state.tabContents[0].id
            }
        }
    },
    extraReducers: builder => {
        builder.addMatcher(
            backtestApi.endpoints.getBacktestDetails.matchFulfilled,
            (state, {payload}) => {
                backtestsTabsAdapter.updateOne(state, {
                    id: payload.id,
                    changes: {
                        name: payload.state.name
                    }
                })
            })
        builder.addMatcher(
            simulationsApi.endpoints.getSimulationDetails.matchFulfilled,
            (state, {payload}) => {
                backtestsTabsAdapter.updateOne(state, {
                    id: payload.id,
                    changes: {
                        name: payload.state.name
                    }
                })
            }
        )
    }
})

export const backtestTabsActions = backtestsTabsSlice.actions