import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {IBacktestsPageQuery} from "../../types/IBacktestsPageQuery";
import {backtestApi, backtestsAdapter} from "./backtest.api";
import {ItemSummary} from "../../types/ItemSummary";
import {IBacktestSummary} from "../../types/IBacktestSummary";

const initialQuery = {
    page: 1,
    itemsPerPage: Math.round((window.innerHeight / 50))
} as IBacktestsPageQuery

export const backtestsSlice = createSlice({
    name: 'backtest',
    initialState: backtestsAdapter.getInitialState({
        pageQuery: initialQuery,
        totalPages: 0
    }),
    reducers: {
        applySearchQuery (state, action: PayloadAction<IBacktestsPageQuery>) {
            state.pageQuery = action.payload
        },
        gotoBacktestsPage (state, action: PayloadAction<number>) {
            state.pageQuery.page = action.payload
        },
        setPageSize (state, action: PayloadAction<number>) {
            state.pageQuery.itemsPerPage = action.payload
        },
        applySummary (state, action: PayloadAction<ItemSummary>) {
            const selection = backtestsAdapter.getSelectors().selectById(state, action.payload.itemId)
            if (selection) {
                const updatedSummary = {
                    progress: action.payload.progress,
                    status: action.payload.status,
                    totalJobs: action.payload.totalJobs,
                    finishedJobs: action.payload.finishedJobs,
                    failedJobs: action.payload.failedJobs
                } as IBacktestSummary
                backtestsAdapter.updateOne(state, {
                    id: action.payload.itemId,
                    changes: {
                        etag: selection.etag,
                        state: {
                            name: selection.state.name,
                            labels: selection.state.labels,
                            mdStart: selection.state.mdStart,
                            mdEnd: selection.state.mdEnd,
                            mdDates: selection.state.mdDates,
                            splitDays: selection.state.splitDays,
                            binaryName: selection.state.binaryName,
                            desiredAgents: selection.state.desiredAgents,
                            enableLogs: selection.state.enableLogs,
                            enableTelemetry: selection.state.enableTelemetry,
                            retryCount: selection.state.retryCount,
                            reportingTimeoutSec: selection.state.reportingTimeoutSec,
                            created: selection.state.created,
                            summary: updatedSummary
                        }
                    }
                })
            }
        }
    },
    extraReducers: builder => {
        builder.addMatcher(
            backtestApi.endpoints.getBacktestsList.matchFulfilled,
            (state, {payload}) => {
                backtestsAdapter.removeAll(state)
                backtestsAdapter.addMany(state, payload.items)
                state.totalPages = payload.totalPages
                state.pageQuery.page = payload.page
            })
        builder.addMatcher(
            backtestApi.endpoints.getBacktestDetails.matchFulfilled,
            (state, {payload}) => {
                backtestsAdapter.upsertOne(state, payload)
            })
    }
})

export const {applySummary} = backtestsSlice.actions
export const backtestActions = backtestsSlice.actions

export default backtestsSlice.reducer
