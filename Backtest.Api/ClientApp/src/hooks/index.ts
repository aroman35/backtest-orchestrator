import {TypedUseSelectorHook, useDispatch, useSelector} from "react-redux";
import {AppDispatch, RootState} from "../store";
import {backtestActions} from "../store/backtests/backtestsSlice";
import {bindActionCreators} from "@reduxjs/toolkit";
import {backtestTabsActions} from "../store/backtests/backtestTabsSlice";

export const useAppDispatch: () => AppDispatch = useDispatch
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector

const actions = {
    ...backtestActions, ...backtestTabsActions
}

export const useActions = () => {
    const dispatch = useDispatch()
    return bindActionCreators(actions, dispatch)
}