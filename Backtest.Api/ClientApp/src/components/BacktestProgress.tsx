import {ProgressBar} from "react-bootstrap";

export interface BacktestProgressProps {
    progress: number
}

export function BacktestProgress ({progress}: BacktestProgressProps) {
    const getVariant = (progress: number) => {
        if (progress < 25) {
            return 'danger';
        } else if (progress < 50) {
            return 'warning';
        } else if (progress < 75) {
            return 'info';
        } else {
            return 'success';
        }
    };

    return(<ProgressBar animated now={progress} label={`${progress}%`} variant={getVariant(progress)}/>)
}