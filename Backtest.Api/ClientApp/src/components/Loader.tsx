import React from "react";
import {Col, Container, Row, Spinner} from "react-bootstrap";

export function Loader() {
    return (
        <Container>
            <Row>
                <Col></Col>
                <Col>
                    <Spinner animation="border" variant="info" />
                </Col>
                <Col></Col>
            </Row>
        </Container>
    )
}