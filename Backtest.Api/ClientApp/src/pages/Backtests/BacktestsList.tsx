import {useActions, useAppSelector} from "../../hooks";
import {backtestsAdapter, useGetBacktestsListQuery} from "../../store/backtests/backtest.api";
import { ColumnDef } from '@tanstack/react-table';
import React, {useMemo} from "react";
import {IViewModel} from "../../types/IViewModel";
import {IBacktest} from "../../types/IBacktest";
import {Table} from "../../components/Table";
import {Loader} from "../../components/Loader";
import {ErrorMessage } from "../../components/ErrorMessage";
import {TaskStatusBadge} from "../../components/TaskStatusBadge";
import {BacktestsSearchPanel} from "./BacktestsSearchPanel";
import {BacktestsPagination} from "./BacktestsPagination";
import {BacktestsTabType} from "../../store/backtests/backtestTabsSlice";
import {Accordion} from "react-bootstrap";

export function BacktestsList () {
    const {pageQuery} = useAppSelector(state => state.backtests)
    const backtestsState = useAppSelector(state => state.backtests)
    const {addBacktestTab, setActiveTab} = useActions()
    const {isLoading, isError, data, error} = useGetBacktestsListQuery(pageQuery)


    const columnsMemo = useMemo<ColumnDef<IViewModel<IBacktest>>[]>(
        () => [
            {
                header: 'Name',
                cell: (cell) => (
                    <span
                        onClick={() => {
                            addBacktestTab({
                                id: cell.row.original.id,
                                type: BacktestsTabType.backtest
                            })
                            setActiveTab(cell.row.original.id)
                        }}
                        style={{cursor: 'pointer'}}
                    >{cell.row.original.state.name}</span>
                )
            },
            {
                header: 'Status',
                cell: (cell) => (<TaskStatusBadge status={cell.row.original.state.summary.status}/>)
            },
            {
                header: 'Progress',
                cell: (cell) => <b>{cell.row.original.state.summary.progress}%</b>
            },
            {
                header: 'Created',
                cell: (cell) => <>{new Date(cell.row.original.state.created).toLocaleString()}</>
            },
            {
                header: 'Jobs',
                cell: (cell) => <span>
                    <b style={{color: "green"}}>{cell.row.original.state.summary.finishedJobs}</b>
                    /{cell.row.original.state.summary.totalJobs}
                    <b style={{color: "red"}}>[{cell.row.original.state.summary.failedJobs}]</b>
                </span>
            },
            {
                header: 'Binary',
                cell: (cell) => <i>{cell.row.original.state.binaryName}</i>
            },
            {
                header: 'MD Start',
                cell: (cell) => <>{new Date(cell.row.original.state.mdStart).toLocaleDateString()}</>
            },
            {
                header: 'MD End',
                cell: (cell) => <>{new Date(cell.row.original.state.mdEnd).toLocaleDateString()}</>
            },
            {
                header: 'Split Days',
                cell: (cell) => cell.row.original.state.splitDays && <b className="bi bi-check-lg" style={{color: 'green'}}/>
            }
        ],
        [addBacktestTab, setActiveTab]
    );

    if (isLoading) return (<Loader/>)

    return (
        <>
            <Accordion>
                <Accordion.Item eventKey="0">
                    <Accordion.Header>Search</Accordion.Header>
                    <Accordion.Body>
                        <BacktestsSearchPanel/>
                    </Accordion.Body>
                </Accordion.Item>
            </Accordion>
            {isError && <ErrorMessage message={JSON.stringify(error)}></ErrorMessage>}
            {data && <Table data={backtestsAdapter.getSelectors().selectAll(backtestsState)} columns={columnsMemo}></Table>}
            <BacktestsPagination/>
        </>
    )
}