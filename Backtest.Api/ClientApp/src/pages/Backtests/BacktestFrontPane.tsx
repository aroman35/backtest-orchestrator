import {Breadcrumb} from "react-bootstrap";
import {useActions} from "../../hooks";
import {BacktestsTabType} from "../../store/backtests/backtestTabsSlice";

export interface BacktestFrontPaneProps {
    backtestName: string
    backtestId: string
    simulationName?: string
    simulationId?: string
    jobName?: string
    jobId?: string
}

export function BacktestFrontPane(props: BacktestFrontPaneProps) {
    const {addBacktestTab, setActiveTab, addSimulationTab} = useActions()

    return (
        <Breadcrumb>
            <Breadcrumb.Item
                onClick={() => {
                    addBacktestTab({
                        id: props.backtestId,
                        type: BacktestsTabType.backtest
                    })
                    setActiveTab(props.backtestId)
                }}
                style={{cursor: 'pointer'}}
            >{props.backtestName}</Breadcrumb.Item>
            {props.simulationName && props.simulationId && <Breadcrumb.Item onClick={() => {
                addSimulationTab({
                    id: props.simulationId!,
                    type: BacktestsTabType.simulation
                })
                setActiveTab(props.simulationId!)
            }}>{props.simulationName}</Breadcrumb.Item>}
            {props.jobName && props.jobId && <Breadcrumb.Item>{props.jobName}</Breadcrumb.Item>}
        </Breadcrumb>
    )
}