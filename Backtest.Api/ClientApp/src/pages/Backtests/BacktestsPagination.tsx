import {Col, Container, Pagination, Row, Form} from "react-bootstrap";
import {useActions, useAppSelector} from "../../hooks";
import React from "react";

export function BacktestsPagination() {
    const {totalPages, currentPage, itemsPerPage} = useAppSelector(state => ({
        totalPages: state.backtests.totalPages,
        currentPage: state.backtests.pageQuery.page,
        itemsPerPage: state.backtests.pageQuery.itemsPerPage
    }));
    const {gotoBacktestsPage, setPageSize} = useActions()
    const handlePageChange = function (page: number) {
        gotoBacktestsPage(page)
    }

    const handlePageSizeChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setPageSize(parseInt(event.target.value))
    }

    const displayPages = () => {
        const maxPagesToShow = 10;
        const middlePage = Math.ceil(maxPagesToShow / 2);
        const pageOffset = currentPage <= middlePage ? 0 : currentPage - middlePage;
        const maxPageNumber = Math.min(totalPages, currentPage + middlePage - pageOffset - 1);

        return (
            <>
                {[...Array(maxPagesToShow)].map((_, index) => {
                    const pageNumber = index + pageOffset + 1;
                    return (
                        pageNumber <= maxPageNumber && (
                            <Pagination.Item
                                key={pageNumber}
                                active={pageNumber === currentPage}
                                onClick={() => handlePageChange(pageNumber)}
                            >
                                {pageNumber}
                            </Pagination.Item>
                        )
                    );
                })}
            </>
        );
    };

    return (
        <Container>
            <Row>
                <Col></Col>
                <Col>
                    <Pagination>
                        <Pagination.First
                            disabled={currentPage === 1}
                            onClick={() => handlePageChange(1)}
                        />
                        <Pagination.Prev
                            disabled={currentPage === 1}
                            onClick={() => handlePageChange(currentPage - 1)}
                        />
                        {displayPages()}
                        <Pagination.Next
                            disabled={currentPage === totalPages}
                            onClick={() => handlePageChange(currentPage + 1)}
                        />
                        <Pagination.Last
                            disabled={currentPage === totalPages}
                            onClick={() => handlePageChange(totalPages)}
                        />
                    </Pagination>
                </Col>
                <Col>
                    <Form.Select
                        className="me-2"
                        value={itemsPerPage}
                        onChange={handlePageSizeChange}
                    >
                        <option value="10">10 items per page</option>
                        <option value="20">20 items per page</option>
                        <option value="50">50 items per page</option>
                        <option value="100">100 items per page</option>
                    </Form.Select>
                </Col>
            </Row>
        </Container>
    )
}