import React, {useMemo} from "react";
import {ColumnDef} from "@tanstack/react-table";
import {TaskStatusBadge} from "../../components/TaskStatusBadge";
import {Table} from "../../components/Table";
import {useActions} from "../../hooks";
import {BacktestsTabType} from "../../store/backtests/backtestTabsSlice";
import {CombinedSimulation, useCombinedSimulationData} from "../../types/useCombinedSimulationData";
import {generateSimulationsListDetailedColumns} from "../../types/utils";

export interface SimulationsListParams {
    backtestId: string
}
export function SimulationsList({backtestId}: SimulationsListParams) {
    const simulations = useCombinedSimulationData(backtestId)
    const {addSimulationTab, setActiveTab} = useActions()
    const detailedColumns = React.useMemo(() => generateSimulationsListDetailedColumns(simulations), [simulations])

    const columnsMemo = useMemo<ColumnDef<CombinedSimulation>[]>(
        () => [
            {
                header: 'Name',
                cell: (cell) => (
                    <span
                        onClick={() => {
                            addSimulationTab({
                                id: cell.row.original.id,
                                type: BacktestsTabType.simulation
                            })
                            setActiveTab(cell.row.original.id)
                        }}
                        style={{cursor: 'pointer'}}
                    >{cell.row.original.state.name}</span>
                )
            },
            {
                header: 'Status',
                cell: (cell) => (<TaskStatusBadge status={cell.row.original.state.summary.status}/>)
            },
            {
                header: 'Progress',
                cell: (cell) => <b>{cell.row.original.state.summary.progress}%</b>
            },
            {
                header: 'Created',
                cell: (cell) => <>{new Date(cell.row.original.state.created).toLocaleString()}</>
            },
            {
                header: 'Jobs',
                cell: (cell) => <span>
                    <b style={{color: "green"}}>{cell.row.original.state.summary.finishedJobs}</b>
                    /{cell.row.original.state.summary.totalJobs}
                    <b style={{color: "red"}}>[{cell.row.original.state.summary.failedJobs}]</b>
                </span>
            },
            ...detailedColumns
        ],
        [addSimulationTab, setActiveTab, detailedColumns]
    );

    return (
        <Table data={simulations} columns={columnsMemo}></Table>
    )
}