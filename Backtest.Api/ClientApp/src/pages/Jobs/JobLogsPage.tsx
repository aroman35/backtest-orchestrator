import {useJobLogsQuery} from "../../store/jobs/jobs.api";
import {Loader} from "../../components/Loader";
import {ErrorMessage} from "../../components/ErrorMessage";
import React, {useMemo} from "react";
import {ColumnDef} from "@tanstack/react-table";
import {JobLogEvent} from "../../types/JobLogEvent";
import {Table} from "../../components/Table";
import {LogLevelBadge} from "../../components/LogLevelBadge";
import {Accordion} from "react-bootstrap";

export interface JobLogsPageParams {
    jobId: string
}

export function JobLogsPage({jobId}: JobLogsPageParams) {
    const {isLoading, isError, data, error} = useJobLogsQuery(jobId)

    const renderMessage = function (message: string, context: string, error: string) {
        const title = <>
            <h6>{message}</h6>
            <i>{context}</i>
        </>
        if (!error)
            return title
        return (
            <Accordion>
                <Accordion.Item eventKey="0">
                    <Accordion.Header>{title}</Accordion.Header>
                    <Accordion.Body>
                        <span>{error}</span>
                    </Accordion.Body>
                </Accordion.Item>
            </Accordion>
        )
    }

    const columnsMemo = useMemo<ColumnDef<JobLogEvent>[]>(() => [
        {
            header: 'Time',
            cell: cell => <span>{new Date(cell.row.original.timestamp).toLocaleString()}</span>
        },
        {
            header: 'Level',
            cell: cell => <span><LogLevelBadge level={cell.row.original.level}></LogLevelBadge></span>
        },
        {
            header: 'Message',
            cell: cell => renderMessage(cell.row.original.message, cell.row.original.sourceContext, cell.row.original.error)
        },
        {
            header: 'Instance',
            cell: cell => <b>{cell.row.original.instance}</b>
        }
    ], [])

    if (isLoading) return (<Loader/>)
    if (isError) return (<ErrorMessage message={JSON.stringify(error)}></ErrorMessage>)
    return (
        <>
            {data && <Table data={data} columns={columnsMemo}></Table>}
        </>
    )
}