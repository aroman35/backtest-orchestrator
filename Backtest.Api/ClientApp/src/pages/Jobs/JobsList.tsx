import React, {useMemo} from "react";
import {TaskStatusBadge} from "../../components/TaskStatusBadge";
import {Table} from "../../components/Table";
import {TaskStatus} from "../../types/TaskStatus";
import {OverlayTrigger, Popover} from "react-bootstrap";
import {CombinedJob, useCombinedJobData} from "../../types/useCombinedData";
import {generateJobsListDetailColumns} from "../../types/utils";
import {ColumnDef} from "@tanstack/react-table";
import {BacktestsTabType} from "../../store/backtests/backtestTabsSlice";
import {useActions} from "../../hooks";

export interface JobsListParams {
    simulationId: string
}

export function JobsList({simulationId}: JobsListParams) {
    const jobs = useCombinedJobData(simulationId)
    const {addTab, setActiveTab} = useActions()
    const detailColumns = React.useMemo(() => generateJobsListDetailColumns(jobs), [jobs]);

    const renderStatus = function(status: TaskStatus, id: string, error?: string) {
        if (status === TaskStatus.Failed) {
            return (
                <>
                    <TaskStatusBadge status={status} />{' '}
                    <OverlayTrigger
                        trigger={["hover","focus"]}
                        placement="right"
                        overlay={
                            <Popover id={`popover-status-error-${id}`}>
                                <Popover.Header as="h3">Job finished with error</Popover.Header>
                                <Popover.Body>{error}</Popover.Body>
                            </Popover>
                        }
                    >
                        <i style={{color: "red"}} className="bi bi-info-circle"></i>
                    </OverlayTrigger>
                </>
            )
        }
        return <TaskStatusBadge status={status} />
    }

    const columnsMemo = useMemo<ColumnDef<CombinedJob>[]>(
        () => [
            {
                header: 'Name',
                cell: (cell) => (
                    <span
                        onClick={() => {
                            addTab({
                                id: cell.row.original.id,
                                type: BacktestsTabType.job
                            })
                            setActiveTab(cell.row.original.id)
                        }}
                        style={{cursor: 'pointer'}}
                    >{cell.row.original.state.name}</span>
                ),
                enableSorting: true,
                enableMultiSort: true,
                enableResizing: true,
                sortingFn: "text"
            },
            {
                header: 'Status',
                cell: (cell) => renderStatus(cell.row.original.state.status, cell.row.original.id, cell.row.original.result?.message)
            },
            {
                header: 'Progress',
                cell: (cell) => <b>{cell.row.original.state.progress}%</b>
            },
            {
                header: 'Started',
                cell: (cell) => <>{cell.row.original.state.started && new Date(cell.row.original.state.started).toLocaleString()}</>
            },
            {
                header: 'Md Start',
                cell: (cell) => <>{new Date(cell.row.original.state.mdStart).toLocaleDateString()}</>
            },
            {
                header: 'Md End',
                cell: (cell) => <>{new Date(cell.row.original.state.mdEnd).toLocaleDateString()}</>
            },
            {
                header: 'Try',
                cell: (cell) => <>{cell.row.original.state.currentTry}/{cell.row.original.state.maximumTries}</>
            },
            {
                header: 'Agent',
                cell: (cell) => <>{cell.row.original.state.agentName}</>
            },
            ...detailColumns
        ],
        [detailColumns, addTab, setActiveTab]
    );

    return (
        <>
            {jobs && <Table data={jobs} columns={columnsMemo}></Table>}
        </>
    )
}