import {Badge, Button, Card, Col, Container, ListGroup, Row} from "react-bootstrap";

export function PricingPage() {
    const startSalaryDate = new Date("2022-12-01")
    const today = new Date()
    const salaryDay = 102
    const diff = Math.abs(today.getTime() - startSalaryDate.getTime());
    const diffDays = Math.ceil(diff / (1000 * 3600 * 24));
    const remained = diffDays * salaryDay
    return(
        <Container>
            <Row>
            </Row>
            <Row>
                <Col/>
                <Col>
                    <Card border="primary" style={{ width: '18rem' }}>
                        <Card.Header>Limited offer</Card.Header>
                        <Card.Body>
                            <Card.Title>Unlock the full version</Card.Title>
                            <ListGroup variant="flush">
                                <ListGroup.Item>Limited 8-hours a day support</ListGroup.Item>
                                <ListGroup.Item>4 Feature requests a month</ListGroup.Item>
                                <ListGroup.Item>2x less production bugs</ListGroup.Item>
                                <ListGroup.Item>Support for Fedor with a 20% discount</ListGroup.Item>
                            </ListGroup>
                            <Badge bg="success">
                                {remained}$
                            </Badge>
                            <hr/>
                            <Button variant="outline-success">Add co cart</Button>
                        </Card.Body>
                    </Card>
                </Col>
                <Col/>
            </Row>
        </Container>
    )
}