import {TaskStatus} from "./TaskStatus";

export interface IJob {
    name: string
    simulationId: string
    backtestId: string
    mdStart: Date
    mdEnd: Date
    agentName: string
    currentTry: number
    maximumTries: number
    started?: Date
    progress: number
    status: TaskStatus
    core: number
    reportingIntervalSec: number
    lastUpdate: Date
    desiredAgents: any[]
    buildingCache: string
    isFinalStatus: boolean
}