export interface IStrategySettings {
    settingsSource: string
    appliedUserParams: Record<string, string>
}