export interface ISimulationParams {
    simulationId: string
    appliedParam: Record<string, any>
}