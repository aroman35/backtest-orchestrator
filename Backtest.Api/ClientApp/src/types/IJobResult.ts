export interface IJobResult {
    isMdExists: boolean
    message: string
    isSuccess: boolean
    mdEntryCount: number
    testDuration: number
    taskDuration: number
    speed: number
    simulationId: string
    mdStart?: Date
    mdEnd?: Date
    detailedResults: Record<string, any>
}