import {TaskStatus} from "./TaskStatus";

export interface IBacktestSummary {
    status: TaskStatus
    totalJobs: number
    finishedJobs: number
    failedJobs: number
    speed: number
    progress: number
    isFinalStatus: boolean
}