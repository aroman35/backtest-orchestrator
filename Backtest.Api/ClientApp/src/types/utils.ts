import {CellContext, ColumnDef} from "@tanstack/react-table";
import {CombinedJob} from "./useCombinedData";
import {CombinedSimulation} from "./useCombinedSimulationData";

export const generateJobsListDetailColumns = (data: CombinedJob[]): ColumnDef<CombinedJob>[] => {
    if (!data.length) {
        return [];
    }

    const uniqueKeys = new Set<string>();
    data.forEach((row) => {
        if (row.result?.detailedResults) {
            Object.keys(row.result.detailedResults).forEach((key) => uniqueKeys.add(key));
        }
    });

    return Array.from(uniqueKeys).map((key) => ({
        header: key,
        cell: (cell: CellContext<CombinedJob, any>) => cell.row.original.result?.detailedResults[key],
        enableSorting: true,
        enableMultiSort: true,
        enableResizing: true
    }));
};

export const generateSimulationsListDetailedColumns = (data: CombinedSimulation[]): ColumnDef<CombinedSimulation>[] => {
    if (!data.length) {
        return [];
    }

    const uniqueKeys = new Set<string>();

    data.forEach((row) => {
        if (row.appliedParams) {
            Object.keys(row.appliedParams).forEach((key) => uniqueKeys.add(key));
        }
    });

    return Array.from(uniqueKeys).map((key) => ({
        header: key,
        cell: (cell: CellContext<CombinedSimulation, any>) => cell.row.original.appliedParams ? cell.row.original.appliedParams[key] : null,
        enableSorting: true,
        enableMultiSort: true,
        enableResizing: true
    }));
}