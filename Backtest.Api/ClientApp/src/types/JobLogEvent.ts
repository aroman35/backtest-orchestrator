import {JobLogLevel} from "./JobLogLevel";

export interface JobLogEvent {
    id: string
    timestamp: Date
    level: JobLogLevel
    message: string
    instance: string
    sourceContext: string
    error: string
}