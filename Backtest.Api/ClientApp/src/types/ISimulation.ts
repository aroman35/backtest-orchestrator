import {IBacktestSummary} from "./IBacktestSummary"
import {TaskStatus} from "./TaskStatus"

export interface ISimulation {
    name: string
    backtestId: string
    status: TaskStatus
    summary: IBacktestSummary
    created: Date
}