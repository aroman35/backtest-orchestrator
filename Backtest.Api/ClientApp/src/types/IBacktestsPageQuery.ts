import {TaskStatus} from "./TaskStatus";

export interface IBacktestsPageQuery {
    nameMatch?: string
    createdAfter?: Date
    createdBefore?: Date
    inStatus?: TaskStatus[]
    labels: string[]
    splitDays?: boolean
    binaryNameMatch?: string
    maxProgress?: number
    created?: boolean
    binaryName?: boolean
    progress?: boolean
    isFinalStatus?: boolean
    page: number
    itemsPerPage: number
}