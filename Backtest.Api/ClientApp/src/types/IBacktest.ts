import {IBacktestSummary} from "./IBacktestSummary";

export interface IBacktest {
    name: string
    labels: string[]
    mdStart: Date
    mdEnd: Date
    mdDates: Date[]
    splitDays: boolean
    binaryName: string
    desiredAgents: string[]
    enableLogs: boolean
    enableTelemetry: boolean
    retryCount: number
    reportingTimeoutSec: number
    created: Date
    summary: IBacktestSummary
}