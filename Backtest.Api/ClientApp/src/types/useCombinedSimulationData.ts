import {IViewModel} from "./IViewModel";
import {ISimulation} from "./ISimulation";
import {useGetSimulationsAppliedParamsQuery, useGetSimulationsQuery} from "../store/simulations/simulations.api";
import React from "react";

export interface CombinedSimulation extends IViewModel<ISimulation> {
    appliedParams?: Record<string, any>
}

export const useCombinedSimulationData = (backtestId: string): CombinedSimulation[] => {
    const simulationsQuery = useGetSimulationsQuery(backtestId)
    const appliedParamsQuery = useGetSimulationsAppliedParamsQuery(backtestId)

    const combinedData = React.useMemo(() => {
        if (simulationsQuery.isSuccess && appliedParamsQuery.isSuccess){
            return simulationsQuery.data.map((simulation) => {
                const appliedParams = appliedParamsQuery.data
                    .find((params) => params.simulationId === simulation.id)?.appliedParam

                return {
                    ...simulation,
                    appliedParams
                }
            })
        }
        return []
    }, [simulationsQuery, appliedParamsQuery])

    return combinedData
}