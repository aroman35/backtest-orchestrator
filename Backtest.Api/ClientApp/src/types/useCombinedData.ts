import {IJob} from "./IJob";
import {IJobResult} from "./IJobResult";
import {useJobResultsQuery, useJobsQuery} from "../store/jobs/jobs.api";
import React from "react";
import {TaskStatus} from "./TaskStatus";
import {IViewModel} from "./IViewModel";

export interface CombinedJob extends IViewModel<IJob> {
    result?: IJobResult
}

export const useCombinedJobData = (simulationId: string): CombinedJob[] => {
    const jobsQuery = useJobsQuery(simulationId)
    const resultsQuery = useJobResultsQuery(simulationId)

    const combinedData = React.useMemo(() => {
        if (jobsQuery.isSuccess && resultsQuery.isSuccess) {
            return jobsQuery.data.map((job) => {
                if (job.state.status === TaskStatus.Running) {
                    return job;
                }

                const result = resultsQuery.data.find(
                    (result) => result.id === job.id
                )?.state;

                return {
                    ...job,
                    result,
                };
            })
        }
        return []
    }, [jobsQuery, resultsQuery])

    return combinedData
}