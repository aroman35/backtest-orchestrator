using Backtest.Persistence;
using Backtest.Persistence.Queries;
using MediatR;

namespace Backtest.Api;

public static class ConfigurationExtensions
{
    public static IServiceCollection AddStateByIdQuery<TState>(this IServiceCollection services)
    {
        services
            .AddTransient<
                IRequestHandler<GenericStateByIdQuery<TState>, PersistenceStateModel<TState>>,
                GenericStateByIdQueryHandler<TState>>();
        return services;
    }
}