namespace Backtest.Api;

public class CorsMiddleware
{
    private readonly RequestDelegate _next;
    private readonly string _allowedOrigins;

    public CorsMiddleware(RequestDelegate next, IConfiguration configuration)
    {
        _next = next;
        _allowedOrigins = configuration["AllowedOrigins"];
    }

    public async Task Invoke(HttpContext context)
    {
        context.Response.Headers.Add("Access-Control-Allow-Origin", _allowedOrigins);
        await _next(context);
    }
}