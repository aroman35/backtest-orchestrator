﻿using Backtest.Domain;

namespace Backtest.Api.Subscriptions;

public class StateObserver : IStateUpdatesObserver
{
    private readonly IStateUpdateClient _updatesClient;

    public StateObserver(IStateUpdateClient updatesClient)
    {
        _updatesClient = updatesClient;
    }

    public async Task StateUpdate(ItemSummary summary)
    {
        await _updatesClient.UpdateReceived(summary);
    }
}