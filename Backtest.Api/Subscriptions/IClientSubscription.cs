﻿namespace Backtest.Api.Subscriptions;

public interface IClientSubscription
{
    Task SubscribeUpdates(IEnumerable<Guid> itemIds);
    Task Disconnect();
}