﻿using System.Collections.Concurrent;
using Backtest.Domain;
using ILogger = Serilog.ILogger;

namespace Backtest.Api.Subscriptions;

public class StateUpdatesSubscription<TGrain> : IClientSubscription where TGrain : IStateUpdates, IGrainWithGuidKey
{
    private readonly IClusterClient _clusterClient;
    private readonly ILogger _logger;
    private readonly IStateUpdateClient _stateUpdateClient;
    private readonly ConcurrentDictionary<Guid, IStateUpdatesObserver> _subscriptions = new();
    private readonly ConcurrentDictionary<Guid, IStateUpdatesObserver> _observers = new();

    public StateUpdatesSubscription(IClusterClient clusterClient, IStateUpdateClient stateUpdateClient, ILogger logger)
    {
        _clusterClient = clusterClient;
        _stateUpdateClient = stateUpdateClient;
        _logger = logger.ForContext<StateUpdatesSubscription<TGrain>>();
    }

    public async Task SubscribeUpdates(IEnumerable<Guid> itemIds)
    {
        foreach (var grainId in itemIds)
        {
            var observerSource = new StateObserver(_stateUpdateClient);
            var observer = _clusterClient.CreateObjectReference<IStateUpdatesObserver>(observerSource);
            var grain = _clusterClient.GetGrain<TGrain>(grainId);
            await grain.SubscribeStateUpdates(observer);
            _subscriptions[grainId] = observer;
            _observers[grainId] = observerSource;
        }
        
        _logger.Information("Subscribed to {Count} {Type} updates", typeof(TGrain).Name, _subscriptions.Count);
    }

    public async Task Disconnect()
    {
        foreach (var grainId in _subscriptions.Keys.ToArray())
        {
            var grain = _clusterClient.GetGrain<TGrain>(grainId);
            if (_subscriptions.TryRemove(grainId, out var observer))
            {
                await grain.UnsubscribeStateUpdates(observer);
                _clusterClient.DeleteObjectReference<IStateUpdatesObserver>(observer);
                _logger.Information("State [{GrainId}]: Client disconnected from backtest updates", grainId);
            }

            _observers.TryRemove(grainId, out _);
        }
    }
}