﻿using Backtest.Domain;

namespace Backtest.Api.Subscriptions;

public interface IStateUpdatesHub<TGrain> where TGrain : IStateUpdates, IGrainWithGuidKey
{
    Task SubscribeUpdates(Guid[] simulationIds);
    Task OnDisconnectedAsync(Exception exception);
}