﻿using Backtest.Domain;

namespace Backtest.Api.Subscriptions;

public interface IStateUpdateClient
{
    Task UpdateReceived(ItemSummary summary);
}