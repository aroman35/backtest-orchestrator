﻿using Backtest.Domain;
using Microsoft.AspNetCore.SignalR;

namespace Backtest.Api.Subscriptions;

public class StateUpdatesHub<TGrain> : Hub<IStateUpdateClient>, IStateUpdatesHub<TGrain> where TGrain : IStateUpdates, IGrainWithGuidKey
{
    private readonly SubscriptionsManager _subscriptionsManager;

    public StateUpdatesHub(SubscriptionsManager subscriptionsManager)
    {
        _subscriptionsManager = subscriptionsManager;
    }

    [HubMethodName("subscribe")]
    public async Task SubscribeUpdates(Guid[] simulationIds)
    {
        var caller = Clients.Caller;
        await _subscriptionsManager.SubscribeStateUpdates<TGrain>(Context.ConnectionId, simulationIds, caller);
    }
    
    public override async Task OnDisconnectedAsync(Exception exception)
    {
        await _subscriptionsManager.Disconnect(Context.ConnectionId);
        await base.OnDisconnectedAsync(exception);
    }
}