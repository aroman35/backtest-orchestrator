﻿using System.Collections.Concurrent;
using Backtest.Domain;
using ILogger = Serilog.ILogger;

namespace Backtest.Api.Subscriptions;

public class SubscriptionsManager
{
    private readonly ConcurrentDictionary<string, IClientSubscription> _subscriptions = new();
    
    private readonly IClusterClient _clusterClient;
    private readonly ILogger _logger;

    public SubscriptionsManager(IClusterClient clusterClient, ILogger logger)
    {
        _clusterClient = clusterClient;
        _logger = logger;
    }

    public async Task SubscribeStateUpdates<TGrain>(string connectionId, Guid[] itemIds, IStateUpdateClient updateClient)
        where TGrain : IStateUpdates, IGrainWithGuidKey
    {
        var updatesSubscription = new StateUpdatesSubscription<TGrain>(_clusterClient, updateClient, _logger);
        await updatesSubscription.SubscribeUpdates(itemIds);
        _subscriptions[connectionId] = updatesSubscription;
    }

    public async Task Disconnect(string connectionId)
    {
        if (_subscriptions.TryRemove(connectionId, out var connection))
            await connection.Disconnect();
    }
}