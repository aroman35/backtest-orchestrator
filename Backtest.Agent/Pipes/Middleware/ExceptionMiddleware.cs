﻿using Backtest.Domain.Models;
using Backtest.Agent.Jobs;
using FlashPipes.Common.Messaging;
using FlashPipes.Server;

using ILogger = Serilog.ILogger;

namespace Backtest.Agent.Pipes.Middleware;

public class ExceptionMiddleware : FlashPipesMiddleware
{
    private readonly ILogger _logger;
    private readonly JobsManager _jobsManager;

    public ExceptionMiddleware(ILogger logger, JobsManager jobsManager)
    {
        _logger = logger;
        _jobsManager = jobsManager;
    }

    public override async Task<FlashPipesMessage> Execute(FlashPipesMessage message)
    {
        try
        {
            return await Next(message);
        }
        catch (Exception exception)
        {
            _logger.Error(exception, "Job [{JobId}]: error occured during pipe request {Route}", message.PipeName, message.Route);
            var job = _jobsManager.GetJobById(Guid.Parse(message.PipeName));
            await job.ReportTestResult(new TaskResult(false, exception.Message, 0, 0));
            throw;
        }
    }
}