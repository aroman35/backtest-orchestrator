﻿using FlashPipes.Common.Messaging;
using FlashPipes.Server;
using ILogger = Serilog.ILogger;

namespace Backtest.Agent.Pipes.Middleware;

public class LoggingMiddleware : FlashPipesMiddleware
{
    private readonly ILogger _logger;

    public LoggingMiddleware(ILogger logger)
    {
        _logger = logger;
    }

    public override async Task<FlashPipesMessage> Execute(FlashPipesMessage message)
    {
        _logger.Debug("Job [{JobId}]: Pipe controller method request {Request} on route {Route}",
            Guid.Parse(message.PipeName),
            message.CompressedMessage,
            message.Route
        );
        var response = await Next(message);
        // _logger.Debug("Job [{JobId}]: Pipe controller method executed on {Route}, {Request}, {Response}",
        //     Guid.Parse(message.PipeName),
        //     message.Route,
        //     message.CompressedMessage,
        //     response.HasValue ? response.CompressedMessage : "no response"
        // );
        return response;
    }
}