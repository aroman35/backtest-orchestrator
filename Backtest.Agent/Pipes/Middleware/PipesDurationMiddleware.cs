﻿using System.Diagnostics;
using FlashPipes.Common.Messaging;
using FlashPipes.Server;
using ILogger = Serilog.ILogger;

namespace Backtest.Agent.Pipes.Middleware;

public class PipesDurationMiddleware : FlashPipesMiddleware
{
    private readonly ILogger _logger;

    public PipesDurationMiddleware(ILogger logger)
    {
        _logger = logger;
    }

    public override async Task<FlashPipesMessage> Execute(FlashPipesMessage message)
    {
        var started = Stopwatch.GetTimestamp();
        var response = await Next(message);
        var elapsed = Stopwatch.GetElapsedTime(started);

        _logger.Debug("Request executed in {Elapsed}", elapsed);
        return response;
    }
}