﻿using System.Diagnostics;
using FlashPipes.Common.Messaging;
using FlashPipes.Server;
using ILogger = Serilog.ILogger;

namespace Backtest.Agent.Pipes.Middleware;

public class StrategyCorrelationMiddleware : FlashPipesMiddleware
{
    private readonly ILogger _logger;

    public StrategyCorrelationMiddleware(ILogger logger)
    {
        _logger = logger;
    }

    public override Task<FlashPipesMessage> Execute(FlashPipesMessage message)
    {
        Trace.CorrelationManager.ActivityId = Guid.Parse(message.PipeName);
        _logger.Debug("Correlation id mapped to {JobId}", message.PipeName);
        return Next(message);
    }
}