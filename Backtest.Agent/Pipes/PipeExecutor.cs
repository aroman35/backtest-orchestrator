﻿using Backtest.Domain.Models;
using Backtest.Agent.Jobs;
using Backtest.Agent.Pipes.Middleware;
using FlashPipes.Server;
using ILogger = Serilog.ILogger;

namespace Backtest.Agent.Pipes;

public class PipeExecutor : IPipeExecutor
{
    private readonly FlashPipesConnectionFactory _pipesConnectionFactory;
    private readonly JobsManager _jobsManager;
    private readonly ILogger _logger;

    private Guid _jobId;
    
    public PipeExecutor(FlashPipesConnectionFactory pipesConnectionFactory, ILogger logger, JobsManager jobsManager)
    {
        _pipesConnectionFactory = pipesConnectionFactory;
        _jobsManager = jobsManager;
        _logger = logger.ForContext<PipeExecutor>().ForContext("OperationId", Guid.NewGuid());;
    }

    public async Task Launch(Guid jobId, CancellationToken cancellationToken)
    {
        _jobId = jobId;
        try
        {
            await using var connection = _pipesConnectionFactory.CreateServer(jobId, cancellationToken);
            connection
                .WithMiddleware<StrategyCorrelationMiddleware>()
                .WithMiddleware<ExceptionMiddleware>()
                .WithMiddleware<LoggingMiddleware>()
                .WithMiddleware<PipesDurationMiddleware>();

            _logger.Information("Job [{JobId}]: pipe created", jobId);
            await connection.StartListeningAsync((error, _) => _logger.Error(error, "Job [{JobId}]: pipe error", jobId),
                cancellationToken);
            _logger.Information("Job [{JobId}]: pipe closed", jobId);
        }
        catch (TaskCanceledException)
        {
        }
        catch (Exception exception)
        {
            _logger.Error(exception, "Job [{JobId}]: Error while listening pipe", jobId);
            var job = _jobsManager.GetJobById(jobId);
            await job.ReportTestResult(new TaskResult(false, exception.Message, 0, 0));
        }
    }
}