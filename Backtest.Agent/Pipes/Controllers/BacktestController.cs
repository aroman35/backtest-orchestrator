﻿using Backtest.Domain.Models;
using Backtest.Agent.Jobs;
using FlashPipes.Server;

namespace Backtest.Agent.Pipes.Controllers;

[PipeRoute("backtest")]
public class BacktestController : FlashPipesController
{
    private readonly JobsManager _jobsManager;
    private IJobExecutor Job => _jobsManager.GetJobById(Guid.Parse(RequestMessage.PipeName));

    public BacktestController(JobsManager jobsManager)
    {
        _jobsManager = jobsManager;
    }

    [PipeRoute("task")]
    public async Task<TaskInfo> GetTask()
    {
        var info = await Job.GetTaskInfo();
        return info;
    }

    [PipeRoute("progress")]
    public async Task<TaskCancellation> HandleProgress(BacktestProgress taskState)
    {
        await Job.ReportTestProgress(double.Round(taskState.Progress, 2, MidpointRounding.ToEven));
        return new TaskCancellation(false);
    }
        
    [PipeRoute("result")]
    public async Task<object> HandleTaskResult(TaskResult taskResult)
    {
        await Job.ReportTestResult(taskResult);
        return "ok";
    }
}