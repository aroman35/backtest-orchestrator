﻿using Backtest.Domain.Models;
using Backtest.Agent.Jobs;
using FlashPipes.Server;

namespace Backtest.Agent.Pipes.Controllers;

[PipeRoute("cache")]
public class CacheController : FlashPipesController
{
    private readonly JobsManager _jobsManager;
    private IJobExecutor Job => _jobsManager.GetJobById(Guid.Parse(RequestMessage.PipeName));

    public CacheController(JobsManager jobsManager)
    {
        _jobsManager = jobsManager;
    }
    
    [PipeRoute("request")]
    public async Task<CacheResponse> CacheRequest(CacheRequest pipeCacheRequest)
    {
        return await Job.CacheRequest(pipeCacheRequest);
    }

    [PipeRoute("progress")]
    public async Task HandleCacheBuildStatus(CacheBuildProgress cacheBuildProgress)
    {
        await Job.ReportCacheProgress(cacheBuildProgress.Progress);
    }

    [PipeRoute("result")]
    public async Task HandleCacheResult(CacheBuildResult result)
    {
        await Job.CacheBuildComplete(result.Success);
    }
}