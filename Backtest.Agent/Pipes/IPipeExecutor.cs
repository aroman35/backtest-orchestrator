namespace Backtest.Agent.Pipes;

public interface IPipeExecutor
{
    Task Launch(Guid jobId, CancellationToken cancellationToken);
}