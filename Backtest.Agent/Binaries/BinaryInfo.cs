using System.Diagnostics;

namespace Backtest.Agent.Binaries;

public class BinaryInfo : IEquatable<BinaryInfo>
{
    private string GetFileVersion(string fileName)
    {
        return FileVersionInfo.GetVersionInfo(fileName).FileVersion;
    }
        
    public BinaryInfo(string fileName)
    {
        var fileInfo = new FileInfo(fileName);
            
        BinaryName = Path.GetFileName(Path.GetDirectoryName(fileName));
        FileName = fileName;
        CreationTimeUtc = fileInfo.CreationTimeUtc;
        Sha256 = string.Empty;
        FileVersion = GetFileVersion(fileName);
    }

    public string BinaryName { get; set; }
    public string FileName { get; set; }
    public string FileVersion { get; set; }
    public string Sha256 { get; set; }
    public DateTime CreationTimeUtc { get; set; }

    public bool Equals(BinaryInfo other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return FileVersion == other.FileVersion && Sha256 == other.Sha256 && CreationTimeUtc.Equals(other.CreationTimeUtc);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((BinaryInfo) obj);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(FileVersion, Sha256, CreationTimeUtc);
    }
}