﻿using System.Collections.Concurrent;
using Backtest.Domain.Agent;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;

namespace Backtest.Agent.Binaries;

public class BinaryManager : IBinaryManager
{
    private readonly ConcurrentDictionary<string, bool> _binaries;
    private readonly IFileProvider _physicalFileProvider;
    private readonly IOptions<AgentSettings> _agentSettings;
    // ReSharper disable once NotAccessedField.Local
    private ICollection<BinaryInfo> _binaryInfosState;

    public BinaryManager(IOptions<AgentSettings> agentSettings)
    {
        _agentSettings = agentSettings;
        _binaries = new ConcurrentDictionary<string, bool>();
        CacheDirectory = _agentSettings.Value.BinaryCache;
        _physicalFileProvider = new PhysicalFileProvider(CacheDirectory);
        WaitForBinaryFolderChanges();
        Initialize();
    }

    public ICollection<BinaryInfo> Binaries { get; set; }

    private IChangeToken _binaryChangedToken;
    private Func<Task> _onBinariesUpdated;

    private void WaitForBinaryFolderChanges()
    {
        _binaryChangedToken = _physicalFileProvider.Watch($"**/{_agentSettings.Value.BinaryName}");
        _binaryChangedToken.RegisterChangeCallback(OnBinaryFolderChanged, default);
    }

    private void OnBinaryFolderChanged(object state)
    {
        var binarySourcesDirectoryInfo = new DirectoryInfo(_agentSettings.Value.BinaryStorage);

        if (!binarySourcesDirectoryInfo.Exists)
            throw new DirectoryNotFoundException("Binaries directory was not found");
            
        var currentState = binarySourcesDirectoryInfo
            .EnumerateFiles(_agentSettings.Value.BinaryName, SearchOption.AllDirectories)
            .Select(x => GetBinaryInfo(x.FullName))
            .ToArray();

        if (!Binaries.SequenceEqual(currentState))
        {
            Binaries.Clear();

            foreach (var binaryState in currentState)
            {
                Binaries.Add(binaryState);
            }
                
            _binaryInfosState = currentState;
                
            _onBinariesUpdated?.Invoke().GetAwaiter();
            WaitForBinaryFolderChanges();
        }
    }

    public void SetBinariesUpdatedHandler(Func<Task> handler)
    {
        _onBinariesUpdated = handler;
    }

    public string CacheDirectory { get; private set; }

    public void Initialize()
    {
        var binarySourcesDirectoryInfo = new DirectoryInfo(_agentSettings.Value.BinaryStorage);

        if (!binarySourcesDirectoryInfo.Exists)
            throw new DirectoryNotFoundException("Binaries directory was not found");
            
        Binaries = binarySourcesDirectoryInfo
            .EnumerateFiles(_agentSettings.Value.BinaryName, SearchOption.AllDirectories)
            .Select(x => new BinaryInfo(x.FullName))
            .ToList();
            
        var cacheDirectoryInfo = new DirectoryInfo(_agentSettings.Value.BinaryCache);
        if (!cacheDirectoryInfo.Exists)
            cacheDirectoryInfo.Create();

        var binaries = cacheDirectoryInfo
            .EnumerateFiles(_agentSettings.Value.BinaryName, SearchOption.AllDirectories)
            .ToList();

        foreach (var binary in binaries)
            Prepare(binary!.Directory!.Name);
    }

    private void Prepare(string binVersion) => Take(binVersion, true);

    public string Take(string binVersion, bool initOnly = false)
    {
        var binCachePath = new FileInfo(Path.Combine(_agentSettings.Value.BinaryCache, binVersion, _agentSettings.Value.BinaryName));
            
        if (_binaries.TryGetValue(binVersion, out _) && binCachePath.Exists)
        {
            return binCachePath.FullName;
        }

        _binaries[binVersion] = !initOnly;

        if (!binCachePath.Exists)
        {
            if (!string.IsNullOrEmpty(binCachePath.DirectoryName) && !Directory.Exists(binCachePath.DirectoryName))
                Directory.CreateDirectory(binCachePath.DirectoryName);

            var originBinPath = Path.Combine(_agentSettings.Value.BinaryStorage, binVersion, _agentSettings.Value.BinaryName);
            var originFileInfo = new FileInfo(originBinPath);

            if (!originFileInfo.Exists)
                throw new FileNotFoundException($"Binary '{binVersion}' doesn't exist");

            var requestedSize = QuotaRemained(originBinPath.Length);
            if (requestedSize < 0 && Clear(requestedSize) < requestedSize)
                return originBinPath;

            SafeCopy(originBinPath, binCachePath.FullName);
        }

        return binCachePath.FullName;
    }

    private static readonly object CopyLock = new();
    private static void SafeCopy(string originPath, string destinationPath)
    {
        lock (CopyLock)
        {
            if (!File.Exists(destinationPath) && !string.IsNullOrEmpty(destinationPath))
                File.Copy(originPath, destinationPath);
        }
    }

    public void Release(string binVersion)
    {
        _binaries[binVersion] = false;
    }

    private long QuotaRemained(long requestedSize)
    {
        var cacheDirInfo = new DirectoryInfo(_agentSettings.Value.BinaryCache);
        var totalSize = cacheDirInfo.EnumerateFiles("*", SearchOption.AllDirectories).Sum(x => x.Length);

        return _agentSettings.Value.BinaryCacheQuota - totalSize - requestedSize;
    }

    private long Clear(long requestedSize)
    {
        var binariesToClean = _binaries
            .Where(x => !x.Value)
            .Select(x => new FileInfo(Path.Combine(_agentSettings.Value.BinaryCache, x.Key, _agentSettings.Value.BinaryName)))
            .OrderBy(x => x.LastAccessTime)
            .ToArray();

        if (!binariesToClean.Any())
            return 0;

        long totalCleaned = 0;

        foreach (var binaryFile in binariesToClean)
        {
            if (totalCleaned > requestedSize) return totalCleaned;

            if (!binaryFile.Exists)
            {
                _binaries.TryRemove(binaryFile.Name, out _);
                continue;
            }

            totalCleaned += binaryFile.Length;
            binaryFile.Delete();
        }

        return totalCleaned;
    }

    private BinaryInfo GetBinaryInfo(string fileName)
    {
        var binaryInfo = new BinaryInfo(fileName);
        return binaryInfo;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        Initialize();
        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}