﻿namespace Backtest.Agent.Binaries;

public interface IBinaryManager : IHostedService
{
    void Initialize();
    ICollection<BinaryInfo> Binaries { get; }
    string Take(string binVersion, bool initOnly = false);
    void Release(string binVersion);
    void SetBinariesUpdatedHandler(Func<Task> handler);
    string CacheDirectory { get; }
}