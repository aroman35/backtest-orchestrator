﻿using Backtest.Domain.Agent;
using Microsoft.Extensions.Options;
using ILogger = Serilog.ILogger;

namespace Backtest.Agent;

public class AgentConnectionService : BackgroundService
{
    private readonly IClusterClient _clusterClient;
    private readonly IOptions<AgentSettings> _agentSettings;
    private readonly ILogger _logger;

    private IAgent _agentGrain;
    private double _keepAlive;
    private IAgentObserver _observer;

    public AgentConnectionService(
        IClusterClient clusterClient,
        IOptions<AgentSettings> agentSettings,
        ILogger logger)
    {
        _clusterClient = clusterClient;
        _agentSettings = agentSettings;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    { 
        System.Diagnostics.FileVersionInfo fvi =
            System.Diagnostics.FileVersionInfo.GetVersionInfo(
                System.Reflection.Assembly.GetExecutingAssembly().Location);
        string version = fvi.FileVersion;
        _logger.Information("Running agent version {version}", version);
        
        using var timer = new PeriodicTimer(TimeSpan.FromSeconds(_keepAlive -1));
        while (!stoppingToken.IsCancellationRequested && await timer.WaitForNextTickAsync(stoppingToken))
        {
            if (!await _agentGrain.KeepAlive())
            {
                await Connect();
                await _agentGrain.Subscribe(_observer);
            }
        }
    }

    public override async Task StartAsync(CancellationToken cancellationToken)
    {
        await Connect(true);
        var observer = new AgentObserver(_agentGrain, _logger);
        _observer = _clusterClient.CreateObjectReference<IAgentObserver>(observer);
        await _agentGrain.Subscribe(_observer);

        await base.StartAsync(cancellationToken);
    }

    private async Task Connect(bool isInitialConnection = false)
    {
        _agentGrain = _clusterClient.GetGrain<IAgent>(_agentSettings.Value.AgentName);
        var connection = await _agentGrain.OnAgentConnected(_agentSettings.Value, isInitialConnection);
        _keepAlive = connection.KeepAlive;
    }

    public override async Task StopAsync(CancellationToken cancellationToken)
    {
        await _agentGrain.Disconnect();
        await base.StopAsync(cancellationToken);
    }
}