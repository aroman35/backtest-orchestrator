﻿using System.Collections.Concurrent;
using Backtest.Domain.Agent;
using Backtest.Domain.Job;
using Backtest.Domain.MarketDataCache;
using Microsoft.Extensions.Options;
using ILogger = Serilog.ILogger;

namespace Backtest.Agent.Cache;

public class AgentCacheManager : IAgentCacheManager
{
    private readonly ConcurrentDictionary<string, AgentCacheLocal> _localCache = new();
    private readonly IClusterClient _clusterClient;
    private readonly IAgentCacheFilesAccessor _cacheFilesAccessor;
    private readonly IOptions<AgentSettings> _agentSettings;
    private readonly IHostApplicationLifetime _applicationLifetime;
    private readonly ILogger _logger;

    public AgentCacheManager(
        IClusterClient clusterClient,
        IAgentCacheFilesAccessor cacheFilesAccessor,
        IHostApplicationLifetime applicationLifetime,
        IOptions<AgentSettings> agentSettings,
        ILogger logger
        )
    {
        _clusterClient = clusterClient;
        _agentSettings = agentSettings;
        _cacheFilesAccessor = cacheFilesAccessor;
        _applicationLifetime = applicationLifetime;

        _logger = logger.ForContext<AgentCacheManager>();
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        var cacheDirectoryInfo = new DirectoryInfo(_agentSettings.Value.CachePath);
        if (!cacheDirectoryInfo.Exists)
            cacheDirectoryInfo.Create();

        var metaFiles = cacheDirectoryInfo
            .GetFiles("*.meta", SearchOption.AllDirectories)
            .ToArray();

        foreach (var metaFileChunk in metaFiles.Select(x => x.Name.Replace(".meta", "")).Chunk(25))
        {
            await Parallel.ForEachAsync(metaFileChunk, cancellationToken, InitSingleCache);
        }
    }

    private async ValueTask InitSingleCache(string hash, CancellationToken cancellationToken)
    {
        var localCache = await AgentCacheLocal.Create(hash, _clusterClient, this, _agentSettings, _cacheFilesAccessor, _logger, cancellationToken);
        _localCache[hash] = localCache;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }

    public async Task BuildComplete(IJob job, string hash, bool isSuccess)
    {
        var localCache = await AgentCacheLocal.Create(hash, _clusterClient, this, _agentSettings, _cacheFilesAccessor, _logger, _applicationLifetime.ApplicationStopped);
        await job.CacheBuildComplete(new CacheBuildCompleteResult(localCache.MetaCommand, isSuccess));
        _localCache[hash] = localCache;
        if (!isSuccess)
            await localCache.Remove();
    }

    public async Task RemoveCache(string hash)
    {
        if (_localCache.TryGetValue(hash, out var localCache))
            await localCache.Remove();
    }

    public async Task CacheRemoved(string hash)
    {
        if (_localCache.TryRemove(hash, out var localCache))
            await localCache.DisposeAsync();
    }
}