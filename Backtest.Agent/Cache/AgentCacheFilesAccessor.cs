﻿using Backtest.Domain.Agent;
using Backtest.Domain.MarketDataCache;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Backtest.Agent.Cache;

public class AgentCacheFilesAccessor : IAgentCacheFilesAccessor
{
    private readonly IOptions<AgentSettings> _agentSettings;
    private string CacheFilePath(string hash) => Path.Combine(_agentSettings.Value.CachePath, $"{hash}.mdp");
    private string MetaFilePath(string hash) => Path.Combine(_agentSettings.Value.CachePath, $"{hash}.meta");

    public AgentCacheFilesAccessor(IOptions<AgentSettings> agentSettings)
    {
        _agentSettings = agentSettings;
    }

    public async Task<FillMarketDataCacheMetaCommand> ReadMeta(string hash)
    {
        if (!File.Exists(CacheFilePath(hash)))
        {
            File.Delete(MetaFilePath(hash));
            throw new ApplicationException($"Cache data file was not found");
        }
        
        var metaInternal = JsonConvert.DeserializeObject<MetaInternal>(await File.ReadAllTextAsync(MetaFilePath(hash)));
        var meta = new FillMarketDataCacheMetaCommand(
            metaInternal.BeginDate,
            metaInternal.EndDate,
            metaInternal.FileSizeBytes,
            metaInternal.MdEntryCount,
            metaInternal.Instruments,
            metaInternal.FilesMap.SelectMany(file => file).ToArray());

        return meta;
    }

    public void RemoveCache(string hash)
    {
        if (File.Exists(CacheFilePath(hash)))
            File.Delete(CacheFilePath(hash));
        if (File.Exists(MetaFilePath(hash)))
            File.Delete(MetaFilePath(hash));
    }
}