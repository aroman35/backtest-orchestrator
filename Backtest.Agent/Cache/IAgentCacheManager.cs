using Backtest.Domain.Job;

namespace Backtest.Agent.Cache;

public interface IAgentCacheManager : IHostedService
{
    Task BuildComplete(IJob job, string hash, bool isSuccess);
    Task CacheRemoved(string hash);
}