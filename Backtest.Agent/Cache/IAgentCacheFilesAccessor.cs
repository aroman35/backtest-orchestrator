﻿using Backtest.Domain.MarketDataCache;

namespace Backtest.Agent.Cache;

public interface IAgentCacheFilesAccessor
{
    Task<FillMarketDataCacheMetaCommand> ReadMeta(string hash);
    void RemoveCache(string hash);
}