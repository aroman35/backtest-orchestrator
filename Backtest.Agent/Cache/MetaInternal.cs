namespace Backtest.Agent.Cache;

public class MetaInternal
{
    public string CacheFile { get; set; }
    public DateTime BeginDate { get; set; }
    public DateTime EndDate { get; set; }
    public long FileSizeBytes { get; set; }
    public long MdEntryCount { get; set; }
    public string[] Instruments { get; set; }
    public string[][] FilesMap { get; set; }
}