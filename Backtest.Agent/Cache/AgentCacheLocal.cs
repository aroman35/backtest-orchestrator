﻿using Backtest.Domain.Agent;
using Backtest.Domain.MarketDataCache;
using Microsoft.Extensions.Options;
using ILogger = Serilog.ILogger;

namespace Backtest.Agent.Cache;

public class AgentCacheLocal : IMarketDataCacheObserver, IAsyncDisposable
{
    private readonly IClusterClient _clusterClient;
    private readonly IAgentCacheManager _agentCacheManager;
    private readonly IAgentCacheFilesAccessor _cacheFilesAccessor;
    private readonly ILogger _logger;

    private readonly IAgent _agent;
    private readonly IMarketDataCacheMeta _metaGrain;
    private IMarketDataCache _cacheGrain;
    private IMarketDataCacheObserver _observer;
    private Task _resubscribeTask;
    public FillMarketDataCacheMetaCommand MetaCommand { get; private set; }

    private AgentCacheLocal(
        string hash,
        IClusterClient clusterClient,
        IAgentCacheManager agentCacheManager,
        IOptions<AgentSettings> agentSettings,
        IAgentCacheFilesAccessor cacheFilesAccessor,
        ILogger logger)
    {
        Hash = hash;
        _clusterClient = clusterClient;
        _agentCacheManager = agentCacheManager;
        _cacheFilesAccessor = cacheFilesAccessor;
        _logger = logger.ForContext<AgentCacheLocal>().ForContext(nameof(Hash), Hash);

        _agent = _clusterClient.GetGrain<IAgent>(agentSettings.Value.AgentName);
        _metaGrain = _clusterClient.GetGrain<IMarketDataCacheMeta>(Hash);
    }

    public string Hash { get; }
    
    public async Task Remove()
    {
        _cacheFilesAccessor.RemoveCache(Hash);
        _logger.Information("Cache [{Hash}]: removed", Hash);
        await _cacheGrain.Removed();
        await _agentCacheManager.CacheRemoved(Hash);
    }

    public static async Task<AgentCacheLocal> Create(
        string hash,
        IClusterClient clusterClient,
        IAgentCacheManager agentCacheManager,
        IOptions<AgentSettings> agentSettings,
        IAgentCacheFilesAccessor cacheFilesAccessor,
        ILogger logger,
        CancellationToken cancellationToken)
    {
        var cache = new AgentCacheLocal(hash, clusterClient, agentCacheManager, agentSettings, cacheFilesAccessor, logger);
        await cache.Initialize(cancellationToken);
        return cache;
    }
    
    private async Task Initialize(CancellationToken cancellationToken)
    {
        try
        {
            MetaCommand = await _cacheFilesAccessor.ReadMeta(Hash);
        
            _cacheGrain = await _metaGrain.GetCacheForAgent(_agent);
            await _metaGrain.Initialize(MetaCommand, _agent);
            _logger.Information("Initialized {Hash} cache", Hash);

            _observer = _clusterClient.CreateObjectReference<IMarketDataCacheObserver>(this);
            var timeToLive = await _cacheGrain.Subscribe(_observer);
            _resubscribeTask = Task.Run(() => ReconnectObserverTask(TimeSpan.FromSeconds(timeToLive), cancellationToken), cancellationToken);
        }
        catch (Exception exception)
        {
            _logger.Error(exception, "Unable to initialize cache {Hash}", Hash);
            throw;
        }
    }

    private async Task ReconnectObserverTask(TimeSpan reconnectionTimeout, CancellationToken cancellationToken)
    {
        using var timer = new PeriodicTimer(reconnectionTimeout);
        while (!cancellationToken.IsCancellationRequested && await timer.WaitForNextTickAsync(cancellationToken))
        {
            await _cacheGrain.Subscribe(_observer);
        }
    }

    public async ValueTask DisposeAsync()
    {
        if (_resubscribeTask is not null)
            await _resubscribeTask;
    }
}