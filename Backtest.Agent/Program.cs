using Backtest.Domain;
using Backtest.Domain.Agent;
using Backtest.Agent;
using Backtest.Agent.Binaries;
using Backtest.Agent.Cache;
using Backtest.Agent.Jobs;
using Backtest.Agent.Pipes;
using Backtest.Agent.Processes;
using Backtest.Common;
using Serilog;
using Serilog.Events;
using static Backtest.Common.ConsulConfigurationExtensions;

var bootstrapLogger = new LoggerConfiguration()
    .WriteTo.Console()
    .MinimumLevel.Debug()
    .CreateBootstrapLogger();

try
{
    var agentName = Environment.GetEnvironmentVariable("AGENT_NAME");
    var consulServiceCatalog = Environment.GetEnvironmentVariable("CONSUL_SERVICE_CATALOG");
    var consulAgentName = Environment.GetEnvironmentVariable("CONSUL_AGENT_NAME");
    ArgumentException.ThrowIfNullOrEmpty(agentName);
    ArgumentException.ThrowIfNullOrEmpty(consulServiceCatalog);
    ArgumentException.ThrowIfNullOrEmpty(consulAgentName);

    await Host.CreateDefaultBuilder(args)
        .ConfigureAppConfiguration(configurationBuilder => configurationBuilder
            .AddConsulConfiguration(CombinePath(consulServiceCatalog, "agents", consulAgentName, "logger-configuration"), true)
            .AddConsulConfiguration(CombinePath(consulServiceCatalog, "agents", consulAgentName, "appsettings"), true)
            .AddConsulConfiguration(CombinePath(consulServiceCatalog, "kafka-streams-configuration"), true)
            .AddConsulConfiguration(CombinePath(consulServiceCatalog, "database-settings"), true)
            .AddEnvironmentVariables())
        .UseOrleansClient((hostContext, clientBuilder) =>
        {
            clientBuilder.ConfigureBacktestClient(hostContext.Configuration, agentName);
        })
        .ConfigureServices((hostContext, services) =>
        {
            services.AddHostedService<AgentConnectionService>();
            services.AddSingleton<IAgentCacheManager, AgentCacheManager>();
            services.AddSingleton<IAgentCacheFilesAccessor, AgentCacheFilesAccessor>();
            services.AddHostedService(provider => provider.GetRequiredService<IAgentCacheManager>());
            services.AddHostedService<JobsFeedService>();
            services.Configure<AgentSettings>(hostContext.Configuration.GetSection(nameof(AgentSettings)));
            services.Configure<KafkaStreamsOptions>(hostContext.Configuration.GetSection(nameof(KafkaStreamsOptions)));
            services.AddScoped<IJobExecutor, JobExecutor>();
            if (Environment.OSVersion.Platform is PlatformID.Unix)
            {
                services.AddScoped<IProcessExecutor, LinuxProcessExecutor>();
                services.AddHostedService<LinuxProcessInitializer>();
            }

            if (Environment.OSVersion.Platform is PlatformID.Win32Windows or PlatformID.Win32NT or PlatformID.Win32S)
            {
                services.AddScoped<IProcessExecutor, WindowsProcessExecutor>();
            }

            services.AddScoped<IPipeExecutor, PipeExecutor>();
            services.AddSingleton<IBinaryManager, BinaryManager>();
            services.AddSingleton<ICoresAccessor, CoresAccessor>();
            services.AddHostedService(provider => provider.GetRequiredService<IBinaryManager>());
            services.AddNamedPipes();
            services.AddSingleton<JobsManager>();
        })
        .UseSerilog((hostContext, loggingConfiguration) => loggingConfiguration
            .ReadFrom.Configuration(hostContext.Configuration)
            .Enrich.WithProperty("Application", "Backtest")
            .Enrich.WithProperty("Instance", agentName)
        )
        .RunConsoleAsync();
}
catch (Exception exception)
{
    bootstrapLogger.Write(LogEventLevel.Error, exception, "Error launching agent");
    throw;
}
finally
{
    await Log.CloseAndFlushAsync();
}