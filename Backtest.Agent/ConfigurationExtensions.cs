﻿using Backtest.Agent.Pipes.Controllers;
using Backtest.Agent.Pipes.Middleware;
using FlashPipes.Server;

namespace Backtest.Agent;

public static class ConfigurationExtensions
{
    public static IServiceCollection AddNamedPipes(this IServiceCollection services)
    {
        services.AddSingleton(FlashPipesConnectionFactory.New);
        services
            .AddScoped<FlashPipesMiddlewareProvider>()
            .AddScoped<BacktestController>()
            .AddScoped<CacheController>()
            .AddScoped<ExceptionMiddleware>()
            .AddScoped<LoggingMiddleware>()
            .AddScoped<PipesDurationMiddleware>()
            .AddScoped<StrategyCorrelationMiddleware>();

        return services;
    }
}