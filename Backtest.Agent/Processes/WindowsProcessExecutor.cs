using System.Diagnostics;
using System.Text;
using Backtest.Agent.Binaries;
using ILogger = Serilog.ILogger;

namespace Backtest.Agent.Processes;

public class WindowsProcessExecutor : IProcessExecutor
{
    private readonly ICoresAccessor _coresAccessor;
    private readonly IBinaryManager _binaryManager;
    private readonly ILogger _logger;
    private bool _coreTaken;
    private int _core;
    private Guid _jobId;
    private Process _process;

    public WindowsProcessExecutor(ICoresAccessor coresAccessor, ILogger logger, IBinaryManager binaryManager)
    {
        _coresAccessor = coresAccessor;
        _logger = logger.ForContext<WindowsProcessExecutor>().ForContext("OperationId", Guid.NewGuid());;
        _binaryManager = binaryManager;
    }

    public async Task StartProcess(Guid jobId, string binaryName, int core, CancellationToken cancellationToken)
    {
        _jobId = jobId;
        _logger.Information("Job [{JobId}]: trying to launch process", _jobId);
        _core = _coresAccessor.TakeCore();
        _coreTaken = true;
        try
        {
            var binaryPath = _binaryManager.Take(binaryName);
            var startInfo = new ProcessStartInfo
            {
                FileName = binaryPath,
                Arguments = jobId.ToString("D"),
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = false,
                StandardErrorEncoding = Encoding.UTF8,
                StandardOutputEncoding = Encoding.UTF8
            };

            _process = new Process { StartInfo = startInfo };
            _process.EnableRaisingEvents = true;
            _process.Start();
            _logger.Information("Job [{JobId}]: launched process. Requested core {Core} (skipped on windows)", jobId, _core);
            cancellationToken.Register(() => _process.Kill());
            // _process.Exited += (sender, args) =>  _logger.Information("Job [{JobId}]: process exited", jobId);
            await _process.WaitForExitAsync();
            _logger.Information("Job [{JobId}]: process exited", jobId);
        }
        catch (Exception exception)
        {
            _logger.Error(exception, "Job [{JobId}]: Error launching process", jobId);
            throw;
        }
        finally
        {
            if (_coreTaken)
            {
                _coresAccessor.ReleaseCore(_core);
                _coreTaken = false;
            }
            _process.Close();
        }
    }
}