﻿namespace Backtest.Agent.Processes;

public interface ICoresAccessor
{
    int FreeCores { get; }
    int TakeCore();
    void ReleaseCore(int core);
}