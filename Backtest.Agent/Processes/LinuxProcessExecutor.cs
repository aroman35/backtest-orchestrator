using System.Diagnostics;
using Backtest.Agent.Binaries;
using ILogger = Serilog.ILogger;

namespace Backtest.Agent.Processes;

public class LinuxProcessExecutor : IProcessExecutor
{
    private const string ScreenCommand = "screen";
    public const string LogsPath = "/tmp/backtests/";
    private static string StartCommand(Guid jobId, string binaryPath, int core) =>
        $"screen -dmS job_{jobId} -- bash -c 'taskset -c {core} {binaryPath} {jobId:D} >> {LogPath(jobId)}'"
            .Replace("\"", "\\\"");

    public static string LogPath(Guid jobId) => $"{LogsPath}{jobId}.log";
    
    private readonly ICoresAccessor _coresAccessor;
    private readonly IBinaryManager _binaryManager;
    private readonly ILogger _logger;
    
    private bool _coreTaken;
    private int _core;
    private Guid _jobId;
    private readonly TaskCompletionSource _completionSource = new();

    public LinuxProcessExecutor(ICoresAccessor coresAccessor, IBinaryManager binaryManager, ILogger logger)
    {
        _coresAccessor = coresAccessor;
        _binaryManager = binaryManager;
        _logger = logger.ForContext<LinuxProcessExecutor>().ForContext("OperationId", Guid.NewGuid());
    }

    public async Task StartProcess(Guid jobId, string binaryName, int core, CancellationToken cancellationToken)
    {
        _jobId = jobId;
        _logger.Information("Job [{JobId}]: trying to launch process", jobId);
        _core = _coresAccessor.TakeCore();
        _coreTaken = true;

        try
        {
            var binaryPath = _binaryManager.Take(binaryName);
            var command = StartCommand(jobId, binaryPath, _core);
            using var process = new Process();
            process.StartInfo.FileName = "/bin/bash";
            process.StartInfo.Arguments = $"-c \"{command}\"";
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;
            process.EnableRaisingEvents = true;
            process.Start();
            cancellationToken.Register(StopProcess);
            _logger.Information("Job [{JobId}]: launched process. Requested core {Core}", jobId, _core);
            await process.WaitForExitAsync();
            _logger.Information("Job [{JobId}]: process launcher exited", jobId);
            await _completionSource.Task;
        }
        catch (Exception exception)
        {
            _logger.Error(exception, "Job [{JobId}]: Error launching process", jobId);
            ReleaseCore();
            throw;
        }
    }

    private void StopProcess()
    {
        using var process = new Process();
        try
        {
            process.StartInfo.FileName = ScreenCommand;
            process.StartInfo.Arguments = $"-S job_{_jobId} -X quit";
            process.StartInfo.UseShellExecute = true;
            process.StartInfo.RedirectStandardError = false;
            process.StartInfo.RedirectStandardOutput = false;
            process.EnableRaisingEvents = false;

            process.Start();
            process.WaitForExit(TimeSpan.FromSeconds(1));
            _logger.Information("Job [{JobId}]: process exited", _jobId);
        }
        catch (Exception exception)
        {
            _logger.Error(exception, "Job [{JobId}]: Error killing the process", _jobId);
        }
        finally
        {
            _completionSource.TrySetResult();
            ReleaseCore();
        }
    }

    private void ReleaseCore()
    {
        if (_coreTaken)
        {
            _coresAccessor.ReleaseCore(_core);
            _coreTaken = false;
            _logger.Information("Job [{JobId}]: core {Core} released", _jobId, _core);
        }
    }

    // public void Dispose()
    // {
    //     _logger.Information("Job [{JobId}]: process disposed", _jobId);
    // }
}