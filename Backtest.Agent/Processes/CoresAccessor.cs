﻿using System.Collections.Concurrent;
using Backtest.Domain.Agent;
using Microsoft.Extensions.Options;
using ILogger = Serilog.ILogger;

namespace Backtest.Agent.Processes;

public class CoresAccessor : ICoresAccessor
{
    private readonly ILogger _logger;
    private readonly ConcurrentQueue<int> _coresQueue = new();
    private readonly Semaphore _coresSemaphore;

    public CoresAccessor(IOptions<AgentSettings> agentSettings, ILogger logger)
    {
        _logger = logger.ForContext<CoresAccessor>();
        foreach (var core in agentSettings.Value.Cores)
        {
            _coresQueue.Enqueue(core);
        }

        _coresSemaphore = new Semaphore(FreeCores, FreeCores);
    }
    
    public int FreeCores => _coresQueue.Count;
    public int TakeCore()
    {
        _logger.Information("Free cores: {FreeCores}", _coresQueue.Count);
        _coresSemaphore.WaitOne();
        if (!_coresQueue.TryDequeue(out var core))
            throw new ApplicationException("No free cores to launch job");
        _logger.Information("Core {Core} has been taken", core);
        return core;
    }

    public void ReleaseCore(int core)
    {
        _coresQueue.Enqueue(core);
        var previousCount = _coresSemaphore.Release();
        _logger.Information("Core {Core} released. Free cores: {FreeCores}", core, previousCount + 1);
    }
}