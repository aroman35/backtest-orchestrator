﻿namespace Backtest.Agent.Processes;

public interface IProcessExecutor
{
    Task StartProcess(Guid jobId, string binaryName, int core, CancellationToken cancellationToken);
}