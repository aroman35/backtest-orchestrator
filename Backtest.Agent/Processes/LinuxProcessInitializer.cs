using System.Diagnostics;
using ILogger = Serilog.ILogger;

namespace Backtest.Agent.Processes;

public class LinuxProcessInitializer : IHostedService
{
    private readonly ILogger _logger;

    public LinuxProcessInitializer(ILogger logger)
    {
        _logger = logger.ForContext<LinuxProcessInitializer>();
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        var logsDir = LinuxProcessExecutor.LogsPath;
        if (!Directory.Exists(logsDir))
        {
            Directory.CreateDirectory(logsDir);
            _logger.Information("Created logs dir");
        }
        
        if (Environment.OSVersion.Platform == PlatformID.Unix)
        {
            using var process = new Process();
            {
                process.StartInfo.FileName = "/bin/bash";
                process.StartInfo.Arguments = "-c \"pkill screen\"";
                process.StartInfo.UseShellExecute = true;
                process.StartInfo.RedirectStandardError = false;
                process.StartInfo.RedirectStandardOutput = false;
                process.EnableRaisingEvents = false;

                process.Start();
                await process.WaitForExitAsync(cancellationToken);
            }
            _logger.Information("Cleared previous screens");
        }
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}