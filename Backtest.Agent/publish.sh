rm -rf publish
dotnet publish -r linux-x64 -f net7.0 -p:IncludeAllContentForSelfExtract=true -c Release --self-contained true -p:PublishSingleFile=true -o publish
rm publish/*.pdb
scp -r publish/* 192.168.10.60:/home/alex/backtest-agent/