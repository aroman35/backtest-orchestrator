﻿using Backtest.Domain.Agent;
using ILogger = Serilog.ILogger;

namespace Backtest.Agent;

public class AgentObserver : IAgentObserver
{
    private readonly ILogger _logger;
    private readonly IAgent _agent;

    public AgentObserver(IAgent agent, ILogger logger)
    {
        _agent = agent;
        _logger = logger.ForContext<AgentObserver>();
    }

    public Task Ping(string message)
    {
        _logger.Information("Ping requested for {AgentName}", message);
        return Task.CompletedTask;
    }

    public async Task RemoveCache(string hash)
    {
        _logger.Warning("Preparing to remove {Hash} cache", hash);
        var cache = await _agent.GetCacheByHash(hash);
        await cache.Removed();
        _logger.Warning("Cache {Hash} has been removed", hash);
    }

    public void StopJob(Guid jobId)
    {
        _logger.Warning("Job {JobId} has been stopped", jobId);
    }

    public void RestartAgent()
    {
       _logger.Warning("Restart requested");
    }
}