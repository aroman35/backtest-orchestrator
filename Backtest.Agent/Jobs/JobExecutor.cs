﻿using Backtest.Agent.Cache;
using Backtest.Domain;
using Backtest.Domain.Agent;
using Backtest.Domain.Job;
using Backtest.Domain.MarketDataCache;
using Backtest.Domain.Models;
using Backtest.Agent.Pipes;
using Backtest.Agent.Processes;
using Microsoft.Extensions.Options;
using Orleans.Runtime;
using Orleans.Streams;
using ILogger = Serilog.ILogger;

namespace Backtest.Agent.Jobs;

public class JobExecutor : IJobExecutor, IJobObserver
{
    private readonly IClusterClient _clusterClient;
    private readonly ILogger _logger;
    private readonly IAgent _agentGrain;
    private readonly JobsManager _jobsManager;
    private readonly CancellationTokenSource _jobCancellationTokenSource = new();
    private readonly TaskCompletionSource _jobCompletion = new();

    private readonly IPipeExecutor _pipeExecutor;
    private readonly IProcessExecutor _processExecutor;
    private readonly IAgentCacheManager _agentCacheManager;
    
    private IAsyncStream<double> _progressStream;
    private IJobObserver _jobObserver;

    private IJob _jobGrain;
    private string _binary;
    private string _buildingCacheHash;
    private int _core;
    private Task _pipeExecution;
    private Task _processExecution;
    private Task _observerTask;

    public JobExecutor(
        IClusterClient clusterClient,
        IOptions<AgentSettings> agentSettings,
        ILogger logger,
        JobsManager jobsManager,
        IPipeExecutor pipeExecutor,
        IProcessExecutor processExecutor,
        IAgentCacheManager agentCacheManager)
    {
        ArgumentNullException.ThrowIfNull(clusterClient);
        ArgumentNullException.ThrowIfNull(agentSettings);
        ArgumentNullException.ThrowIfNull(logger);
        ArgumentNullException.ThrowIfNull(jobsManager);
        ArgumentNullException.ThrowIfNull(pipeExecutor);
        ArgumentNullException.ThrowIfNull(processExecutor);
        
        _logger = logger.ForContext<JobExecutor>().ForContext("OperationId", Guid.NewGuid());;
        _jobsManager = jobsManager;
        _pipeExecutor = pipeExecutor;
        _processExecutor = processExecutor;
        _agentCacheManager = agentCacheManager;
        _clusterClient = clusterClient;
        _agentGrain = _clusterClient.GetGrain<IAgent>(agentSettings.Value.AgentName);
    }

    public Guid JobId { get; private set; }

    #region Management

    public async Task<Task> Launch(Guid jobId, int core, string binaryName)
    {
        JobId = jobId;
        _core = core;
        _binary = binaryName;
        _logger.Information("Job [{JobId}]: prepared to launch", jobId);
        _jobGrain = _clusterClient.GetGrain<IJob>(jobId);
        try
        {
            _jobObserver = _clusterClient.CreateObjectReference<IJobObserver>(this);
            _observerTask = Task.Run(ReconnectObserver);

            _logger.Information("Job [{JobId}]: subscribed to events", JobId);

            var streamId = StreamId.Create(Constants.STATUS_UPDATES_STREAM, jobId);
            _progressStream = _clusterClient.GetStreamProvider(Constants.STREAM_PROVIDER_NAME)
                .GetStream<double>(streamId);

            _jobsManager.Add(this, _jobCancellationTokenSource.Token);

            _pipeExecution = Task.Run(() => _pipeExecutor.Launch(JobId, _jobCancellationTokenSource.Token));
            _processExecution = Task.Run(() => _processExecutor.StartProcess(JobId, _binary, _core, _jobCancellationTokenSource.Token));

            await _jobGrain.Launched(_core);
            _jobCancellationTokenSource.Token.Register(() => _logger.Information("Job [{JobId}]: job exited", JobId));
            return _jobCompletion.Task;
        }
        catch (Exception exception)
        {
            _logger.Error(exception, "Job [{JobId}]: Error launching job", JobId);
            _jobCancellationTokenSource.Cancel();
            throw;
        }
    }
    
    public async Task Cancel(bool force)
    {
        _logger.Information("Job [{JobId}]: job canceled", JobId);
        await ReportTestResult(new JobFinishedCommand(false, "Job was cancelled", 0, 0, true));
    }

    public async Task ReconnectObserver()
    {
        var observerTtl = await _jobGrain.Subscribe(_jobObserver);
        using var timer = new PeriodicTimer(TimeSpan.FromSeconds(observerTtl));
        while (!_jobCancellationTokenSource.IsCancellationRequested && await timer.WaitForNextTickAsync(_jobCancellationTokenSource.Token))
        {
            await _jobGrain.Subscribe(_jobObserver);
        }
    }

    #endregion
    
    #region Test

    public async Task ReportTestProgress(double progress)
    {
        await _progressStream.OnNextAsync(progress);
        _logger.Information("Job [{JobId}]: test progress received {Progress}%", JobId, progress);
    }

    public async Task ReportTestResult(TaskResult result)
    {
        await ReportTestResult(new JobFinishedCommand(result.Success, result.Message, result.MdEntryCount, result.TestDuration, false));
    }

    public async Task<TaskInfo> GetTaskInfo()
    {
        var startInfo = await _jobGrain.GetStartInfo();
        _logger.Information("Job [{JobId}]: info requested", JobId);
        return startInfo;
    }
    
    #endregion

    #region Cache
    
    public async Task ReportCacheProgress(double progress)
    {
        await _jobGrain.CacheBuildProgress(progress);
        _logger.Information("Job [{JobId}]: cache progress received {Progress}%", JobId, progress);
    }

    public async Task CacheBuildComplete(bool isSuccess)
    {
        await _agentCacheManager.BuildComplete(_jobGrain, _buildingCacheHash, isSuccess);
        _logger.Information("Job [{JobId}]: cache {Hash} build completed", JobId, _buildingCacheHash);
        _buildingCacheHash = string.Empty;
    }

    public async Task<CacheResponse> CacheRequest(CacheRequest cacheRequest)
    {
        var cacheResponse = new CacheResponse(new CacheEntry[cacheRequest.Hashes.Length]);
        var ready = 0;
        var building = 0;

        for (var i = 0; i < cacheRequest.Hashes.Length; i++)
        {
            _logger.Information("Job [{JobId}]: cache requested {Hash}", JobId, cacheRequest.Hashes[i]);
            var meta = _clusterClient.GetGrain<IMarketDataCacheMeta>(cacheRequest.Hashes[i]);
            var cache = await meta.GetCacheForAgent(_agentGrain);
            var cacheItem = await cache.Take(_jobGrain);
            cacheResponse.Entries[i] = cacheItem;
            if (cacheItem.Status is CacheEntryStatus.Ready)
                ready++;
            if (cacheItem.Status is CacheEntryStatus.BuildIt)
            {
                _buildingCacheHash = cacheRequest.Hashes[i];
                _logger.Information("Job [{JobId}]: building cache {Hash}", JobId, _buildingCacheHash);
                building++;
            }
        }

        _logger.Information("Job [{JobId}]: cache requested {Ready}/{Total}[{Building}]", JobId, ready, cacheRequest.Hashes.Length, building);
        return cacheResponse;
    }
    
    #endregion
    
    private async Task ReportTestResult(JobFinishedCommand command)
    {
        await ReportTestProgress(100);
        
        _jobCompletion.SetResult();
        if (command.Success)
            _logger.Information("Job [{JobId}]: result received", JobId);
        else
            _logger.Error("Job [{JobId}]: failed: {Error}", JobId, command.Message);
        _jobCancellationTokenSource.Cancel(false);
        _jobCancellationTokenSource.Dispose();
        await _jobGrain.JobFinished(command);
        // await Task.WhenAll(_processExecution, _pipeExecution, _observerTask).WaitAsync(TimeSpan.FromSeconds(1));
    }
}