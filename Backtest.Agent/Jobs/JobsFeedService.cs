﻿using Backtest.Domain;
using Backtest.Domain.Agent;
using Microsoft.Extensions.Options;
using Orleans.Runtime;
using Orleans.Streams;
using ILogger = Serilog.ILogger;

namespace Backtest.Agent.Jobs;

public class JobsFeedService : IHostedService
{
    private readonly IClusterClient _clusterClient;
    private readonly ILogger _logger;
    private readonly IServiceScopeFactory _serviceScopeFactory;
    private readonly string _agentName;

    private IAsyncStream<JobLaunchCommand> _jobsStream;
    private StreamSubscriptionHandle<JobLaunchCommand> _jobsSubscription;

    public JobsFeedService(
        IClusterClient clusterClient,
        IOptions<AgentSettings> agentSettings,
        ILogger logger,
        IServiceScopeFactory serviceScopeFactory)
    {
        _clusterClient = clusterClient;
        _logger = logger;
        _serviceScopeFactory = serviceScopeFactory;
        _agentName = agentSettings.Value.AgentName;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        var streamId = StreamId.Create(Constants.LAUNCH_JOB_STREAM, _agentName);
        _jobsStream = _clusterClient.GetStreamProvider(Constants.STREAM_PROVIDER_NAME).GetStream<JobLaunchCommand>(streamId);
        _jobsSubscription = await _jobsStream.SubscribeAsync(launchCommands => Task.WhenAll(
            launchCommands.Select(launchCommand => EnqueueJob(
                launchCommand.Item.JobId,
                launchCommand.Item.Core,
                launchCommand.Item.BinaryName))));
        _logger.Information("Agent [{AgentName}]: connected to streams", _agentName);
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        await _jobsSubscription.UnsubscribeAsync();
    }
    
    private async Task EnqueueJob(Guid jobId, int core, string binaryName)
    {
        _logger.Information("Job [{JobId}]: received job from scheduler", jobId);

        await using var scope = _serviceScopeFactory.CreateAsyncScope();
        var executor = scope.ServiceProvider.GetRequiredService<IJobExecutor>();
        await executor.Launch(jobId, core, binaryName);
    }
}