using Backtest.Domain.Models;

namespace Backtest.Agent.Jobs;

public interface IJobExecutor
{
    Guid JobId { get; }
    Task<Task> Launch(Guid jobId, int core, string binaryName);
    Task ReportTestProgress(double progress);
    Task ReportTestResult(TaskResult result);
    Task ReportCacheProgress(double progress);
    Task CacheBuildComplete(bool isSuccess);
    Task<CacheResponse> CacheRequest(CacheRequest cacheRequest);
    Task<TaskInfo> GetTaskInfo();
    Task Cancel(bool force);
    Task ReconnectObserver();
}