﻿using System.Collections.Concurrent;
using ILogger = Serilog.ILogger;

namespace Backtest.Agent.Jobs;

public class JobsManager
{
    private readonly ConcurrentDictionary<Guid, IJobExecutor> _runningJobs = new();
    private readonly ConcurrentDictionary<Guid, TaskCompletionSource> _jobsWaitQueue = new();
    private readonly ILogger _logger;

    public JobsManager(ILogger logger)
    {
        _logger = logger.ForContext<JobsManager>();
    }

    public void Add(IJobExecutor jobExecutor, CancellationToken cancellationToken)
    {
        if (_runningJobs.TryGetValue(jobExecutor.JobId, out _))
            throw new ApplicationException($"Job {jobExecutor.JobId} is already running!");

        cancellationToken.Register(() => Remove(jobExecutor.JobId));
        _runningJobs.TryAdd(jobExecutor.JobId, jobExecutor);
        if (_jobsWaitQueue.TryRemove(jobExecutor.JobId, out var waiter))
            waiter.SetResult();
    }
    
    public IJobExecutor GetJobById(Guid jobId)
    {
        if (!_runningJobs.TryGetValue(jobId, out var job))
            throw new ApplicationException($"Job {jobId} was not found!");
        return job;
    }

    public Task WaitJobToBeAdded(Guid jobId)
    {
        if (_runningJobs.ContainsKey(jobId))
            return Task.CompletedTask;
        var waitJob = _jobsWaitQueue.GetOrAdd(jobId, new TaskCompletionSource());
        return waitJob.Task;
    }

    private void Remove(Guid jobId)
    {
        _runningJobs.TryRemove(jobId, out _);
        _logger.Information("Job [{JobId}]: removed", jobId);
    }
}