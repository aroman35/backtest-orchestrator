using System.Diagnostics.Metrics;
using Backtest.Common;
using Backtest.Domain;
using Backtest.Silo;
using Orleans.Runtime;
using OrleansDashboard;
using Serilog;
using Serilog.Events;
using static Backtest.Domain.Constants;

var bootstrapLogger = new LoggerConfiguration()
    .WriteTo.Console()
    .MinimumLevel.Debug()
    .CreateBootstrapLogger();

try
{
    var webBuilder = WebApplication.CreateBuilder();
    webBuilder.Configuration
        .AddJsonFile("appsettings.json", true, true)
        .AddConsulConfiguration("silo")
        .AddConsulConfiguration("logger-configuration")
        .AddConsulConfiguration("kafka-streams-configuration")
        .AddConsulConfiguration("database-settings")
        .AddConsulConfiguration("jaeger-configuration")
        .AddEnvironmentVariables();
    webBuilder.Host.UseOrleans((hostContext, builder) => builder.ConfigureSiloCluster(hostContext.Configuration));
    webBuilder.Host.UseConsoleLifetime();
    webBuilder.Host.UseSerilog((hostContext, loggingConfiguration) => loggingConfiguration
        .ReadFrom.Configuration(hostContext.Configuration)
        .Enrich.WithProperty(APPLICATION_NAME_PROP, APPLICATION_NAME_VAL)
        .Enrich.WithProperty(INSTANCE_NAME_PROP, "Silo"));
    var siloWebPort = webBuilder.Configuration.GetSection(nameof(SiloConfiguration)).Get<SiloConfiguration>().SiloWebPort;
    webBuilder.WebHost.UseKestrel(kestrel => kestrel.ListenAnyIP(siloWebPort));
    await using var app = webBuilder.Build();
    app.MapPrometheusScrapingEndpoint();
    app.UseOrleansDashboard(new DashboardOptions
    {
        BasePath = "dashboard"
    });
    await app.RunAsync();
}
catch (Exception exception)
{
    bootstrapLogger.Write(LogEventLevel.Fatal, exception, "Error launching host");
    throw;
}
finally
{
    Log.CloseAndFlush();
}

public class MockMeterService : IStartupTask
{
    private readonly Counter<int> _counter;

    public MockMeterService()
    {
        var meter = new Meter("MY_CUSTOM_METRIC");
        _counter = meter.CreateCounter<int>("MY_CUSTOM_COUNTER");
    }
    public Task Execute(CancellationToken cancellationToken)
    {
        _counter.Add(10);
        return Task.CompletedTask;
    }
}