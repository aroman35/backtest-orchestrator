﻿using Backtest.Domain;
using Backtest.Core.Agent;
using Backtest.Core.Job;
using Backtest.Domain.Queries;
using Backtest.Persistence;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using OpenTelemetry;
using OpenTelemetry.Exporter;
using OpenTelemetry.Metrics;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;
using Orleans.Configuration;
using Orleans.Providers.MongoDB.Configuration;
using Orleans.Providers.MongoDB.StorageProviders.Serializers;
using Orleans.Runtime;
using Orleans.Statistics;
using Serilog;
using static Backtest.Domain.Constants;

namespace Backtest.Silo;

public static class ConfigurationExtensions
{
    public static ISiloBuilder ConfigureSiloCluster(this ISiloBuilder siloBuilder, IConfiguration configuration)
    {
        var databaseSettings = configuration.GetSection(nameof(DatabaseSettings)).Get<DatabaseSettings>();
        var kafkaStreamSettings = configuration.GetSection(nameof(KafkaStreamsOptions)).Get<KafkaStreamsOptions>();
        var siloConfiguration = configuration.GetSection(nameof(SiloConfiguration)).Get<SiloConfiguration>();
        var jaegerConfiguration = configuration.GetSection(nameof(JaegerOptions)).Get<JaegerOptions>();

        BsonSerializer.RegisterSerializer(new GuidSerializer(BsonType.String));
        
        siloBuilder.UseMongoDBClient(databaseSettings.ConnectionString);
        siloBuilder.UseMongoDBClustering(options =>
        {
            options.DatabaseName = databaseSettings.DatabaseName;
            options.Strategy = MongoDBMembershipStrategy.Multiple;
        });
        siloBuilder.AddRedisGrainStorage(REDIS_STORAGE_DB0, options =>
        {
            options.Configure(redis =>
            {
                redis.DeleteOnClear = true;
                redis.ConnectionString = configuration.GetConnectionString(REDIS_STORAGE);
                redis.DatabaseNumber = 0;
            });
        });
        siloBuilder.AddRedisGrainStorage(REDIS_STORAGE_DB1, options =>
        {
            options.Configure(redis =>
            {
                redis.DeleteOnClear = true;
                redis.ConnectionString = configuration.GetConnectionString(REDIS_STORAGE);
                redis.DatabaseNumber = 1;
            });
        });
        siloBuilder.AddRedisGrainStorage(REDIS_STORAGE_DB2, options =>
        {
            options.Configure(redis =>
            {
                redis.DeleteOnClear = true;
                redis.ConnectionString = configuration.GetConnectionString(REDIS_STORAGE);
                redis.DatabaseNumber = 2;
            });
        });
        siloBuilder.AddRedisGrainStorage(REDIS_STORAGE_DB3, options =>
        {
            options.Configure(redis =>
            {
                redis.DeleteOnClear = true;
                redis.ConnectionString = configuration.GetConnectionString(REDIS_STORAGE);
                redis.DatabaseNumber = 3;
            });
        });
        siloBuilder.AddMemoryGrainStorage(MEMORY_STORAGE);
        siloBuilder.UseMongoDBReminders(options =>
        {
            options.DatabaseName = databaseSettings.DatabaseName;
            options.CollectionPrefix = "reminders-";
            options.CollectionConfigurator = collection =>
            {
                collection.AssignIdOnInsert = true;
            };
        });
        siloBuilder.AddMongoDBGrainStorageAsDefault(options =>
        {
            options.DatabaseName = databaseSettings.DatabaseName;
            options.CollectionPrefix = "state-";
            options.GrainStateSerializer = new BsonGrainStateSerializer();
            options.KeyGenerator = grainId => grainId.TryGetGuidKey(out var guidKey, out _) ? guidKey.ToString("D") : grainId.ToString();
        });
        siloBuilder.AddMongoDBGrainStorage("PubSubStore", options =>
        {
            options.CollectionPrefix = "streams-";
            options.DatabaseName = databaseSettings.DatabaseName;
        });
        siloBuilder.ConfigureEndpoints(siloConfiguration.IpAddress, siloConfiguration.Port, siloConfiguration.GatewayPort, true);
        siloBuilder.AddGrainService<QueriesGrainService>();
        siloBuilder.ConfigureServices(services => services
            .Configure<ClusterOptions>(configuration.GetSection(nameof(ClusterOptions)))
            .Configure<JobExecutionOptions>(configuration.GetSection(nameof(JobExecutionOptions)))
            .Configure<AgentConnectivitySettings>(configuration.GetSection(nameof(AgentConnectivitySettings)))
            .Configure<DatabaseSettings>(configuration.GetSection(nameof(DatabaseSettings)))
            .AddSingleton<IMongoDbContext, MongoDbContext>()
            .AddHostedService(provider => provider.GetRequiredService<IMongoDbContext>())
            .AddSingleton<IHostEnvironmentStatistics, HostEnvironmentStatistics>()
            .AddSingleton<IQueriesServiceClient, QueriesServiceClient>()
            .AddDashboard()
            // https://www.mytechramblings.com/posts/getting-started-with-opentelemetry-metrics-and-dotnet-part-2/
            .AddOpenTelemetry()
            .WithTracing(tracing =>
            {
                tracing.SetResourceBuilder(ResourceBuilder.CreateDefault()
                        .AddService(serviceName: APPLICATION_NAME_VAL, serviceVersion: "1.0", autoGenerateServiceInstanceId: true)
                        .AddAttributes(new[]
                        {
                            new KeyValuePair<string, object>(APPLICATION_NAME_PROP, APPLICATION_NAME_VAL),
                            new KeyValuePair<string, object>(INSTANCE_NAME_PROP, "Silo"),
                            new KeyValuePair<string, object>("endpoint", siloConfiguration.IpAddress.ToString()),
                        }));
                tracing.AddSource("Microsoft.Orleans.Runtime");
                tracing.AddSource("Microsoft.Orleans.Application");
                tracing.AddJaegerExporter(jaeger =>
                {
                    jaeger.Protocol = JaegerExportProtocol.UdpCompactThrift;
                    jaeger.ExportProcessorType = ExportProcessorType.Batch;
                    jaeger.AgentPort = jaegerConfiguration.AgentPort;
                    jaeger.AgentHost = jaegerConfiguration.AgentHost;
                });
            })
            .WithMetrics(metrics =>
            {
                metrics
                    .SetResourceBuilder(ResourceBuilder.CreateDefault()
                        .AddService(APPLICATION_NAME_VAL, serviceVersion: "1.0", autoGenerateServiceInstanceId: true)
                        .AddAttributes(new[]
                        {
                            new KeyValuePair<string, object>(APPLICATION_NAME_PROP, APPLICATION_NAME_VAL),
                            new KeyValuePair<string, object>(INSTANCE_NAME_PROP, "Silo"),
                            new KeyValuePair<string, object>("endpoint", siloConfiguration.IpAddress.ToString()),
                        }))
                    .AddRuntimeInstrumentation()
                    .AddProcessInstrumentation()
                    .AddPrometheusExporter()
                    .AddMeter("MY_CUSTOM_METRIC", "Microsoft.Orleans");
            })
        );

        siloBuilder.ConfigureLogging(logging => logging.SetMinimumLevel(LogLevel.Information).AddSerilog());
        siloBuilder.AddKafkaStreams(kafkaStreamSettings);
        // siloBuilder.AddMemoryStreams(STREAM_PROVIDER_NAME);
        siloBuilder.AddActivityPropagation();
        siloBuilder.AddStartupTask<MockMeterService>();
        return siloBuilder;
    }
}