﻿using Backtest.Core.Agent;
using Backtest.Domain.Agent;
using Backtest.Domain.Job;
using Backtest.Domain.MarketDataCache;
using Backtest.Domain.Models;
using Backtest.Domain.States;
using Backtest.Domain.States.Relations;
using Backtest.Core.Exceptions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Orleans.Runtime;
using Orleans.Utilities;
using static Backtest.Domain.Constants;
using ILogger = Serilog.ILogger;

namespace Backtest.Core.MarketDataCache;

public class MarketDataCache : Grain, IMarketDataCache, IRemindable
{
    private const string BUILDING_CACHE_REMINDER_NAME = "cache-build-timeout";
    
    private readonly IPersistentState<MarketDataCacheState> _marketDataCacheState;
    private readonly IPersistentState<RunningJobsCacheMap> _usedByJobs;
    private readonly IOptions<AgentConnectivitySettings> _agentConnectivitySettings;
    private readonly ObserverManager<IMarketDataCacheObserver> _cacheObserverManager;

    private ILogger _logger;
    private IAgent _agent;
    private IJob _buildingJob;
    private IGrainReminder _cacheBuildReminder;
    private IMarketDataCacheMeta _meta;

    public MarketDataCache(
        [PersistentState(MARKET_DATA_CACHE)]IPersistentState<MarketDataCacheState> marketDataCacheState,
        [PersistentState(MARKET_DATA_JOBS_MAP, REDIS_STORAGE_DB1)]IPersistentState<RunningJobsCacheMap> usedByJobs,
        IOptions<AgentConnectivitySettings> agentConnectivitySettings,        
        ILogger logger,
        ILoggerFactory loggerFactory)
    {
        _marketDataCacheState = marketDataCacheState;
        _usedByJobs = usedByJobs;
        _agentConnectivitySettings = agentConnectivitySettings;
        _logger = logger.ForContext<MarketDataCache>();
        _cacheObserverManager = new ObserverManager<IMarketDataCacheObserver>(
            TimeSpan.FromSeconds(_agentConnectivitySettings.Value.KeepAliveIntervalSec),
            loggerFactory.CreateLogger<ObserverManager<IMarketDataCacheObserver>>());
    }

    public Task<CacheInfo> GetEntryInfo()
    {
        return Task.FromResult(_marketDataCacheState.State.CreateCacheInfo());
    }

    public async Task Initialize(IAgent agent, IMarketDataCacheMeta meta, CacheEntryStatus initialStatus)
    {
        _agent = agent;
        _meta = meta;
        var info = await _agent.GetCacheInfo();
        
        _marketDataCacheState.State.Hash = _meta.GetPrimaryKeyString();
        _marketDataCacheState.State.AgentName = _agent.GetPrimaryKeyString();
        _marketDataCacheState.State.Compression = info.Compression;
        _marketDataCacheState.State.Type = info.Type;
        _marketDataCacheState.State.Status = initialStatus;
        _marketDataCacheState.State.DataFilePath = Path.Combine(info.CachePath, $"{_marketDataCacheState.State.Hash}.mdp");
        _marketDataCacheState.State.MetaFilePath = Path.Combine(info.CachePath, $"{_marketDataCacheState.State.Hash}.meta");
        _marketDataCacheState.State.LastAccess = DateTime.UtcNow;
        _marketDataCacheState.State.IsInitialized = true;

        _logger = _logger
            .ForContext("Hash", _marketDataCacheState.State.Hash)
            .ForContext("AgentName", _marketDataCacheState.State.AgentName);
        
        _logger.Information("Cache [{Hash}]: Initialized on {AgentName}",  _meta.GetPrimaryKeyString(), _agent.GetPrimaryKeyString());
    }

    public async Task ReportProgress(double progress)
    {
        var reminderDelay = TimeSpan.FromSeconds(_marketDataCacheState.State.BuildTimeout);
        _cacheBuildReminder = await this.RegisterOrUpdateReminder(BUILDING_CACHE_REMINDER_NAME, reminderDelay, reminderDelay);
        
        _marketDataCacheState.State.Progress = progress;
        _marketDataCacheState.State.LastAccess = DateTime.UtcNow;
        _marketDataCacheState.State.LastBuildReport = DateTime.UtcNow;
    }

    public async Task<CacheEntry> Take(IJob job)
    {
        if (_agent is null)
            _logger.Error("Job [{JobId}]: Агент блять не привязан к этому кэшу!!!!", job.GetPrimaryKey());
        AssertAgentIsMapped();
        AssertMetaIsMapped();
        
        _usedByJobs.State.Jobs.Add(job.GetPrimaryKey());
        _marketDataCacheState.State.LastAccess = DateTime.UtcNow;
        await job.MapCache(_marketDataCacheState.State.Hash, this);

        if (!_marketDataCacheState.State.IsBuilding && _marketDataCacheState.State.Status is CacheEntryStatus.NotReady && await job.TryBuildCache(this))
        {
            _buildingJob = job;
            _marketDataCacheState.State.BuildingJob = _buildingJob.GetPrimaryKey();
            _marketDataCacheState.State.Progress = 0;
            _marketDataCacheState.State.LastBuildReport = DateTime.UtcNow;
            _marketDataCacheState.State.IsBuilding = true;
            
            var reminderDelay = TimeSpan.FromSeconds(_marketDataCacheState.State.BuildTimeout);
            _cacheBuildReminder = await this.RegisterOrUpdateReminder(BUILDING_CACHE_REMINDER_NAME, reminderDelay, reminderDelay);
            
            return _marketDataCacheState.State.CreateCacheInfo().CacheEntry.CreateBuildTask();
        }

        return _marketDataCacheState.State.CreateCacheInfo().CacheEntry;
    }

    public Task Free(IJob job)
    {
        _usedByJobs.State.Jobs.Remove(job.GetPrimaryKey());
        return Task.CompletedTask;
    }

    public async Task CacheBuildComplete(CacheBuildCompleteResult result)
    {
        AssertMetaIsMapped();
        AssertAgentIsMapped();
        
        await this.UnregisterReminder(_cacheBuildReminder);
        if (result.IsSuccess)
        {
            await _meta.BuildComplete(result.MetaCommand);
            await _agent.CacheCreated(result.MetaCommand.FileSizeBytes);
            _marketDataCacheState.State.Progress = 100;
        }
        
        _marketDataCacheState.State.IsBuilding = false;
        _marketDataCacheState.State.Status = result.IsSuccess ? CacheEntryStatus.Ready : CacheEntryStatus.NotReady;
    }

    public async Task<CacheUseInfo> GetCacheUseInfo()
    {
        AssertMetaIsMapped();
        var size = await _meta.DataFileSizeBytes();
        return new CacheUseInfo(
            this.GetPrimaryKey(),
            _marketDataCacheState.State.Hash,
            _marketDataCacheState.State.LastAccess,
            _usedByJobs.State.Jobs.Count,
            size,
            !_marketDataCacheState.State.PendingForRemove);
    }

    public async Task Remove()
    {
        _marketDataCacheState.State.PendingForRemove = true;
        await _cacheObserverManager.Notify(x => x.Remove());
    }

    public async Task Removed()
    {
        AssertAgentIsMapped();
        AssertMetaIsMapped();
        
        var size = await _meta.DataFileSizeBytes();
        await _meta.CacheRemoved(_agent.GetPrimaryKeyString());
        await _agent.CacheRemoved(_marketDataCacheState.State.Hash, size);
        await _marketDataCacheState.ClearStateAsync();
        await _usedByJobs.ClearStateAsync();
        DeactivateOnIdle();
    }

    public Task<long> EstimatedCacheSize()
    {
        AssertMetaIsMapped();
        return _meta.DataFileSizeBytes();
    }

    public Task<double> Subscribe(IMarketDataCacheObserver observer)
    {
        _cacheObserverManager.Unsubscribe(observer);
        _cacheObserverManager.Subscribe(observer, observer);
        return Task.FromResult(_agentConnectivitySettings.Value.KeepAliveIntervalSec);
    }

    async Task IRemindable.ReceiveReminder(string reminderName, TickStatus status)
    {
        var lastReport = DateTime.UtcNow - _marketDataCacheState.State.LastBuildReport;
        if (lastReport > TimeSpan.FromSeconds(_marketDataCacheState.State.BuildTimeout))
        {
            if (_buildingJob is not null)
            {
                var error = $"There are was no any cache building reports during {lastReport}";
                await _buildingJob.JobFinished(new JobFinishedCommand(false, error, 0, 0, false));
                await CacheBuildComplete(CacheBuildCompleteResult.ErrorResult(error));
            }
        }
    }

    public override async Task OnActivateAsync(CancellationToken cancellationToken)
    {
        await base.OnActivateAsync(cancellationToken);
        if (!string.IsNullOrEmpty(_marketDataCacheState.State.AgentName))
            _agent = GrainFactory.GetGrain<IAgent>(_marketDataCacheState.State.AgentName);
        
        if (_marketDataCacheState.State.IsBuilding && _marketDataCacheState.State.BuildingJob.HasValue)
        {
            _buildingJob = GrainFactory.GetGrain<IJob>(_marketDataCacheState.State.BuildingJob.Value);
            _cacheBuildReminder = await this.GetReminder(BUILDING_CACHE_REMINDER_NAME);
        }

        if (!string.IsNullOrEmpty(_marketDataCacheState.State.Hash))
            _meta = GrainFactory.GetGrain<IMarketDataCacheMeta>(_marketDataCacheState.State.Hash);
    }

    public override async Task OnDeactivateAsync(DeactivationReason reason, CancellationToken cancellationToken)
    {
        await _marketDataCacheState.WriteStateAsync();
        await _usedByJobs.WriteStateAsync();
        await base.OnDeactivateAsync(reason, cancellationToken);
    }

    private void AssertAgentIsMapped()
    {
        if (_agent is null)
            throw new ObjectNotMappedException<IMarketDataCache, IAgent>();
    }

    private void AssertBuildingJobIsMapped()
    {
        if (_buildingJob is null)
            throw new ObjectNotMappedException<IMarketDataCache, IJob>();
    }

    private void AssertMetaIsMapped()
    {
        if (_meta is null)
            throw new ObjectNotMappedException<IMarketDataCache, IMarketDataCacheMeta>();
    }

    public Task<MarketDataCacheState> GetState()
    {
        return Task.FromResult(_marketDataCacheState.State);
    }
}