using Backtest.Domain.Agent;
using Backtest.Domain.MarketDataCache;
using Backtest.Domain.Models;
using Backtest.Domain.States;
using Orleans.Runtime;
using static Backtest.Domain.Constants;

namespace Backtest.Core.MarketDataCache;

public class MarketDataCacheMeta : Grain, IMarketDataCacheMeta
{
    private readonly IPersistentState<MarketDataCacheMetaState> _metaState;
    private readonly IPersistentState<MarketDataCacheMap> _cacheMap;

    public MarketDataCacheMeta(
        [PersistentState(MARKET_DATA_META)]IPersistentState<MarketDataCacheMetaState> metaState,
        [PersistentState(MARKET_DATA_CACHE_MAP, REDIS_STORAGE_DB1)]IPersistentState<MarketDataCacheMap> cacheMap
    )
    {
        _metaState = metaState;
        _cacheMap = cacheMap;
    }

    public Task BuildComplete(FillMarketDataCacheMetaCommand fillCommand)
    {
        _metaState.State.StartDate = fillCommand.BeginDate;
        _metaState.State.EndDate = fillCommand.EndDate;
        _metaState.State.MdEntryCount = fillCommand.MdEntryCount;
        _metaState.State.DataFileSize = fillCommand.FileSizeBytes;
        _metaState.State.Instruments = fillCommand.Instruments;
        _metaState.State.FilesMap = fillCommand.FilesMap;

        return Task.CompletedTask;
    }

    public Task CacheRemoved(string agentName)
    {
        _cacheMap.State.MarketDataCache.Remove(agentName);
        return Task.CompletedTask;
    }

    public Task<long> DataFileSizeBytes()
    {
        return Task.FromResult(_metaState.State.DataFileSize);
    }

    public async Task<IMarketDataCache> GetCacheForAgent(IAgent agent)
    {
        var agentKey = agent.GetPrimaryKeyString();
        if (_cacheMap.State.MarketDataCache.TryGetValue(agentKey, out var cacheId))
        {
            return GrainFactory.GetGrain<IMarketDataCache>(cacheId);
        }
        cacheId = Guid.NewGuid();
        var cache = GrainFactory.GetGrain<IMarketDataCache>(cacheId);
        await cache.Initialize(agent, this, CacheEntryStatus.NotReady);
        await agent.CacheAdded(this, cache);
        _cacheMap.State.MarketDataCache.Add(agentKey, cacheId);
        return cache;
    }

    public async Task Initialize(FillMarketDataCacheMetaCommand fillCommand, IAgent agent)
    {
        var agentKey = agent.GetPrimaryKeyString();
        if (!_cacheMap.State.MarketDataCache.TryGetValue(agentKey, out var cacheId))
        {
            cacheId = Guid.NewGuid();
            _cacheMap.State.MarketDataCache.TryAdd(agentKey, cacheId);
        }
        var cache = GrainFactory.GetGrain<IMarketDataCache>(cacheId);
        await cache.Initialize(agent, this, CacheEntryStatus.Ready);
    }

    public override async Task OnDeactivateAsync(DeactivationReason reason, CancellationToken cancellationToken)
    {
        await _metaState.WriteStateAsync();
        await _cacheMap.WriteStateAsync();
        await base.OnDeactivateAsync(reason, cancellationToken);
    }

    public Task<MarketDataCacheMetaState> GetState()
    {
        return Task.FromResult(_metaState.State);
    }
}