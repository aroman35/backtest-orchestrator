﻿using Backtest.Domain;
using Orleans.EventSourcing;
using Orleans.EventSourcing.CustomStorage;
using Orleans.Runtime;

namespace Backtest.Core.Abstractions;

public abstract class JournalGrainBase<TState, TEventType> : JournaledGrain<TState>, IStatefulGrain<TState>, ICustomStorageInterface<TState, TEventType>
    where TState : GrainStateBase, new()
{
    private protected readonly IPersistentState<TState> MainState;
    
    protected JournalGrainBase(IPersistentState<TState> mainState)
    {
        MainState = mainState;
    }

    public Task<TState> GetState()
    {
        return Task.FromResult(State);
    }

    public Task<KeyValuePair<int, TState>> ReadStateFromStorage()
    {
        return Task.FromResult(new KeyValuePair<int,TState>(MainState.State.Version, MainState.State));
    }
    
    public async Task<bool> ApplyUpdatesToStorage(IReadOnlyList<TEventType> updates, int expectedversion)
    {
        MainState.State = State;
        MainState.State.Version = Version;
        await MainState.WriteStateAsync();
        return true;
    }
}