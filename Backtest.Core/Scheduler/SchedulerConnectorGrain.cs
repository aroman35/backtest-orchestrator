﻿using System.Collections.Concurrent;
using Backtest.Domain.Agent;
using Backtest.Domain.Job;
using Backtest.Domain.Queries;
using Backtest.Domain.Scheduler;
using Backtest.Domain.States;
using Orleans.Runtime;
using Orleans.Streams;
using static Backtest.Domain.Constants;
using ILogger = Serilog.ILogger;

namespace Backtest.Core.Scheduler;

public class SchedulerConnectorGrain : Grain, ISchedulerConnector
{
    #region AgentsSubscriptions

    private readonly ConcurrentDictionary<string, IAgent> _allAgents = new();

    #endregion
    
    #region Queues
    
    private readonly ConcurrentDictionary<Guid, IJob> _allQueueItems = new();
    private readonly ConcurrentQueue<Guid> _mainQueue = new();
    private readonly ConcurrentQueue<Guid> _retryQueue = new();
    private readonly ConcurrentDictionary<IAgent, ConcurrentQueue<Guid>> _directMainQueue = new();
    private readonly ConcurrentDictionary<IAgent, ConcurrentQueue<Guid>> _directRetryQueue = new();
    private readonly ConcurrentDictionary<IAgent, int> _agentSlots = new();
    
    #endregion

    #region Streams
    
    private readonly StreamId _jobsQueueStreamId = StreamId.Create(JOBS_QUEUE_STREAM, Guid.Empty);
    private readonly StreamId _jobCancelledStreamId = StreamId.Create(JOB_CANCELLED_STREAM, Guid.Empty);
    
    private IAsyncStream<QueueItem> JobsQueueStream => this.GetStreamProvider(STREAM_PROVIDER_NAME).GetStream<QueueItem>(_jobsQueueStreamId);
    private IAsyncStream<Guid> JobCancelledStream => this.GetStreamProvider(STREAM_PROVIDER_NAME).GetStream<Guid>(_jobCancelledStreamId);

    private StreamSubscriptionHandle<QueueItem> _jobsQueueStreamSubscription;
    private StreamSubscriptionHandle<Guid> _jobCancelledSubscription;

    #endregion

    private readonly IPersistentState<SchedulerState> _schedulerState;
    private readonly ILogger _logger;
    private readonly IQueriesServiceClient _queriesServiceClient;

    public SchedulerConnectorGrain(
        [PersistentState(SCHEDULER)]IPersistentState<SchedulerState> schedulerState,
        ILogger logger,
        IQueriesServiceClient queriesServiceClient)
    {
        _schedulerState = schedulerState;
        _queriesServiceClient = queriesServiceClient;
        _logger = logger.ForContext<SchedulerConnectorGrain>();
    }
    
    public Task AddAgent(IAgent agent, int totalSlots)
    {
        _agentSlots[agent] = totalSlots;
        var agentName = agent.GetPrimaryKeyString();
        _schedulerState.State.Agents.Add(agentName);

        _allAgents[agentName] = agent;
        return Task.CompletedTask;
    }

    public Task RemoveAgent(IAgent agent)
    {
        _schedulerState.State.Agents.Remove(agent.GetPrimaryKeyString());
        
        var agentName = agent.GetPrimaryKeyString();
        _allAgents.Remove(agentName, out _);

        return Task.CompletedTask;
    }

    public Task<List<string>> GetAgents()
    {
        return Task.FromResult(_schedulerState.State.Agents.ToList());
    }

    public override async Task OnActivateAsync(CancellationToken cancellationToken)
    {
        await base.OnActivateAsync(cancellationToken);

        // _jobsQueueStreamSubscription = await JobsQueueStream.SubscribeAsync(queueItems =>
        // {
        //     foreach (var queueItem in queueItems)
        //     {
        //         Enqueue(queueItem.Item);
        //     }
        //     return Task.CompletedTask;
        // });
        // _jobCancelledSubscription = await JobCancelledStream.SubscribeAsync(jobIds =>
        // {
        //     foreach (var queueItem in jobIds)
        //     {
        //         _allQueueItems.Remove(queueItem.Item, out _);
        //     }
        //
        //     return Task.CompletedTask;
        // });
        // _schedulerState.State.StreamsInitialized = true;
        //
        // var initialQueue = await _queriesServiceClient.GetInitialQueue();
        // foreach (var queueItem in initialQueue)
        // {
        //     Enqueue(queueItem);
        // }
    }

    public override async Task OnDeactivateAsync(DeactivationReason reason, CancellationToken cancellationToken)
    {
        await _schedulerState.WriteStateAsync();
        await base.OnDeactivateAsync(reason, cancellationToken);
    }
    
    private void Enqueue(QueueItem queueItem)
    {
        var jobId = queueItem.JobId;
        var job = GrainFactory.GetGrain<IJob>(jobId);
        _allQueueItems[queueItem.JobId] = job;
        if (queueItem.AgentNames.Any())
        {
            foreach (var agentName in queueItem.AgentNames)
            {
                var agent = GrainFactory.GetGrain<IAgent>(agentName);
                var directQueue = queueItem.IsRetry
                    ? _directRetryQueue.GetOrAdd(agent, _ => new ConcurrentQueue<Guid>()) 
                    : _directMainQueue.GetOrAdd(agent, _ => new ConcurrentQueue<Guid>());
                directQueue.Enqueue(jobId);
            }
        }
        else
        {
            if (queueItem.IsRetry) _retryQueue.Enqueue(jobId);
            else _mainQueue.Enqueue(jobId);
        }
        _logger.Information("Job [{JobId}]: added to queue", queueItem.JobId);
    }

    public Task<List<IJob>> JobsRequest(IAgent agent, int count)
    {
        var jobs = new List<IJob>();
        var agentName = agent.GetPrimaryKeyString();
        if (_directRetryQueue.TryGetValue(agent, out var directQueue))
        {
            while (jobs.Count < count && directQueue.TryDequeue(out var jobId))
            {
                if (_allQueueItems.TryRemove(jobId, out var job))
                    jobs.Add(job);
            }
        }
        if (_directMainQueue.TryGetValue(agent, out directQueue))
        {
            while (jobs.Count < count && directQueue.TryDequeue(out var jobId))
            {
                if (_allQueueItems.TryRemove(jobId, out var job))
                    jobs.Add(job);
            }
        }

        while (jobs.Count < count && (_retryQueue.TryDequeue(out var jobId) || _mainQueue.TryDequeue(out jobId)))
        {
            if (_allQueueItems.TryRemove(jobId, out var job))
                jobs.Add(job);
        }
        
        _logger.Information("Found {Count} jobs to be launched on {AgentName}", jobs.Count, agentName);
        _agentSlots[agent] = count - jobs.Count;
        return Task.FromResult(jobs);
    }

    public Task<string> AgentRequest()
    {
        foreach (var agentSlot in _agentSlots)
        {
            if (agentSlot.Value > 0)
            {
                _agentSlots[agentSlot.Key] = agentSlot.Value - 1;
                return Task.FromResult(agentSlot.Key.GetPrimaryKeyString());
            }
        }

        return Task.FromResult(string.Empty);
    }

    public Task<ScheduleInfo> GetScheduleInfo()
    {
        var totalInQueue = _allQueueItems.Count;
        var totalInMainQueue = _mainQueue.Count;
        var totalInRetryMainQueue = _retryQueue.Count;
        var totalInDirectQueue = _directMainQueue.Sum(x => x.Value.Count);
        var totalInRetryDirectQueue = _directRetryQueue.Sum(x => x.Value.Count);

        var info = new ScheduleInfo(
            totalInQueue,
            totalInMainQueue,
            totalInRetryMainQueue,
            totalInDirectQueue,
            totalInRetryDirectQueue, _agentSlots.ToDictionary(x => x.Key.GetPrimaryKeyString(), x => x.Value));

        return Task.FromResult(info);
    }
}