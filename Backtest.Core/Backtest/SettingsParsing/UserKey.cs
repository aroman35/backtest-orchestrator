﻿namespace Backtest.Core.Backtest.SettingsParsing;

public class UserKey
{
    private UserKey(string key)
    {
        JsonParamInfo = new JsonParamInfo(key);
        UserParamName = key
            .Replace('[', '.')
            .Replace("]", "")
            .Split('.')
            .Last();
        FullName = key;
    }

    public UserKey(string key, string value) : this(key)
    {
        Value = value;
    }

    public string Value { get; }
    public JsonParamInfo JsonParamInfo { get; }
    public string UserParamName { get; }
    public string FullName { get; }
}