﻿using Newtonsoft.Json.Linq;

namespace Backtest.Core.Backtest.SettingsParsing;

public class UserKeyGroup
{
    public UserKeyGroup(JsonParamInfo jsonParamInfo, IEnumerable<UserKey> keys)
    {
        JsonParamInfo = jsonParamInfo;
        UserValues = keys.ToDictionary(x => x.UserParamName, x => x.Value);
    }

    public IDictionary<string, string> UserValues { get; }
    public JsonParamInfo JsonParamInfo { get; }

    private string ToKeyValueString() => string.Join(';', UserValues.Select(x => $"{x.Key}={x.Value}"));

    private string ToKeyValueString(string existingParams)
    {
        // Исходные параметры могут быть:
        // Key1=Value1;Key2=Value2
        // Key1=Value1;Key2=Value2;
        // Преобразуем их в словарик
        var paramsMap = existingParams
            .Split(';')
            .Where(x => !string.IsNullOrEmpty(x))
            .Select(x => x.Split('='))
            .ToDictionary(x => x[0], x => x[1]);
        
        // Существующие параметры обогащаем/перезаписываем пользовательскими
        foreach (var (userKey, userValue) in UserValues)
        {
            paramsMap[userKey] = userValue;
        }

        var paramsAsString = string.Join(';', paramsMap.Select(x => $"{x.Key}={x.Value}"));
        return paramsAsString;
        // return string.Join(';', existingParams, ToKeyValueString());
    }

    private JToken ToKeyValueString(JToken sourceObj)
    {
        var key = sourceObj.SelectToken(JsonParamInfo.ToString())
                  ?? throw new ArgumentException("Strategy settings does not contain user key", JsonParamInfo.ToString());
            
        if (key.Type == JTokenType.Object)
            return new JObject(UserValues.Select(x => GenerateProperty(x.Key, x.Value)));

        return string.IsNullOrEmpty((string)key)
            ? ToKeyValueString()
            : ToKeyValueString((string)key);
    }

    private JProperty GenerateProperty(string key, string stringValue)
    {
        if (int.TryParse(stringValue, out var intNum))
            return new JProperty(key, intNum);
        if (decimal.TryParse(stringValue, out var floatNum))
            return new JProperty(key, floatNum);
        if (bool.TryParse(stringValue, out var boolVal))
            return new JProperty(key, boolVal);
        return new JProperty(key, stringValue);
    }

    public override string ToString()
    {
        return ToKeyValueString();
    }

    public JObject GenerateJsonNode(JObject sourceObj)
    {
        var jKeys = JsonParamInfo.JsonPathSegments.Reverse().ToArray();

        JContainer lastProp = null;
        bool isLastArray = false;
        for (var i = 0; i < jKeys.Length; i++)
        {
            if (i == 0)
            {
                if (int.TryParse(jKeys[i], out var firstArrIdx))
                {
                    var array = new JArray();
                    for (var k = 0; k < firstArrIdx; k++)
                        array.Add(new JObject());
                    
                    array.Add(ToKeyValueString(sourceObj));
                    lastProp = array;
                    isLastArray = true;
                    continue;
                }
                lastProp = new JProperty(jKeys[i], ToKeyValueString(sourceObj));
                continue;
            }

            if (int.TryParse(jKeys[i], out var arrIdx))
            {
                var array = new JArray();
                for (var k = 0; k < arrIdx; k++)
                    array.Add(new JObject());
                if (lastProp is not null) array.Add(new JObject(lastProp));

                lastProp = new JProperty(jKeys[++i], array);
                continue;
            }

            if (lastProp is not null)
            {
                lastProp = isLastArray 
                    ? new JProperty(jKeys[i], lastProp)
                    : new JProperty(jKeys[i], new JObject(lastProp));
            }
        }

        var paramObj = lastProp is not null ? new JObject(lastProp) : new JObject();
        return paramObj;
    }
}