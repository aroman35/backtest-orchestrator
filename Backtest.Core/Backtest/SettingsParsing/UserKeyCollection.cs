﻿using Newtonsoft.Json.Linq;

namespace Backtest.Core.Backtest.SettingsParsing;

public class UserKeyCollection
{
    public UserKeyCollection(IEnumerable<UserKey> userKeys)
    {
        KeyGroups = userKeys.GroupBy(x => x.JsonParamInfo).Select(x => new UserKeyGroup(x.Key, x)).ToArray();
    }

    private ICollection<UserKeyGroup> KeyGroups { get; }

    public JObject MergeJson(JObject sourceObj)
    {
        var clonedInstance = (JObject)sourceObj.DeepClone();
        foreach (var userKeyGroup in KeyGroups.Select(x => x.GenerateJsonNode(sourceObj)))
        {
            clonedInstance.Merge(userKeyGroup, new JsonMergeSettings
            {
                MergeArrayHandling = MergeArrayHandling.Merge
            });
        }

        return clonedInstance;
    }

    public Dictionary<string, string> GetSourceParams()
    {
        return KeyGroups
            .Select(x => x.UserValues.ToDictionary(o => $"{x.JsonParamInfo.JsonParamName}.{o.Key}", o => o.Value))
            .SelectMany(x => x)
            .ToDictionary(x => x.Key, x => x.Value);
    }
}