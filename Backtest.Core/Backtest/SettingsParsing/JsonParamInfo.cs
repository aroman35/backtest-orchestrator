﻿namespace Backtest.Core.Backtest.SettingsParsing;

public class JsonParamInfo : IEquatable<JsonParamInfo>
{
    public JsonParamInfo(string key)
    {
        KeyFullPath = key
            .Replace('[', '.')
            .Replace("]", "")
            .Split('.')
            .ToLinkedList();
    }

    public LinkedList<string> KeyFullPath { get; }
    public string[] JsonPathSegments => KeyFullPath.SkipLast(1).ToArray();

    public string JsonParamName =>
        string.Join('.', KeyFullPath
            .SkipLast(1)
            .Aggregate((prev, next) => prev + (int.TryParse(next, out var idx) ? $"[{idx}]" : $".{next}")));

    public bool Equals(JsonParamInfo other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Equals(JsonParamName, other.JsonParamName);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != GetType()) return false;
        return Equals((JsonParamInfo)obj);
    }

    public override int GetHashCode()
    {
        return JsonParamName is not null ? JsonParamName.GetHashCode() : 0;
    }

    public override string ToString()
    {
        return JsonParamName;
    }
}