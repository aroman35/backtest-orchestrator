﻿namespace Backtest.Core.Backtest.SettingsParsing;

public static class EnumerableExtensions
{
    public static LinkedList<T> ToLinkedList<T>(this IEnumerable<T> source)
    {
        var result = new LinkedList<T>();
        using var enumerator = source.GetEnumerator();
        while (enumerator.MoveNext())
            result.AddLast(enumerator.Current);
        return result;
    }

    public static UserKeyCollection ToUserKeyCollection(this IEnumerable<UserKey> userKeys)
    {
        return new UserKeyCollection(userKeys);
    }
}