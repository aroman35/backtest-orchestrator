﻿using Backtest.Domain;
using Backtest.Domain.Backtest;
using Backtest.Domain.Events;
using Backtest.Domain.Simulation;
using Backtest.Domain.States;
using Backtest.Domain.States.Settings;
using Backtest.Core.Backtest.SettingsParsing;
using Backtest.Domain.Queries;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Orleans.Runtime;
using Orleans.Streams;
using Orleans.Utilities;
using ZstdSharp.Unsafe;
using static Backtest.Domain.Constants;
using ILogger = Serilog.ILogger;
using TaskStatus = Backtest.Domain.TaskStatus;

namespace Backtest.Core.Backtest;

public class BacktestGrain : Grain, IBacktest
{
    private readonly IPersistentState<BacktestState> _backtestState;
    private readonly IPersistentState<BacktestSettings> _backtestSettings;
    private readonly IPersistentState<StrategySettings> _strategySettings;
    private readonly IPersistentState<UserParams> _userParams;
    private readonly IPersistentState<RuntimeSummaryMap> _runtimeSummaryMap;
    private readonly ObserverManager<IStateUpdatesObserver> _updatesObserverManager;
    private readonly IQueriesServiceClient _queriesServiceClient;
    private ILogger _logger;
    
    private ISimulation[] _simulations;
    
    private IStreamProvider StreamProvider => this.GetStreamProvider(STREAM_PROVIDER_NAME);
    private IAsyncStream<ItemSummary> UpdatesStream => StreamProvider.GetStream<ItemSummary>(StreamId.Create(STATUS_UPDATES_STREAM, this.GetPrimaryKey()));
    private IAsyncStream<JobRestartedEvent> JobRestartedStream => StreamProvider.GetStream<JobRestartedEvent>(StreamId.Create(JOB_RETRY_STREAM, this.GetPrimaryKey()));
    private IAsyncStream<JobFinishedEvent> JobFinishedStream => StreamProvider.GetStream<JobFinishedEvent>(StreamId.Create(JOB_FINISHED_STREAM, this.GetPrimaryKey()));
    private IAsyncStream<bool> CancellationStream => StreamProvider.GetStream<bool>(StreamId.Create(BACKTEST_CANCELLATION_STREAM, this.GetPrimaryKey()));
    
    private StreamSubscriptionHandle<ItemSummary> _progressSubscriptionHandle;
    private StreamSubscriptionHandle<JobFinishedEvent> _jobFinishedStreamHandle;
    private StreamSubscriptionHandle<JobRestartedEvent> _jobRestartedStreamHandle;

    private GrainCancellationTokenSource _backtestCancellationTokenSource = new();
    
    public BacktestGrain(
        [PersistentState(BACKTEST)] IPersistentState<BacktestState> backtestState,
        [PersistentState(BACKTEST_SETTINGS)] IPersistentState<BacktestSettings> backtestSettings,
        [PersistentState(STRATEGY_SETTINGS)] IPersistentState<StrategySettings> strategySettings,
        [PersistentState(USER_PARAMS)] IPersistentState<UserParams> userParams,
        [PersistentState(BACKTEST_RUNTIME_SUMMARY_MAP, REDIS_STORAGE_DB0)] IPersistentState<RuntimeSummaryMap> runtimeSummaryMap,
        ILoggerFactory loggerFactory,
        ILogger logger,
        IQueriesServiceClient queriesServiceClient)
    {
        _logger = logger.ForContext<BacktestGrain>();
        _userParams = userParams;
        _runtimeSummaryMap = runtimeSummaryMap;
        _queriesServiceClient = queriesServiceClient;
        _backtestSettings = backtestSettings;
        _strategySettings = strategySettings;
        _backtestState = backtestState;
        _updatesObserverManager = new ObserverManager<IStateUpdatesObserver>(TimeSpan.FromHours(24), loggerFactory.CreateLogger<IStateUpdatesObserver>());
    }
    
    public async Task Create(CreateBacktestCommand createCommand)
    {
        _backtestSettings.State.SettingsSource = createCommand.BacktestSettings;
        _strategySettings.State.SettingsSource = createCommand.StrategySettings;
        _userParams.State.ParamsMap = createCommand.UserParams;

        _backtestState.State.Name = createCommand.Name;
        _backtestState.State.Labels = createCommand.Labels;
        _backtestState.State.MdStart = createCommand.MdStart;
        _backtestState.State.MdEnd = createCommand.MdEnd;
        _backtestState.State.MdDates = createCommand.MdDates;
        _backtestState.State.SplitDays = createCommand.SplitDays;
        _backtestState.State.BinaryName = createCommand.BinaryName;
        _backtestState.State.DesiredAgents = createCommand.DesiredAgentIds;
        _backtestState.State.EnableLogs = createCommand.EnableLogs;
        _backtestState.State.EnableTelemetry = createCommand.EnableTelemetry;
        _backtestState.State.RetryCount = createCommand.RetryCount;
        _backtestState.State.ReportingTimeoutSec = createCommand.ReportingTimeoutSec;
        _backtestState.State.Created = DateTime.UtcNow;
        _backtestState.State.Summary.Status = TaskStatus.Queued;
        _backtestState.State.UserCreated = createCommand.User;
        _backtestState.State.Namespace = createCommand.Namespace;

        await GenerateSimulations();
        
        await _backtestState.WriteStateAsync();
        await _backtestSettings.WriteStateAsync();
        await _strategySettings.WriteStateAsync();
        await _userParams.WriteStateAsync();
        _logger.Information("Backtest [{BacktestId}]: created", this.GetPrimaryKey());
    }

    public async Task Create(CreateBacktestWithPredefinedSettingsCommand createCommand)
    {
        _backtestSettings.State.SettingsSource = createCommand.BacktestSettings;

        _backtestState.State.Name = createCommand.Name;
        _backtestState.State.Labels = createCommand.Labels;
        _backtestState.State.MdStart = createCommand.MdStart;
        _backtestState.State.MdEnd = createCommand.MdEnd;
        _backtestState.State.MdDates = createCommand.MdDates;
        _backtestState.State.SplitDays = createCommand.SplitDays;
        _backtestState.State.BinaryName = createCommand.BinaryName;
        _backtestState.State.DesiredAgents = createCommand.DesiredAgentIds;
        _backtestState.State.EnableLogs = createCommand.EnableLogs;
        _backtestState.State.EnableTelemetry = createCommand.EnableTelemetry;
        _backtestState.State.RetryCount = createCommand.RetryCount;
        _backtestState.State.ReportingTimeoutSec = createCommand.ReportingTimeoutSec;
        _backtestState.State.UserCreated = createCommand.User;
        _backtestState.State.Namespace = createCommand.Namespace;
        _backtestState.State.Created = DateTime.UtcNow;
        _backtestState.State.Summary.Status = TaskStatus.Queued;
        _backtestState.State.Labels.Add("multiple-settings");

        var jobsGenerationOptions = new JobsGenerationOptions(
            GetMdDates(),
            _backtestState.State.SplitDays,
            _backtestState.State.DesiredAgents,
            _backtestState.State.ReportingTimeoutSec,
            _backtestState.State.RetryCount);

        _simulations = new ISimulation[createCommand.StrategySettings.Length];
        for (var i = 0; i < createCommand.StrategySettings.Length; i++)
        {
            var settings = createCommand.StrategySettings[i];
            var simulationCommand = new CreateSimulationCommand
            {
                Name = $"simulation[{i:0000}]",
                StrategySettings = settings,
                AppliedUserParams = new Dictionary<string, string>()
            };
            
            await CreateSimulation(simulationCommand, jobsGenerationOptions, i);
        }
        
        await _backtestState.WriteStateAsync();
        await _backtestSettings.WriteStateAsync();
        await _strategySettings.WriteStateAsync();
        await _userParams.WriteStateAsync();
        _logger.Information("Backtest [{BacktestId}]: created", this.GetPrimaryKey());
    }

    public Task<BacktestInfo> GetBacktestInfo()
    {
        var backtestInfo = new BacktestInfo(
            _backtestState.State.BinaryName,
            _backtestSettings.State.SettingsSource,
            _backtestState.State.EnableLogs,
            _backtestState.State.EnableTelemetry);

        return Task.FromResult(backtestInfo);
    }

    public Task<string> BinaryName()
    {
        return Task.FromResult(_backtestState.State.BinaryName);
    }

    public async Task Cancel(bool force)
    {
        // await CancellationStream.OnNextAsync(force);
        await _backtestCancellationTokenSource.Cancel();
        _logger.Information("Backtest [{BacktestId}]: canceled", this.GetPrimaryKey());
    }

    public Task<ISimulation[]> GetSimulations()
    {
        return Task.FromResult(_simulations);
    }

    public Task<BacktestSettings> GetBacktestSettings()
    {
        return Task.FromResult(_backtestSettings.State);
    }

    public Task<StrategySettings> GetStrategySettings()
    {
        return Task.FromResult(_strategySettings.State);
    }

    public Task SubscribeStateUpdates(IStateUpdatesObserver observer)
    {
        _updatesObserverManager.Subscribe(observer, observer);
        return Task.CompletedTask;
    }

    public Task UnsubscribeStateUpdates(IStateUpdatesObserver observer)
    {
        _updatesObserverManager.Unsubscribe(observer);
        return Task.CompletedTask;
    }

    public override async Task OnActivateAsync(CancellationToken cancellationToken)
    {
        await base.OnActivateAsync(cancellationToken);

        _logger = _logger.WithBacktestId(this);
        await CreateUpdatesStreams();
        _simulations = await _queriesServiceClient.GetSimulationsByBacktest(this);
        _logger.Information("Backtest [{BacktestId}]: activated", this.GetPrimaryKey());
    }

    public override async Task OnDeactivateAsync(DeactivationReason reason, CancellationToken cancellationToken)
    {
        await _runtimeSummaryMap.WriteStateAsync();
        await _backtestState.WriteStateAsync();
        _logger.Information("Backtest [{BacktestId}]: deactivated - {Reason}", this.GetPrimaryKey(), reason.Description);
        await base.OnDeactivateAsync(reason, cancellationToken);
    }

    private async Task GenerateSimulations()
    {
        var userParams = _userParams.State.ParamsMap;
        var parsedSettings = JObject.Parse(_strategySettings.State.SettingsSource);
        var jobsGenerationOptions = new JobsGenerationOptions(
            GetMdDates(),
            _backtestState.State.SplitDays,
            _backtestState.State.DesiredAgents,
            _backtestState.State.ReportingTimeoutSec,
            _backtestState.State.RetryCount);
        
        if (userParams.Any())
        {
            var keysCollection = userParams
                .Select(x => x.Value)
                .Aggregate((acc, next) => acc.Join(next, _ => true, _ => true, (l, r) => string.Join(';', l, r)).ToArray())
                .Select(x => x
                    .Split(';')
                    .Select((value, idx) => new UserKey(userParams.Keys.ToArray()[idx], value))
                    .ToUserKeyCollection());
                
            var strategySettingsCollection = keysCollection
                .Select((x, idx) => new CreateSimulationCommand
                {
                    Name = $"simulation[{idx:0000}]",
                    StrategySettings = x.MergeJson(parsedSettings).ToString(Formatting.Indented),
                    AppliedUserParams = x.GetSourceParams()
                })
                .ToArray();

            _simulations = new ISimulation[strategySettingsCollection.Length];

            for (var i = 0; i < strategySettingsCollection.Length; i++)
            {
                await CreateSimulation(strategySettingsCollection[i], jobsGenerationOptions, i);
            }
        }
        else
        {
            _simulations = new ISimulation[1];
            var settings = new CreateSimulationCommand
            {
                Name = "simulation",
                StrategySettings = _strategySettings.State.SettingsSource
            };
            await CreateSimulation(settings, jobsGenerationOptions, 0);
        }
        _logger.Information("Backtest [{BacktestId}]: created with {SimulationsCount} simulations and {JobsCount} jobs", this.GetPrimaryKey(), _simulations.Length, _backtestState.State.Summary.TotalJobs);
    }

    private async Task CreateSimulation(CreateSimulationCommand command, JobsGenerationOptions jobsGenerationOptions, int idx)
    {
        var simulationId = Guid.NewGuid();
        var simulation = GrainFactory.GetGrain<ISimulation>(simulationId);
        await simulation.Create(this, command);
        var jobsCount = await simulation.GenerateJobs(jobsGenerationOptions, _backtestCancellationTokenSource.Token);
        
        _backtestState.State.Summary.TotalJobs += jobsCount;
        _runtimeSummaryMap.State.Append(simulationId);
        
        _logger.Debug("Simulation {SimulationId} created", simulationId);
        _simulations[idx] = simulation;
    }

    private DateTime[] GetMdDates()
    {
        if (_backtestState.State.MdStart.HasValue && _backtestState.State.MdEnd.HasValue)
            return Enumerable.Range(0, (int)(_backtestState.State.MdEnd.Value - _backtestState.State.MdStart.Value).TotalDays)
                .Select(x => _backtestState.State.MdStart.Value.Date.AddDays(x))
                .ToArray();

        if (_backtestState.State.MdDates is { Length: > 0 })
            return _backtestState.State.MdDates;

        throw new ApplicationException("strategy dates are invalid");
    }

    private async Task CreateUpdatesStreams()
    {
        _progressSubscriptionHandle = await UpdatesStream.SubscribeAsync(ReceiveUpdate);
        _jobFinishedStreamHandle = await JobFinishedStream.SubscribeAsync(ReceiveJobFinishedUpdates);
        _jobRestartedStreamHandle = await JobRestartedStream.SubscribeAsync(ReceiveJobRestartedUpdates);

        _logger.Information("Backtest [{BacktestId}]: updates stream created", this.GetPrimaryKey());
    }
    
    private async Task UnsubscribeUpdatesStreams()
    {
        await _progressSubscriptionHandle.UnsubscribeAsync();
        await _jobFinishedStreamHandle.UnsubscribeAsync();
        await _jobRestartedStreamHandle.UnsubscribeAsync();
        _logger.Information("Backtest [{BacktestId}]: unsubscribed from updates streams", this.GetPrimaryKey());
    }

    private async Task ReceiveUpdate(IEnumerable<SequentialItem<ItemSummary>> updateCommands)
    {
        var updates = updateCommands
            .GroupBy(x => x.Item.ItemId)
            .ToDictionary(
                x => x.Key,
                x => x.MaxBy(summary => summary.Token.SequenceNumber).Item);
        _runtimeSummaryMap.State.Update(updates);
        _backtestState.State.Summary.Progress = _runtimeSummaryMap.State.TotalProgress();
        _backtestState.State.Summary.Status = _runtimeSummaryMap.State.TotalStatus();
        var currentSummary = _backtestState.State.Summary.GetSummary(this.GetPrimaryKey());
        await _updatesObserverManager.Notify(observer => observer.StateUpdate(currentSummary));
            
        if (_backtestState.State.Summary.IsFinalStatus)
            await _backtestState.WriteStateAsync();
        _logger.Information(
            "Backtest [{BacktestId}]: received update. Status: {Status}\tProgress: {Progress}",
            this.GetPrimaryKey(),
            _backtestState.State.Summary.Status.ToString(),
            _backtestState.State.Summary.Progress
        );
    }

    private async Task ReceiveJobFinishedUpdates(IEnumerable<SequentialItem<JobFinishedEvent>> commands)
    {
        foreach (var command in commands)
        {
            _backtestState.State.Summary.JobFinished(command.Item);
        }
        await _updatesObserverManager.Notify(observer => observer.StateUpdate(_backtestState.State.Summary.GetSummary(this.GetPrimaryKey())));
        _logger.Information("Backtest [{BacktestId}]: received job finished event", this.GetPrimaryKey());
    }
    
    private async Task ReceiveJobRestartedUpdates(IEnumerable<SequentialItem<JobRestartedEvent>> commands)
    {
        foreach (var command in commands)
        {
            _backtestState.State.Summary.JobRestarted(command.Item);
        }
        await _updatesObserverManager.Notify(observer => observer.StateUpdate(_backtestState.State.Summary.GetSummary(this.GetPrimaryKey())));
        _logger.Information("Backtest [{BacktestId}]: job was restarted", this.GetPrimaryKey());
    }
    
    public Task<BacktestState> GetState()
    {
        return Task.FromResult(_backtestState.State);
    }
}
