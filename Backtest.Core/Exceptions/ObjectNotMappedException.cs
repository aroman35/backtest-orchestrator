﻿namespace Backtest.Core.Exceptions;

[GenerateSerializer]
public class ObjectNotMappedException<TSource, TDestination> : ApplicationException
{
    public ObjectNotMappedException() : base($"{typeof(TDestination).Name} is missing for the {typeof(TSource).Name}")
    {
    }
}