﻿using Backtest.Domain.Backtest;
using Backtest.Domain.Job;
using Backtest.Domain.Simulation;
using Serilog;

namespace Backtest.Core;

public static class Extensions
{
    public static ILogger WithBacktestId(this ILogger logger, IBacktest backtest)
    {
        return logger.ForContext("BacktestId", backtest.GetPrimaryKey());
    }

    public static ILogger WithSimulationId(this ILogger logger, ISimulation simulation)
    {
        return logger.ForContext("SimulationId", simulation.GetPrimaryKey());
    }

    public static ILogger WithJobId(this ILogger logger, IJob job)
    {
        return logger.ForContext("JobId", job);
    }

    public static ILogger WithAgentName(this ILogger logger, string agentName)
    {
        return logger
            .ForContext("AgentName", agentName);
    }

    public static TValue GetOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue addValue)
    {
        if (dictionary.TryGetValue(key, out var value))
            return value;
        dictionary.Add(key, addValue);
        return value;
    }
}