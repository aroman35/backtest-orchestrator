﻿using Backtest.Domain;
using Backtest.Domain.Agent;
using Backtest.Domain.Backtest;
using Backtest.Domain.Events;
using Backtest.Domain.Job;
using Backtest.Domain.MarketDataCache;
using Backtest.Domain.Models;
using Backtest.Domain.Scheduler;
using Backtest.Domain.Simulation;
using Backtest.Domain.States;
using Backtest.Domain.States.Relations;
using Backtest.Core.Exceptions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Orleans.Runtime;
using Orleans.Streams;
using Orleans.Utilities;
using static Backtest.Domain.Constants;
using ILogger = Serilog.ILogger;
using TaskStatus = Backtest.Domain.TaskStatus;

namespace Backtest.Core.Job;

public class JobGrain : Grain, IJob, IRemindable
{
    private readonly IPersistentState<JobState> _jobState;
    private readonly IPersistentState<JobResult> _jobResult;
    private readonly IPersistentState<CacheMap> _cacheMap;
    private readonly ObserverManager<IJobObserver> _agentObserverManager;
    private readonly IOptions<JobExecutionOptions> _jobOptions;
    private readonly ObserverManager<IStateUpdatesObserver> _jobUpdatesObservers;
    private readonly ObserverManager<IJobResultObserver> _jobResultObservers;

    private ILogger _logger;
    private IBacktest _backtest;
    private ISimulation _simulation;
    private IAgent _agent;
    // private IGrainReminder _jobTimeoutReminder;
    private IMarketDataCache _buildingCache;

    #region Streams
    private IStreamProvider StreamProvider => this.GetStreamProvider(STREAM_PROVIDER_NAME);
    private IAsyncStream<double> JobProgressStream => StreamProvider.GetStream<double>(StreamId.Create(STATUS_UPDATES_STREAM, this.GetPrimaryKey()));
    private IAsyncStream<bool> CancellationStream => StreamProvider.GetStream<bool>(StreamId.Create(BACKTEST_CANCELLATION_STREAM, _backtest.GetPrimaryKey()));
    
    private IAsyncStream<ItemSummary> SimulationUpdatesStream => StreamProvider.GetStream<ItemSummary>(StreamId.Create(STATUS_UPDATES_STREAM, _simulation.GetPrimaryKey()));
    private IAsyncStream<JobFinishedEvent> JobFinishedBacktestStream => StreamProvider.GetStream<JobFinishedEvent>(StreamId.Create(JOB_FINISHED_STREAM, _backtest.GetPrimaryKey()));
    private IAsyncStream<JobFinishedEvent> JobFinishedSimulationStream => StreamProvider.GetStream<JobFinishedEvent>(StreamId.Create(JOB_FINISHED_STREAM, _simulation.GetPrimaryKey()));
    private IAsyncStream<JobRestartedEvent> JobRestartedBacktestStream => StreamProvider.GetStream<JobRestartedEvent>(StreamId.Create(JOB_RETRY_STREAM, _backtest.GetPrimaryKey()));
    private IAsyncStream<JobRestartedEvent> JobRestartedSimulationStream => StreamProvider.GetStream<JobRestartedEvent>(StreamId.Create(JOB_RETRY_STREAM, _simulation.GetPrimaryKey()));
    private IAsyncStream<QueueItem> JobsQueueStream => StreamProvider.GetStream<QueueItem>(StreamId.Create(JOBS_QUEUE_STREAM, Guid.Empty));
    private IAsyncStream<Guid> LaunchConfirmedStream => StreamProvider.GetStream<Guid>(StreamId.Create(LAUNCH_CONFIRMED_STREAM, Guid.Empty));
    private IAsyncStream<Guid> JobReleasedStream => StreamProvider.GetStream<Guid>(StreamId.Create(JOB_RELEASED_STREAM, Guid.Empty));
    private IAsyncStream<Guid> JobCancelledStream => StreamProvider.GetStream<Guid>(StreamId.Create(JOB_CANCELLED_STREAM, Guid.Empty));

    private StreamSubscriptionHandle<double> _progressSubscriptionHandle;
    private StreamSubscriptionHandle<bool> _cancellationSubscriptionHandle;
    #endregion

    public JobGrain(
        [PersistentState(JOB)]IPersistentState<JobState> jobState,
        [PersistentState(JOB_RESULT)]IPersistentState<JobResult> jobResult,
        [PersistentState(JOB_CACHE_MAP, REDIS_STORAGE_DB0)]IPersistentState<CacheMap> cacheMap,
        ILogger logger,
        ILoggerFactory loggerFactory,
        IOptions<JobExecutionOptions> jobOptions)
    {
        _logger = logger.ForContext<JobGrain>();
        _jobState = jobState;
        _jobResult = jobResult;
        _jobOptions = jobOptions;
        _cacheMap = cacheMap;
        _agentObserverManager = new ObserverManager<IJobObserver>(
            TimeSpan.FromSeconds(_jobOptions.Value.ObserverTtlSec),
            loggerFactory.CreateLogger<ObserverManager<IJobObserver>>());
        
        _jobUpdatesObservers = new ObserverManager<IStateUpdatesObserver>(TimeSpan.FromHours(24), loggerFactory.CreateLogger<ObserverManager<IStateUpdatesObserver>>());
        _jobResultObservers = new ObserverManager<IJobResultObserver>(TimeSpan.FromHours(24), loggerFactory.CreateLogger<ObserverManager<IJobResultObserver>>());
    }

    public async Task Create(IBacktest backtest, ISimulation simulation, CreateJobCommand createCommand, GrainCancellationToken cancellationToken)
    {
        MapBacktest(backtest);
        MapSimulation(simulation);
        _jobState.State.Initialize(createCommand);

        _logger.Debug("Job created");
        await JobsQueueStream.OnNextAsync(await CreateQueueItem());
        _logger.Debug("Job was sent to queue");
    }

    public async Task<TaskInfo> GetStartInfo()
    {
        AssertAgentIsMapped();
        await CatchUpdate();
        
        var backtestInfo = await _backtest.GetBacktestInfo();
        var strategySettingsSource = await _simulation.GetStrategySettings();
        var strategySettings = _jobState.State.PopulateStrategySettings(strategySettingsSource.SettingsSource);

        var agentInfo = await _agent.GetAgentInfo();

        var startInfo = new TaskInfo(
            backtestInfo.BinaryName,
            backtestInfo.BacktestSettings,
            strategySettings,
            backtestInfo.EnableLogs,
            backtestInfo.EnableTelemetry,
            Path.Combine(agentInfo.LogsPath, $"{this.GetPrimaryKey()}.log"),
            Path.Combine(agentInfo.TelemetryPath, $"{this.GetPrimaryKey()}.dat")
        );
        return startInfo;
    }

    public Task<double> Subscribe(IJobObserver jobObserver)
    {
        _agentObserverManager.Unsubscribe(jobObserver);
        _agentObserverManager.Subscribe(jobObserver, jobObserver);
        return Task.FromResult(_jobOptions.Value.ObserverTtlSec);
    }

    public Task SubscribeStateUpdates(IStateUpdatesObserver jobUpdatesObserver)
    {
        _jobUpdatesObservers.Subscribe(jobUpdatesObserver, jobUpdatesObserver);
        return Task.CompletedTask;
    }

    public Task UnsubscribeStateUpdates(IStateUpdatesObserver jobUpdatesObserver)
    {
        _jobUpdatesObservers.Unsubscribe(jobUpdatesObserver);
        return Task.CompletedTask;
    }

    /// <summary>
    /// Confirm that this job was successfully sent to agent
    /// </summary>
    /// <param name="agent">An agent grain which nad taken this job to work</param>
    public async Task SentForExecution(IAgent agent)
    {
        if (_jobState.State.Status is not TaskStatus.Queued)
            return;
        _agent = agent;
        _jobState.State.SentForExecution(_agent.GetPrimaryKeyString());
        await ChangeStatus(TaskStatus.Running);
        var binaryName = await _backtest.BinaryName();
        await InitializeStreams();
        if (!await agent.SendJobToLaunch(this, binaryName))
        {
            _jobState.State.RevokeExecution();
            await ChangeStatus(TaskStatus.Queued);
            await JobsQueueStream.OnNextAsync(await CreateQueueItem());
            _logger.Warning("Job [{JobId}]: job was rescheduled after unsuccessful launch", this.GetPrimaryKey());
            return;
        }
        await CatchUpdate();
        _logger.Information("Job [{JobId}]: was sent to {AgentName}", this.GetPrimaryKey(), agent.GetPrimaryKeyString());
    }

    public async Task Launched(int core)
    {
        AssertBacktestIsMapped();
        AssertSimulationIsMapped();
        
        _jobState.State.Core = core;
        _jobState.State.Started = DateTime.UtcNow;
        
        await CatchUpdate();
        await LaunchConfirmedStream.OnNextAsync(this.GetPrimaryKey());
        _logger.Information("Job [{JobId}]: was launched", this.GetPrimaryKey());
    }

    public async Task JobFinished(JobFinishedCommand command)
    {
        try
        {
            if (command.IsCanceled)
                return;
            
            var reminder = await this.GetReminder(JOB_LIFETIME_REMINDER_NAME);
            if (reminder is not null)
                await this.UnregisterReminder(reminder);
            
            AssertAgentIsMapped();

            var isMdNotFoundError = command.Message.Contains("Backtest client(strategy) must have at least one existing marketdata file");
            _jobResult.State.FillResult(
                !isMdNotFoundError,
                command.Success,
                command.Message,
                command.MdEntryCount,
                command.TestDuration,
                _jobState.State.Started.HasValue ? (DateTime.UtcNow - _jobState.State.Started.Value).TotalSeconds : 0,
                _simulation.GetPrimaryKey(),
                _jobState.State.MdStart,
                _jobState.State.MdEnd
            );

            _jobState.State.LastUpdate = DateTime.UtcNow;
            _jobState.State.Progress = 100;
            if (!command.Success && _jobState.State.CurrentTry < _jobState.State.MaximumTries)
            {
                await _jobResult.ClearStateAsync();
                _logger.Warning("Job [{JobId}]: attempt {Try}/{TotalTries} to reschedule job as it was finished with error: {Error}", this.GetPrimaryKey(), _jobState.State.CurrentTry, _jobState.State.MaximumTries, command.Message);
                
                await JobRestartedBacktestStream.OnNextAsync(new JobRestartedEvent(TaskStatus.Failed));
                await JobRestartedSimulationStream.OnNextAsync(new JobRestartedEvent(TaskStatus.Failed));
                
                _jobState.State.Retry();
                await JobsQueueStream.OnNextAsync(new QueueItem
                {
                    AgentNames = _jobState.State.DesiredAgents,
                    Created = DateTime.UtcNow,
                    IsRetry = true,
                    JobId = this.GetPrimaryKey()
                });
            }
            else
            {
                await ChangeStatus(_jobResult.State.FinalState());
            }

            await JobReleasedStream.OnNextAsync(this.GetPrimaryKey());
            await ReportUpdate();
            await JobFinishedSimulationStream.OnNextAsync(new JobFinishedEvent(command.Success, _jobResult.State.Speed));
            await JobFinishedBacktestStream.OnNextAsync(new JobFinishedEvent(command.Success, _jobResult.State.Speed));
            await _agent.JobFinished(this);

            await Task.WhenAll(_cacheMap.State.Cache.Values.Select(x => GrainFactory.GetGrain<IMarketDataCache>(x).Free(this)));

            _logger.Information("Job [{JobId}]: job finished", this.GetPrimaryKey());
        }
        catch (Exception exception)
        {
            _logger.Error(exception, "Job [{JobId}]: Error applying result", this.GetPrimaryKey());
            throw;
        }
    }

    public Task MapCache(string hash, IMarketDataCache marketDataCache)
    {
        if (_cacheMap.State.Cache.TryAdd(hash, marketDataCache.GetPrimaryKey()))
        {
            _logger.Information("Job [{JobId}]: mapped cache {CacheHash}", this.GetPrimaryKey(), hash);
        }

        return Task.CompletedTask;
    }

    public async Task<bool> TryBuildCache(IMarketDataCache marketDataCache)
    {
        if (_jobState.State.BuildingCache is not null)
            return false;
        
        _buildingCache = marketDataCache;
        _jobState.State.BuildingCache = _buildingCache.GetPrimaryKey();
        await CatchUpdate();
        return true;
    }

    public async Task CacheBuildProgress(double progress)
    {
        AssertBuildingCacheIsMapped();
        await CatchUpdate();
        await _buildingCache.ReportProgress(progress);
        _logger.Information("Job [{JobId}]: cache build progress {Progress}", this.GetPrimaryKey(), progress);
    }

    public async Task CacheBuildComplete(CacheBuildCompleteResult result)
    {
        AssertBuildingCacheIsMapped();
        _jobState.State.BuildingCache = null;
        await CatchUpdate();
        await _buildingCache.CacheBuildComplete(result);
        _buildingCache = null;
        _logger.Information("Job [{JobId}]: cache build finished", this.GetPrimaryKey());
    }

    public Task<JobResult> GetResult()
    {
        return Task.FromResult(_jobResult.State);
    }

    public Task<QueueItem> CreateQueueItem()
    {
        var queueItem = new QueueItem
        {
            AgentNames = _jobState.State.DesiredAgents,
            Created = _jobState.State.Created,
            IsRetry = false,
            JobId = this.GetPrimaryKey()
        };
        return Task.FromResult(queueItem);
    }

    async Task IRemindable.ReceiveReminder(string reminderName, TickStatus status)
    {
        switch (reminderName)
        {
            case JOB_LIFETIME_REMINDER_NAME:
                var notRespondingTime = DateTime.UtcNow - _jobState.State.LastUpdate;
                if (_jobState.State.Status is TaskStatus.Running && notRespondingTime > TimeSpan.FromSeconds(_jobState.State.ReportingIntervalSec))
                    await JobFinished(new JobFinishedCommand(false, $"Job was not responding during {notRespondingTime.TotalSeconds} sec", 0, 0, false));
                break;
        }
    }
    
    public override async Task OnActivateAsync(CancellationToken cancellationToken)
    {
        await base.OnActivateAsync(cancellationToken);

        _logger = _logger.WithJobId(this);

        if (_jobState.State.BacktestId is not null)
            MapBacktest(GrainFactory.GetGrain<IBacktest>(_jobState.State.BacktestId.Value));
        if (_jobState.State.SimulationId is not null)
            MapSimulation(GrainFactory.GetGrain<ISimulation>(_jobState.State.SimulationId.Value));
        if (!string.IsNullOrEmpty(_jobState.State.AgentName))
            _agent = GrainFactory.GetGrain<IAgent>(_jobState.State.AgentName);
        if (_jobState.State.BuildingCache.HasValue)
            _buildingCache = GrainFactory.GetGrain<IMarketDataCache>(_jobState.State.BuildingCache.Value);
        if (_jobState.State.Status is TaskStatus.Running)
            await InitializeStreams();
    }

    public override async Task OnDeactivateAsync(DeactivationReason reason, CancellationToken cancellationToken)
    {
        await _cacheMap.WriteStateAsync();
        await _jobState.WriteStateAsync();
        await _jobResult.WriteStateAsync();
        _logger.Information("Job [{JobId}]: deactivated - {Reason}", this.GetPrimaryKey(), reason.Description);
        await base.OnDeactivateAsync(reason, cancellationToken);
    }

    private async Task Cancel(bool force)
    {
        await JobCancelledStream.OnNextAsync(this.GetPrimaryKey());
        if (_jobState.State.Status is TaskStatus.Queued or TaskStatus.Running)
        {
            if (_jobState.State.Status is TaskStatus.Running)
            {
                await _agentObserverManager.Notify(agent => agent.Cancel(force));
                var reminder = await this.GetReminder(JOB_LIFETIME_REMINDER_NAME);
                if (reminder is not null)
                    await this.UnregisterReminder(reminder);

                AssertAgentIsMapped();
                await _agent.JobFinished(this);
                await Task.WhenAll(_cacheMap.State.Cache.Values.Select(x => GrainFactory.GetGrain<IMarketDataCache>(x).Free(this)));
            }
            _jobResult.State.FillResult(true, false, "Job was cancelled", 0, 0, 0, _simulation.GetPrimaryKey(), _jobState.State.MdStart, _jobState.State.MdEnd);
            _jobState.State.LastUpdate = DateTime.UtcNow;
            await ChangeStatus(TaskStatus.Canceled);

            _logger.Information("Job [{JobId}]: job canceled", this.GetPrimaryKey());
        }
    }

    private void MapBacktest(IBacktest backtest)
    {
        _backtest = backtest;
        _jobState.State.BacktestId = _backtest.GetPrimaryKey();
        _logger = _logger.WithBacktestId(_backtest);
        _logger.Debug("Backtest added");
    }

    private void MapSimulation(ISimulation simulation)
    {
        _simulation = simulation;
        _jobState.State.SimulationId = _simulation.GetPrimaryKey();
        _logger = _logger.WithSimulationId(_simulation);
        _logger.Debug("Simulation added");
    }

    private async Task CatchUpdate()
    {
        var reminderDelay = TimeSpan.FromSeconds(_jobState.State.ReportingIntervalSec);
        await this.RegisterOrUpdateReminder(JOB_LIFETIME_REMINDER_NAME, reminderDelay, reminderDelay);
        
        _jobState.State.LastUpdate = DateTime.UtcNow;
        await ReportUpdate();
    }

    private async Task TestProgress(double progress)
    {
        if (!_jobState.State.IsFinalStatus)
        {
            _jobState.State.UpdateProgress(progress);
            await CatchUpdate();
        }
    }

    private async Task InitializeStreams()
    {
        AssertBacktestIsMapped();
        AssertSimulationIsMapped();

        _progressSubscriptionHandle = await JobProgressStream.SubscribeAsync((progress, _) => TestProgress(progress));
        _cancellationSubscriptionHandle = await CancellationStream.SubscribeAsync((force, _) => Cancel(force));
        
        _logger.Information("Job [{JobId}]: streams created", this.GetPrimaryKey());
    }
    
    private void AssertBuildingCacheIsMapped()
    {
        if (_buildingCache is null)
            throw new ObjectNotMappedException<IJob,IMarketDataCache>();
    }

    private void AssertAgentIsMapped()
    {
        if (_agent is null)
            throw new ObjectNotMappedException<IJob, IAgent>();
    }

    private void AssertBacktestIsMapped()
    {
        if (_jobState.State.BacktestId is null)
            throw new ObjectNotMappedException<IJob, IBacktest>();
    }
    
    private void AssertSimulationIsMapped()
    {
        if (_jobState.State.SimulationId is null)
            throw new ObjectNotMappedException<IJob, ISimulation>();
    }

    private Task ChangeStatus(TaskStatus status)
    {
        var oldStatus = _jobState.State.Status;
        _jobState.State.ChangeStatus(status);
        _logger.Information("Job [{JobId}]: status changed: {OldStatus} -> {NewStatus}", this.GetPrimaryKey(), oldStatus, _jobState.State.Status);
        return Task.CompletedTask;
    }

    private async Task ReportUpdate()
    {
        var update = _jobState.State.GetSummary(this.GetPrimaryKey());
        await SimulationUpdatesStream.OnNextAsync(update);
        await _jobUpdatesObservers.Notify(x => x.StateUpdate(update));
    }

    public Task<JobState> GetState()
    {
        return Task.FromResult(_jobState.State);
    }

    private async Task SetJobCancellation(GrainCancellationToken cancellationToken)
    {
        var registration = cancellationToken.CancellationToken.Register(() => Task.Factory.StartNew(() => Cancel(true)).Unwrap());
    }
}