﻿namespace Backtest.Core.Job;

public class JobExecutionOptions
{
    public double ObserverTtlSec { get; set; } = 60;
    public double CacheTtlSec { get; set; } = 3600;
}