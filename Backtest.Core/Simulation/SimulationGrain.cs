﻿using Backtest.Domain;
using Backtest.Domain.Backtest;
using Backtest.Domain.Events;
using Backtest.Domain.Job;
using Backtest.Domain.Queries;
using Backtest.Domain.Simulation;
using Backtest.Domain.States;
using Backtest.Domain.States.Settings;
using Microsoft.Extensions.Logging;
using Orleans.Runtime;
using Orleans.Streams;
using Orleans.Utilities;
using static Backtest.Domain.Constants;
using ILogger = Serilog.ILogger;
using TaskStatus = Backtest.Domain.TaskStatus;

namespace Backtest.Core.Simulation;

public class SimulationGrain : Grain, ISimulation
{
    private readonly IPersistentState<StrategySettings> _strategySettings;
    private readonly IPersistentState<SimulationState> _simulationState;
    private readonly IPersistentState<RuntimeSummaryMap> _runtimeSummaryMap;
    private readonly ObserverManager<IStateUpdatesObserver> _updatesObserverManager;
    private readonly IQueriesServiceClient _queriesServiceClient;

    private ILogger _logger;
    private IBacktest _backtest;
    private IJob[] _jobs;

    private IStreamProvider StreamProvider => this.GetStreamProvider(STREAM_PROVIDER_NAME);
    private IAsyncStream<ItemSummary> ProgressStream => StreamProvider.GetStream<ItemSummary>(StreamId.Create(STATUS_UPDATES_STREAM, this.GetPrimaryKey())); 
    private IAsyncStream<ItemSummary> BacktestUpdatesStream => StreamProvider.GetStream<ItemSummary>(StreamId.Create(STATUS_UPDATES_STREAM, _backtest.GetPrimaryKey()));
    private IAsyncStream<JobFinishedEvent> JobFinishedStream => StreamProvider.GetStream<JobFinishedEvent>(StreamId.Create(JOB_FINISHED_STREAM, this.GetPrimaryKey()));
    private IAsyncStream<JobRestartedEvent> JobRestartedStream => StreamProvider.GetStream<JobRestartedEvent>(StreamId.Create(JOB_RETRY_STREAM, this.GetPrimaryKey()));

    private StreamSubscriptionHandle<ItemSummary> _progressSubscriptionHandle;
    private StreamSubscriptionHandle<JobFinishedEvent> _jobFinishedStreamHandle;
    private StreamSubscriptionHandle<JobRestartedEvent> _jobRestartedStreamHandle;

    public SimulationGrain(
        [PersistentState(STRATEGY_SETTINGS)]IPersistentState<StrategySettings> strategySettings,
        [PersistentState(SIMULATION)]IPersistentState<SimulationState> simulationState,
        [PersistentState(SIMULATION_RUNTIME_SUMMARY_MAP, REDIS_STORAGE_DB0)]IPersistentState<RuntimeSummaryMap> runtimeSummaryMap,
        ILoggerFactory loggerFactory,
        ILogger logger,
        IQueriesServiceClient queriesServiceClient)
    {
        _logger = logger.ForContext<SimulationGrain>();
        _strategySettings = strategySettings;
        _simulationState = simulationState;
        _runtimeSummaryMap = runtimeSummaryMap;
        _queriesServiceClient = queriesServiceClient;
        _updatesObserverManager = new ObserverManager<IStateUpdatesObserver>(TimeSpan.FromHours(24), loggerFactory.CreateLogger<IStateUpdatesObserver>());
    }

    public async Task Create(IBacktest backtest, CreateSimulationCommand createCommand)
    {
        MapBacktest(backtest);
        
        _simulationState.State.Name = createCommand.Name;
        _simulationState.State.BacktestId = backtest.GetPrimaryKey();
        _simulationState.State.Created = DateTime.UtcNow;

        _strategySettings.State.SettingsSource = createCommand.StrategySettings;
        _strategySettings.State.AppliedUserParams = createCommand.AppliedUserParams;

        await CreateUpdatesStream();
        await _strategySettings.WriteStateAsync();
        _logger.Information("Simulation [{SimulationId}]: created", this.GetPrimaryKey());
    }

    public async Task<int> GenerateJobs(JobsGenerationOptions jobsGenerationOptions, GrainCancellationToken cancellationToken)
    {
        var mdDates = jobsGenerationOptions.MdDates;
        var totalJobs = jobsGenerationOptions.SplitDays ? mdDates.Length : 1;
        _jobs = new IJob[totalJobs];
        _simulationState.State.Summary.TotalJobs = totalJobs;

        if (jobsGenerationOptions.SplitDays)
        {
            var jobsCreateTasks = new Task[totalJobs];
            for (var i = 0; i < mdDates.Length; i++)
            {
                var date = mdDates[i];
                var jobName = $"{_simulationState.State.Name}[{i}]-{date:dd.MM.yyyy}";
                jobsCreateTasks[i] = CreateSingleJob(date, date, jobName, i, jobsGenerationOptions.ReportingIntervalSec, jobsGenerationOptions.MaximumTries, jobsGenerationOptions.DesiredAgents, cancellationToken);
            }

            await Task.WhenAll(jobsCreateTasks);
        }
        else
        {
            if (mdDates.Length < 2)
                throw new ApplicationException("Md start date or md end date was not found");
            
            var mdStart = mdDates[0];
            var mdEnd = mdDates[^1];
            var jobName = $"{_simulationState.State.Name}[{0}]-{mdStart:dd.MM.yyyy}-{mdEnd:dd.MM.yyyy}";
            await CreateSingleJob(mdStart, mdEnd, jobName, 0, jobsGenerationOptions.ReportingIntervalSec, jobsGenerationOptions.MaximumTries, jobsGenerationOptions.DesiredAgents, cancellationToken);
        }

        _simulationState.State.Summary.Status = TaskStatus.Queued;

        _logger.Information("Simulation [{SimulationId}]: created {JobsCount} jobs", this.GetPrimaryKey(), totalJobs);
        return totalJobs;
    }

    public Task<StrategySettings> GetStrategySettings()
    {
        return Task.FromResult(_strategySettings.State);
    }

    public Task<IJob[]> GetJobs()
    {
        return Task.FromResult(_jobs);
    }

    public Task SubscribeStateUpdates(IStateUpdatesObserver observer)
    {
        _updatesObserverManager.Subscribe(observer, observer);
        return Task.CompletedTask;
    }

    public Task UnsubscribeStateUpdates(IStateUpdatesObserver observer)
    {
        _updatesObserverManager.Unsubscribe(observer);
        return Task.CompletedTask;
    }

    private async Task CreateSingleJob(DateTime mdStart, DateTime mdEnd, string jobName, int idx, double reportingIntervalSec, int maximumTries, List<string> assignedAgents, GrainCancellationToken cancellationToken)
    {
        var jobCreateCommand = new CreateJobCommand(jobName, mdStart, mdEnd, reportingIntervalSec, maximumTries, assignedAgents.ToArray());
        var jobId = Guid.NewGuid();
        var job = GrainFactory.GetGrain<IJob>(jobId);
        await job.Create(_backtest, this, jobCreateCommand, cancellationToken);
        _jobs[idx] = job;
        _runtimeSummaryMap.State.Append(jobId);
    }

    public override async Task OnActivateAsync(CancellationToken cancellationToken)
    {
        await base.OnActivateAsync(cancellationToken);
        _logger = _logger.WithSimulationId(this);
        
        if (_simulationState.State.BacktestId is not null)
        {
            MapBacktest(GrainFactory.GetGrain<IBacktest>(_simulationState.State.BacktestId.Value));
            await CreateUpdatesStream();
        }

        _jobs = await _queriesServiceClient.GetJobsBySimulation(this);
        _logger.Information("Simulation [{SimulationId}]: activated", this.GetPrimaryKey());
    }

    public override async Task OnDeactivateAsync(DeactivationReason reason, CancellationToken cancellationToken)
    {
        await _runtimeSummaryMap.WriteStateAsync();
        await _simulationState.WriteStateAsync();
        _logger.Information("Simulation [{SimulationId}]: deactivated - {Reason}", this.GetPrimaryKey(), reason.Description);
        await base.OnDeactivateAsync(reason, cancellationToken);
    }

    private void MapBacktest(IBacktest backtest)
    {
        _backtest = backtest;
        _logger = _logger.WithBacktestId(backtest);
        _logger.Debug("Backtest added");
    }
    
    private async Task CreateUpdatesStream()
    {
        _progressSubscriptionHandle = await ProgressStream.SubscribeAsync(ReceiveUpdate);
        _jobFinishedStreamHandle = await JobFinishedStream.SubscribeAsync(ReceiveJobFinishedUpdates);
        _jobRestartedStreamHandle = await JobRestartedStream.SubscribeAsync(ReceiveJobRestartedUpdates);
        
        _logger.Information("Simulation [{SimulationId}]: updates stream created", this.GetPrimaryKey());
    }

    private async Task UnsubscribeUpdatesStreams()
    {
        await _progressSubscriptionHandle.UnsubscribeAsync();
        await _jobFinishedStreamHandle.UnsubscribeAsync();
        await _jobRestartedStreamHandle.UnsubscribeAsync();
        _logger.Information("Simulation [{SimulationId}]: unsubscribed from updates streams", this.GetPrimaryKey());
    }

    private async Task ReceiveUpdate(IEnumerable<SequentialItem<ItemSummary>> updateCommands)
    {
        var updates = updateCommands
            .GroupBy(x => x.Item.ItemId)
            .ToDictionary(
                x => x.Key,
                x => x.MaxBy(summary => summary.Token.SequenceNumber).Item);
        _runtimeSummaryMap.State.Update(updates);
        _simulationState.State.Summary.Progress = _runtimeSummaryMap.State.TotalProgress();
        _simulationState.State.Summary.Status = _runtimeSummaryMap.State.TotalStatus();
        await BacktestUpdatesStream.OnNextAsync(_simulationState.State.Summary.GetSummary(this.GetPrimaryKey()));
        await _updatesObserverManager.Notify(observer => observer.StateUpdate(_simulationState.State.Summary.GetSummary(this.GetPrimaryKey())));
        if (_simulationState.State.Summary.IsFinalStatus)
            await _simulationState.WriteStateAsync();
        _logger.Information(
            "Simulation [{SimulationId}]: received update. Status: {Status}\tProgress: {Progress}",
            this.GetPrimaryKey(),
            _simulationState.State.Summary.Status,
            _simulationState.State.Summary.Progress
        );
    }

    private async Task ReceiveJobFinishedUpdates(IEnumerable<SequentialItem<JobFinishedEvent>> commands)
    {
        foreach (var command in commands)
        {
            _simulationState.State.Summary.JobFinished(command.Item);
        }
        await _updatesObserverManager.Notify(observer => observer.StateUpdate(_simulationState.State.Summary.GetSummary(this.GetPrimaryKey())));
        _logger.Information("Simulation [{SimulationId}]: received job finished event", this.GetPrimaryKey());
    }

    private async Task ReceiveJobRestartedUpdates(IEnumerable<SequentialItem<JobRestartedEvent>> commands)
    {
        foreach (var command in commands)
        {
            _simulationState.State.Summary.JobRestarted(command.Item);
        }
        await _updatesObserverManager.Notify(observer => observer.StateUpdate(_simulationState.State.Summary.GetSummary(this.GetPrimaryKey())));
        _logger.Information("Simulation [{SimulationId}]: job was restarted", this.GetPrimaryKey());
    }
    
    public Task<SimulationState> GetState()
    {
        return Task.FromResult(_simulationState.State);
    }
}