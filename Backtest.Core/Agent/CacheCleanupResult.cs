namespace Backtest.Core.Agent;

[GenerateSerializer]
public record struct CacheCleanupResult(long TotalCleaned, long PendingCacheRemovalBytes);