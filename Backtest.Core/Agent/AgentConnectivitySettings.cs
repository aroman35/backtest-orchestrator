namespace Backtest.Core.Agent;

public class AgentConnectivitySettings
{
    public double KeepAliveIntervalSec { get; set; } = 10;
    public double CacheBuildTimeout { get; set; } = 60;
    public double CacheCleanupJobInterval { get; set; } = 60;
    public double CacheCleanupQuotaPercent { get; set; } = 80;
}