using System.Collections.Concurrent;
using Backtest.Domain.Agent;
using Backtest.Domain.Job;
using Backtest.Domain.MarketDataCache;
using Backtest.Domain.Models;
using Backtest.Domain.Scheduler;
using Backtest.Domain.States;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Orleans.Runtime;
using Orleans.Streams;
using Orleans.Utilities;
using static Backtest.Domain.Constants;
using ILogger = Serilog.ILogger;

namespace Backtest.Core.Agent;

public class Agent : Grain, IAgent, IRemindable
{
    private const string CACHE_CLEANUP_JOB_NAME = "cache-cleanup";

    private readonly HashSet<IJob> _runningJobs = new();
    private readonly ConcurrentDictionary<string, Guid> _cache = new();
    private readonly ConcurrentQueue<int> _readyCores = new();

    private readonly IPersistentState<AgentState> _agentState;
    private readonly ObserverManager<IAgentObserver> _agentObserverManager;
    private readonly IOptions<AgentConnectivitySettings> _agentConnectivitySettings;
    private readonly ISchedulerConnector _schedulerConnector;

    private ILogger _logger;
    private IAsyncStream<JobLaunchCommand> _jobLaunchStream;
    private IGrainReminder _cacheCleanupJob;
    private IDisposable _keepAliveTimer;

    public Agent(
        [PersistentState(AGENT, MEMORY_STORAGE)]IPersistentState<AgentState> agentState,
        ILoggerFactory loggerFactory,
        ILogger logger,
        IOptions<AgentConnectivitySettings> agentConnectivitySettings)
    {
        _logger = logger.ForContext<Agent>();
        _agentState = agentState;
        _agentConnectivitySettings = agentConnectivitySettings;
        _agentObserverManager = new ObserverManager<IAgentObserver>(
            TimeSpan.FromSeconds(_agentConnectivitySettings.Value.KeepAliveIntervalSec),
            loggerFactory.CreateLogger<ObserverManager<IAgentObserver>>());
        _schedulerConnector = GrainFactory.GetGrain<ISchedulerConnector>(Guid.Empty);
    }

    public Task<double> Subscribe(IAgentObserver agentObserver)
    {
        _agentObserverManager.Subscribe(agentObserver, agentObserver);
        return Task.FromResult(_agentConnectivitySettings.Value.KeepAliveIntervalSec);
    }

    public async Task<AgentConnectedResponse> OnAgentConnected(AgentSettings agentSettings, bool initialConnection)
    {
        _agentState.State.AgentName = agentSettings.AgentName;
        _agentState.State.Cores = agentSettings.Cores;
        _agentState.State.TelemetryPath = agentSettings.TelemetryPath;
        _agentState.State.BinariesPath = agentSettings.BinariesPath;
        _agentState.State.CachePath = agentSettings.CachePath;
        _agentState.State.LogsPath = agentSettings.LogsPath;
        _agentState.State.CacheMaxSizeBytes = agentSettings.CacheMaxSizeBytes;
        _agentState.State.IsActive = true;
        _agentState.State.LastPingEvent = DateTime.UtcNow;

        if (initialConnection)
        {
            _readyCores.Clear();
            foreach (var core in _agentState.State.Cores)
            {
                _readyCores.Enqueue(core);
            }
        }

        _logger = _logger.WithAgentName(_agentState.State.AgentName);
        
        await _schedulerConnector.AddAgent(this, _readyCores.Count);
        
        var keepAlive =  TimeSpan.FromSeconds(_agentConnectivitySettings.Value.KeepAliveIntervalSec);
        _keepAliveTimer = RegisterTimer(KeepAliveCheck, null, keepAlive, keepAlive);
        var cacheCleanupJobInterval = TimeSpan.FromSeconds(_agentConnectivitySettings.Value.CacheCleanupJobInterval);
        _cacheCleanupJob = await this.RegisterOrUpdateReminder(CACHE_CLEANUP_JOB_NAME, cacheCleanupJobInterval, cacheCleanupJobInterval);
        
        _logger.Information("Agent [{AgentName}]: connected with {Cores} cores", this.GetPrimaryKeyString(), _readyCores.Count);
        
        var streamId = StreamId.Create(LAUNCH_JOB_STREAM, this.GetPrimaryKeyString());
        _jobLaunchStream = this.GetStreamProvider(STREAM_PROVIDER_NAME).GetStream<JobLaunchCommand>(streamId);
        // await LaunchNextBatch();
        return new AgentConnectedResponse(_agentConnectivitySettings.Value.KeepAliveIntervalSec);
    }

    public ValueTask<int> AvailableCores()
    {
        return ValueTask.FromResult(_agentState.State.Cores.Length - _runningJobs.Count);
    }

    public async Task<bool> SendJobToLaunch(IJob job, string binaryName)
    {
        if (_agentState.State.Cores.Length - _runningJobs.Count <= 0)
            return false;
        _runningJobs.Add(job);
        _logger.Information("Job [{JobId}]: added to {AgentName}", job.GetPrimaryKey(), this.GetPrimaryKeyString());
        await _jobLaunchStream.OnNextAsync(new JobLaunchCommand(job.GetPrimaryKey(), 0, binaryName));
        _logger.Information("Job [{JobId}]: sent to {AgentName}", job.GetPrimaryKey(), this.GetPrimaryKeyString());
        return true;
    }

    public Task JobFinished(IJob job)
    {
        _runningJobs.Remove(job);
        _logger.Information("Job [{JobId}]: Unmapped from agent", job.GetPrimaryKey());
        return Task.CompletedTask;
        // await LaunchNextBatch();
    }

    public Task<bool> KeepAlive()
    {
        _agentState.State.LastPingEvent = DateTime.UtcNow;

        return Task.FromResult(_agentState.State.IsActive);
    }

    public async Task Disconnect()
    {
        _keepAliveTimer.Dispose();
        _agentState.State.IsActive = false;
        await _schedulerConnector.RemoveAgent(this);
        await this.UnregisterReminder(_cacheCleanupJob);
        _logger.Warning("Agent was disconnected");
    }

    public Task<AgentInfo> GetAgentInfo()
    {
        return Task.FromResult(new AgentInfo(_agentState.State.LogsPath, _agentState.State.TelemetryPath));
    }

    public Task CacheAdded(IMarketDataCacheMeta meta, IMarketDataCache cache)
    {
        _cache[meta.GetPrimaryKeyString()] = cache.GetPrimaryKey();
        return Task.CompletedTask;
    }

    public Task CacheCreated(long dataFileSizeBytes)
    {
        _agentState.State.CacheSizeBytes += dataFileSizeBytes;
        return Task.CompletedTask;
    }

    public Task CacheRemoved(string hash, long dataFileSize)
    {
        _cache.Remove(hash, out _);
        _agentState.State.CacheSizeBytes -= dataFileSize;

        _logger.Information("{Hash} has been removed", hash);
        return Task.CompletedTask;
    }

    public Task<IMarketDataCache> GetCacheByHash(string hash)
    {
        if (_cache.TryGetValue(hash, out var cacheId))
            return Task.FromResult(GrainFactory.GetGrain<IMarketDataCache>(cacheId));
        throw new ApplicationException($"Attempt to request cache {hash} which is not existing on {this.GetPrimaryKeyString()}");
    }

    public Task<AgentCacheInfo> GetCacheInfo()
    {
        return Task.FromResult(new AgentCacheInfo(_agentState.State.CachePath, CacheEntryCompression.Gzip, CacheEntryType.FileStream));
    }

    public Task<IJob[]> GetRunningJobs()
    {
        return Task.FromResult(_runningJobs.ToArray());
    }

    private async Task KeepAliveCheck(object _)
    {
        if (!_agentState.State.IsActive)
            return;
        
        var keepAliveTimeout = TimeSpan.FromSeconds(_agentConnectivitySettings.Value.KeepAliveIntervalSec);
        if (DateTime.UtcNow - (_agentState.State.LastPingEvent + keepAliveTimeout) > TimeSpan.Zero)
        {
            await Disconnect();
        }
    }
    
    public override async Task OnActivateAsync(CancellationToken cancellationToken)
    {
        await base.OnActivateAsync(cancellationToken);
        _logger = _logger.WithAgentName(_agentState.State.AgentName);
        _readyCores.Clear();
        foreach (var core in _agentState.State.Cores)
        {
            _readyCores.Enqueue(core);
        }
    }

    public override async Task OnDeactivateAsync(DeactivationReason reason, CancellationToken cancellationToken)
    {
        await base.OnDeactivateAsync(reason, cancellationToken);
        await _agentState.ClearStateAsync();

        _logger.Warning("Agent {Name} was deactivated", _agentState.State.AgentName);
    }

    async Task IRemindable.ReceiveReminder(string reminderName, TickStatus status)
    {
        switch (reminderName)
        {
            case CACHE_CLEANUP_JOB_NAME: await RunCacheCleanup(); break;
        }
    }

    private async Task<CacheCleanupResult> RunCacheCleanup()
    {
        if (_agentState.State.CacheMaxSizeBytes == 0)
            return new CacheCleanupResult();

        var sizeBeforeRemoval = _agentState.State.CacheSizeBytes;
        var pendingCacheRemoval = 0L;
        long CacheUsedSizePercent() => _agentState.State.CacheSizeBytes /
            (_agentState.State.CacheMaxSizeBytes - pendingCacheRemoval == 0 ? 1 : _agentState.State.CacheMaxSizeBytes - pendingCacheRemoval) * 100;

        while (CacheUsedSizePercent() > _agentConnectivitySettings.Value.CacheCleanupQuotaPercent)
        {
            var useInfo = await Task.WhenAll(_cache.Values.Select(cacheId => GrainFactory.GetGrain<IMarketDataCache>(cacheId).GetCacheUseInfo()));
            var candidateForRemoval = useInfo
                .Where(x => x.IsActive && x.RunningJobsCount == 0)
                .MinBy(x => x.LastAccessDate);

            pendingCacheRemoval = useInfo
                .Where(x => !x.IsActive)
                .Sum(x => x.DataFileSizeBytes);

            if (candidateForRemoval is not null)
            {
                _logger.Information(
                    "Preparing to remove {CacheId} with {Size} bytes length",
                    candidateForRemoval.CacheId,
                    candidateForRemoval.DataFileSizeBytes);

                var cache = GrainFactory.GetGrain<IMarketDataCache>(candidateForRemoval.CacheId);
                await cache.Remove();
                _logger.Information("Agent notified to remove cache");
            }
            else
            {
                _logger.Warning("Noting to remove. Cache used {Used}%, pending for removal: {Pending}", CacheUsedSizePercent(), pendingCacheRemoval);
                break;
            }
        }

        return new CacheCleanupResult(sizeBeforeRemoval - _agentState.State.CacheSizeBytes, pendingCacheRemoval);
    }

    public Task<AgentState> GetState()
    {
        return Task.FromResult(_agentState.State);
    }
}