﻿using Backtest.Domain;
using Backtest.Domain.Scheduler;
using MongoDB.Driver;
using Orleans.Runtime;
using Orleans.Streams;
using ILogger = Serilog.ILogger;

namespace Backtest.Scheduler;

public class QueueReceiverAdaptor : IHostedService
{
    private readonly IMongoDatabase _database;
    private readonly IClusterClient _clusterClient;
    private readonly ILogger _logger;
    
    private readonly StreamId _jobsQueueStreamId = StreamId.Create(Constants.JOBS_QUEUE_STREAM, Guid.Empty);
    private readonly StreamId _launchConfirmedStreamId = StreamId.Create(Constants.LAUNCH_CONFIRMED_STREAM, Guid.Empty);
    private readonly StreamId _jobReleaseStreamId = StreamId.Create(Constants.JOB_RELEASED_STREAM, Guid.Empty);

    private IMongoCollection<QueueItem> Collection => _database.GetCollection<QueueItem>("scheduler-queue");
    private IAsyncStream<QueueItem> JobsQueueStream => _clusterClient.GetStreamProvider(Constants.STREAM_PROVIDER_NAME).GetStream<QueueItem>(_jobsQueueStreamId);
    private IAsyncStream<Guid> LaunchConfirmedStream => _clusterClient.GetStreamProvider(Constants.STREAM_PROVIDER_NAME).GetStream<Guid>(_launchConfirmedStreamId);
    private IAsyncStream<Guid> JobReleasedStream => _clusterClient.GetStreamProvider(Constants.STREAM_PROVIDER_NAME).GetStream<Guid>(_jobReleaseStreamId);
    
    private StreamSubscriptionHandle<QueueItem> _jobsQueueStreamSubscription;
    private StreamSubscriptionHandle<Guid> _launchConfirmedSubscription;
    private StreamSubscriptionHandle<Guid> _jobReleasedSubscription;

    public QueueReceiverAdaptor(IMongoDatabase database, IClusterClient clusterClient, ILogger logger)
    {
        _database = database;
        _clusterClient = clusterClient;
        _logger = logger.ForContext<QueueReceiverAdaptor>();
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        _jobsQueueStreamSubscription = await JobsQueueStream.SubscribeAsync((queueItem, _) => InsertOne(queueItem, cancellationToken));
        _launchConfirmedSubscription = await LaunchConfirmedStream.SubscribeAsync((jobId, _) => LaunchConfirmed(jobId, cancellationToken));
        _jobReleasedSubscription = await JobReleasedStream.SubscribeAsync((jobId, _) => JobReleased(jobId, cancellationToken));
        
        _logger.Information("Subscribed to jobs queue stream");
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        await _jobsQueueStreamSubscription.UnsubscribeAsync();
        await _launchConfirmedSubscription.UnsubscribeAsync();
        await _jobReleasedSubscription.UnsubscribeAsync();
        
        _logger.Information("Unsubscribed from jobs queue stream");
    }

    private async Task InsertOne(QueueItem queueItem, CancellationToken cancellationToken)
    {
        await Collection.InsertOneAsync(queueItem, new InsertOneOptions
        {
            BypassDocumentValidation = true
        }, cancellationToken);
        _logger.Information("Job [{JobId}]: added to queue", queueItem.JobId);
    }

    private async Task LaunchConfirmed(Guid jobId, CancellationToken cancellationToken)
    {
        await Collection.UpdateOneAsync(Builders<QueueItem>.Filter.Eq(x => x.JobId, jobId),
            Builders<QueueItem>.Update.Set(x => x.Status, QueueItemStatus.Confirmed), new UpdateOptions
            {
                IsUpsert = true
            }, cancellationToken: cancellationToken);
    }

    private async Task JobReleased(Guid jobId, CancellationToken cancellationToken)
    {
        await Collection.DeleteOneAsync(Builders<QueueItem>.Filter.Eq(x => x.JobId, jobId), cancellationToken: cancellationToken);
    }
}