using Backtest.Domain;
using Backtest.Common;
using Backtest.Scheduler;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Serilog;
using Serilog.Events;
using static Backtest.Domain.Constants;

var bootstrapLogger = new LoggerConfiguration()
    .WriteTo.Console()
    .MinimumLevel.Debug()
    .CreateBootstrapLogger();

using var host = Host.CreateDefaultBuilder(args)
    .ConfigureAppConfiguration(configurationBuilder => configurationBuilder
        .AddConsulConfiguration("scheduler")
        .AddConsulConfiguration("logger-configuration")
        .AddConsulConfiguration("kafka-streams-configuration")
        .AddConsulConfiguration("database-settings")
        .AddEnvironmentVariables())
    .UseOrleansClient((hostContext, clientBuilder) =>
    {
        clientBuilder.ConfigureBacktestClient(hostContext.Configuration, "scheduler");
    })
    .ConfigureServices((hostContext, services) =>
    {
        services
            .Configure<DatabaseSettings>(hostContext.Configuration.GetSection(nameof(DatabaseSettings)))
            .AddSingleton<IMongoClient>(provider => new MongoClient(provider.GetRequiredService<IOptions<DatabaseSettings>>().Value.ConnectionString))
            .AddSingleton(provider => provider.GetRequiredService<IMongoClient>().GetDatabase(provider.GetRequiredService<IOptions<DatabaseSettings>>().Value.DatabaseName))
            .AddHostedService<QueueReaderService>()
            .AddHostedService<QueueReceiverAdaptor>();
    })
    .UseSerilog((hostContext, loggingConfiguration) => loggingConfiguration
        .ReadFrom.Configuration(hostContext.Configuration)
        .Enrich.WithProperty(APPLICATION_NAME_PROP, APPLICATION_NAME_VAL)
        .Enrich.WithProperty(INSTANCE_NAME_PROP, "Scheduler"))
    .UseConsoleLifetime()
    .Build();

try
{
    await host.RunAsync();
}
catch (Exception exception)
{
    bootstrapLogger.Write(LogEventLevel.Fatal, exception, "Error launching host");
    throw;
}