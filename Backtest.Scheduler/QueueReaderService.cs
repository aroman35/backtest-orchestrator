﻿using System.Diagnostics;
using Backtest.Domain.Agent;
using Backtest.Domain.Job;
using Backtest.Domain.Scheduler;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using Polly;
using ILogger = Serilog.ILogger;

namespace Backtest.Scheduler;

public class QueueReaderService : BackgroundService
{
    private readonly IMongoClient _mongoClient;
    private readonly IClusterClient _clusterClient;
    private readonly IMongoDatabase _database;
    private readonly ILogger _logger;

    private ISchedulerConnector _schedulerConnector;
    private IMongoCollection<QueueItem> Collection => _database.GetCollection<QueueItem>("scheduler-queue");

    public QueueReaderService(IMongoClient mongoClient, IMongoDatabase database, IClusterClient clusterClient, ILogger logger)
    {
        _mongoClient = mongoClient;
        _database = database;
        _clusterClient = clusterClient;
        _logger = logger.ForContext<QueueReaderService>();
    }

    public override async Task StartAsync(CancellationToken cancellationToken)
    {
        BsonSerializer.RegisterSerializer(new GuidSerializer(BsonType.String));
        _schedulerConnector = _clusterClient.GetGrain<ISchedulerConnector>(Guid.Empty);
        await base.StartAsync(cancellationToken);
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var policy = Policy.Handle<Exception>().WaitAndRetryAsync(5,
            currentTry => TimeSpan.FromMilliseconds(100 * currentTry),
            (exception, span) => _logger.Error(exception, "Error updating queue. Retry in {Delay}", span));
        
        using var timer = new PeriodicTimer(TimeSpan.FromMilliseconds(500));
        while (!stoppingToken.IsCancellationRequested && await timer.WaitForNextTickAsync(stoppingToken))
        {
            var agentNames = await _schedulerConnector.GetAgents();
            if (!agentNames.Any())
            {
                _logger.Warning("No agents connected");
                await Task.Delay(5000, stoppingToken);
            }
            foreach (var agentName in agentNames)
            {
                var agent = _clusterClient.GetGrain<IAgent>(agentName);
                var cores = await agent.AvailableCores();
                if (cores <= 0)
                {
                    _logger.Information("Agent [{AgentName}]: has no free cores. Skipping", agentName);
                    continue;
                }
                
                var operationStarted = Stopwatch.GetTimestamp();
                using var session = await _mongoClient.StartSessionAsync(cancellationToken: stoppingToken);
                session.StartTransaction(new TransactionOptions());
                var queuedJobs = await Collection
                    .Aggregate()
                    .Match(FilterDefinition(agentName))
                    .Sort(_sortDefinition)
                    .Limit(cores)
                    .ToListAsync(stoppingToken);

                if (!queuedJobs.Any())
                {
                    continue;
                }

                var updates = new UpdateOneModel<QueueItem>[queuedJobs.Count];
                for (var i = 0; i < queuedJobs.Count; i++)
                {
                    var jobId = queuedJobs[i].JobId;
                    var job = _clusterClient.GetGrain<IJob>(jobId);
                    await job.SentForExecution(agent);
                    _logger.Information("Job [{JobId}]: was sent to agent {AgentName}", jobId, agentName);
                    updates[i] = new UpdateOneModel<QueueItem>(QueueItemByIdFilter(queuedJobs[i].Id), UpdateDefinition());
                }

                var updateResult = await policy.ExecuteAndCaptureAsync(ctx => Collection.BulkWriteAsync(session, updates, cancellationToken: ctx), stoppingToken);
                    
                if (updateResult.Outcome is OutcomeType.Successful)
                {
                    await session.CommitTransactionAsync(stoppingToken);
                    var elapsed = Stopwatch.GetElapsedTime(operationStarted);
                    _logger.Information("Total {JobsCount} jobs were sent to {AgentName} in {Elapsed}", updates.Length, agentName, elapsed);
                }
                if (updateResult.Outcome is OutcomeType.Failure)
                {
                    await session.AbortTransactionAsync(stoppingToken);
                    _logger.Error(updateResult.FinalException, "Couldn't handle jobs queue for {AgentName}", agentName);
                }
            }
        }
    }

    private FilterDefinition<QueueItem> FilterDefinition(string agentName)
    {
        return Builders<QueueItem>.Filter.And(
            Builders<QueueItem>.Filter.Or(
                Builders<QueueItem>.Filter.AnyEq(x => x.AgentNames, agentName),
                Builders<QueueItem>.Filter.Size(x => x.AgentNames, 0)
                ),
            Builders<QueueItem>.Filter.Eq(x => x.Status, QueueItemStatus.Queued)
        );
    }

    private FilterDefinition<QueueItem> QueueItemByIdFilter(Guid id) => Builders<QueueItem>.Filter.Eq(x => x.Id, id);

    private UpdateDefinition<QueueItem> UpdateDefinition()
    {
        return Builders<QueueItem>.Update.Combine(
            Builders<QueueItem>.Update.Set(x => x.Status, QueueItemStatus.Pending),
            Builders<QueueItem>.Update.Set(x => x.Updated, DateTime.UtcNow)
        );
    }

    private SortDefinition<QueueItem> _sortDefinition = Builders<QueueItem>.Sort.Combine(
        Builders<QueueItem>.Sort.Descending(x => x.AgentNames),
        Builders<QueueItem>.Sort.Descending(x => x.IsRetry),
        Builders<QueueItem>.Sort.Ascending(x => x.Created)
        );
}