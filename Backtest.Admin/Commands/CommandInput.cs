﻿namespace Backtest.Admin.Commands;

public static class CommandInput
{
    private const string SHOW_AGENTS_COMMAND = "agent";
    public static async Task WaitForCommand(IClusterClient clusterClient)
    {
        Console.Write(">");
        var command = Console.ReadLine();
        if (string.IsNullOrWhiteSpace(command))
            return;
        var args = command.Split(' ');
        switch (args[0])
        {
            case SHOW_AGENTS_COMMAND: await AgentCommands.AgentCommand(clusterClient, args); break;
        }
    }
}