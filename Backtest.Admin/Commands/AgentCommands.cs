﻿using Backtest.Domain.Agent;
using Backtest.Domain.Scheduler;

namespace Backtest.Admin.Commands;

public static class AgentCommands
{
    public static async Task AgentCommand(IClusterClient clusterClient, params string[] arguments)
    {
        if (arguments.Length < 2)
            return;
        switch (arguments[1])
        {
            case "ls": await ShowAgents(clusterClient); break;
            case "jobs": if (arguments.Length == 3)
                await ShowRunningJobs(clusterClient, arguments[2]);
                break;
        }
    }

    private static async Task ShowAgents(IClusterClient clusterClient)
    {
        var schedulerGrain = clusterClient.GetGrain<ISchedulerConnector>(Guid.Empty);
        var agentNames = await schedulerGrain.GetAgents();
        if (!agentNames.Any())
            Console.WriteLine("No agents connected");
        foreach (var agentName in agentNames)
        {
            var agentGrain = clusterClient.GetGrain<IAgent>(agentName);
            var state = await agentGrain.GetState();
            var readyCores = await agentGrain.AvailableCores();
            Console.WriteLine($"{agentName} Cores: {readyCores}/{state.Cores.Length}");
        }
    }

    private static async Task ShowRunningJobs(IClusterClient clusterClient, string agentName)
    {
        var agentGrain = clusterClient.GetGrain<IAgent>(agentName);
        var runningJobs = await agentGrain.GetRunningJobs();
        var jobStates = await Task.WhenAll(runningJobs.Select(x => x.GetState()));
        foreach (var jobState in jobStates)
        {
            Console.WriteLine($"Started: {jobState.Started}\tLastUpdate: {jobState.LastUpdate}\tProgress:{jobState.Progress}%");
        }
    }
}