using Backtest.Admin.Commands;
using Backtest.Domain;
using Serilog;
using Serilog.Events;

var bootstrapLogger = new LoggerConfiguration()
    .WriteTo.Console()
    .MinimumLevel.Debug()
    .CreateBootstrapLogger();

try
{
    var host = Host.CreateDefaultBuilder(args)
        .ConfigureServices((hostContext, services) => services
            .Configure<KafkaStreamsOptions>(hostContext.Configuration.GetSection(nameof(KafkaStreamsOptions)))
            .Configure<DatabaseSettings>(hostContext.Configuration.GetSection(nameof(DatabaseSettings))))
        .UseOrleansClient((hostContext, clientBuilder) =>
        {
            clientBuilder.ConfigureBacktestClient(hostContext.Configuration, "admin");
        })
        .UseSerilog((_, configuration) =>
            configuration.MinimumLevel.Debug().WriteTo.Async(logging => logging.File("session.log")))
        .Build();

    await host.StartAsync();
    bootstrapLogger.Write(LogEventLevel.Information, "Connected admin session");
    
    await using var scope = host.Services.CreateAsyncScope();
    var clusterClient = scope.ServiceProvider.GetRequiredService<IClusterClient>();
    while (true)
    {
        await CommandInput.WaitForCommand(clusterClient);
    }
}
catch (Exception exception)
{
    bootstrapLogger.Write(LogEventLevel.Fatal, exception, "Error launching admin session");
}
finally
{
    Log.CloseAndFlush();
}
