﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FlashPipes.Common.Exceptions;
using FlashPipes.Common.Messaging;
using Newtonsoft.Json;

namespace FlashPipes.Common
{
    public static class PipeExtensions
    {
        public static void WriteMessage(this PipeStream pipeStream, FlashPipesMessage message)
        {
            var compressedMessage = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message));
            var messageSizeCompressed = BitConverter.GetBytes(compressedMessage.Length);
            var md5Compressed = Encoding.UTF8.GetBytes(message.Md5Hash);

            pipeStream.Write(messageSizeCompressed);
            pipeStream.Write(compressedMessage);
            pipeStream.Write(md5Compressed);
        }

        public static async Task WriteMessageAsync(this PipeStream pipeStream, FlashPipesMessage message, CancellationToken cancellationToken = default)
        {
            var compressedMessage = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message));
            var messageSizeCompressed = BitConverter.GetBytes(compressedMessage.Length);
            var md5Compressed = Encoding.UTF8.GetBytes(message.Md5Hash);
            
            await pipeStream.WriteAsync(new ReadOnlyMemory<byte>(messageSizeCompressed), cancellationToken).ConfigureAwait(false);
            await pipeStream.WriteAsync(new ReadOnlyMemory<byte>(compressedMessage), cancellationToken).ConfigureAwait(false);
            await pipeStream.WriteAsync(new ReadOnlyMemory<byte>(md5Compressed), cancellationToken).ConfigureAwait(false);
        }

        public static bool ReadMessage(this PipeStream pipeStream, out FlashPipesMessage message)
        {
            message = FlashPipesMessage.Default;
            if (!pipeStream.IsConnected) return false;
            var compressedSize = pipeStream.Read(sizeof(int));
            var size = BitConverter.ToInt32(compressedSize);
            if (!pipeStream.IsConnected) return false;
            var compressedMessage = pipeStream.Read(size);
            if (!pipeStream.IsConnected) return false;
            var md5Compressed = pipeStream.Read(32);
            // var md5 = Encoding.UTF8.GetString(md5Compressed);
            var jsonMessage = Encoding.UTF8.GetString(compressedMessage);
            message = JsonConvert.DeserializeObject(jsonMessage, typeof(FlashPipesMessage)) as FlashPipesMessage;
            // return md5 == message?.Md5Hash;
            return true;
        }

        private static readonly int s_bufferSize = 8192;
        private static readonly int s_stackMaxSize = 1048576;

        public static bool ReadAsMessage(this PipeStream pipeStream, ref FlashPipesMessage message)
        {
            Span<byte> compressedSize = stackalloc byte[sizeof(int)];
            if (!pipeStream.ReadAsSpan(ref compressedSize))
                return false;
            var size = BitConverter.ToInt32(compressedSize);
            
            var compressedMessage = size < s_stackMaxSize ? stackalloc byte[size] : new byte[size];
            if (!pipeStream.ReadAsSpan(ref compressedMessage))
                return false;

            Span<byte> md5Compressed = stackalloc byte[32];
            var _ = pipeStream.ReadAsSpan(ref md5Compressed);

            var utf8Message = size < s_stackMaxSize ? stackalloc char[size] : new char[size];
            for (var i = 0; i < size; i++)
            {
                utf8Message[i] = (char)compressedMessage[i];
            }

            var jsonMessage = new string(utf8Message);
            message = JsonConvert.DeserializeObject<FlashPipesMessage>(jsonMessage);

            return message is not null;
        }
        
        private static bool ReadAsSpan(this PipeStream pipeStream, ref Span<byte> message)
        {
            var length = message.Length;

            if (length < s_bufferSize)
            {
                pipeStream.Read(message);
                return true;
            }

            var totalBytesRead = 0;
            var memBuffer = length < s_bufferSize ? stackalloc byte[length] : new byte[s_bufferSize];
            while (pipeStream.IsConnected && totalBytesRead < length)
            {
                var remained = length - totalBytesRead;
                
                if (remained < s_bufferSize)
                    memBuffer = new byte[remained];
                
                var bytesRead = pipeStream.Read(memBuffer);
                if (bytesRead == 0)
                    return false;
                
                for (var i = 0; i < bytesRead; i++)
                {
                    message[i + totalBytesRead] = memBuffer[i];
                }

                totalBytesRead += bytesRead;
                memBuffer.Clear();
            }

            return true;
        }

        private static byte[] Read(this PipeStream pipeStream, int length)
        {
            using var memoryStream = new MemoryStream();
            var totalRead = 0;
            
            while (pipeStream.IsConnected && pipeStream.CanRead && totalRead != length)
            {
                var remainingLength = length - totalRead;
                Span<byte> buffer = new byte[remainingLength < 8192 ? remainingLength : 8192];
                var readBytes = pipeStream.Read(buffer);

                memoryStream.Write(buffer[..readBytes]);
                totalRead += readBytes;
            }

            return memoryStream.ToArray();
        }

        public static async Task<FlashPipesMessage> ReadMessageAsync(this PipeStream pipeStream, CancellationToken cancellationToken)
        {
            var compressedSize = await pipeStream.ReadAsync(sizeof(int), cancellationToken).ConfigureAwait(false);
            var size = BitConverter.ToInt32(compressedSize);
            var compressedMessage = await pipeStream.ReadAsync(size, cancellationToken).ConfigureAwait(false);
            var md5Compressed = await pipeStream.ReadAsync(32, cancellationToken);
            var md5 = Encoding.UTF8.GetString(md5Compressed);
            var jsonMessage = Encoding.UTF8.GetString(compressedMessage);
            var message = JsonConvert.DeserializeObject<FlashPipesMessage>(jsonMessage);

            if (md5 == message?.Md5Hash)
                return message;

            throw new FlashPipesException("Md5 error");
        }

        private static async Task<byte[]> ReadAsync(this Stream pipeStream, int length, CancellationToken cancellationToken)
        {
            await using var memoryStream = new MemoryStream();
            var totalRead = 0;

            while (pipeStream.CanRead && totalRead != length)
            {
                var remainingLength = length - totalRead;
                var buffer = new Memory<byte>(new byte[remainingLength < 8192 ? remainingLength : 8192]);
                
                var readBytes = await pipeStream.ReadAsync(buffer, cancellationToken).ConfigureAwait(false);
                memoryStream.Write(buffer[..readBytes].Span);
                totalRead += readBytes;
            }

            return memoryStream.ToArray();
        }
    }
}