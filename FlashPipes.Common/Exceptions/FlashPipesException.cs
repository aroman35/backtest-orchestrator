﻿using System;

namespace FlashPipes.Common.Exceptions
{
    public class FlashPipesException : Exception
    {
        public FlashPipesException(string message) : base(message)
        {
            
        }
    }
}