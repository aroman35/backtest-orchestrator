﻿using FlashPipes.Common.Messaging;

namespace FlashPipes.Common.Exceptions
{
    public class FlashPipesPipeControllerException : FlashPipesException
    {
        public FlashPipesMessage RequestMessage { get; }

        public FlashPipesPipeControllerException(FlashPipesMessage requestMessage, string message) : base(message)
        {
            RequestMessage = requestMessage;
        }
    }
}