﻿using System;

namespace FlashPipes.Common.Exceptions
{
    public class FlashPipesPipeRouterException : FlashPipesException
    {
        public Guid PipeName { get; }
        public string Route { get; }

        public FlashPipesPipeRouterException(Guid pipeName, string route, string message = "unable to route the request") : base(message)
        {
            PipeName = pipeName;
            Route = route;
        }
    }
}