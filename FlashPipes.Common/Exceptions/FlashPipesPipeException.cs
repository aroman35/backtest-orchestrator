﻿using System;

namespace FlashPipes.Common.Exceptions
{
    public class FlashPipesPipeException : Exception
    {
        public string Reason { get; }
        public string Route { get; }
        
        public FlashPipesPipeException(string reason, string route) : base(reason)
        {
            Reason = reason;
            Route = route;
        }
    }
}