﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;

namespace FlashPipes.Common.Messaging
{
    public sealed class FlashPipesMessage : IEquatable<FlashPipesMessage>
    {
        [JsonProperty(Required = Required.Always)]
        public string Route { get; set; } = EmptyRoute;

        [JsonProperty(Required = Required.Always)]
        public DateTime CreatedTimeUtc { get; set; }

        [JsonProperty(Required = Required.Always)]
        public FlashPipesMessageType MessageType { get; set; }

        [JsonProperty(Required = Required.Default)]
        public string CompressedMessage { get; private set; }

        [JsonProperty(Required = Required.Default)]
        public string ErrorMessage { get; set; }

        [JsonProperty(Required = Required.Always)]
        public bool IsPost { get; set; }
        
        [JsonProperty(Required = Required.Always)]
        public string Md5Hash
        {
            get => HasValue ? _md5Hash : EmptyHash;
            private set => _md5Hash = value;
        }

        [JsonProperty(Required = Required.Default)]
        public string PipeName { get; set; }

        [JsonIgnore]
        public bool IsError => !string.IsNullOrEmpty(ErrorMessage);

        [JsonIgnore]
        public bool HasValue => CompressedMessage != null;

        [JsonIgnore]
        public static readonly string EmptyRoute = "empty";

        [JsonIgnore]
        public static readonly FlashPipesMessage Default = new()
        {
            Route = EmptyRoute
        };

        private string _md5Hash;

        public object GetPayload(Type messageType)
        {
            if (!HasValue)
                throw new Exception("Message is empty");
            // if (!IsValid())
            //     throw new Exception("Message is invalid");

            return JsonConvert.DeserializeObject(CompressedMessage, messageType);
        }

        public void SetPayload(object payload)
        {
            CompressedMessage = JsonConvert.SerializeObject(payload);

            GetHash();
        }

        private string GetHash()
        {
            using var md5 = MD5.Create();
            var hashes = md5.ComputeHash(Encoding.UTF8.GetBytes(CompressedMessage));
            return Md5Hash = hashes.Aggregate("", (current, b) => current + b.ToString("x2"));
        }

        private bool IsValid()
        {
            return Md5Hash == GetHash();
        }

        public bool Equals(FlashPipesMessage other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Route, other.Route, StringComparison.InvariantCultureIgnoreCase) &&
                   CreatedTimeUtc.Equals(other.CreatedTimeUtc) &&
                   MessageType == other.MessageType &&
                   string.Equals(ErrorMessage, other.ErrorMessage, StringComparison.InvariantCultureIgnoreCase) &&
                   IsPost == other.IsPost &&
                   string.Equals(PipeName, other.PipeName, StringComparison.InvariantCultureIgnoreCase);
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj) || obj is FlashPipesMessage other && Equals(other);
        }

        public override int GetHashCode()
        {
            var hashCode = new HashCode();
            hashCode.Add(Route, StringComparer.InvariantCultureIgnoreCase);
            hashCode.Add(CreatedTimeUtc);
            hashCode.Add((int) MessageType);
            hashCode.Add(ErrorMessage, StringComparer.InvariantCultureIgnoreCase);
            hashCode.Add(IsPost);
            hashCode.Add(PipeName, StringComparer.InvariantCultureIgnoreCase);
            return hashCode.ToHashCode();
        }

        [JsonIgnore]
        private static readonly string EmptyHash = new(Enumerable.Repeat(' ', 32).ToArray());
    }
}