namespace FlashPipes.Common.Messaging
{
    public enum FlashPipesMessageType : byte
    {
        Request,
        Response
    }
}