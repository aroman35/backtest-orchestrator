namespace FlashPipes.Common.Serializers
{
    public interface IFlashPipesSerializer
    {
        byte[] Serialize<TMessage>(TMessage message)
            where TMessage : class;

        byte[] Serialize(object message);
    }
}