using System;
using System.IO;

namespace FlashPipes.Common.Serializers
{
    public interface IFlashPipesDeserializer
    {
        TMessage Deserialize<TMessage>(byte[] array)
            where TMessage : class;

        TMessage Deserialize<TMessage>(Stream encoded) where TMessage : class;

        object Deserialize(Type type, byte[] array);
    }
}