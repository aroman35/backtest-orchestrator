﻿using Backtest.Domain;
using Backtest.Domain.Backtest;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Backtest.Tests.Infrastructure;

public static class BacktestsFactory
{
    public static async Task<IBacktest> CreateBacktest(this IGrainFactory grainFactory, CreateBacktestCommand createBacktestCommand)
    {
        var backtest = grainFactory.GetGrain<IBacktest>(Guid.NewGuid());
        await backtest.Create(createBacktestCommand);
        return backtest;
    }

    private static string BacktestSettings() => File.ReadAllText("MockSettings/BacktestSettings.json");

    private static Dictionary<string, string[]> UserParams(int count)
    {
        return new Dictionary<string, string[]>()
        {
            {
                "Minis[0].ExecIniString.RepeatedParam",
                Enumerable.Range(0, count).Select(x => x.ToString()).ToArray()
            }
        };
    }

    public static CreateBacktestCommand CreateBacktestCommand(int daysCount, bool splitDays, int? paramsCount)
    {
        var startDate = DateTime.Today;
        var endDate = startDate.AddDays(daysCount);
        var strategySettings = new FakeStrategySettings();
        var strategySettingsJson = JsonSerializer.Serialize(strategySettings);

        var command = new CreateBacktestCommand(
            "unit-tests-bt",
            BacktestSettings(),
            strategySettingsJson,
            paramsCount.HasValue ? UserParams(paramsCount.Value) : new Dictionary<string, string[]>(),
            startDate,
            endDate,
            Array.Empty<DateTime>(),
            splitDays,
            "BacktestLauncher",
            false,
            false,
            3,
            60,
            new List<string>(),
            new List<string>(),
            Constants.DEFAULT_USER,
            Constants.DEFAULT_NAMESPACE
            );

        return command;
    }
}

public class FakeStrategySettings
{
    public DateTime StartDateTime { get; set; }
    public DateTime EndDateTime { get; set; }
    public Mini[] Minis { get; set; } = new[] { new Mini() };
}

public class Mini
{
    public ExecIniString ExecIniString { get; set; } = new();
}

public class ExecIniString
{
    
}