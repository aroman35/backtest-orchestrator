using System.Text.Json;
using Backtest.Agent.Processes;
using Backtest.Domain.Models;
using FlashPipes.Client;

namespace Backtest.Tests.Infrastructure;

public class FakeProcessExecutor : IProcessExecutor
{ 
    public Task StartProcess(Guid jobId, string binaryName, int core, CancellationToken cancellationToken)
    {
        var pipeClient = FlashPipesClientFactory.New().CreateClient(jobId);
        var startInfo = pipeClient.Get<TaskInfo>("backtest/task");
        var strategySettings = JsonSerializer.Deserialize<FakeStrategySettings>(startInfo.StrategySettings);
        var daysCount = (int)(strategySettings.EndDateTime - strategySettings.StartDateTime).TotalDays;
        var cacheHashes = Enumerable.Range(0, daysCount)
            .Select(x => $"DAY-{strategySettings.StartDateTime.AddDays(x):yyyy-MM-dd}")
            .ToArray();
        var cacheRequest = new CacheRequest(cacheHashes);
        var cacheResponse = pipeClient.Send<CacheRequest, CacheResponse>("cache/request", cacheRequest);
        while (cacheResponse.Entries.Any(x => x.Status is not CacheEntryStatus.Ready))
        {
            if (!cacheResponse.Entries.Any(x => x.Status is CacheEntryStatus.BuildIt))
            {
                cacheResponse = pipeClient.Send<CacheRequest, CacheResponse>("cache/request", new CacheRequest(cacheHashes));
                continue;
            }
            var cacheProgress = 0;
            var cacheProgressRand = new Random();
            while (cacheProgress < 100)
            {
                cacheProgress += cacheProgressRand.Next(2, 10);
                if (cacheProgress > 100)
                    cacheProgress = 100;
                pipeClient.Send("cache/progress", new CacheBuildProgress(cacheProgress));
            }
            pipeClient.Send("cache/result", new CacheBuildResult(true));
            cacheResponse = pipeClient.Send<CacheRequest, CacheResponse>("cache/request", new CacheRequest(cacheHashes));
        }
        
        var progress = 0;
        var progressRand = new Random();
        while (progress < 100)
        {
            progress += progressRand.Next(2, 10);
            if (progress > 100)
                progress = 100;
            pipeClient.Send<BacktestProgress, TaskCancellation>("backtest/progress", new BacktestProgress(progress));
        }

        pipeClient.Send("backtest/result", new TaskResult(
            true,
            JsonSerializer.Serialize(new Dictionary<string, string>()),
            10000000,
            10));

        return Task.CompletedTask;
    }
}