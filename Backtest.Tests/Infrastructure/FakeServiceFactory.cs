using Backtest.Domain.Backtest;
using Backtest.Domain.Job;
using Backtest.Domain.Queries;
using Backtest.Domain.Simulation;
using Moq;

namespace Backtest.Tests.Infrastructure;

public static class FakeServiceFactory
{
    public static IQueriesServiceClient CreateEmptyClient()
    {
        var clientMock = new Mock<IQueriesServiceClient>();
        clientMock.Setup(x => x.GetSimulationsByBacktest(It.IsAny<IBacktest>())).Returns(Task.FromResult(Array.Empty<ISimulation>()));
        clientMock.Setup(x => x.GetJobsBySimulation(It.IsAny<ISimulation>())).Returns(Task.FromResult(Array.Empty<IJob>()));
        return clientMock.Object;
    }
}