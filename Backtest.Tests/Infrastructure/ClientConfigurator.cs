using Backtest.Agent;
using Backtest.Agent.Cache;
using Backtest.Agent.Jobs;
using Backtest.Agent.Pipes;
using Backtest.Agent.Processes;
using Backtest.Domain;
using Backtest.Domain.Agent;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Orleans.Streams;
using Orleans.TestingHost;
using Serilog;
using Serilog.Events;
using Xunit.Sdk;

namespace Backtest.Tests.Infrastructure;

public class ClientConfigurator : IClientBuilderConfigurator
{
    public void Configure(IConfiguration configuration, IClientBuilder clientBuilder)
    {
        var testOutputHelper = new TestOutputHelper().CreateTestLogger(LogEventLevel.Information);
        clientBuilder.AddMemoryStreams(Constants.STREAM_PROVIDER_NAME, stream =>
        {
            stream.ConfigureStreamPubSub(StreamPubSubType.ExplicitGrainBasedOnly);
        });
        clientBuilder.Configure<AgentSettings>(options =>
        {
            var agentName = "test-agent";
            options.AgentName = agentName;
            options.Cores = Enumerable.Range(0, 100).ToArray();
            options.LogsPath = Path.Combine(Path.GetTempPath(), agentName, nameof(AgentSettings.LogsPath));
            options.TelemetryPath = Path.Combine(Path.GetTempPath(), agentName, nameof(AgentSettings.TelemetryPath));
            options.CachePath = Path.Combine(Path.GetTempPath(), agentName, nameof(AgentSettings.CachePath));
            options.BinariesPath = Path.Combine(Path.GetTempPath(), agentName, nameof(AgentSettings.BinariesPath));
            options.CacheMaxSizeBytes = 100_000_000_000;
            options.BinaryName = "BacktestLauncher";
            options.BinaryStorage = Path.Combine(Path.GetTempPath(), agentName, nameof(AgentSettings.BinaryStorage));
            options.BinaryCache = Path.Combine(Path.GetTempPath(), agentName, nameof(AgentSettings.BinaryCache));
            options.BinaryCacheQuota = 100_000_000;
        });
        clientBuilder.ConfigureServices(services =>
        {
            services.AddSingleton<IAgentCacheManager, AgentCacheManager>();
            services.AddSingleton<IAgentCacheFilesAccessor, FakeAgentCacheFilesAccessor>();
            services.AddSingleton<JobsManager>();
            services.AddScoped<IJobExecutor, JobExecutor>();
            services.AddScoped<IProcessExecutor, FakeProcessExecutor>();
            services.AddScoped<IPipeExecutor, PipeExecutor>();
            services.AddNamedPipes();
            services.AddTransient<ILogger>(_ => testOutputHelper);
            services.AddHostedService<JobsFeedService>();
            services.AddHostedService<AgentConnectionService>();
        });
    }
}