﻿using Orleans.TestingHost;

namespace Backtest.Tests.Infrastructure;

public static class TestClusterFactory
{
    public static TestCluster Create()
    {
        var builder = new TestClusterBuilder();
        builder.AddSiloBuilderConfigurator<SiloConfigurator>();
        builder.AddClientBuilderConfigurator<ClientConfigurator>();
        var testCluster = builder.Build();
        testCluster.Deploy();
        return testCluster;
    }
}