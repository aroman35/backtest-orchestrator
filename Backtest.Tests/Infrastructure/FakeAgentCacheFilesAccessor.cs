﻿using System.Globalization;
using System.Text.RegularExpressions;
using Backtest.Agent.Cache;
using Backtest.Domain.MarketDataCache;

namespace Backtest.Tests.Infrastructure;

public class FakeAgentCacheFilesAccessor : IAgentCacheFilesAccessor
{
    public Task<FillMarketDataCacheMetaCommand> ReadMeta(string hash)
    {
        var pattern = @"(\d{4}-\d{2}-\d{2})";
        var match = Regex.Match(hash, pattern);

        var day = DateTime.Parse(match.Value, CultureInfo.InvariantCulture);
        var meta = new FillMarketDataCacheMetaCommand(
            day,
            day,
            100000,
            1000,
            new[] { "BTCUSDT" },
            new[] { "BTCUSDT.gz" });
        return Task.FromResult(meta);
    }

    public void RemoveCache(string hash)
    {
    }
}