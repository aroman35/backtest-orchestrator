﻿using Backtest.Core.Agent;
using Backtest.Core.Job;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Orleans.Streams;
using Orleans.TestingHost;
using Serilog;
using Serilog.Events;
using Xunit.Sdk;
using ILogger = Serilog.ILogger;
using static Backtest.Domain.Constants;

namespace Backtest.Tests.Infrastructure;

public class SiloConfigurator : ISiloConfigurator
{
    public void Configure(ISiloBuilder siloBuilder)
    {
        var testOutputHelper = new TestOutputHelper().CreateTestLogger(LogEventLevel.Information);
        siloBuilder.AddMemoryGrainStorageAsDefault();
        siloBuilder.AddMemoryGrainStorage(REDIS_STORAGE_DB0);
        siloBuilder.AddMemoryGrainStorage(REDIS_STORAGE_DB1);
        siloBuilder.AddMemoryGrainStorage(REDIS_STORAGE_DB2);
        siloBuilder.AddMemoryGrainStorage(REDIS_STORAGE_DB3);
        siloBuilder.AddMemoryGrainStorage(MEMORY_STORAGE);
        siloBuilder.UseInMemoryReminderService();
        siloBuilder.AddMemoryGrainStorage("PubSubStore");
        siloBuilder.ConfigureLogging(logging => logging.SetMinimumLevel(LogLevel.Information).AddSerilog(testOutputHelper));
        siloBuilder.AddMemoryStreams(STREAM_PROVIDER_NAME, stream =>
        {
            stream.ConfigureStreamPubSub(StreamPubSubType.ExplicitGrainBasedOnly);
        });
        siloBuilder.ConfigureServices(services =>
        {
            services.AddTransient<ILogger>(_ => new TestOutputHelper().CreateTestLogger(LogEventLevel.Information));
            services.AddSingleton(FakeServiceFactory.CreateEmptyClient());
            services.Configure<JobExecutionOptions>(options =>
            {
                options.ObserverTtlSec = 60;
                options.CacheTtlSec = 60;
            });
            services.Configure<AgentConnectivitySettings>(options =>
            {
                options.CacheBuildTimeout = 60;
                options.CacheCleanupJobInterval = 60;
                options.CacheCleanupQuotaPercent = 80;
                options.KeepAliveIntervalSec = 120;
            });
        });
    }
}