﻿using Backtest.Domain.Agent;

namespace Backtest.Tests.Infrastructure;

public static class AgentsFactory
{
    public static async Task<IAgent> CreateAgent(this IGrainFactory grainFactory, string agentName, int coresCount)
    {
        var settings = new AgentSettings
        {
            AgentName = agentName,
            Cores = Enumerable.Range(0, coresCount).ToArray(),
            LogsPath = Path.Combine(Path.GetTempPath(), agentName, nameof(AgentSettings.LogsPath)),
            TelemetryPath = Path.Combine(Path.GetTempPath(), agentName, nameof(AgentSettings.TelemetryPath)),
            CachePath = Path.Combine(Path.GetTempPath(), agentName, nameof(AgentSettings.CachePath)),
            BinariesPath = Path.Combine(Path.GetTempPath(), agentName, nameof(AgentSettings.BinariesPath)),
            CacheMaxSizeBytes = 100_000_000_000,
            BinaryName = "BacktestLauncher",
            BinaryStorage = Path.Combine(Path.GetTempPath(), agentName, nameof(AgentSettings.BinaryStorage)),
            BinaryCache = Path.Combine(Path.GetTempPath(), agentName, nameof(AgentSettings.BinaryCache)),
            BinaryCacheQuota = 100_000_000
        };
        var agent = grainFactory.GetGrain<IAgent>(agentName);
        await agent.OnAgentConnected(settings, true);
        return agent;
    }
}