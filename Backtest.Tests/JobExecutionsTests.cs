﻿using Backtest.Domain;
using Backtest.Domain.Backtest;
using Backtest.Tests.Infrastructure;
using Orleans.TestingHost;
using Serilog;
using Shouldly;
using Xunit.Abstractions;
using TaskStatus = Backtest.Domain.TaskStatus;

namespace Backtest.Tests;

public class JobExecutionsTests
{
    private const double WAIT_FACTOR = 100; // For GitHub CI
    private readonly TestCluster _testCluster;
    private readonly ILogger _logger;
    
    private IGrainFactory GrainFactory => _testCluster.GrainFactory;
    private IClusterClient Client => _testCluster.Client;

    public JobExecutionsTests(ITestOutputHelper testOutputHelper)
    {
        _logger = testOutputHelper.CreateTestLogger();
        _testCluster = TestClusterFactory.Create();
    }

    [Fact(DisplayName = "Execute backtest with no cache and split days", Skip = "Skipped until test infrastructure update")]
    public async Task ExecuteBacktestWithNoCacheAndSplitDays()
    {
        var daysCount = 25;
        var paramsCount = 10;
        var backtestCreateCommand = BacktestsFactory.CreateBacktestCommand(daysCount, true, paramsCount);
        var backtest = await GrainFactory.CreateBacktest(backtestCreateCommand);
        await AwaitBacktestToBeCompleted(backtest, daysCount * WAIT_FACTOR);
    }
    
    [Fact(DisplayName = "Execute backtest with no cache and split days off", Skip = "Skipped until test infrastructure update")]
    public async Task ExecuteBacktestWithNoCacheAndNotSplitDays()
    {
        var daysCount = 25;
        var paramsCount = 10;
        var backtestCreateCommand = BacktestsFactory.CreateBacktestCommand(daysCount, false, paramsCount);
        var backtest = await GrainFactory.CreateBacktest(backtestCreateCommand);
        await AwaitBacktestToBeCompleted(backtest, daysCount * WAIT_FACTOR);
    }

    private async Task AwaitStatefulGrainToBeCompleted(IStateUpdates statefulGrain, double timeoutSec)
    {
        var completionSource = new TaskCompletionSource();
        var updatesObserver = new TestStateUpdatesObserver(completionSource, _logger);
        var observer = Client.CreateObjectReference<IStateUpdatesObserver>(updatesObserver);
        await statefulGrain.SubscribeStateUpdates(observer);
        await Should.NotThrowAsync(completionSource.Task.WaitAsync(TimeSpan.FromSeconds(timeoutSec)));
    }

    private async Task AwaitBacktestToBeCompleted(IBacktest backtest, double timeoutSec)
    {
        var simulations = await backtest.GetSimulations();
        var itemsForExecution = simulations.Select(x => AwaitStatefulGrainToBeCompleted(x, timeoutSec)).ToList();
        itemsForExecution.Add(AwaitStatefulGrainToBeCompleted(backtest, timeoutSec));
        await Task.WhenAll(itemsForExecution);
        
        var backtestState = await backtest.GetState();
        backtestState.Summary.Progress.ShouldBe(100);
        backtestState.Summary.Status.ShouldBe(TaskStatus.Finished);
        backtestState.Summary.TotalJobs.ShouldBe(backtestState.Summary.FinishedJobs + backtestState.Summary.FailedJobs);
        
        foreach (var simulation in simulations)
        {
            var simulationState = await simulation.GetState();
            simulationState.Summary.Status.ShouldBe(TaskStatus.Finished);
            simulationState.Summary.Progress.ShouldBe(100);
            simulationState.Summary.TotalJobs.ShouldBe(simulationState.Summary.FinishedJobs + simulationState.Summary.FailedJobs);
        }
    }
}

public class TestStateUpdatesObserver : IStateUpdatesObserver
{
    private readonly TaskCompletionSource _jobFinishedCompletionSource;
    private readonly ILogger _logger;

    public TestStateUpdatesObserver(TaskCompletionSource jobFinishedCompletionSource, ILogger logger)
    {
        _jobFinishedCompletionSource = jobFinishedCompletionSource;
        _logger = logger.ForContext<TestStateUpdatesObserver>();
    }
    public Task StateUpdate(ItemSummary summary)
    {
        _logger.Information("{Summary}", summary);
        if (Math.Abs(summary.Progress - 100d) < 0.001d
            && summary.Status is TaskStatus.Finished or TaskStatus.Failed or TaskStatus.Canceled or TaskStatus.NoMd
            && summary.TotalJobs == summary.FinishedJobs + summary.FailedJobs)
        {
            _jobFinishedCompletionSource.SetResult();
        }
        return Task.CompletedTask;
    }
}