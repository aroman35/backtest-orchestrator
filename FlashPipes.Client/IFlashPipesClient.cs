using System;

namespace FlashPipes.Client
{
    public interface IFlashPipesClient : IDisposable
    {
        TResponse Send<TRequest, TResponse>(string uri, TRequest request, int timeoutMs = 30000);
        void Send<TRequest>(string uri, TRequest request, int timeoutMs = 30000);
        TResponse Get<TResponse>(string uri, int timeoutMs = 30000);
        void SetErrorHandler(Action<string, Exception> errorHandler);
    }
}