﻿using System;
using System.IO.Pipes;
using System.Threading;
using System.Threading.Tasks;
using FlashPipes.Common;
using FlashPipes.Common.Messaging;

namespace FlashPipes.Client
{
    internal class PipeClient : IPipeClient
    {
        private readonly NamedPipeClientStream _pipeClient;
        private readonly Guid _pipeName;
        private Action<string, Exception> _errorHandler;
        
        public bool IsDisposed { get; private set; }

        public PipeClient(Guid pipeName)
        {
            _pipeName = pipeName;
            _pipeClient = new NamedPipeClientStream(
                ".",
                pipeName.ToString(),
                PipeDirection.InOut,
                PipeOptions.Asynchronous);
        }

        public void Send(FlashPipesMessage message, TimeSpan timeout)
        {
            message.PipeName = _pipeName.ToString();
            message.CreatedTimeUtc = DateTime.UtcNow;

            if (!_pipeClient.IsConnected)
                _pipeClient.Connect();

            if (_pipeClient.CanWrite)
            {
                try
                {
                    _pipeClient.WriteMessage(message);
                }
                catch (TaskCanceledException exception)
                {
                    _errorHandler?.Invoke("Timeout reached during the request", exception);
                    throw;
                }
            }
        }

        public bool ReadMessage(out FlashPipesMessage message, TimeSpan timeout)
        {
            using var cancellationTokenSource = new CancellationTokenSource(timeout);
            try
            {
                return _pipeClient.ReadMessage(out message);
            }
            catch (TaskCanceledException exception)
            {
                _errorHandler?.Invoke("Timeout reached during the request", exception);
                throw;
            }
            catch (Exception exception)
            {
                _errorHandler?.Invoke("Error while reading message", exception);
                message = FlashPipesMessage.Default;
                return false;
            }
        }

        public void SetErrorHandler(Action<string, Exception> errorHandler)
        {
            _errorHandler = errorHandler;
        }
        
        public void Dispose()
        {
            if (IsDisposed) return;
            
            _pipeClient.Close();
            _pipeClient.Dispose();
            IsDisposed = true;
        }
    }
}