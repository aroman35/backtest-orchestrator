using System;
using System.Collections.Concurrent;

namespace FlashPipes.Client
{
    public class FlashPipesClientFactory
    {
        private static FlashPipesClientFactory _factory;
        private static readonly object Sync = new();
        private readonly ConcurrentDictionary<Guid, IPipeClient> _clients;

        public static FlashPipesClientFactory New()
        {
            lock (Sync) _factory ??= new FlashPipesClientFactory();
            return _factory;
        }
        
        private FlashPipesClientFactory()
        {
            _clients = new ConcurrentDictionary<Guid, IPipeClient>();
        }
        
        public IFlashPipesClient CreateClient(Guid pipeName)
        {
            lock (Sync)
            {
                if (_clients.TryGetValue(pipeName, out var existingClient) && !existingClient.IsDisposed)
                    return new FlashPipesClient(existingClient);
                
                existingClient = new PipeClient(pipeName);
                _clients.TryAdd(pipeName, existingClient);
                return new FlashPipesClient(existingClient);
            }
        }

        public IFlashPipesClient CreateClient(Guid pipeName, Action<string, Exception> errorHandler)
        {
            var client = CreateClient(pipeName);
            client.SetErrorHandler(errorHandler);
            return client;
        }
    }
}