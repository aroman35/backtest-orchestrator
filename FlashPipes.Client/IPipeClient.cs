﻿using System;
using FlashPipes.Common.Messaging;

namespace FlashPipes.Client
{
    internal interface IPipeClient : IDisposable
    {
        bool IsDisposed { get; }
        void Send(FlashPipesMessage message, TimeSpan timeout);
        void SetErrorHandler(Action<string, Exception> errorHandler);
        bool ReadMessage(out FlashPipesMessage message, TimeSpan timeout);
    }
}