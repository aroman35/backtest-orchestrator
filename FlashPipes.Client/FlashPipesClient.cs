﻿using System;
using FlashPipes.Common.Exceptions;
using FlashPipes.Common.Messaging;

namespace FlashPipes.Client
{
    public class FlashPipesClient : IFlashPipesClient
    {
        private readonly IPipeClient _pipeClient;
        private static readonly object Sync = new();

        internal FlashPipesClient(IPipeClient pipeClient)
        {
            _pipeClient = pipeClient;
        }

        public TResponse Send<TRequest, TResponse>(string uri, TRequest request, int timeoutMs = 30000)
        {
            var requestMessage = new FlashPipesMessage();
            requestMessage.SetPayload(request);
            requestMessage.MessageType = FlashPipesMessageType.Request;
            requestMessage.CreatedTimeUtc = DateTime.UtcNow;
            requestMessage.Route = uri;

            lock (Sync)
            {
                _pipeClient.Send(requestMessage, TimeSpan.FromMilliseconds(timeoutMs));
                
                if (!_pipeClient.ReadMessage(out var responseMessage, TimeSpan.FromMilliseconds(timeoutMs)))
                    throw new FlashPipesPipeException($"Nothing received", requestMessage.Route);

                return HandleResponse<TResponse>(responseMessage);
            }
        }

        public void Send<TRequest>(string uri, TRequest request, int timeoutMs = 30000)
        {
            var requestMessage = new FlashPipesMessage();
            requestMessage.SetPayload(request);
            requestMessage.MessageType = FlashPipesMessageType.Request;
            requestMessage.CreatedTimeUtc = DateTime.UtcNow;
            requestMessage.Route = uri;
            requestMessage.IsPost = true;

            lock (Sync) _pipeClient.Send(requestMessage, TimeSpan.FromMilliseconds(timeoutMs));
        }

        public TResponse Get<TResponse>(string uri, int timeoutMs = 30000)
        {
            var requestMessage = new FlashPipesMessage();
            requestMessage.MessageType = FlashPipesMessageType.Request;
            requestMessage.CreatedTimeUtc = DateTime.UtcNow;
            requestMessage.Route = uri;

            lock (Sync)
            {
                _pipeClient.Send(requestMessage, TimeSpan.FromMilliseconds(timeoutMs));
                
                if (!_pipeClient.ReadMessage(out var responseMessage, TimeSpan.FromMilliseconds(timeoutMs)))
                    throw new FlashPipesPipeException($"Nothing received within", requestMessage.Route);

                if (responseMessage.IsError)
                    throw new FlashPipesPipeException(requestMessage.ErrorMessage, requestMessage.Route);
                return HandleResponse<TResponse>(responseMessage);
            }
        }

        public void SetErrorHandler(Action<string, Exception> errorHandler)
        {
            lock (Sync) _pipeClient.SetErrorHandler(errorHandler);
        }

        private TResponse HandleResponse<TResponse>(FlashPipesMessage pipeMessage)
        {
            if (pipeMessage.IsError)
                throw new FlashPipesPipeException(pipeMessage.ErrorMessage, pipeMessage.Route);
            if (pipeMessage.MessageType == FlashPipesMessageType.Request)
                throw new FlashPipesPipeException("Received response but expected request", pipeMessage.Route);
                
            return (TResponse) pipeMessage.GetPayload(typeof(TResponse));
        }

        /// <summary>
        /// When it is called disposed, the pipe stream disposes too. If it is the only client for the pipe, the pipe becomes broken
        /// </summary>
        public void Dispose()
        {
            _pipeClient.Dispose();
        }
    }
}