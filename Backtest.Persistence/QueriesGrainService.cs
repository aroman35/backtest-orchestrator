﻿using Backtest.Domain.Backtest;
using Backtest.Domain.Job;
using Backtest.Domain.Queries;
using Backtest.Domain.Scheduler;
using Backtest.Domain.Simulation;
using Backtest.Domain.States;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using Orleans.Concurrency;
using Orleans.Runtime;
using Orleans.Runtime.Services;
using TaskStatus = Backtest.Domain.TaskStatus;

namespace Backtest.Persistence;

[Reentrant]
public class QueriesGrainService : GrainService, IQueriesGrainService
{
    private readonly IGrainFactory _grainFactory;
    private readonly IMongoDbContext _mongoDbContext;

    public QueriesGrainService(
        IServiceProvider services,
        GrainId id,
        Orleans.Runtime.Silo silo,
        ILoggerFactory loggerFactory,
        IGrainFactory grainFactory, IMongoDbContext mongoDbContext)
        : base(id, silo, loggerFactory)
    {
        _grainFactory = grainFactory;
        _mongoDbContext = mongoDbContext;
    }

    public async Task<ISimulation[]> GetSimulationsByBacktest(IBacktest backtest)
    {
        var simulationsFilter = Builders<PersistenceStateModel<SimulationState>>.Filter.Eq(x => x.State.BacktestId, backtest.GetPrimaryKey());
        var simulationIds = await _mongoDbContext.Collection<SimulationState>()
            .Aggregate()
            .Match(simulationsFilter)
            .Project(x => new { x.Id })
            .ToListAsync();
        
        var simulations = simulationIds.Select(x => _grainFactory.GetGrain<ISimulation>(x.Id)).ToArray();
        return simulations;
    }

    public async Task<IJob[]> GetJobsBySimulation(ISimulation simulation)
    {
        var jobsFilter = Builders<PersistenceStateModel<JobState>>.Filter.Eq(x => x.State.SimulationId, simulation.GetPrimaryKey());
        var jobIds = await _mongoDbContext.Collection<JobState>()
            .Aggregate()
            .Match(jobsFilter)
            .Project(x => new { x.Id })
            .ToListAsync();
        
        var jobs = jobIds.Select(x => _grainFactory.GetGrain<IJob>(x.Id)).ToArray();
        return jobs;
    }

    public async Task<QueueItem[]> GetInitialQueue()
    {
        var filterDefinition = Builders<PersistenceStateModel<JobState>>.Filter.Eq(x => x.State.Status, TaskStatus.Queued);
        var sortDefinition = Builders<PersistenceStateModel<JobState>>.Sort.Ascending(x => x.State.Created);
        var jobIds = await _mongoDbContext.Collection<JobState>()
            .Aggregate()
            .Match(filterDefinition)
            .Sort(sortDefinition)
            .Project(x => new { x.Id })
            .ToListAsync();
        
        var jobs = jobIds.Select(x => _grainFactory.GetGrain<IJob>(x.Id)).ToArray();
        var queueItems = await Task.WhenAll(jobs.Select(x => x.CreateQueueItem()));
        return queueItems;
    }
}

public class QueriesServiceClient : GrainServiceClient<IQueriesGrainService>, IQueriesServiceClient
{
    private IQueriesGrainService GrainService => GetGrainService(CurrentGrainReference.GrainId);
    public QueriesServiceClient(IServiceProvider serviceProvider) : base(serviceProvider)
    {
    }

    public Task<ISimulation[]> GetSimulationsByBacktest(IBacktest backtest) => GrainService.GetSimulationsByBacktest(backtest);

    public Task<IJob[]> GetJobsBySimulation(ISimulation simulation) => GrainService.GetJobsBySimulation(simulation);
    public Task<QueueItem[]> GetInitialQueue() => GrainService.GetInitialQueue();
}