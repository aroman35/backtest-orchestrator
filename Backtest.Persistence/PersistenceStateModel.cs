namespace Backtest.Persistence;

public class PersistenceStateModel<TState>
{
    public Guid Id { get; set; }
    public Guid Etag { get; set; }
    public TState State { get; set; }
}