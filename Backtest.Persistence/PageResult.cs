namespace Backtest.Persistence;

public class PageResult<T>
{
    public ICollection<PersistenceStateModel<T>> Items { get; set; }
    public int Page { get; set; }
    public long TotalPages { get; set; }
    public int ItemsPerPage { get; set; }

    public static PageResult<T> Empty(int page, int itemsPerPage, long totalPages) => new()
    {
        Items = Array.Empty<PersistenceStateModel<T>>(),
        Page = page,
        TotalPages = totalPages,
        ItemsPerPage = itemsPerPage
    };
}

public class PageQueryRequest
{
    public int Page { get; set; }
    public int ItemsPerPage { get; set; }
}