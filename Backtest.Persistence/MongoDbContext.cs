﻿using System.ComponentModel;
using System.Reflection;
using Backtest.Domain;
using Backtest.Domain.States;
using Backtest.Domain.States.Settings;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using Serilog;

namespace Backtest.Persistence;

public class MongoDbContext : IMongoDbContext
{
    private const string STATE_COLLECTION_PREFIX = "state-";
    private readonly IMongoDatabase _database;
    private readonly ILogger _logger;

    public MongoDbContext(IOptions<DatabaseSettings> settings, ILogger logger)
    {
        var client = new MongoClient(settings.Value.ConnectionString);
        _database = client.GetDatabase(settings.Value.DatabaseName);
        _logger = logger.ForContext<MongoDbContext>();
    }

    public IMongoCollection<PersistenceStateModel<TState>> Collection<TState>(string collectionName)
    {
        if (string.IsNullOrEmpty(collectionName))
        {
            var type = typeof(TState);
            var displayName = type.GetCustomAttribute<DisplayNameAttribute>();
            if (displayName is null)
                throw new ApplicationException($"No collection name or display name provided for type {type.Name}");
            collectionName = displayName.DisplayName;
        }
        
        var collection = _database.GetCollection<PersistenceStateModel<TState>>(STATE_COLLECTION_PREFIX + collectionName);
        return collection;
    }

    public async Task<PersistenceStateModel<TState>> GetById<TState>(Guid id, CancellationToken cancellationToken, string collectionName = null)
    {
        var state = await Collection<TState>(collectionName)
            .Find(Builders<PersistenceStateModel<TState>>.Filter.Eq(x => x.Id, id))
            .FirstOrDefaultAsync(cancellationToken);

        return state;
    }

    private async Task InitializeCollection<TState>(string collectionName = null)
    {
        if (string.IsNullOrEmpty(collectionName))
        {
            var type = typeof(TState);
            var displayName = type.GetCustomAttribute<DisplayNameAttribute>();
            if (displayName is null)
                throw new ApplicationException($"No collection name or display name provided for type {type.Name}");
            collectionName = displayName.DisplayName;
        }
        await _database.CreateCollectionAsync(STATE_COLLECTION_PREFIX + collectionName);
        BsonClassMap.RegisterClassMap<PersistenceStateModel<TState>>(classMap =>
        {
            classMap.MapIdProperty(x => x.Id).SetElementName("_id");
            classMap.MapProperty(x => x.Etag).SetElementName("_etag");
            classMap.MapProperty(x => x.State).SetElementName("_doc");
            classMap.AutoMap();
        });
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        // BsonSerializer.RegisterSerializer(new GuidSerializer(BsonType.String));
        await InitializeCollection<BacktestState>();
        await InitializeCollection<SimulationState>();
        await InitializeCollection<JobState>();
        await InitializeCollection<BacktestSettings>();
        await InitializeCollection<StrategySettings>();
        await InitializeCollection<UserParams>();
        await InitializeCollection<JobResult>();
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}