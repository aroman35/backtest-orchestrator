using Backtest.Domain.States;
using MediatR;

namespace Backtest.Persistence.Queries.Backtest;

public class BacktestsPageQuery : PageQueryRequest, IRequest<PageResult<BacktestState>>
{
    #region Filters
    public string NameMatch { get; set; }
    public DateTime? CreatedAfter { get; set; }
    public DateTime? CreatedBefore { get; set; }
    public bool? SplitDays { get; set; }
    public string BinaryNameMatch { get; set; }
    public double? MaxProgress { get; set; }
    public string InStatus { get; set; }
    public string[] Labels { get; set; } = Array.Empty<string>();
    #endregion
    
    #region Sorting
    public bool? Created { get; set; }
    public bool? BinaryName { get; set; }
    public bool? Progress { get; set; }
    public bool? IsFinalStatus { get; set; }
    #endregion
}