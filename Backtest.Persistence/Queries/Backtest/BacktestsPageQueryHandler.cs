using Backtest.Domain;
using Backtest.Domain.States;
using MediatR;
using MongoDB.Driver;
using static System.Enum;
using TaskStatus = Backtest.Domain.TaskStatus;

namespace Backtest.Persistence.Queries.Backtest;

public class BacktestsPageQueryHandler : IRequestHandler<BacktestsPageQuery, PageResult<BacktestState>>
{
    private readonly IMongoDbContext _dbContext;

    public BacktestsPageQueryHandler(IMongoDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<PageResult<BacktestState>> Handle(BacktestsPageQuery request, CancellationToken cancellationToken)
    {
        var backtestsCollection = _dbContext.Collection<BacktestState>(Constants.BACKTEST);

        var filterDefinitions = new List<FilterDefinition<PersistenceStateModel<BacktestState>>>();
        if (!string.IsNullOrEmpty(request.NameMatch))
            filterDefinitions.Add(Builders<PersistenceStateModel<BacktestState>>.Filter.Regex(x => x.State.Name, request.NameMatch));
        if (request.CreatedAfter.HasValue)
            filterDefinitions.Add(Builders<PersistenceStateModel<BacktestState>>.Filter.Gte(x => x.State.Created, request.CreatedAfter.Value));
        if (request.CreatedBefore.HasValue)
            filterDefinitions.Add(Builders<PersistenceStateModel<BacktestState>>.Filter.Lte(x => x.State.Created, request.CreatedBefore.Value));
        if (!string.IsNullOrEmpty(request.InStatus))
        {
            var statuses = request.InStatus.Split(',').Select(x => TryParse(x, out TaskStatus status) ? status : TaskStatus.NotDefined).Where(x => x is not TaskStatus.NotDefined);
            filterDefinitions.Add(Builders<PersistenceStateModel<BacktestState>>.Filter.In(x => x.State.Summary.Status, statuses));
        }
        if (request.Labels.Any())
            filterDefinitions.Add(Builders<PersistenceStateModel<BacktestState>>.Filter.AnyIn(x => x.State.Labels, request.Labels));
        if (request.SplitDays.HasValue)
            filterDefinitions.Add(Builders<PersistenceStateModel<BacktestState>>.Filter.Eq(x => x.State.SplitDays, request.SplitDays.Value));
        if (!string.IsNullOrEmpty(request.BinaryNameMatch))
            filterDefinitions.Add(Builders<PersistenceStateModel<BacktestState>>.Filter.Regex(x => x.State.BinaryName, request.BinaryNameMatch));
        if (request.MaxProgress.HasValue)
            filterDefinitions.Add(Builders<PersistenceStateModel<BacktestState>>.Filter.Lte(x => x.State.Summary.Progress, request.MaxProgress.Value));

        var filterDefinition = filterDefinitions.Any()
            ? Builders<PersistenceStateModel<BacktestState>>.Filter.And(filterDefinitions)
            : Builders<PersistenceStateModel<BacktestState>>.Filter.Empty;
        
        var sortDefinitions = new List<SortDefinition<PersistenceStateModel<BacktestState>>>();
        if (request.Created.HasValue)
            sortDefinitions.Add(request.Created.Value
                ? Builders<PersistenceStateModel<BacktestState>>.Sort.Ascending(x => x.State.Created)
                : Builders<PersistenceStateModel<BacktestState>>.Sort.Descending(x => x.State.Created));
        if (request.BinaryName.HasValue)
            sortDefinitions.Add(request.BinaryName.Value
                ? Builders<PersistenceStateModel<BacktestState>>.Sort.Ascending(x => x.State.BinaryName)
                : Builders<PersistenceStateModel<BacktestState>>.Sort.Descending(x => x.State.BinaryName));
        if (request.Progress.HasValue)
            sortDefinitions.Add(request.Progress.Value
                ? Builders<PersistenceStateModel<BacktestState>>.Sort.Ascending(x => x.State.Summary.Progress)
                : Builders<PersistenceStateModel<BacktestState>>.Sort.Descending(x => x.State.Summary.Progress));
        if (request.IsFinalStatus.HasValue)
            sortDefinitions.Add(request.IsFinalStatus.Value
                ? Builders<PersistenceStateModel<BacktestState>>.Sort.Ascending(x => x.State.Summary.IsFinalStatus)
                : Builders<PersistenceStateModel<BacktestState>>.Sort.Descending(x => x.State.Summary.IsFinalStatus));

        var sortDefinition = sortDefinitions.Any()
            ? Builders<PersistenceStateModel<BacktestState>>.Sort.Combine(sortDefinitions)
            : Builders<PersistenceStateModel<BacktestState>>.Sort.Descending(x => x.State.Created);

        var totalItems = await backtestsCollection.Find(filterDefinition).CountDocumentsAsync(cancellationToken);
        var totalPages = totalItems / request.ItemsPerPage + 1;

        if (totalPages < request.Page)
            return PageResult<BacktestState>.Empty(request.Page, request.ItemsPerPage, totalPages);
        
        using var cursor = await backtestsCollection.FindAsync(
            filterDefinition,
            new FindOptions<PersistenceStateModel<BacktestState>>
            {
                Sort = sortDefinition,
                Limit = request.ItemsPerPage,
                Skip = (request.Page - 1) * request.ItemsPerPage
            }, cancellationToken);

        var items = await cursor.ToListAsync(cancellationToken);

        return new PageResult<BacktestState>
        {
            Items = items,
            ItemsPerPage = request.ItemsPerPage,
            Page = request.Page,
            TotalPages = totalPages
        };
    }
}