﻿using Backtest.Domain.States;
using MediatR;

namespace Backtest.Persistence.Queries.Backtest;

public record BacktestsListQuery() : IStreamRequest<PersistenceStateModel<BacktestState>>;