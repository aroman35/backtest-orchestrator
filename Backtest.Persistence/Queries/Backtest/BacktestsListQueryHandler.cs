using System.Runtime.CompilerServices;
using Backtest.Domain;
using Backtest.Domain.States;
using MediatR;
using MongoDB.Driver;

namespace Backtest.Persistence.Queries.Backtest;

public class BacktestsListQueryHandler : IStreamRequestHandler<BacktestsListQuery, PersistenceStateModel<BacktestState>>
{
    private readonly IMongoDbContext _mongoDbContext;

    public BacktestsListQueryHandler(IMongoDbContext mongoDbContext)
    {
        _mongoDbContext = mongoDbContext;
    }

    public async IAsyncEnumerable<PersistenceStateModel<BacktestState>> Handle(BacktestsListQuery request, [EnumeratorCancellation] CancellationToken cancellationToken)
    {
        using var cursor = await _mongoDbContext.Collection<BacktestState>(Constants.BACKTEST).FindAsync(Builders<PersistenceStateModel<BacktestState>>.Filter.Empty, cancellationToken: cancellationToken);
        while (await cursor.MoveNextAsync(cancellationToken))
        {
            foreach (var stateModel in cursor.Current)
            {
                yield return stateModel;
            }
        }
    }
}