﻿using System.Runtime.CompilerServices;
using Backtest.Domain;
using Backtest.Domain.States;
using MediatR;
using MongoDB.Driver;

namespace Backtest.Persistence.Queries.Jobs;

public record JobResultsBySimulationIdQuery(Guid SimulationId) : IStreamRequest<PersistenceStateModel<JobResult>>;

public class JobResultsBySimulationIdQueryHandler : IStreamRequestHandler<JobResultsBySimulationIdQuery, PersistenceStateModel<JobResult>>
{
    private readonly IMongoDbContext _dbContext;

    public JobResultsBySimulationIdQueryHandler(IMongoDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async IAsyncEnumerable<PersistenceStateModel<JobResult>> Handle(JobResultsBySimulationIdQuery request, [EnumeratorCancellation] CancellationToken cancellationToken)
    {
        using var cursor = await _dbContext.Collection<JobResult>(Constants.JOB_RESULT).FindAsync(Builders<PersistenceStateModel<JobResult>>.Filter.Eq(x => x.State.SimulationId, request.SimulationId), cancellationToken: cancellationToken);
        while (await cursor.MoveNextAsync(cancellationToken))
        {
            foreach (var stateModel in cursor.Current)
            {
                yield return stateModel;
            }
        }
    }
}