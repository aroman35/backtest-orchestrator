using System.Runtime.CompilerServices;
using Backtest.Domain;
using Backtest.Domain.States;
using MediatR;
using MongoDB.Driver;

namespace Backtest.Persistence.Queries.Jobs;

public class JobsBySimulationIdQueryHandler : IStreamRequestHandler<JobsBySimulationIdQuery, PersistenceStateModel<JobState>>
{
    private readonly IMongoDbContext _dbContext;

    public JobsBySimulationIdQueryHandler(IMongoDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async IAsyncEnumerable<PersistenceStateModel<JobState>> Handle(JobsBySimulationIdQuery request, [EnumeratorCancellation] CancellationToken cancellationToken)
    {
        using var cursor = await _dbContext.Collection<JobState>(Constants.JOB).FindAsync(Builders<PersistenceStateModel<JobState>>.Filter.Eq(x => x.State.SimulationId, request.SimulationId), cancellationToken: cancellationToken);
        while (await cursor.MoveNextAsync(cancellationToken))
        {
            foreach (var stateModel in cursor.Current)
            {
                yield return stateModel;
            }
        }
    }
}