using Backtest.Domain.States;
using MediatR;

namespace Backtest.Persistence.Queries.Jobs;

public record JobsBySimulationIdQuery(Guid SimulationId) : IStreamRequest<PersistenceStateModel<JobState>>;