using Backtest.Domain.States;
using MediatR;

namespace Backtest.Persistence.Queries.Simulation;

public record SimulationsByBacktestIdQuery(Guid BacktestId) : IStreamRequest<PersistenceStateModel<SimulationState>>;