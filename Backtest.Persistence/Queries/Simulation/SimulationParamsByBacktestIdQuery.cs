﻿using MediatR;

namespace Backtest.Persistence.Queries.Simulation;

public record SimulationParamsByBacktestIdQuery (Guid BacktestId) : IRequest<Dictionary<Guid, Dictionary<string, string>>>;