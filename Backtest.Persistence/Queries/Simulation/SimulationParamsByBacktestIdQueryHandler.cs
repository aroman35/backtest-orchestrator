﻿using Backtest.Domain;
using Backtest.Domain.States;
using Backtest.Domain.States.Settings;
using MediatR;
using MongoDB.Driver;

namespace Backtest.Persistence.Queries.Simulation;

public class SimulationParamsByBacktestIdQueryHandler : IRequestHandler<SimulationParamsByBacktestIdQuery, Dictionary<Guid, Dictionary<string, string>>>
{
    private readonly IMongoDbContext _dbContext;

    public SimulationParamsByBacktestIdQueryHandler(IMongoDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<Dictionary<Guid, Dictionary<string, string>>> Handle(SimulationParamsByBacktestIdQuery request, CancellationToken cancellationToken)
    {
        var simulationsFilter = Builders<PersistenceStateModel<SimulationState>>.Filter.Eq(x => x.State.BacktestId, request.BacktestId);
        var simulationIdsQueryResult = await _dbContext.Collection<SimulationState>(Constants.SIMULATION)
            .Aggregate()
            .Match(simulationsFilter)
            .Project(x => new { SimulationId = x.Id })
            .ToListAsync(cancellationToken: cancellationToken);

        var simulationIds = simulationIdsQueryResult.Select(x => x.SimulationId);

        var settingsFilter = Builders<PersistenceStateModel<StrategySettings>>.Filter.In(x => x.Id, simulationIds);
        var strategySettings = await _dbContext.Collection<StrategySettings>(Constants.STRATEGY_SETTINGS)
            .Aggregate()
            .Match(settingsFilter)
            .Project(x => new { SimulationId = x.Id, AppliedParams = x.State.AppliedUserParams })
            .ToListAsync(cancellationToken);

        return strategySettings.ToDictionary(x => x.SimulationId, x => x.AppliedParams);
    }
}