using System.Runtime.CompilerServices;
using Backtest.Domain;
using Backtest.Domain.States;
using MediatR;
using MongoDB.Driver;

namespace Backtest.Persistence.Queries.Simulation;

public class SimulationsByBacktestIdQueryHandler : IStreamRequestHandler<SimulationsByBacktestIdQuery, PersistenceStateModel<SimulationState>>
{
    private readonly IMongoDbContext _dbContext;

    public SimulationsByBacktestIdQueryHandler(IMongoDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async IAsyncEnumerable<PersistenceStateModel<SimulationState>> Handle(SimulationsByBacktestIdQuery request, [EnumeratorCancellation] CancellationToken cancellationToken)
    {
        var filter = Builders<PersistenceStateModel<SimulationState>>.Filter.Eq(x => x.State.BacktestId, request.BacktestId);
        using var cursor = await _dbContext.Collection<SimulationState>(Constants.SIMULATION).FindAsync(filter, cancellationToken: cancellationToken);
        while (await cursor.MoveNextAsync(cancellationToken))
        {
            foreach (var stateModel in cursor.Current)
            {
                yield return stateModel;
            }
        }
    }
}