using System.ComponentModel;
using System.Reflection;
using MediatR;

namespace Backtest.Persistence.Queries;

public record GenericStateByIdQuery<TState>(Guid Id) : IRequest<PersistenceStateModel<TState>>;

public class GenericStateByIdQueryHandler<TState> : IRequestHandler<GenericStateByIdQuery<TState>, PersistenceStateModel<TState>>
{
    private readonly IMongoDbContext _dbContext;

    public GenericStateByIdQueryHandler(IMongoDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public Task<PersistenceStateModel<TState>> Handle(GenericStateByIdQuery<TState> request, CancellationToken cancellationToken)
    {
        var displayName = typeof(TState).GetCustomAttribute<DisplayNameAttribute>();
        if (displayName is null)
            throw new ApplicationException("State name is not defined");
        
        return _dbContext.GetById<TState>(request.Id, cancellationToken);
    }
}