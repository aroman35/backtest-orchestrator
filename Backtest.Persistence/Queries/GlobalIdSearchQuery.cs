using Backtest.Domain;
using Backtest.Domain.States;
using MediatR;
using MongoDB.Driver;
using TaskStatus = Backtest.Domain.TaskStatus;

namespace Backtest.Persistence.Queries;

public record GlobalIdSearchQuery(Guid Id) : IRequest<GlobalIdSearchResult>;

public record GlobalIdSearchResult(Guid Id, StateType Type, string Name, double Progress, TaskStatus Status);

public enum StateType
{
    NotFound = 0,
    Backtest = 1,
    Simulation = 2,
    Job = 3
}

public class GlobalIdSearchQueryHandler : IRequestHandler<GlobalIdSearchQuery, GlobalIdSearchResult>
{
    private readonly IMongoDbContext _dbContext;

    public GlobalIdSearchQueryHandler(IMongoDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<GlobalIdSearchResult> Handle(GlobalIdSearchQuery request, CancellationToken cancellationToken)
    {
        var backtest = await _dbContext.GetById<BacktestState>(request.Id, cancellationToken);
        if (backtest is not null)
            return new GlobalIdSearchResult(backtest.Id, StateType.Backtest, backtest.State.Name, backtest.State.Summary.Progress, backtest.State.Summary.Status);

        var simulation = await _dbContext.GetById<SimulationState>(request.Id, cancellationToken);
        if (simulation is not null)
            return new GlobalIdSearchResult(simulation.Id, StateType.Simulation, simulation.State.Name, simulation.State.Summary.Progress, simulation.State.Summary.Status);

        var job = await _dbContext.GetById<JobState>(request.Id, cancellationToken);
        if (job is not null)
            return new GlobalIdSearchResult(job.Id, StateType.Job, job.State.Name, job.State.Progress, job.State.Status);

        return new GlobalIdSearchResult(Guid.Empty, StateType.NotFound, string.Empty, 0, TaskStatus.NotDefined);
    }
}