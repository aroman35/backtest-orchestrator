using Microsoft.Extensions.Hosting;
using MongoDB.Driver;

namespace Backtest.Persistence;

public interface IMongoDbContext : IHostedService
{
    IMongoCollection<PersistenceStateModel<TState>> Collection<TState>(string collectionName = null);
    Task<PersistenceStateModel<TState>> GetById<TState>(Guid id, CancellationToken cancellationToken, string collectionName = null);
}