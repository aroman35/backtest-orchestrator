﻿namespace Backtest.Domain.Scheduler;

[GenerateSerializer]
public class QueueItem
{
    [Id(0)]
    public Guid Id { get; set; } = Guid.NewGuid();
    [Id(1)]
    public Guid JobId { get; set; }
    [Id(2)]
    public QueueItemStatus Status { get; set; }
    [Id(3)]
    public string[] AgentNames { get; set; } = Array.Empty<string>();
    [Id(4)]
    public DateTime Created { get; set; }
    [Id(5)]
    public DateTime? Updated { get; set; }
    [Id(6)]
    public bool IsRetry { get; set; }
}

[GenerateSerializer]
public enum QueueItemStatus
{
    Queued,
    Pending,
    Confirmed,
}