﻿using Backtest.Domain.Agent;
using Backtest.Domain.Job;

namespace Backtest.Domain.Scheduler;

public interface ISchedulerConnector : IGrainWithGuidKey
{
    Task AddAgent(IAgent agent, int totalSlots);
    Task RemoveAgent(IAgent agent);
    Task<List<string>> GetAgents();
    Task<List<IJob>> JobsRequest(IAgent agent, int count);
    Task<string> AgentRequest();
    Task<ScheduleInfo> GetScheduleInfo();
}

[GenerateSerializer]
public record ScheduleInfo(int TotalInQueue, int TotalInMainQueues, int TotalInMainRetryQueue, int TotalInDirectQueues, int TotalInDirectRetryQueue, Dictionary<string, int> AgentSlots);