﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using Orleans.Configuration;
using Orleans.Providers.MongoDB.Configuration;
using Orleans.Streams.Kafka.Config;
using Serilog;
using static Backtest.Domain.Constants;

namespace Backtest.Domain;

public static class ConfigurationExtensions
{
    public static IClientBuilder ConfigureBacktestClient(
        this IClientBuilder clientBuilder,
        IConfiguration configuration,
        string instanceName,
        bool withStreams = true)
    {
        var kafkaStreamSettings = configuration.GetSection(nameof(KafkaStreamsOptions)).Get<KafkaStreamsOptions>();
        var databaseSettings = configuration.GetSection(nameof(DatabaseSettings)).Get<DatabaseSettings>();
        ArgumentNullException.ThrowIfNull(kafkaStreamSettings);
        ArgumentNullException.ThrowIfNull(databaseSettings);

        clientBuilder
            .UseMongoDBClient(_ => MongoClientSettings.FromConnectionString(databaseSettings.ConnectionString))
            .UseMongoDBClustering(options =>
            {
                options.DatabaseName = databaseSettings.DatabaseName;
                options.Strategy = MongoDBMembershipStrategy.Multiple;
            })
            .Configure<ConnectionOptions>(options =>
            {
                options.ConnectionRetryDelay = TimeSpan.FromSeconds(1);
                options.OpenConnectionTimeout = TimeSpan.FromMinutes(10);
            })
            .Configure<ClusterOptions>(options =>
            {
                options.ClusterId = "backtest";
                options.ServiceId = instanceName;
            });
        clientBuilder.UseConnectionRetryFilter<ConnectionRetryService>();

        if (withStreams)
            clientBuilder.AddKafkaStreams(kafkaStreamSettings);
        return clientBuilder;
    }

    private static void AddKafkaStreams(this IClientBuilder clientBuilder, KafkaStreamsOptions kafkaStreamSettings)
    {
        clientBuilder.
            AddKafka(STREAM_PROVIDER_NAME)
            .WithOptions(ConfigureOptions(kafkaStreamSettings))
            .AddJson()
            .Build();
    }
    
    public static void AddKafkaStreams(this ISiloBuilder clientBuilder, KafkaStreamsOptions kafkaStreamSettings)
    {
        clientBuilder
            .AddKafka(STREAM_PROVIDER_NAME)
            .WithOptions(ConfigureOptions(kafkaStreamSettings))
            .AddJson()
            .Build();
    }

    private static Action<KafkaStreamOptions> ConfigureOptions(KafkaStreamsOptions kafkaStreamSettings)
    {
        var topicConfiguration = new TopicCreationConfig
        {
            AutoCreate = kafkaStreamSettings.AutoCreate,
            Partitions = kafkaStreamSettings.PartitionsCount,
            RetentionPeriodInMs = kafkaStreamSettings.RetentionPeriodInMs
        };

        return options =>
        {
            options.BrokerList = kafkaStreamSettings.BrokersList;
            options.ConsumerGroupId = kafkaStreamSettings.ConsumerGroupId;
            options.ConsumeMode = kafkaStreamSettings.ConsumeMode;
            options.SaslMechanism = SaslMechanism.Plain;
            options.SecurityProtocol = SecurityProtocol.Plaintext;

            options.AddTopic(STATUS_UPDATES_STREAM, topicConfiguration);
            options.AddTopic(JOB_FINISHED_STREAM, topicConfiguration);
            options.AddTopic(JOB_RETRY_STREAM, topicConfiguration);
            options.AddTopic(LAUNCH_JOB_STREAM, topicConfiguration);
            options.AddTopic(BACKTEST_CANCELLATION_STREAM, topicConfiguration);
            options.AddTopic(JOBS_QUEUE_STREAM, topicConfiguration);
            options.AddTopic(LAUNCH_CONFIRMED_STREAM, topicConfiguration);
            options.AddTopic(JOB_RELEASED_STREAM, topicConfiguration);
        };
    }
}

public class ConnectionRetryService : IClientConnectionRetryFilter
{
    private readonly ILogger _logger;

    public ConnectionRetryService(ILogger logger)
    {
        _logger = logger.ForContext<ConnectionRetryService>();
    }

    public async Task<bool> ShouldRetryConnectionAttempt(Exception exception, CancellationToken cancellationToken)
    {
        _logger.Error(exception, "Connection to cluster was lost");
        await Task.Delay(1000, cancellationToken);
        return true;
    }
}