using Backtest.Domain.Agent;
using Backtest.Domain.Backtest;
using Backtest.Domain.MarketDataCache;
using Backtest.Domain.Models;
using Backtest.Domain.Scheduler;
using Backtest.Domain.Simulation;
using Backtest.Domain.States;
using Orleans.Concurrency;

namespace Backtest.Domain.Job;

public interface IJob : IGrainWithGuidKey, IStateUpdates, IStatefulGrain<JobState>
{
    Task Create(IBacktest backtest, ISimulation simulation, CreateJobCommand createCommand, GrainCancellationToken cancellationToken);
    [AlwaysInterleave]
    Task<TaskInfo> GetStartInfo();
    /// <summary>
    /// Subscribe to job events
    /// </summary>
    /// <param name="jobObserver"></param>
    /// <returns>Subscription alive time seconds</returns>
    Task<double> Subscribe(IJobObserver jobObserver);
    Task SentForExecution(IAgent agent);
    Task Launched(int core);
    Task JobFinished(JobFinishedCommand command);
    Task MapCache(string hash, IMarketDataCache marketDataCache);
    Task<bool> TryBuildCache(IMarketDataCache marketDataCache);
    Task CacheBuildProgress(double progress);
    Task CacheBuildComplete(CacheBuildCompleteResult result);
    [AlwaysInterleave]
    Task<JobResult> GetResult();
    [AlwaysInterleave]
    Task<QueueItem> CreateQueueItem();
}