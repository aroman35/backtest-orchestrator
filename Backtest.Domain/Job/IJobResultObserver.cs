﻿namespace Backtest.Domain.Job;

public interface IJobResultObserver : IGrainObserver
{
    Task ResultReceived(bool isMdExists, string message, bool isSuccess, long mdEntryCount, double testDuration, double taskDuration, double speed, Guid simulationId, DateTime mdStart, DateTime mdEnd, Dictionary<string, object> detailedResults);
}