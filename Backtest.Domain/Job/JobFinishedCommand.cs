namespace Backtest.Domain.Job;

[GenerateSerializer]
public record struct JobFinishedCommand(
    bool Success,
    string Message,
    long MdEntryCount,
    double TestDuration,
    bool IsCanceled);