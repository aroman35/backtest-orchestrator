namespace Backtest.Domain.Job;

[GenerateSerializer]
public record struct CreateJobCommand(string Name, DateTime MdStart, DateTime MdEnd, double ReportingIntervalSec, int MaximumTries, string[] AssignedAgents);