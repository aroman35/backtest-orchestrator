﻿namespace Backtest.Domain.Job;

public interface IJobObserver : IGrainObserver
{
    Task Cancel(bool force);
}