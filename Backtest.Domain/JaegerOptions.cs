﻿namespace Backtest.Domain;

public class JaegerOptions
{
    public int AgentPort { get; set; }
    public string AgentHost { get; set; }
}