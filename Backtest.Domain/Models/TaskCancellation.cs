namespace Backtest.Domain.Models;

[GenerateSerializer]
public record struct TaskCancellation(bool CancelRequested);