namespace Backtest.Domain.Models;

public enum CacheEntryStatus
{
    Ready,
    NotReady,
    BuildIt
}