namespace Backtest.Domain.Models;

[GenerateSerializer]
public record struct CacheResponse(CacheEntry[] Entries);