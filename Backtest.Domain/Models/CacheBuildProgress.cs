namespace Backtest.Domain.Models;

[GenerateSerializer]
public record struct CacheBuildProgress(double Progress);