﻿namespace Backtest.Domain.Models;

[GenerateSerializer]
public record struct CacheInfo(
    CacheEntry CacheEntry,
    bool IsBuilding
);