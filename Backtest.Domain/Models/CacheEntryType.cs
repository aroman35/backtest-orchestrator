namespace Backtest.Domain.Models;

public enum CacheEntryType
{
    FileStream,
    SharedMemory
}