namespace Backtest.Domain.Models;

[GenerateSerializer]
public record struct CacheEntry(
    string Hash,
    string FilePath,
    string MetaPath,
    CacheEntryCompression Compression,
    CacheEntryType Type,
    CacheEntryStatus Status)
{
    public CacheEntry CreateBuildTask()
    {
        return this with { Status = CacheEntryStatus.BuildIt };
    }
}