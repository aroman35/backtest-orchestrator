﻿namespace Backtest.Domain.Models;

[GenerateSerializer]
public record struct TaskInfo(
    string BinaryName,
    string BacktestFactorySettings,
    string StrategySettings,
    bool EnableLog,
    bool EnableTelemetry,
    string LogFile,
    string TelemetryFile
    );