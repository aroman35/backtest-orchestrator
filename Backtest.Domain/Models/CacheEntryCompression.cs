namespace Backtest.Domain.Models;

public enum CacheEntryCompression
{
    None,
    Gzip
}