namespace Backtest.Domain.Models;

[GenerateSerializer]
public record struct CacheRequest(string[] Hashes);