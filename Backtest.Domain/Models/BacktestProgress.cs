﻿namespace Backtest.Domain.Models;

[GenerateSerializer]
public record struct BacktestProgress(double Progress);