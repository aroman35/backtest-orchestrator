namespace Backtest.Domain.Models;

[GenerateSerializer]
public record struct TaskResult(
    bool Success,
    string Message,
    long MdEntryCount,
    double TestDuration);