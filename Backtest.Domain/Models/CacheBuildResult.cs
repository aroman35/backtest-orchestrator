namespace Backtest.Domain.Models;

[GenerateSerializer]
public record struct CacheBuildResult(bool Success);