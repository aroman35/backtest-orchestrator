﻿using Orleans.Streams.Kafka.Config;

namespace Backtest.Domain;

public class KafkaStreamsOptions
{
    public bool AutoCreate { get; set; } = true;
    public int PartitionsCount { get; set; } = 16;
    public ulong RetentionPeriodInMs { get; set; } = 1000 * 60 * 60;
    public string[] BrokersList { get; set; }
    public string ConsumerGroupId { get; set; }
    public ConsumeMode ConsumeMode { get; set; } = ConsumeMode.StreamEnd;
}