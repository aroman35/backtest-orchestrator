﻿namespace Backtest.Domain;

public class DatabaseSettings
{
    public string DatabaseName { get; set; }
    public string ConnectionString { get; set; }
}