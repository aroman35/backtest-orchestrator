﻿using Orleans.Statistics;

namespace Backtest.Domain
{
    public class HostEnvironmentStatistics : IHostEnvironmentStatistics
    {
        public long? TotalPhysicalMemory { get; }
        public float? CpuUsage { get; }
        public long? AvailableMemory { get; }
    }
}