// ReSharper disable InconsistentNaming
namespace Backtest.Domain;

public static class Constants
{
    public const string REDIS_STORAGE = "Redis";
    public const string REDIS_STORAGE_DB0 = "Redis_DB0";
    public const string REDIS_STORAGE_DB1 = "Redis_DB1";
    public const string REDIS_STORAGE_DB2 = "Redis_DB2";
    public const string REDIS_STORAGE_DB3 = "Redis_DB3";
    public const string MEMORY_STORAGE = "Memory";
    public const string BACKTEST = "backtest";
    public const string BACKTEST_RUNTIME_SUMMARY_MAP = "backtest-runtime-summary-map";
    public const string SIMULATION_RUNTIME_SUMMARY_MAP = "simulation-runtime-summary-map";
    public const string SIMULATION = "simulation";
    public const string BACKTEST_SETTINGS = "backtest-settings";
    public const string STRATEGY_SETTINGS = "strategy-settings";
    public const string USER_PARAMS = "user-params";
    public const string AGENT_CACHE_MAP = "agent-cache-map";
    public const string JOB_CACHE_MAP = "job-cache-map";
    public const string AGENT = "agent";
    public const string JOB = "job";
    public const string JOB_RESULT = "job-result";
    public const string SCHEDULER = "scheduler";
    public const string MARKET_DATA_CACHE = "market-data-cache";
    public const string MARKET_DATA_JOBS_MAP = "cache-using-jobs";
    public const string MARKET_DATA_META = "market-data-meta";
    public const string MARKET_DATA_CACHE_MAP = "market-data-cache-map";
    public const string JOB_LIFETIME_REMINDER_NAME = "job-lifetime";
    public const string APPLICATION_NAME_PROP = "Application";
    public const string APPLICATION_NAME_VAL = "Backtest";
    public const string INSTANCE_NAME_PROP = "Instance";

    #region Streams
    
    public const string STREAM_PROVIDER_NAME = "kafka-stream";
    public const string STATUS_UPDATES_STREAM = "job.updates";
    public const string JOB_FINISHED_STREAM = "job.finished";
    public const string JOB_RETRY_STREAM = "job.retry";
    public const string LAUNCH_JOB_STREAM = "launch.job";
    public const string BACKTEST_CANCELLATION_STREAM = "backtest.cancel";
    public const string JOBS_QUEUE_STREAM = "jobs.queue";
    public const string LAUNCH_CONFIRMED_STREAM = "jobs.queue.confirmed";
    public const string JOB_RELEASED_STREAM = "jobs.queue.release";
    public const string JOB_CANCELLED_STREAM = "jobs.queue.cancelled";
    public const string CACHE_REMOVAL_STREAM = "cache.remove";

    #endregion

    public const string DEFAULT_USER = "some";
    public const string DEFAULT_NAMESPACE = "default";
}