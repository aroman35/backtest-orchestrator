namespace Backtest.Domain.Agent;

public interface IAgentObserver : IGrainObserver
{
    Task Ping(string message);
    Task RemoveCache(string hash);
    void StopJob(Guid jobId);
    void RestartAgent();
}