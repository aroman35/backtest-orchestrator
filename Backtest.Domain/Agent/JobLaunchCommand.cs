namespace Backtest.Domain.Agent;

[GenerateSerializer]
public record struct JobLaunchCommand(Guid JobId, int Core, string BinaryName);