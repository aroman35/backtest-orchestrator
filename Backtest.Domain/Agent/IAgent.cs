using Backtest.Domain.Job;
using Backtest.Domain.MarketDataCache;
using Backtest.Domain.States;
using Orleans.Concurrency;

namespace Backtest.Domain.Agent;

public interface IAgent : IGrainWithStringKey, IStatefulGrain<AgentState>
{
    Task<double> Subscribe(IAgentObserver agentObserver);
    Task<AgentConnectedResponse> OnAgentConnected(AgentSettings agentSettings, bool initialConnection);
    ValueTask<int> AvailableCores();
    Task<bool> SendJobToLaunch(IJob job, string binaryName);
    [AlwaysInterleave]
    Task JobFinished(IJob job);
    Task<bool> KeepAlive();
    Task Disconnect();
    [AlwaysInterleave]
    Task<AgentInfo> GetAgentInfo();
    Task CacheAdded(IMarketDataCacheMeta meta, IMarketDataCache cache);
    Task CacheCreated(long dataFileSizeBytes);
    Task CacheRemoved(string hash, long dataFileSize);
    Task<IMarketDataCache> GetCacheByHash(string hash);
    Task<AgentCacheInfo> GetCacheInfo();
    Task<IJob[]> GetRunningJobs();
}