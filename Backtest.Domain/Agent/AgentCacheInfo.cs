using Backtest.Domain.Models;

namespace Backtest.Domain.Agent;

[GenerateSerializer]
public record struct AgentCacheInfo(string CachePath, CacheEntryCompression Compression, CacheEntryType Type);