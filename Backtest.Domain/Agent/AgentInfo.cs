﻿namespace Backtest.Domain.Agent;

[GenerateSerializer]
public record struct AgentInfo(string LogsPath, string TelemetryPath);