﻿namespace Backtest.Domain.Agent;

[GenerateSerializer]
public record struct AgentConnectedResponse(double KeepAlive);