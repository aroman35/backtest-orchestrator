﻿namespace Backtest.Domain.Agent;

[GenerateSerializer]
public class AgentSettings
{
    [Id(0)]
    public string AgentName { get; set; }
    [Id(1)]
    public int[] Cores { get; set; }
    [Id(2)]
    public string LogsPath { get; set; }
    [Id(3)]
    public string TelemetryPath { get; set; }
    [Id(4)]
    public string CachePath { get; set; }
    [Id(5)]
    public string BinariesPath { get; set; }
    [Id(6)]
    public long CacheMaxSizeBytes { get; set; }
    [Id(7)]
    public string BinaryName { get; set; }
    [Id(8)]
    public string BinaryStorage { get; set; }
    [Id(9)]
    public string BinaryCache { get; set; }
    [Id(10)]
    public long BinaryCacheQuota { get; set; }
}