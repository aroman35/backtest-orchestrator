﻿namespace Backtest.Domain;

public enum TaskStatus
{
    NotDefined = 0,
    Queued = 1,
    Running = 2,
    Finished = 3,
    Canceled = 4,
    Failed = 5,
    NoMd = 6
}