using System.Net;
using System.Net.Sockets;

namespace Backtest.Domain;

public class SiloConfiguration
{
    public int? SiloPort { get; set; }
    public int SiloWebPort { get; set; }
    public string SiloIp { get; set; }
    public int GatewayPort { get; set; } = 30000;
    public int? DashboardPort { get; set; }
    public IPAddress IpAddress => IPAddress.Parse(SiloIp);
    public int Port => SiloPort ?? FreeTcpPort();
    
    private static int FreeTcpPort()
    {
        var l = new TcpListener(IPAddress.Loopback, 0);
        l.Start();
        var port = ((IPEndPoint)l.LocalEndpoint).Port;
        l.Stop();
        return port;
    }
}