using System.Globalization;

namespace Backtest.Domain.Backtest;

public static class Utils
{
    public static IEnumerable<string> RepeatParams(string[] input)
    {
        yield return input[0];
        // строка и первая цифра - массив
        // 
        // "Строкой (Sequence)"
        if (input.Length == 2)
        {
            if (decimal.TryParse(input[1], out var singleValue))
            {
                //num
                yield return singleValue.ToString(CultureInfo.InvariantCulture);
                yield break;
            }

            if (bool.TryParse(input[1], out var boolValue))
            {
                //bool
                yield return boolValue.ToString(CultureInfo.InvariantCulture);
                yield break;
            }

            var split = input[1].Split(',');
            if (split.Any() &&
                split.All(x => decimal.TryParse(x, NumberStyles.Any, CultureInfo.InvariantCulture, out _)))
            {
                //массив
                foreach (var s in input[1].Split(','))
                    if (decimal.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out var value))
                        yield return value.ToString(CultureInfo.InvariantCulture);
                yield break;
            }

            yield return input[1];
            yield break;
        }

        // "Арифметическая прогрессия (Linear)"
        if (input.Length == 4 &&
            decimal.TryParse(input[1], NumberStyles.Any, CultureInfo.InvariantCulture, out var start) &&
            decimal.TryParse(input[2], NumberStyles.Any, CultureInfo.InvariantCulture, out var stop) &&
            decimal.TryParse(input[3], NumberStyles.Any, CultureInfo.InvariantCulture, out var step))
        {
            if (step < 0)
            {
                while (start >= stop)
                {
                    yield return start.ToString(CultureInfo.InvariantCulture);
                    start += step;
                }
            }
            else
            {
                while (start <= stop)
                {
                    yield return start.ToString(CultureInfo.InvariantCulture);
                    start += step;
                }
            }

            yield break;
        }

        // "Возведение в степень (Power)"
        if (input.Length == 4 && input[3].ToLower().StartsWith("n"))
        {
            var num = Convert.ToDouble(input[3].Substring(1, input[3].Length - 1));
            var from = Convert.ToDouble(input[1]);
            var to = Convert.ToDouble(input[2]);
            var fixedFrom = Math.Pow(num, Math.Ceiling(Math.Log(from, num)));
            for (var d = fixedFrom; d <= to; d *= num)
            {
                yield return d.ToString(CultureInfo.InvariantCulture);
            }

            yield break;
        }

        // bool
        if (bool.TryParse(input[1], out var b1) &&
            bool.TryParse(input[2], out var b2) &&
            bool.TryParse(input[3], out var b3))
        {
            foreach (var param in GenerateBoolParams((b1, b2, b3)))
            {
                yield return param.ToString();
            }
        }
    }

    private static IEnumerable<bool> GenerateBoolParams((bool, bool, bool) boolValues)
    {
        var begin = boolValues.Item1 ? 1 : 0;
        var end = boolValues.Item2 ? 1 : 0;
        var step = boolValues.Item3 ? 1 : 0;

        if (step == 0)
            throw new InvalidOperationException($"Zero Step for BoolParameter");
        if (begin < end)
            for (var i = begin; i <= end; i++)
                yield return i == 1;
        for (var i = begin; i >= end; i--) yield return i == 1;
    }
}