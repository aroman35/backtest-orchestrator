namespace Backtest.Domain.Backtest;

[GenerateSerializer]
public record struct CreateBacktestCommand(
    string Name,
    string BacktestSettings,
    string StrategySettings,
    Dictionary<string, string[]> UserParams,
    DateTime? MdStart,
    DateTime? MdEnd,
    DateTime[] MdDates,
    bool SplitDays,
    string BinaryName,
    bool EnableLogs,
    bool EnableTelemetry,
    int RetryCount,
    double ReportingTimeoutSec,
    List<string> DesiredAgentIds,
    List<string> Labels,
    string User,
    string Namespace
    );
    
[GenerateSerializer]
public record struct CreateBacktestWithPredefinedSettingsCommand(
    string Name,
    string BacktestSettings,
    string[] StrategySettings,
    DateTime? MdStart,
    DateTime? MdEnd,
    DateTime[] MdDates,
    bool SplitDays,
    string BinaryName,
    bool EnableLogs,
    bool EnableTelemetry,
    int RetryCount,
    double ReportingTimeoutSec,
    List<string> DesiredAgentIds,
    List<string> Labels,
    string User,
    string Namespace
);