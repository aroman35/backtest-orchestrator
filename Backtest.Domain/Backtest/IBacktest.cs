using Backtest.Domain.Simulation;
using Backtest.Domain.States;
using Backtest.Domain.States.Settings;
using Orleans.Concurrency;

namespace Backtest.Domain.Backtest;

public interface IBacktest : IGrainWithGuidKey, IStateUpdates, IStatefulGrain<BacktestState>
{
    Task Create(CreateBacktestCommand createCommand);
    Task Create(CreateBacktestWithPredefinedSettingsCommand createCommand);
    [AlwaysInterleave]
    Task<BacktestInfo> GetBacktestInfo();
    [AlwaysInterleave]
    Task<string> BinaryName();
    [OneWay]
    Task Cancel(bool force);
    [AlwaysInterleave]
    Task<ISimulation[]> GetSimulations();
    [AlwaysInterleave]
    Task<BacktestSettings> GetBacktestSettings();
    [AlwaysInterleave]
    Task<StrategySettings> GetStrategySettings();
}