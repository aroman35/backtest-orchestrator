﻿namespace Backtest.Domain.Backtest;

[GenerateSerializer]
public record BacktestInfo(string BinaryName, string BacktestSettings, bool EnableLogs, bool EnableTelemetry);