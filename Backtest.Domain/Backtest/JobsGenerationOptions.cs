﻿namespace Backtest.Domain.Backtest;

[GenerateSerializer]
public record JobsGenerationOptions(DateTime[] MdDates, bool SplitDays, List<string> DesiredAgents, double ReportingIntervalSec, int MaximumTries);