﻿using Backtest.Domain.Backtest;
using Backtest.Domain.Job;
using Backtest.Domain.Scheduler;
using Backtest.Domain.Simulation;
using Orleans.Services;

namespace Backtest.Domain.Queries;

public interface IQueriesGrainService : IGrainService
{
    Task<ISimulation[]> GetSimulationsByBacktest(IBacktest backtest);
    Task<IJob[]> GetJobsBySimulation(ISimulation simulation);
    Task<QueueItem[]> GetInitialQueue();
}

public interface IQueriesServiceClient : IGrainServiceClient<IQueriesGrainService>, IQueriesGrainService
{
}