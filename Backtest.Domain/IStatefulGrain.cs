﻿using Orleans.Concurrency;

namespace Backtest.Domain;

public interface IStatefulGrain<TMainState> : IGrain
    where TMainState : class, new()
{
    [AlwaysInterleave]
    Task<TMainState> GetState();
}