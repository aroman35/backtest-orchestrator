﻿namespace Backtest.Domain;

[GenerateSerializer]
public record struct ItemSummary(Guid ItemId, double Progress, TaskStatus Status, int TotalJobs, int FinishedJobs,
    int FailedJobs)
{
    public override string ToString()
    {
        return $"Item [{ItemId}]: Progress: {Progress}%, Status: {Status}, Jobs: {FinishedJobs}/{TotalJobs}[{FailedJobs}]";
    }
}