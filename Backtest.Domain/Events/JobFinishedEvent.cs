namespace Backtest.Domain.Events;

[GenerateSerializer]
public record struct JobFinishedEvent(bool FinishedSuccess, double Speed);