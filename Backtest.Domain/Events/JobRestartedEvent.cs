namespace Backtest.Domain.Events;

[GenerateSerializer]
public record struct JobRestartedEvent(TaskStatus PreviousStatus);