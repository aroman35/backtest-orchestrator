﻿namespace Backtest.Domain.MarketDataCache;

[GenerateSerializer]
public record struct FillMarketDataCacheMetaCommand(
    DateTime BeginDate,
    DateTime EndDate,
    long FileSizeBytes,
    long MdEntryCount,
    string[] Instruments,
    string[] FilesMap
)
{
    public static FillMarketDataCacheMetaCommand Empty()
    {
        return new FillMarketDataCacheMetaCommand();
    }
}