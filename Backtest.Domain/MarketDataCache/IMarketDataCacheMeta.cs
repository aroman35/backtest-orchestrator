using Backtest.Domain.Agent;
using Backtest.Domain.States;

namespace Backtest.Domain.MarketDataCache;

public interface IMarketDataCacheMeta : IGrainWithStringKey, IStatefulGrain<MarketDataCacheMetaState>
{
    Task BuildComplete(FillMarketDataCacheMetaCommand fillCommand);
    Task CacheRemoved(string agentName);
    Task<long> DataFileSizeBytes();
    Task<IMarketDataCache> GetCacheForAgent(IAgent agent);
    Task Initialize(FillMarketDataCacheMetaCommand fillCommand, IAgent agent);
}