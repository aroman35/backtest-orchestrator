using Backtest.Domain.Agent;
using Backtest.Domain.Job;
using Backtest.Domain.Models;
using Backtest.Domain.States;

namespace Backtest.Domain.MarketDataCache;

public interface IMarketDataCache : IGrainWithGuidKey, IStatefulGrain<MarketDataCacheState>
{
    Task<CacheInfo> GetEntryInfo();
    Task Initialize(IAgent agent, IMarketDataCacheMeta meta, CacheEntryStatus initialStatus);
    Task ReportProgress(double progress);
    Task<CacheEntry> Take(IJob job);
    Task Free(IJob job);
    Task CacheBuildComplete(CacheBuildCompleteResult result);
    Task<CacheUseInfo> GetCacheUseInfo();
    Task Remove();
    Task Removed();
    Task<long> EstimatedCacheSize();
    Task<double> Subscribe(IMarketDataCacheObserver observer);
}

public interface IMarketDataCacheObserver : IGrainObserver
{
    Task Remove();
}