﻿namespace Backtest.Domain.MarketDataCache;

[GenerateSerializer]
public record struct CacheBuildCompleteResult(FillMarketDataCacheMetaCommand MetaCommand, bool IsSuccess, string Error = null)
{
    public static CacheBuildCompleteResult ErrorResult(string error)
    {
        return new CacheBuildCompleteResult(FillMarketDataCacheMetaCommand.Empty(), false, error);
    }
}