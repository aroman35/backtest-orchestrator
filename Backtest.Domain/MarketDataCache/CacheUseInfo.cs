namespace Backtest.Domain.MarketDataCache;

[GenerateSerializer]
public record CacheUseInfo(Guid CacheId, string Hash, DateTime LastAccessDate, int RunningJobsCount, long DataFileSizeBytes, bool IsActive);