﻿namespace Backtest.Domain.Connections;

public interface IClientBacktestsSession : IGrainWithStringKey
{
    Task SubscribeUpdates(string clientConnectionId, Guid[] backtestIds);
}