﻿using Orleans.Concurrency;

namespace Backtest.Domain;

public interface IStateUpdatesObserver : IGrainObserver
{
    Task StateUpdate(ItemSummary summary);
}

public interface IStateUpdates
{
    [AlwaysInterleave]
    Task SubscribeStateUpdates(IStateUpdatesObserver observer);
    [AlwaysInterleave]
    Task UnsubscribeStateUpdates(IStateUpdatesObserver observer);
}