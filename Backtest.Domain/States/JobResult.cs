﻿using System.ComponentModel;
using Backtest.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Backtest.Domain.States;

[DisplayName(Constants.JOB_RESULT)]
[GenerateSerializer]
public class JobResult
{
    [Id(0)]
    private Dictionary<string, object> _detailedResults;
    
    public void FillResult(
        bool isMdExists,
        bool isSuccess,
        string message,
        long mdEntryCount,
        double testDuration,
        double taskDuration,
        Guid simulationId,
        DateTime mdStart,
        DateTime?mdEnd)
    {
        Message = message;
        IsMdExists = isMdExists;
        IsSuccess = isSuccess;
        MdEntryCount = mdEntryCount;
        TestDuration = testDuration;
        TaskDuration = taskDuration;
        SimulationId = simulationId;
        MdStart = mdStart;
        MdEnd = mdEnd;
        if (TestDuration != 0)
            Speed = Math.Round(MdEntryCount / TestDuration);
        
        if (IsSuccess && Message.IsJson())
            _detailedResults = ParseResult();
    }

    [Id(1)]
    public bool IsMdExists { get; set; }
    [Id(2)]
    public string Message { get; set; }
    [Id(3)]
    public bool IsSuccess { get; set; }
    [Id(4)]
    public long MdEntryCount { get; set; }
    [Id(5)]
    public double TestDuration { get; set; }
    [Id(6)]
    public double TaskDuration { get; set; }
    [Id(7)]
    public double Speed { get; set; }
    [Id(8)]
    public Guid SimulationId { get; set; }
    [Id(9)]
    public DateTime? MdStart { get; set; }
    [Id(10)]
    public DateTime? MdEnd { get; set; }

    public Dictionary<string, object> DetailedResults => _detailedResults ??= ParseResult();

    private Dictionary<string, object> ParseResult()
    {
        return Message.IsJson()
            ? _detailedResults = JsonConvert.DeserializeObject<Dictionary<string, object>>(Message)
                .ToDictionary(
                    x => x.Key,
                    x => x.Value is JArray jArray
                        ? jArray.ToString(Formatting.None)
                        : x.Value is JObject jObject
                            ? jObject.ToString(Formatting.None)
                            : x.Value)
            : new Dictionary<string, object>();

    }

    public TaskStatus FinalState()
    {
        if (IsSuccess) return TaskStatus.Finished;
        if (!IsMdExists) return TaskStatus.NoMd;
        return TaskStatus.Failed;
    }
}