using System.ComponentModel;
using Backtest.Domain.Job;
using Newtonsoft.Json.Linq;

namespace Backtest.Domain.States;

[GenerateSerializer]
[DisplayName(Constants.JOB)]
public class JobState
{
    [Id(0)]
    public string Name { get; set; }
    [Id(1)]
    public Guid? SimulationId { get; set; }
    [Id(2)]
    public Guid? BacktestId { get; set; }
    [Id(3)]
    public DateTime MdStart { get; set; }
    [Id(4)]
    public DateTime MdEnd { get; set; }
    [Id(5)]
    public string AgentName { get; set; }
    [Id(6)]
    public int? CurrentTry { get; set; }
    [Id(7)]
    public int MaximumTries { get; set; }
    [Id(8)]
    public DateTime? Started { get; set; }
    [Id(9)]
    public double Progress { get; set; }
    [Id(10)]
    public TaskStatus Status { get; private set; }
    [Id(11)]
    public int? Core { get; set; }
    [Id(12)]
    public double ReportingIntervalSec { get; set; }
    [Id(13)]
    public DateTime LastUpdate { get; set; }
    [Id(14)]
    public string[] DesiredAgents { get; set; } = Array.Empty<string>();
    [Id(15)]
    public Guid? BuildingCache { get; set; }
    [Id(16)]
    public DateTime Created { get; set; }
    
    public bool IsFinalStatus => Status is TaskStatus.Canceled or TaskStatus.Failed or TaskStatus.Finished or TaskStatus.NoMd;

    public void UpdateProgress(double progress)
    {
        Progress = progress;
    }

    public void Initialize(CreateJobCommand command)
    {
        Name = command.Name;
        MdStart = command.MdStart;
        MdEnd = command.MdEnd;
        ReportingIntervalSec = command.ReportingIntervalSec;
        DesiredAgents = command.AssignedAgents;
        MaximumTries = command.MaximumTries;
        LastUpdate = DateTime.UtcNow;
        Status = TaskStatus.Queued;
        Created = DateTime.UtcNow;
    }

    public string PopulateStrategySettings(string strategySettings)
    {
        var settingsObject = JObject.Parse(strategySettings);
        settingsObject["StartDateTime"] = MdStart.ToString("s");
        settingsObject["EndDateTime"] = MdEnd.ToString("s");
        return settingsObject.ToString();
    }

    public void SentForExecution(string agentName)
    {
        AgentName = agentName;
        CurrentTry = CurrentTry is null ? 1 : ++CurrentTry;
    }

    public void RevokeExecution()
    {
        AgentName = string.Empty;
        CurrentTry = null;
    }

    public ItemSummary GetSummary(Guid jobId)
    {
        return new ItemSummary
        {
            ItemId = jobId,
            Progress = Progress,
            Status = Status
        };
    }

    public void Retry()
    {
        Status = TaskStatus.Queued;
        Progress = 0;
        Core = null;
        Started = null;
    }

    public void ChangeStatus(TaskStatus status)
    {
        Status = status;
    }
}