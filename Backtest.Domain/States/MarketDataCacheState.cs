﻿using Backtest.Domain.Models;

namespace Backtest.Domain.States;

[GenerateSerializer]
public class MarketDataCacheState
{
    [Id(0)]
    public string Hash { get; set; }
    [Id(1)]
    public string AgentName { get; set; }
    [Id(2)]
    public string DataFilePath { get; set; }
    [Id(3)]
    public string MetaFilePath { get; set; }
    [Id(4)]
    public CacheEntryCompression Compression { get; set; }
    [Id(5)]
    public CacheEntryType Type { get; set; }
    [Id(6)]
    public CacheEntryStatus Status { get; set; }
    [Id(7)]
    public DateTime LastAccess { get; set; }
    [Id(8)]
    public double Progress { get; set; }
    [Id(9)]
    public Guid? BuildingJob { get; set; }

    [Id(10)]
    public double BuildTimeout { get; set; } = 60;
    [Id(11)]
    public DateTime? LastBuildReport { get; set; }
    [Id(12)]
    public string BuildError { get; set; }
    [Id(13)]
    public bool IsBuilding { get; set; }
    [Id(14)]
    public bool PendingForRemove { get; set; }
    [Id(15)]
    public bool IsInitialized { get; set; }

    public CacheEntry CreateCacheEntry()
    {
        return new CacheEntry(Hash, DataFilePath, MetaFilePath, Compression, Type, Status);
    }

    public CacheInfo CreateCacheInfo()
    {
        return new CacheInfo(CreateCacheEntry(), IsBuilding);
    }
}