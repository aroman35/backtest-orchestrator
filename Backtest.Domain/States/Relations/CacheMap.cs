﻿using System.ComponentModel;

namespace Backtest.Domain.States.Relations;

[DisplayName(Constants.AGENT_CACHE_MAP)]
[GenerateSerializer]
public class CacheMap
{
    [Id(0)] public Dictionary<string, Guid> Cache { get; set; } = new();
}