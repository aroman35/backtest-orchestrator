namespace Backtest.Domain.States.Relations;

[GenerateSerializer]
public class RunningJobsCacheMap
{
    [Id(0)]
    public HashSet<Guid> Jobs { get; set; } = new();
}