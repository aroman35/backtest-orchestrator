﻿using Backtest.Common;

namespace Backtest.Domain.States;

[GenerateSerializer]
public class RuntimeSummaryMap
{
    [Id(0)]
    public Dictionary<Guid, ItemSummary> Summary { get; set; } = new();

    public void Append(Guid itemId)
    {
        Summary.Add(itemId, new ItemSummary());
    }

    public double TotalProgress()
    {
        return Summary.Values.Select(x => x.Progress).ToArray().Mean(2);
    }

    public TaskStatus TotalStatus()
    {
        var allStatuses = Summary.Values.Select(x => x.Status).ToArray();
        if (allStatuses.All(x => x is TaskStatus.Failed))
            return TaskStatus.Failed;
        if (allStatuses.Any(x => x is TaskStatus.Canceled))
            return TaskStatus.Canceled;
        if (allStatuses.All(x => x is TaskStatus.NoMd))
            return TaskStatus.NoMd;
        if (allStatuses.All(x => x is TaskStatus.Queued))
            return TaskStatus.Queued;
        if (allStatuses.Any(x => x is TaskStatus.Running))
            return TaskStatus.Running;
        return TaskStatus.Finished;
    }
    
    public void Update(ItemSummary itemSummary)
    {
        Summary[itemSummary.ItemId] = itemSummary;
    }

    public void Update(Dictionary<Guid, ItemSummary> finalStatuses)
    {
        foreach (var (itemId, summary) in finalStatuses)
        {
            Summary[itemId] = summary;
        }
    }
}