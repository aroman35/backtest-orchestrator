using System.ComponentModel;

namespace Backtest.Domain.States;

[DisplayName("agent")]
[GenerateSerializer]
public class AgentState
{
    [Id(0)]
    public string AgentName { get; set; }
    [Id(1)] 
    public int[] Cores { get; set; } = Array.Empty<int>();
    [Id(2)]
    public string LogsPath { get; set; }
    [Id(3)]
    public string TelemetryPath { get; set; }
    [Id(4)]
    public string CachePath { get; set; }
    [Id(5)]
    public string BinariesPath { get; set; }
    [Id(6)]
    public bool IsActive { get; set; }
    [Id(7)]
    public DateTime? LastPingEvent { get; set; }
    [Id(8)]
    public long CacheSizeBytes { get; set; }
    [Id(9)]
    public long CacheMaxSizeBytes { get; set; }
}