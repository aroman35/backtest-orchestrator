﻿using System.ComponentModel;

namespace Backtest.Domain.States;

[GenerateSerializer]
[DisplayName("scheduler")]
public class SchedulerState
{
    [Id(0)]
    public HashSet<string> Agents { get; set; } = new();

    [Id(1)]
    public bool StreamsInitialized { get; set; }
}