﻿using Backtest.Domain.Events;

namespace Backtest.Domain.States;

[GenerateSerializer]
public class RuntimeSummary
{
    [Id(0)]
    public TaskStatus Status { get; set; }
    [Id(1)]
    public int TotalJobs { get; set; }
    [Id(2)]
    public int FinishedJobs { get; set; }
    [Id(3)]
    public int FailedJobs { get; set; }
    [Id(4)]
    public double Speed { get; set; }
    [Id(5)]
    public double Progress { get; set; }

    public bool IsFinalStatus => Status is TaskStatus.Canceled or TaskStatus.Failed or TaskStatus.Finished or TaskStatus.NoMd;
    public ItemSummary GetSummary(Guid itemId)
    {
        return new ItemSummary
        {
            ItemId = itemId,
            Progress = Progress,
            Status = Status,
            FinishedJobs = FinishedJobs,
            TotalJobs = TotalJobs,
            FailedJobs = FailedJobs
        };
    }

    public void JobFinished(JobFinishedEvent @event)
    {
        if (@event.FinishedSuccess)
        {
            FinishedJobs++;
            Speed = double.Round((Speed + @event.Speed) / FinishedJobs, 0);
        }
        else
            FailedJobs++;
    }

    public void JobRestarted(JobRestartedEvent @event)
    {
        if (@event.PreviousStatus is TaskStatus.Failed)
            FailedJobs--;
        if (@event.PreviousStatus is TaskStatus.Finished or TaskStatus.Canceled or TaskStatus.NoMd)
            FinishedJobs--;
    }
}