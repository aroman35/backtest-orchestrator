﻿using System.ComponentModel;

namespace Backtest.Domain.States;

[DisplayName(Constants.SIMULATION)]
[GenerateSerializer]
public class SimulationState
{
    [Id(0)]
    public string Name { get; set; }
    [Id(1)]
    public Guid? BacktestId { get; set; }
    [Id(2)]
    public RuntimeSummary Summary { get; set; } = new();
    [Id(3)]
    public DateTime Created { get; set; }
}