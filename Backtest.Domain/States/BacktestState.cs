﻿using System.ComponentModel;
using static Backtest.Domain.Constants;

namespace Backtest.Domain.States;

[DisplayName(BACKTEST)]
[GenerateSerializer]
public class BacktestState
{
    [Id(0)]
    public string Name { get; set; }
    [Id(1)]
    public List<string> Labels { get; set; }
    [Id(2)]
    public DateTime? MdStart { get; set; }
    [Id(3)]
    public DateTime? MdEnd { get; set; }
    [Id(4)]
    public DateTime[] MdDates { get; set; }
    [Id(5)]
    public bool SplitDays { get; set; }
    [Id(6)]
    public string BinaryName { get; set; }
    [Id(7)]
    public List<string> DesiredAgents { get; set; }
    [Id(8)]
    public bool EnableLogs { get; set; }
    [Id(9)]
    public bool EnableTelemetry { get; set; }
    [Id(10)]
    public int RetryCount { get; set; }
    [Id(11)]
    public double ReportingTimeoutSec { get; set; }
    [Id(12)]
    public RuntimeSummary Summary { get; set; } = new();
    [Id(13)]
    public DateTime Created { get; set; }
    [Id(14)]
    public string UserCreated { get; set; } = DEFAULT_USER;
    [Id(15)]
    public string Namespace { get; set; } = DEFAULT_NAMESPACE;
}