﻿using System.ComponentModel;

namespace Backtest.Domain.States.Settings;

[DisplayName(Constants.STRATEGY_SETTINGS)]
[GenerateSerializer]
public class StrategySettings
{
    [Id(0)]
    public string SettingsSource { get; set; }
    [Id(1)]
    public Dictionary<string, string> AppliedUserParams { get; set; }
}