﻿using System.ComponentModel;

namespace Backtest.Domain.States.Settings;

[DisplayName(Constants.BACKTEST_SETTINGS)]
[GenerateSerializer]
public class BacktestSettings
{
    [Id(0)]
    public string SettingsSource { get; set; }
}