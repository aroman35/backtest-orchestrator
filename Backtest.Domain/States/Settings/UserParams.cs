﻿using System.ComponentModel;

namespace Backtest.Domain.States.Settings;

[DisplayName(Constants.USER_PARAMS)]
[GenerateSerializer]
public class UserParams
{
    [Id(0)]
    public Dictionary<string, string[]> ParamsMap { get; set; }
}