using System.ComponentModel;

namespace Backtest.Domain.States;

[DisplayName(Constants.MARKET_DATA_CACHE_MAP)]
public class MarketDataCacheMap
{
    public Dictionary<string, Guid> MarketDataCache { get; set; } = new();
}