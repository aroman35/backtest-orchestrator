using System.ComponentModel;

namespace Backtest.Domain.States;

[DisplayName(Constants.MARKET_DATA_CACHE)]
public class MarketDataCacheMetaState
{
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public long MdEntryCount { get; set; }
    public long DataFileSize { get; set; }
    public string[] Instruments { get; set; } = Array.Empty<string>();
    public string[] FilesMap { get; set; } = Array.Empty<string>();
}