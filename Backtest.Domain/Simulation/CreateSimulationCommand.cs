namespace Backtest.Domain.Simulation;

[GenerateSerializer]
public record struct CreateSimulationCommand(
    string Name,
    string StrategySettings,
    Dictionary<string, string> AppliedUserParams);