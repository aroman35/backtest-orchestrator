﻿using Backtest.Domain.Backtest;
using Backtest.Domain.Job;
using Backtest.Domain.States;
using Backtest.Domain.States.Settings;
using Orleans.Concurrency;

namespace Backtest.Domain.Simulation;

public interface ISimulation : IGrainWithGuidKey, IStateUpdates, IStatefulGrain<SimulationState>
{
    Task Create(IBacktest backtest, CreateSimulationCommand createCommand);
    Task<int> GenerateJobs(JobsGenerationOptions options, GrainCancellationToken cancellationToken);
    [AlwaysInterleave]
    Task<StrategySettings> GetStrategySettings();
    [AlwaysInterleave]
    Task<IJob[]> GetJobs();
}