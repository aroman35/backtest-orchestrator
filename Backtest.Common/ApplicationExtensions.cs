﻿using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
using Newtonsoft.Json.Linq;

namespace Backtest.Common;

public static class ApplicationExtensions
{
    public static bool IsJson(this string input)
    {
        if (string.IsNullOrEmpty(input))
            return false;
        try
        {
            JObject.Parse(input);
            return true;
        }
        catch
        {
            return false;
        }
    }
    
    public static double Mean(this double[] source, int round = -1)
    {
        if (source.Length == 0)
            return 0;
        var sum = AvxSum(source);
        var avg = sum / source.Length;
        return round > -1 ? Math.Round(avg, round, MidpointRounding.ToZero) : avg;
    }

    public static unsafe double AvxSum(this double[] values)
    {
        if (!Avx2.IsSupported)
            return values.Sum();
            
        var vectorSize = 256 / sizeof(double) / 8;
        var accVector = Vector256<double>.Zero;
        int i;
        fixed (double* ptr = values) {
            for (i = 0; i <= values.Length - vectorSize; i += vectorSize) {
                var v = Avx2.LoadVector256(ptr + i);
                accVector = Avx2.Add(accVector, v);
            }
        }
        double result = 0;
        var temp = stackalloc double[vectorSize];
        Avx2.Store(temp, accVector);
            
        for (var j = 0; j < vectorSize; j++)
            result += temp[j];
        for (; i < values.Length; i++)
            result += values[i];
        return result;
    }
}