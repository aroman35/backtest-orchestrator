﻿using Consul;
using Microsoft.Extensions.Configuration;
using Winton.Extensions.Configuration.Consul;

namespace Backtest.Common;

public static class ConsulConfigurationExtensions
{
    public static IConfigurationBuilder AddConsulConfiguration(this IConfigurationBuilder configurationBuilder, string applicationKey, bool directConsulKey = false)
    {
        ArgumentException.ThrowIfNullOrEmpty(applicationKey);
        var consulPath = directConsulKey
            ? string.Empty
            : Environment.GetEnvironmentVariable("CONSUL_PATH") ??
              throw new ConsulConfigurationException("No env variable named consul path has been configured");

        var consulUri = Environment.GetEnvironmentVariable("CONSUL_URI");
        var consulToken = Environment.GetEnvironmentVariable("CONSUL_TOKEN");
        ArgumentException.ThrowIfNullOrEmpty(consulUri);

        var key = CombinePath(consulPath, $"{applicationKey}.json");
            
        configurationBuilder.AddConsul(
            key,
            consulConfiguration =>
            {
                consulConfiguration.ConsulConfigurationOptions = options =>
                {
                    options.Address = new Uri(consulUri);
                    if (!string.IsNullOrEmpty(consulToken))
                        options.Token = consulToken;
                };
                consulConfiguration.Optional = false;
                consulConfiguration.ReloadOnChange = true;
                consulConfiguration.OnLoadException = exceptionContext => { exceptionContext.Ignore = false; };
            });

        return configurationBuilder;
    }

    public static string CombinePath(params string[] pathParts)
    {
        return Path.Combine(pathParts).Replace('\\', '/');
    }
}