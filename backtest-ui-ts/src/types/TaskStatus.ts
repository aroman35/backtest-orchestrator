export enum TaskStatus {
    NotDefined = "NotDefined",
    Queued = "Queued",
    Running = "Running",
    Finished = "Finished",
    Canceled = "Canceled",
    Failed = "Failed",
    NoMd = "NoMd"
}