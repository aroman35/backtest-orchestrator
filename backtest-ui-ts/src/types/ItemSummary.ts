import {TaskStatus} from "./TaskStatus";

export interface ItemSummary {
    itemId: string
    progress: number
    status: TaskStatus
    totalJobs: number
    finishedJobs: number
    failedJobs: number
}