export enum ItemType {
    NotFound = "NotFound",
    Backtest = "Backtest",
    Simulation = "Simulation",
    Job = "Job"
}