import {ItemType} from "./ItemType";
import {TaskStatus} from "./TaskStatus";

export interface IGlobalIdSearchResult {
    id: string
    type: ItemType
    name: string
    progress: number
    status: TaskStatus
}