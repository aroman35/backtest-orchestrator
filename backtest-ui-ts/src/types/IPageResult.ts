export interface IPageResult<T> {
    items: T[]
    page: number
    totalPages: number
    itemsPerPage: number
}