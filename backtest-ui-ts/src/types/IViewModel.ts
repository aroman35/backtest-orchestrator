export interface IViewModel<TModel> {
    id: string
    etag: string
    state: TModel
}