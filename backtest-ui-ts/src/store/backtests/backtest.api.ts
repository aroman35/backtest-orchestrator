import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/dist/query/react";
import {IPageResult} from "../../types/IPageResult";
import {IViewModel} from "../../types/IViewModel";
import {IBacktest} from "../../types/IBacktest";
import {IBacktestsPageQuery} from "../../types/IBacktestsPageQuery";
import {createEntityAdapter} from "@reduxjs/toolkit";
import * as signalR from "@microsoft/signalr";
import {JsonHubProtocol} from "@microsoft/signalr";
import {ItemSummary} from "../../types/ItemSummary";
import {IBacktestSettings} from "../../types/IBacktestSettings";

export const backtestsAdapter = createEntityAdapter<IViewModel<IBacktest>>({
    selectId: (backtest) => backtest.id
})

export const backtestApi = createApi({
    reducerPath: 'backtest/api',
    baseQuery: fetchBaseQuery({
        baseUrl: process.env.REACT_APP_BACKTEST_API_URL + "/api/backtest/"
    }),
    refetchOnFocus: true,
    refetchOnReconnect: true,
    refetchOnMountOrArgChange: true,
    endpoints: (builder) => ({
        getBacktestDetails: builder.query<IViewModel<IBacktest>, string>({
            query: (backtestId) => ({
                url: `/${backtestId}`
            }),
            async onCacheEntryAdded(
                arg,
                { updateCachedData, cacheDataLoaded, cacheEntryRemoved, dispatch }
            ) {
                const cacheData = await cacheDataLoaded
                const backtestId = cacheData.data.id

                const connection = new signalR.HubConnectionBuilder()
                    .withUrl(process.env.REACT_APP_BACKTEST_API_URL + "/backtests")
                    .withHubProtocol(new JsonHubProtocol())
                    .withAutomaticReconnect()
                    .build();

                connection.on("updateReceived", (message: ItemSummary) => {
                    updateCachedData(cachedValue => {
                        cachedValue.state.summary.progress = message.progress
                        cachedValue.state.summary.failedJobs = message.failedJobs
                        cachedValue.state.summary.totalJobs = message.totalJobs
                        cachedValue.state.summary.finishedJobs = message.finishedJobs
                        cachedValue.state.summary.status = message.status
                    })
                })

                await connection.start()
                await connection.send("subscribe", [backtestId])

                await cacheEntryRemoved
                await connection.stop()
            }
        }),
        getBacktestSettings: builder.query<IViewModel<IBacktestSettings>, string>({
            query: (backtestId) => ({
                url: `/${backtestId}/backtest-settings`
            })
        }),
        getBacktestsList: builder.query<IPageResult<IViewModel<IBacktest>>, IBacktestsPageQuery>({
            query: (pageQuery) => ({
                url: 'list',
                params: pageQuery,
                method: 'GET'
            }),
            async onCacheEntryAdded(
                arg,
                { updateCachedData, cacheDataLoaded, cacheEntryRemoved, dispatch }
            ) {
                const cacheData = await cacheDataLoaded
                const backtestIds = cacheData.data.items.map(backtest => backtest.id)

                const connection = new signalR.HubConnectionBuilder()
                    .withUrl(process.env.REACT_APP_BACKTEST_API_URL + "/backtests")
                    .withHubProtocol(new JsonHubProtocol())
                    .withAutomaticReconnect()
                    .build();

                connection.on("updateReceived", (message: ItemSummary) => {
                    updateCachedData(draft => {
                        const existingBacktest = draft.items.find(
                            (backtest) => backtest.id === message.itemId
                        )
                        if (existingBacktest) {
                            existingBacktest.state.summary = {
                                ...existingBacktest.state.summary,
                                progress: message.progress,
                                status: message.status,
                                totalJobs: message.totalJobs,
                                finishedJobs: message.finishedJobs,
                                failedJobs: message.failedJobs,
                            }
                        }
                    })
                })
                await connection.start()
                await connection.send("subscribe", backtestIds)

                await cacheEntryRemoved
                await connection.stop()
            }
        }),
    })
})

export const {
    useGetBacktestsListQuery,
    useGetBacktestDetailsQuery,
    useGetBacktestSettingsQuery,
} = backtestApi