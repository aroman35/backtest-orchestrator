import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {IBacktestsPageQuery} from "../../types/IBacktestsPageQuery";

const initialQuery = {
    page: 1,
    itemsPerPage: Math.round((window.innerHeight / 50))
} as IBacktestsPageQuery

export const backtestsSlice = createSlice({
    name: 'backtest',
    initialState: {
        pageQuery: initialQuery,
        totalPages: 0
    },
    reducers: {
        applySearchQuery (state, action: PayloadAction<IBacktestsPageQuery>) {
            state.pageQuery = action.payload
        },
        gotoBacktestsPage (state, action: PayloadAction<number>) {
            state.pageQuery.page = action.payload
        },
        setPageSize (state, action: PayloadAction<number>) {
            state.pageQuery.itemsPerPage = action.payload
        }
    }
})

export const backtestActions = backtestsSlice.actions

export default backtestsSlice.reducer
