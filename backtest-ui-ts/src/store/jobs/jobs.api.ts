import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/dist/query/react";
import {IViewModel} from "../../types/IViewModel";
import {IJob} from "../../types/IJob";
import {IJobResult} from "../../types/IJobResult";
import * as signalR from "@microsoft/signalr";
import {JsonHubProtocol} from "@microsoft/signalr";
import {ItemSummary} from "../../types/ItemSummary";
import {JobLogEvent} from "../../types/JobLogEvent";

export const jobsApi = createApi({
    reducerPath: 'jobs/api',
    baseQuery: fetchBaseQuery({
        baseUrl: process.env.REACT_APP_BACKTEST_API_URL + '/api'
    }),
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
    refetchOnReconnect: true,
    endpoints: (builder) => ({
        jobs: builder.query<IViewModel<IJob>[], string>({
            query: (simulationId) => ({
                url: `/simulation/${simulationId}/jobs`
            }),
            async onCacheEntryAdded(arg, { updateCachedData, cacheDataLoaded, cacheEntryRemoved, dispatch }) {
                const cacheData = await cacheDataLoaded
                const jobIds = cacheData.data.map(job => job.id)

                const connection = new signalR.HubConnectionBuilder()
                    .withUrl(process.env.REACT_APP_BACKTEST_API_URL + "/jobs")
                    .withHubProtocol(new JsonHubProtocol())
                    .withAutomaticReconnect()
                    .build();

                connection.on("updateReceived", (message: ItemSummary) => {
                    updateCachedData(draft => {
                        const existingJob = draft.find(
                            (job) => job.id === message.itemId
                        )
                        if (existingJob){
                            existingJob.state = {
                                ...existingJob.state,
                                progress: message.progress,
                                status: message.status
                            }
                        }
                    })
                })
                await connection.start()
                await connection.send("subscribe", jobIds)

                await cacheEntryRemoved
                await connection.stop()
            }
        }),
        jobResults: builder.query<IViewModel<IJobResult>[], string>({
            query: (simulationId) => ({
                url: `/simulation/${simulationId}/results`
            })
        }),
        job: builder.query<IViewModel<IJob>, string>({
            query: (jobId) => ({
                url: `/job/${jobId}`
            })
        }),
        jobResult: builder.query<IViewModel<IJobResult>, string>({
            query: (jobId) => ({
                url: `/job/${jobId}/result`
            })
        }),
        jobLogs: builder.query<JobLogEvent[], string>({
            query: (jobId) => ({
                url: `/job/logs?jobId=${jobId}&logsCount=100`
            })
        })
    })
})

export const {
    useJobResultQuery,
    useJobQuery,
    useJobsQuery,
    useJobResultsQuery,
    useJobLogsQuery
} = jobsApi