import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/dist/query/react";
import {IGlobalIdSearchResult} from "../../types/IGlobalIdSearchResult";

export const globalApi = createApi({
    reducerPath: 'global/api',
    baseQuery: fetchBaseQuery({
        baseUrl: process.env.REACT_APP_BACKTEST_API_URL + '/api'
    }),
    refetchOnFocus: false,
    refetchOnMountOrArgChange: true,
    refetchOnReconnect: true,
    endpoints: (builder) => ({
        oneMessageSearch: builder.query<IGlobalIdSearchResult, string>({
            query: (searchId) => ({
                url: `/global/${searchId}`
            })
        })
    })
})

export const {
    useOneMessageSearchQuery,
    useLazyOneMessageSearchQuery
} = globalApi