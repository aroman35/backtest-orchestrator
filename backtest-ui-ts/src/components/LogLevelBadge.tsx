import {JobLogLevel} from "../types/JobLogLevel";
import {Badge} from "react-bootstrap";

export interface LogLevelBadgeProps {
    level: JobLogLevel
}

export function LogLevelBadge ({level}: LogLevelBadgeProps) {
    const getClassName = () => {
        switch (level) {
            case JobLogLevel.Verbose: return "secondary";
            case JobLogLevel.Debug: return "secondary";
            case JobLogLevel.Information: return "success";
            case JobLogLevel.Warning: return "warning";
            case JobLogLevel.Error: return "danger";
            case JobLogLevel.Fatal: return "danger";
            default: return "secondary";
        }
    }

    return (<Badge bg={getClassName()}>{level.toString()}</Badge>)
}