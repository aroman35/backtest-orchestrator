import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import {Button, Modal} from "react-bootstrap";
import React, {useState} from "react";
import {GlobalIdSearch} from "./GlobalIdSearch";

export function Navigation() {
    const [lgShow, setLgShow] = useState(false);
    return (
        <>
        <Navbar bg="primary" variant="dark">
            <Container>
                <Nav className="me-auto">
                    <Nav.Link href="/backtests">Backtests</Nav.Link>
                    <Nav.Link href="/pricing">Pricing</Nav.Link>
                </Nav>
                <Button className="d-flex" variant="outline" onClick={() => setLgShow(true)}>
                    <b className="bi bi-search" style={{color: 'white'}}/>
                </Button>
            </Container>
        </Navbar>
        <Modal
            size="lg"
            show={lgShow}
            onHide={() => setLgShow(false)}
            aria-labelledby="global-is-search-modal"
        >
            <Modal.Header closeButton>
                <Modal.Title id="global-is-search-modal">
                    Find item by id
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <GlobalIdSearch />
            </Modal.Body>
        </Modal>
        </>
    )
}