import {TaskStatus} from "../types/TaskStatus";
import {Badge} from "react-bootstrap";

interface TaskStatusBadgeProps {
    status: TaskStatus
}

export function TaskStatusBadge({status}: TaskStatusBadgeProps) {
    const getClassName = () => {
        switch (status) {
            case TaskStatus.NotDefined: return "secondary";
            case TaskStatus.Queued: return "secondary";
            case TaskStatus.Running: return "primary";
            case TaskStatus.Finished: return "success";
            case TaskStatus.Canceled: return "danger";
            case TaskStatus.Failed: return "danger";
            case TaskStatus.NoMd: return "warning";
            default: return "secondary";
        }
    }

    return (<Badge bg={getClassName()}>{TaskStatus[status]}</Badge>)
}