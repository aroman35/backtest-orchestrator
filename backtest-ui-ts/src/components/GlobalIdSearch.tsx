import React from "react";
import {Form, Formik, FormikHelpers} from "formik";
import {Button, Card, FormControl} from "react-bootstrap";
import {useLazyOneMessageSearchQuery} from "../store/global/global.api";
import {TaskStatusBadge} from "./TaskStatusBadge";
import {ItemType} from "../types/ItemType";
import {BacktestsTabType} from "../store/backtests/backtestTabsSlice";
import {useActions} from "../hooks";

interface GlobalIdSearch {
    messageToFind: string
}
export function GlobalIdSearch() {
    const [searchQuery, result] = useLazyOneMessageSearchQuery()
    const {addTab, setActiveTab} = useActions()
    const getTabType = (type: ItemType) =>{
        switch (type){
            case ItemType.Backtest: return BacktestsTabType.backtest
            case ItemType.Simulation: return BacktestsTabType.simulation
            case ItemType.Job: return  BacktestsTabType.job
            default: return BacktestsTabType.list
        }
    }
    return (
        <>
            <Formik
                initialValues={{messageToFind: ''}}
                onSubmit={(
                    values: GlobalIdSearch,
                    { setSubmitting }: FormikHelpers<GlobalIdSearch>
                ) => {
                    searchQuery(values.messageToFind)
                }}
                >
                {({ values, errors, touched, handleChange, handleBlur, setFieldValue }) =>(
                        <Form>
                            <FormControl
                                id="input-message"
                                name="messageToFind"
                                placeholder="Enter item id here..."
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.messageToFind}
                            />
                        </Form>
                    )}
            </Formik>
            <hr/>
            {result.data &&
                <Card border="primary">
                    <Card.Header>{result.data.type}</Card.Header>
                    <Card.Body>
                        <Card.Title>{result.data.name}</Card.Title>
                        <Card.Text>
                            Progress: {result.data.progress}%
                            <br/>
                            Status: <TaskStatusBadge status={result.data.status}></TaskStatusBadge>
                        </Card.Text>
                    </Card.Body>
                    <hr/>
                    <Button
                        disabled={result.data.type === ItemType.NotFound}
                        onClick={_ => {
                            addTab({
                                id: result.data!.id,
                                type: getTabType(result.data!.type)
                            })
                            setActiveTab(result.data!.id)
                        }}
                    >Open</Button>
                </Card>
            }
        </>
    )
}