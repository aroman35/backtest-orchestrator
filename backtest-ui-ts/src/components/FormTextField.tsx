import React, {ElementType} from "react";
import { Form, InputGroup } from "react-bootstrap";
import { Field } from "formik";
import {FieldProps} from "formik/dist/Field";
import {FieldInputProps, FormikProps} from "formik/dist/types";

export interface FormTextFieldProps {
    as: ElementType
    md: any
    controlId: string
    label: string
    name: string
    type: string
    inputGroupPrepend: any
}

const FormTextField = (props: FormTextFieldProps) => {
    return (<></>
        // <Field
        //     name={name}
        //     render={(field: FieldInputProps<any>, form: FormikProps<any>) => {
        //         const isValid = !form.errors[field.name];
        //         const isInvalid = form.touched[field.name] && !isValid;
        //
        //         return (<Form.Group as={props.as} md={props.md} controlId={props.controlId}>
        //             <Form.Label>{props.label}</Form.Label>
        //             <InputGroup>
        //                 {props.inputGroupPrepend}
        //                 <Form.Control
        //                     {...field}
        //                     type={props.type}
        //                     isValid={form.touched[field.name] && isValid}
        //                     isInvalid={isInvalid}
        //                     feedback={form.errors[field.name]}
        //                 />
        //
        //                 <Form.Control.Feedback type="invalid">
        //                     {form.errors[field.name]}
        //                 </Form.Control.Feedback>
        //             </InputGroup>
        //         </Form.Group>)
        //     }
        // }
        // />
    );
};

FormTextField.defaultProps = {
    type: "text",
    inputGroupPrepend: null
};

export default FormTextField;
