import React, {useState} from "react";
import {Alert, Button} from "react-bootstrap";

interface ErrorMessageProps {
    message: string
}

export interface Errors {
    page: string[];
}

export interface ProblemDetails {
    type: string;
    title: string;
    status: number;
    instance: string;
    traceId: string;
    errors: Errors;
}

export function ErrorMessage({message}: ErrorMessageProps) {
    const [show, setShow] = useState(true);

    return (
        <>
            <Alert show={show} variant="danger">
                <Alert.Heading>Shit happens</Alert.Heading>
                <p>
                    {message}
                </p>
                <hr />
                <div className="d-flex justify-content-end">
                    <Button onClick={() => setShow(false)} variant="outline-danger">
                        <i className="bi bi-x-lg" color={'red'}> Close</i>
                    </Button>
                </div>
            </Alert>
        </>
    )
}