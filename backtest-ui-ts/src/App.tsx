import React from 'react';
import {Navigation} from "./components/Navigation";
import {BacktestsPage} from "./pages/Backtests/BacktestsPage";
import {Route, Routes} from "react-router-dom";
import {PricingPage} from "./pages/Pricing/PricingPage";

function App() {
    return (
        <>
            <Navigation/>
            <Routes>
                <Route path="/backtests" element={<BacktestsPage/>}></Route>
                <Route path="/pricing" element={<PricingPage/>}></Route>
            </Routes>
        </>
    )
}

export default App;