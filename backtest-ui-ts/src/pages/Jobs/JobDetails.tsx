import {Col, Row, Tab} from "react-bootstrap"
import Nav from "react-bootstrap/Nav";
import {JobLogsPage} from "./JobLogsPage";
import {useJobQuery} from "../../store/jobs/jobs.api";
import {BacktestFrontPane} from "../Backtests/BacktestFrontPane";
import React from "react";
import {Loader} from "../../components/Loader";
import {ErrorMessage} from "../../components/ErrorMessage";
import {useGetSimulationDetailsQuery} from "../../store/simulations/simulations.api";
import {useGetBacktestDetailsQuery} from "../../store/backtests/backtest.api";
import {JobResult} from "./JobResult";
import {TaskStatus} from "../../types/TaskStatus";

export interface JobDetailsProps {
    jobId: string
}

export function JobDetails({jobId}: JobDetailsProps) {
    const {
        isLoading: jobIsLoading,
        isError: jobIsError,
        data: jobData,
        error: jobError
    } = useJobQuery(jobId)

    const {
        isLoading: simulationIsLoading,
        isError: simulationIsError,
        data: simulationData,
        error: simulationError
    } = useGetSimulationDetailsQuery(
        jobData?.state.simulationId ?? '',
        {
            skip: !jobData
        }
    )

    const {
        isLoading: backtestIsLoading,
        isError: backtestIsError,
        data: backtestData,
        error: backtestError,
    } = useGetBacktestDetailsQuery(
        simulationData?.state.backtestId ?? '',
        {
            skip: !simulationData,
        }
    )

    if (jobIsLoading || simulationIsLoading || backtestIsLoading)
        return <Loader />
    if (jobIsError)
        return <ErrorMessage message={JSON.stringify(jobError)} />
    if (simulationIsError)
        return <ErrorMessage message={JSON.stringify(simulationError)} />
    if (backtestIsError)
        return <ErrorMessage message={JSON.stringify(backtestError)} />
    if (!jobData) return <ErrorMessage message="No job data" />
    if (!simulationData) return <ErrorMessage message="No simulation data" />
    if (!backtestData) return <ErrorMessage message="No backtest data" />

    return(
        <>
            <BacktestFrontPane
                backtestId={backtestData.id}
                backtestName={backtestData.state.name}
                simulationId={simulationData.id}
                simulationName={simulationData.state.name}
                jobId={jobData.id}
                jobName={jobData.state.name}
            />
            <Tab.Container id="left-tabs-example" defaultActiveKey="first">
                <Row>
                    <Col sm={2}>
                        <Nav variant="pills" className="flex-column">
                            <Nav.Item>
                                <Nav.Link eventKey="job-logs">Logs</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="job-result">Result</Nav.Link>
                            </Nav.Item>
                        </Nav>
                    </Col>
                    <Col sm={9}>
                        <Tab.Content>
                            <Tab.Pane eventKey="job-logs">
                                <JobLogsPage jobId={jobId}/>
                            </Tab.Pane>
                            <Tab.Pane eventKey="job-result">
                                <JobResult jobId={jobId}/>
                            </Tab.Pane>
                        </Tab.Content>
                    </Col>
                </Row>
            </Tab.Container>
        </>)
}