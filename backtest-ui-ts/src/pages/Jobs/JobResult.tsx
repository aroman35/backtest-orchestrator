import {JobDetailsProps} from "./JobDetails";
import {useJobResultQuery} from "../../store/jobs/jobs.api";
import React from "react";
import JSONPretty from "react-json-pretty";
import {Loader} from "../../components/Loader";
import {ErrorMessage} from "../../components/ErrorMessage";

export function JobResult({jobId}: JobDetailsProps) {
    const {
        isLoading,
        isError,
        data,
        error
    } = useJobResultQuery(jobId)
    if (isLoading)
        return <Loader/>
    if (isError)
        return <ErrorMessage message={JSON.stringify(error)}/>
    if (!data)
        return <ErrorMessage message="No result data" />
    return(
        <>
            {data.state.isSuccess &&
                <JSONPretty
                    id={`job-result-${data.id}`}
                    data={data.state.message}
                />}
        </>)
}