import {useGetStrategySettingsQuery} from "../../store/simulations/simulations.api";
import {Loader} from "../../components/Loader";
import {ErrorMessage} from "../../components/ErrorMessage";
import React from "react";
import JSONPretty from "react-json-pretty";

export interface StrategySettingsProps {
    id: string
}

export function StrategySettings({id}: StrategySettingsProps) {
    const {isLoading, isError, data, error} = useGetStrategySettingsQuery(id)

    if (isLoading) return (<Loader/>)
    if (isError) return (<ErrorMessage message={JSON.stringify(error)}></ErrorMessage>)

    return (
        <>
            {data && <JSONPretty
                id={`s-settings-${id}`}
                data={data.state.settingsSource}
            />}
        </>
    )
}