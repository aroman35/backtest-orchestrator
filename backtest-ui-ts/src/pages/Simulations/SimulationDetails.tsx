import {Loader} from "../../components/Loader";
import {ErrorMessage} from "../../components/ErrorMessage";
import React from "react";
import {useGetSimulationDetailsQuery} from "../../store/simulations/simulations.api";
import {BacktestFrontPane} from "../Backtests/BacktestFrontPane";
import {Accordion, Card} from "react-bootstrap";
import {BacktestProgress} from "../../components/BacktestProgress";
import {useGetBacktestDetailsQuery} from "../../store/backtests/backtest.api";
import {StrategySettings} from "./StrategySettings";
import {JobsList} from "../Jobs/JobsList";

export interface SimulationDetailsProps {
    simulationId: string
}

export function SimulationDetails({simulationId}: SimulationDetailsProps) {
    const {
        isLoading: simulationIsLoading,
        isError: simulationIsError,
        data: simulationData,
        error: simulationError
    } = useGetSimulationDetailsQuery(simulationId!)

    const {
        isLoading: backtestIsLoading,
        isError: backtestIsError,
        data: backtestData,
        error: backtestError,
    } = useGetBacktestDetailsQuery(
        simulationData?.state.backtestId ?? '',
        {
            skip: !simulationData,
        }
    )

    if (simulationIsLoading || backtestIsLoading) return <Loader />
    if (simulationIsError)
        return <ErrorMessage message={JSON.stringify(simulationError)} />
    if (backtestIsError)
        return <ErrorMessage message={JSON.stringify(backtestError)} />
    if (!simulationData) return <ErrorMessage message="No simulation data" />
    if (!backtestData) return <ErrorMessage message="No backtest data" />

    return (
        <>
            <BacktestFrontPane
                backtestId={backtestData.id}
                backtestName={backtestData.state.name}
                simulationId={simulationData.id}
                simulationName={simulationData.state.name}
            />
            <Card className="mx-auto w-75">
                <Card.Header className="text-center" style={{ position: 'relative' }}>
                    {simulationData.state.name}
                </Card.Header>
                <Card.Body>
                    <blockquote className="blockquote mb-0">
                        <Card.Text>
                            <strong>Start Date: </strong>
                            {new Date(backtestData.state.mdStart).toLocaleDateString()}
                            <br />
                            <strong>End Date: </strong>
                            {new Date(backtestData.state.mdEnd).toLocaleDateString()}
                            <br />
                            <strong>Created: </strong>
                            {new Date(simulationData.state.created).toLocaleString()}
                        </Card.Text>
                        <footer className="blockquote-footer">
                            <BacktestProgress progress={simulationData.state.summary.progress ?? 0}/>
                        </footer>
                    </blockquote>
                    <br/>
                    <Accordion>
                        <Accordion.Item eventKey="0">
                            <Accordion.Header>Strategy Settings</Accordion.Header>
                            <Accordion.Body>
                                <StrategySettings id={simulationData.id}></StrategySettings>
                            </Accordion.Body>
                        </Accordion.Item>
                        <Accordion.Item eventKey="1">
                            <Accordion.Header>Applied Params</Accordion.Header>
                            <Accordion.Body>

                            </Accordion.Body>
                        </Accordion.Item>
                    </Accordion>
                </Card.Body>
            </Card>
            <h1 className="m-2">Jobs</h1>
            <div className="m-2">
                <JobsList simulationId={simulationData.id}></JobsList>
            </div>
        </>
    )
}