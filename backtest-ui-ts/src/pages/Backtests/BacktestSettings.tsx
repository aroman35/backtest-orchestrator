import {useGetBacktestSettingsQuery} from "../../store/backtests/backtest.api";
import {Loader} from "../../components/Loader";
import {ErrorMessage} from "../../components/ErrorMessage";
import React from "react";
import JSONPretty from 'react-json-pretty';
import 'react-json-pretty/themes/acai.css';
import './json-view.css'

type BacktestSettingsProps = {
    id: string
}

export function BacktestSettings({id}: BacktestSettingsProps) {
    const {isLoading, isError, data, error} = useGetBacktestSettingsQuery(id)

    if (isLoading) return (<Loader/>)
    if (isError) return (<ErrorMessage message={JSON.stringify(error)}></ErrorMessage>)

    return (
       <>
           {data && <JSONPretty
               id={`bt-settings-${id}`}
               data={data.state.settingsSource}
           />}
       </>
)}