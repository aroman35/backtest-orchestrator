import Tabs from 'react-bootstrap/Tabs';
import {useActions, useAppSelector} from "../../hooks";
import {backtestsTabsAdapter, BacktestsTabType} from "../../store/backtests/backtestTabsSlice";
import {Spinner, Tab} from "react-bootstrap";
import {BacktestDetails} from "./BacktestDetails";
import {BacktestsList} from "./BacktestsList";
import React, {useEffect} from "react";
import {SimulationDetails} from "../Simulations/SimulationDetails";
import {useLocation, useNavigate} from "react-router-dom";
import {JobDetails} from "../Jobs/JobDetails";

export interface OpenBacktestsProps {
    backtestId: string
    simulationId: string
    jobId: string
}

export function BacktestsPage () {
    const { activeTab, tabContents, selectTabContent, totalPages, page} = useAppSelector(
        (state) => ({
        activeTab: state.backtestsTabs.activeTab,
        tabContents: backtestsTabsAdapter.getSelectors().selectAll(state.backtestsTabs),
        selectTabContent: (id: string) => backtestsTabsAdapter.getSelectors().selectById(state.backtestsTabs, id),
        totalPages: state.backtests.totalPages,
        page: state.backtests.pageQuery.page
    }))
    const {addTab, removeBacktestTab, setActiveTab} = useActions()
    const {search} = useLocation()
    const navigate = useNavigate()
    const queryParams = new URLSearchParams(search);

    useEffect(() => {
        const backtestId = queryParams.get("backtestId")
        if (backtestId)
        {
            addTab({
                id: backtestId,
                type: BacktestsTabType.backtest
            })
            setActiveTab(backtestId)
            navigate("/backtests")
        }
        const simulationId = queryParams.get("simulationId")
        if (simulationId)
        {
            addTab({
                id: simulationId,
                type: BacktestsTabType.simulation
            })
            setActiveTab(simulationId)
            navigate("/backtests")
        }
        const jobId = queryParams.get("jobId")
        if (jobId)
        {
            addTab({
                id: jobId,
                type: BacktestsTabType.job
            })
            setActiveTab(jobId)
            navigate("/backtests")
        }

    }, [queryParams, addTab, setActiveTab, navigate])

    const handleTabChange = (eventKey: string) => {
        setActiveTab(eventKey)
    }

    const handleRemoveTab = (id: string) => {
        removeBacktestTab(id)
    };

    const handleTabTitleMouseDown = (
        event: React.MouseEvent<HTMLDivElement, MouseEvent>,
        id: string
        ) => {
        if (event.button === 1) {
            handleRemoveTab(id);
        }
    }

    const renderTabTitle = (title: string, id: string, isLoading: boolean) => {
        return (
            <div
                onMouseDown={event => handleTabTitleMouseDown(event, id)}
                onContextMenu={event => event.preventDefault()}
                >
                {title}{' '}
                {isLoading && <Spinner animation="border" variant="success" size="sm" />}
                <b
                    className="bi bi-x-lg"
                    style={{color: 'green', cursor: 'pointer'}}
                    onClick={_ => handleRemoveTab(id)}
                />
            </div>
        )
    }

    const prepareTab = function (id: string) {
        const tabItem = selectTabContent(id)

        switch (tabItem?.type) {
            case BacktestsTabType.backtest:
                return (
                    <Tab key={id} eventKey={id} title={renderTabTitle(`backtest [${tabItem.name ? tabItem.name : "Loading..."}]`, id, !tabItem.name)}>
                        <BacktestDetails id={id}/>
                    </Tab>
                )
            case BacktestsTabType.simulation:
                return (
                    <Tab key={id} eventKey={id} title={renderTabTitle(`simulation [${tabItem.name ? tabItem.name : "Loading..."}]`, id, !tabItem.name)}>
                        <SimulationDetails simulationId={id}/>
                    </Tab>
                )
            case BacktestsTabType.job:
                return (
                    <Tab key={id} eventKey={id} title={renderTabTitle(`job [${tabItem.name ? tabItem.name : "Loading..."}]`, id, !tabItem.name)}>
                        <JobDetails jobId={id}/>
                    </Tab>
                )
        }
    }

    return (
        <Tabs
            defaultActiveKey="backtestsList"
            id="backtests-page"
            className="mb-3"
            activeKey={activeTab}
            onSelect={x => handleTabChange(x!)}
        >
            <Tab key="backtests-list" eventKey="backtests-list" title={`Backtests page ${page} of ${totalPages}`}>
                <BacktestsList/>
            </Tab>
            {tabContents.map(tab => prepareTab(tab.id))}
        </Tabs>
    )
}