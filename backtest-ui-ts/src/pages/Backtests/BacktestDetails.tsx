import {BacktestFrontPane} from "./BacktestFrontPane";
import {useGetBacktestDetailsQuery} from "../../store/backtests/backtest.api";
import {Loader} from "../../components/Loader";
import React from "react";
import {ErrorMessage} from "../../components/ErrorMessage";
import {Accordion, Badge, Card} from "react-bootstrap";
import {BacktestProgress} from "../../components/BacktestProgress";
import {BacktestSettings} from "./BacktestSettings";
import {SimulationsList} from "../Simulations/SimulationsList";
import {StrategySettings} from "../Simulations/StrategySettings";

type  BacktestDetailsProps = {
    id: string
}

export function BacktestDetails({id}: BacktestDetailsProps) {
    const {isLoading, isError, data, error} = useGetBacktestDetailsQuery(id!)

    if (isLoading) return (<Loader/>)
    if (isError) return (<ErrorMessage message={JSON.stringify(error)}></ErrorMessage>)

    return (
        <>
            <BacktestFrontPane backtestId={id!} backtestName={data!.state.name}/>
            <Card className="mx-auto w-75">
                <Card.Header className="text-center" style={{ position: 'relative' }}>
                    {data?.state.name}
                    {data && data.state.splitDays &&
                        <Badge bg="success" className="position-absolute" style={{ top: 0, right: 0 }}>
                            Split Days
                        </Badge>
                    }
                </Card.Header>
                <Card.Body>
                    <blockquote className="blockquote mb-0">
                        <Card.Text>
                            <strong>Start Date: </strong>
                            {data && new Date(data.state.mdStart).toLocaleDateString()}
                            <br />
                            <strong>End Date: </strong>
                            {data && new Date(data.state.mdEnd).toLocaleDateString()}
                            <br />
                            <strong>Enable Logs: </strong>
                            {data?.state.enableLogs ? 'Yes' : 'No'}
                            <br />
                            <strong>Enable Telemetry: </strong>
                            {data?.state.enableTelemetry ? 'Yes' : 'No'}
                            <br />
                            <strong>Retry Count: </strong>
                            {data?.state.retryCount}
                            <br />
                            <strong>Reporting Timeout: </strong>
                            {data?.state.reportingTimeoutSec} seconds
                            <br />
                            <strong>Created: </strong>
                            {data && new Date(data.state.created).toLocaleString()}
                        </Card.Text>
                        <footer className="blockquote-footer">
                            <BacktestProgress progress={data?.state.summary.progress ?? 0}/>
                        </footer>
                    </blockquote>
                    <br/>
                    <Accordion>
                        <Accordion.Item eventKey="0">
                            <Accordion.Header>Backtest Settings</Accordion.Header>
                            <Accordion.Body>
                                <BacktestSettings id={id}/>
                            </Accordion.Body>
                        </Accordion.Item>
                        <Accordion.Item eventKey="1">
                            <Accordion.Header>Strategy Settings</Accordion.Header>
                            <Accordion.Body>
                                <StrategySettings id={id}></StrategySettings>
                            </Accordion.Body>
                        </Accordion.Item>
                    </Accordion>
                </Card.Body>
            </Card>
            <h1 className="m-2">Simulations</h1>
            <div className="m-2">
                <SimulationsList backtestId={id}></SimulationsList>
            </div>
        </>
    )
}