import {Formik, Form } from "formik";
import { Row, Col, FormGroup, FormLabel, FormControl, Button} from "react-bootstrap";
import {useActions, useAppSelector} from "../../hooks";
import {IBacktestsPageQuery} from "../../types/IBacktestsPageQuery";
import {TaskStatus} from "../../types/TaskStatus";
import * as Yup from 'yup';
import Select from 'react-select';
import FormRange from "react-bootstrap/FormRange";

export function BacktestsSearchPanel() {
    const {pageQuery} = useAppSelector(state => state.backtests);
    const {applySearchQuery} = useActions()

    const validationSchema = Yup.object().shape({
        nameMatch: Yup.string(),
        createdAfter: Yup.date(),
        createdBefore: Yup.date(),
        inStatus: Yup.array(),
        splitDays: Yup.boolean(),
        binaryNameMatch: Yup.string(),
        maxProgress: Yup.number().integer().min(0).max(100),
        page: Yup.number().integer().min(1),
        itemsPerPage: Yup.number().integer().min(1)
    });

    const statusOptions = Object.values(TaskStatus).map((status) => ({
        value: status,
        label: status,
    }));

    const handleSubmit = (values: IBacktestsPageQuery) => {
        if (values.inStatus && values.inStatus.length === 0) {
            const { inStatus, ...rest } = values;
            applySearchQuery(rest)
        } else {
            applySearchQuery(values)
        }
    }

    return (
        <>
            <Formik initialValues={pageQuery} validationSchema={validationSchema} onSubmit={handleSubmit}>
                {({ values, errors, touched, handleChange, handleBlur, setFieldValue }) => (
                    <Form>
                        <Row>
                            <Col xs={12} sm={6} md={4}>
                                <FormGroup>
                                    <FormLabel>Name Match</FormLabel>
                                    <FormControl
                                        type="text"
                                        name="nameMatch"
                                        value={values.nameMatch ? values.nameMatch : ""}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        isInvalid={touched.nameMatch && !!errors.nameMatch}
                                    />
                                    <FormControl.Feedback type="invalid">{errors.nameMatch}</FormControl.Feedback>
                                </FormGroup>
                            </Col>
                            <Col xs={12} sm={6} md={4}>
                                <FormGroup>
                                    <FormLabel>Created After</FormLabel>
                                    <FormControl
                                        type="date"
                                        name="createdAfter"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        isInvalid={touched.createdAfter && !!errors.createdAfter}
                                    />
                                    <FormControl.Feedback type="invalid">{errors.createdAfter}</FormControl.Feedback>
                                </FormGroup>
                            </Col>
                            <Col xs={12} sm={6} md={4}>
                                <FormGroup>
                                    <FormLabel>Created Before</FormLabel>
                                    <FormControl
                                        type="date"
                                        name="createdBefore"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        isInvalid={touched.createdBefore &&
                                            !!errors.createdBefore}
                                    />
                                    <FormControl.Feedback type="invalid">{errors.createdBefore}</FormControl.Feedback>
                                </FormGroup>
                            </Col>
                            <Col xs={12} sm={6} md={4}>
                                <FormGroup>
                                    <FormLabel>In Status</FormLabel>
                                    <Select
                                        name="inStatus"
                                        options={statusOptions}
                                        isMulti
                                        // value={statusOptions.filter((option) => values.inStatus?.includes())}
                                        onChange={(selectedOptions) =>
                                            setFieldValue('inStatus', selectedOptions.map((option) => option.value))
                                        }
                                        onBlur={handleBlur}
                                        // isInvalid={touched.inStatus && !!errors.inStatus}
                                    />
                                    <FormControl.Feedback type="invalid">{errors.inStatus}</FormControl.Feedback>
                                </FormGroup>
                            </Col>
                            <Col xs={12} sm={6} md={4}>
                                <FormGroup>
                                    <FormLabel>Binary Name Match</FormLabel>
                                    <FormControl
                                        type="text"
                                        name="binaryNameMatch"
                                        value={values.binaryNameMatch}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        isInvalid={touched.binaryNameMatch && !!errors.binaryNameMatch}
                                    />
                                    <FormControl.Feedback type="invalid">{errors.binaryNameMatch}</FormControl.Feedback>
                                </FormGroup>
                            </Col>
                            <Col xs={12} sm={6} md={4}>
                                <FormGroup>
                                    <FormLabel>Max Progress {values.maxProgress && <b>{values.maxProgress}%</b>}</FormLabel>
                                    <FormRange
                                        min={0}
                                        max={100}
                                        name="maxProgress"
                                        // value={values.maxProgress ?? 0}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                    />
                                    <FormControl.Feedback type="invalid">{errors.maxProgress}</FormControl.Feedback>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Button type="submit" variant="outline-primary" style={{ marginTop: '10px', margin: 'auto', display: 'block' }} >Search</Button>
                    </Form>
                )}
            </Formik>
        </>)
}