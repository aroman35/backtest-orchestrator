import {Breadcrumb, Button, OverlayTrigger, Tooltip} from "react-bootstrap";
import {useActions} from "../../hooks";
import {BacktestsTabType} from "../../store/backtests/backtestTabsSlice";
import {Share, BoxArrowUp} from 'react-bootstrap-icons';


export interface BacktestFrontPaneProps {
    backtestName: string
    backtestId: string
    simulationName?: string
    simulationId?: string
    jobName?: string
    jobId?: string
}

export function BacktestFrontPane(props: BacktestFrontPaneProps) {
    const {addBacktestTab, setActiveTab, addSimulationTab} = useActions()
    const linkAddress = () =>{
        if (props.jobId)
            return `jobId=${props.jobId}`
        if (props.simulationId)
            return `simulationId=${props.simulationId}`
        if (props.backtestId)
            return `backtestId=${props.backtestId}`
    }
    const renderTooltip = (props: any) => (
        <Tooltip id="button-tooltip" {...props}>
            {`${window.location.protocol}//${window.location.host}/backtests?${linkAddress()}`}
        </Tooltip>
    );
    return (
        <Breadcrumb>
            <OverlayTrigger
                trigger="click"
                placement="bottom"
                delay={{show: 250, hide: 400}}
                overlay={renderTooltip}
            >
                <BoxArrowUp color="white" size={24}/>
            </OverlayTrigger> {' '}
            <Breadcrumb.Item
                onClick={() => {
                    addBacktestTab({
                        id: props.backtestId,
                        type: BacktestsTabType.backtest
                    })
                    setActiveTab(props.backtestId)
                }}
                style={{cursor: 'pointer'}}
            >{props.backtestName}</Breadcrumb.Item>
            {props.simulationName && props.simulationId && <Breadcrumb.Item onClick={() => {
                addSimulationTab({
                    id: props.simulationId!,
                    type: BacktestsTabType.simulation
                })
                setActiveTab(props.simulationId!)
            }}>{props.simulationName}</Breadcrumb.Item>}
            {props.jobName && props.jobId && <Breadcrumb.Item>{props.jobName}</Breadcrumb.Item>}
        </Breadcrumb>
    )
}