cd Backtest.Silo
dotnet publish -c Release --os linux --arch x64 -p:PublishProfile=DefaultContainer
cd ./../Backtest.Api
dotnet publish -c Release --os linux --arch x64 -p:PublishProfile=DefaultContainer
cd ./../Backtest.Scheduler
dotnet publish -c Release --os linux --arch x64 -p:PublishProfile=DefaultContainer
cd ./../

docker tag backtest-silo:latest 192.168.9.10:5000/services/backtest-silo:latest
docker image push 192.168.9.10:5000/services/backtest-silo:latest
docker tag backtest-scheduler:latest 192.168.9.10:5000/services/backtest-scheduler:latest
docker image push 192.168.9.10:5000/services/backtest-scheduler:latest
docker tag backtest-api:latest 192.168.9.10:5000/services/backtest-api:latest
docker image push 192.168.9.10:5000/services/backtest-api:latest