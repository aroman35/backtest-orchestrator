﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace FlashPipes.Server
{
    public class FlashPipesConnectionFactory
    {
        private static FlashPipesConnectionFactory _factory;
        private static readonly object Sync = new();
        
        private readonly IServiceProvider _serviceProvider;
        private readonly ConcurrentDictionary<Guid, IFlashPipesServer> _servers;

        private FlashPipesConnectionFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _servers = new ConcurrentDictionary<Guid, IFlashPipesServer>();
        }

        public static FlashPipesConnectionFactory New(IServiceProvider serviceProvider)
        {
            lock (Sync) _factory ??= new FlashPipesConnectionFactory(serviceProvider);
            return _factory;
        }

        public IFlashPipesConnection CreateServer(Guid pipeName, CancellationToken cancellationToken = default)
        {
            lock (Sync)
            {
                if (_servers.TryRemove(pipeName, out var existingServer))
                    existingServer.Close();

                existingServer = new FlashPipesServer(pipeName);
                _servers.AddOrUpdate(pipeName, _ => existingServer, (_, __) => existingServer);
                return new FlashPipesConnection(existingServer, _serviceProvider);
            }
        }
    }
}