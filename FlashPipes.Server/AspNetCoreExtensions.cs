﻿using System;
using System.Linq;
using System.Reflection;
using FlashPipes.Common.Exceptions;
using Microsoft.Extensions.DependencyInjection;

namespace FlashPipes.Server
{
    public static class AspNetCoreExtensions
    {
        /// <summary>
        /// Map flash pipes controllers and middlewares from current domain
        /// </summary>
        public static IServiceCollection AddFlashPipesServer(this IServiceCollection services)
        {
            return services.AddFlashPipesServer(AppDomain.CurrentDomain.GetAssemblies());
        }

        /// <summary>
        /// Map flash pipes controllers and middlewares from specific assemblies
        /// </summary>
        public static IServiceCollection AddFlashPipesServer(this IServiceCollection services, params Assembly[] assemblies)
        {
            if (!assemblies.Any())
                throw new FlashPipesException("No assemblies were specified");

            var controllersFromAssemblies = assemblies
                .SelectMany(s => s.GetTypes())
                .Where(p => typeof(FlashPipesMiddleware).IsAssignableFrom(p) && !p.IsAbstract);

            foreach (var controller in controllersFromAssemblies)
                services.AddScoped(controller);

            return services;
        }
    }
}