using System.Threading.Tasks;
using FlashPipes.Common.Messaging;

namespace FlashPipes.Server
{
    public abstract class FlashPipesMiddleware
    {
        private FlashPipesMiddleware _next;

        internal void AddNext(FlashPipesMiddleware next)
        {
            _next = next;
        }

        public abstract Task<FlashPipesMessage> Execute(FlashPipesMessage message);

        protected Task<FlashPipesMessage> Next(FlashPipesMessage message)
        {
            return _next is not null ? _next.Execute(message) : Task.FromResult(message);
        }
    }
}