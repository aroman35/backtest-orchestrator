﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using FlashPipes.Common.Exceptions;
using FlashPipes.Common.Messaging;

namespace FlashPipes.Server
{
    [PipeRoute("base")]
    public abstract class FlashPipesController : FlashPipesMiddleware
    {
        public Func<Task> Callback { get; protected set; }
        protected FlashPipesMessage RequestMessage { get; private set; }
        public FlashPipesMessage ResponseMessage { get; private set; }
        protected string Route;
        
        private IDictionary<string, MethodInfo> _actionMethodInputParameters;

        protected FlashPipesController()
        {
            ValidateControllerInitialState();
        }
        
        public override async Task<FlashPipesMessage> Execute(FlashPipesMessage message)
        {
            await HandlePipeRequest(message);
            return ResponseMessage;
        }

        private async Task HandlePipeRequest(FlashPipesMessage flashPipesMessage)
        {
            Route = string.Join('/', flashPipesMessage.Route
                    .Split('/')
                    .Where(x => !string.IsNullOrEmpty(x))
                    .Select(x => x.ToLowerInvariant())
                    .Skip(1));
            
            RequestMessage = flashPipesMessage;
            if (flashPipesMessage.MessageType == FlashPipesMessageType.Response)
            {
                ResponseMessage = new FlashPipesMessage
                {
                    MessageType = FlashPipesMessageType.Response,
                    ErrorMessage = "Expected request, but received response"
                };
                return;
            }

            if (!_actionMethodInputParameters.TryGetValue(Route, out var callingMethod))
            {
                ResponseMessage = new FlashPipesMessage
                {
                    MessageType = FlashPipesMessageType.Response,
                    ErrorMessage = $"Method \"{Route}\" is not declared"
                };
                return;
            }
            
            var inputType = callingMethod
                .GetParameters()
                .FirstOrDefault(x => x.ParameterType != typeof(CancellationToken))?.ParameterType;
            
            if (RequestMessage.HasValue && inputType != null)
                await InvokeAsync(callingMethod, RequestMessage.GetPayload(inputType)).ConfigureAwait(false);
            else
                await InvokeAsync(callingMethod).ConfigureAwait(false);
        }
        
        private async Task InvokeAsync(MethodInfo callingMethod, params object[] parameters)
        {
            var isAwaitable = callingMethod.GetCustomAttributes<AsyncStateMachineAttribute>().Any();

            object result = null;
            bool isVoid;
            var returnType = callingMethod.ReturnType;
            
            if (isAwaitable)
            {
                isVoid = !callingMethod.ReturnType.IsGenericType;
                if (isVoid)
                {
                    await InvokeAsyncMethod(callingMethod, parameters).ConfigureAwait(false);
                }
                else
                {
                    returnType = callingMethod.ReturnType.GenericTypeArguments[0];
                    result = await InvokeAsyncMethodAndReturn(callingMethod, parameters).ConfigureAwait(false);
                }
            }
            else
            {
                isVoid = callingMethod.ReturnType == typeof(void);
                if (isVoid)
                    callingMethod.Invoke(this, parameters);
                else
                    result = callingMethod.Invoke(this, parameters);
            }

            if (!isVoid)
            {
                var entireResult = Convert.ChangeType(result, returnType);
                ResponseMessage = new FlashPipesMessage
                {
                    MessageType = FlashPipesMessageType.Response,
                    Route = Route
                };
                ResponseMessage.SetPayload(entireResult);
            }
            
            if (isVoid)
                ResponseMessage = FlashPipesMessage.Default;
        }
        
        private async Task<object> InvokeAsyncMethodAndReturn(MethodInfo @this, params object[] parameters)
        {
            var task = (Task)@this.Invoke(this, parameters);
            if (task == null)
                throw  new ArgumentNullException();
            
            await task.ConfigureAwait(false);
            var resultProperty = task.GetType().GetProperty("Result");
            return resultProperty?.GetValue(task);
        }

        private async Task InvokeAsyncMethod(MethodInfo @this, params object[] parameters)
        {
            var result = @this.Invoke(this, parameters);
            
            if (result is Task task)
                await task;
            
            ResponseMessage = FlashPipesMessage.Default;
        }

        private void ValidateControllerInitialState()
        {
            var actionMethods = GetType()
                .GetMethods(BindingFlags.Public|BindingFlags.Instance)
                .Where(x => !string.IsNullOrEmpty(x.GetCustomAttribute<PipeRouteAttribute>()?.Route))
                .ToArray();

            if (actionMethods.Select(x =>
                x.GetParameters()
                    .Where(param => param.ParameterType != typeof(CancellationToken))
                    .Select(param => param.Name)
                    .ToArray())
                .Any(x => x.Length > 1))
            {
                throw ControllerException("Input arguments count must be 1");
            }

            _actionMethodInputParameters = actionMethods
                .ToDictionary(
                    x => x.GetCustomAttribute<PipeRouteAttribute>()!.Route,
                    x => x);
        }

        private FlashPipesPipeControllerException ControllerException(string message) => new(RequestMessage, message);
    }
}