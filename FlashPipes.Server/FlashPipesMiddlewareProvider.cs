using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using FlashPipes.Common.Exceptions;
using FlashPipes.Common.Messaging;
using Microsoft.Extensions.DependencyInjection;

namespace FlashPipes.Server
{
    public class FlashPipesMiddlewareProvider : FlashPipesMiddleware
    {
        private FlashPipesMiddleware _middlewareChain;
        private readonly ConcurrentStack<FlashPipesMiddleware> _registeredMiddlewares;

        public FlashPipesMiddlewareProvider()
        {
            _registeredMiddlewares = new ConcurrentStack<FlashPipesMiddleware>();
        }

        internal void Add(IServiceScope scope, Type middlewareType)
        {
            var middleware = scope.ServiceProvider.GetRequiredService(middlewareType) as FlashPipesMiddleware
                             ?? throw new FlashPipesException($"Middleware {middlewareType.Name} was not registered");

            _registeredMiddlewares.Push(middleware);
        }

        private void Build()
        {
            while (_registeredMiddlewares.TryPop(out var middleware))
            {
                if (_middlewareChain is not null)
                    middleware.AddNext(_middlewareChain);
                _middlewareChain = middleware;
            }
        }

        internal Task<FlashPipesMessage> RunMiddlewareChain(FlashPipesController controller, FlashPipesMessage message)
        {
            _registeredMiddlewares.Push(controller);
            Build();
            return Execute(message);
        }

        public override Task<FlashPipesMessage> Execute(FlashPipesMessage message)
        {
            return _middlewareChain.Execute(message);
        }
    }
}