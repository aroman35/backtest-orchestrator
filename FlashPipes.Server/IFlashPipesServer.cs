﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FlashPipes.Common.Messaging;

namespace FlashPipes.Server
{
    public interface IFlashPipesServer : IDisposable, IAsyncDisposable
    {
        Guid PipeName { get; }
        bool IsConnected { get; }
        bool IsTerminated { get; }
        bool IsDisposed { get; }
        Task StartListeningAsync(Func<FlashPipesMessage, CancellationToken, Task> payloadHandler, CancellationToken cancellationToken);
        void SetErrorHandler(Action<Exception, string> handler);
        Task SendAsync(FlashPipesMessage message, CancellationToken cancellationToken = default);
        void Close();
    }
}