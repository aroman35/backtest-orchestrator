﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using FlashPipes.Common.Exceptions;
using FlashPipes.Common.Messaging;
using Microsoft.Extensions.DependencyInjection;

namespace FlashPipes.Server
{
    public class FlashPipesConnection : IFlashPipesConnection
    {
        private readonly IDictionary<string, Type> _baseRoutePaths;
        private readonly IServiceProvider _serviceProvider;
        private readonly IFlashPipesServer _flashPipesServer;

        private readonly List<Type> _middlewareTypes;

        internal FlashPipesConnection(
            IFlashPipesServer flashPipesServer,
            IServiceProvider serviceProvider)
        {
            _flashPipesServer = flashPipesServer;
            _serviceProvider = serviceProvider;
            _baseRoutePaths = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => typeof(FlashPipesController).IsAssignableFrom(p) && !p.IsAbstract)
                .ToDictionary(
                    x => x.GetCustomAttribute<PipeRouteAttribute>(false)?.Route ??
                         x.Name.Replace("Controller", "").ToLowerInvariant(),
                    x => x);
            _middlewareTypes = new List<Type>();
        }

        public Task StartListeningAsync(Action<Exception, string> errorHandler, CancellationToken cancellationToken = default, int responseTimeoutMs = 30000)
        {
            _flashPipesServer.SetErrorHandler(errorHandler);
            return _flashPipesServer.StartListeningAsync(HandlePipeRequest, cancellationToken);
        }

        public IFlashPipesConnection WithMiddleware<TMiddleware>()
            where TMiddleware : FlashPipesMiddleware
        {
            _middlewareTypes.Add(typeof(TMiddleware));
            return this;
        }

        public void StartListening(Action<Exception, string> errorHandler, CancellationToken cancellationToken = default, int responseTimeoutMs = 30_000)
        {
            _flashPipesServer.SetErrorHandler(errorHandler);
            Task.Factory.StartNew(() => _flashPipesServer.StartListeningAsync(HandlePipeRequest, cancellationToken), TaskCreationOptions.LongRunning);
        }

        public void Close()
        {
            _flashPipesServer.Close();
        }

        private async Task HandlePipeRequest(FlashPipesMessage flashPipesMessage, CancellationToken cancellationToken)
        {
            if (flashPipesMessage.Route == FlashPipesMessage.EmptyRoute)
            {
                await _flashPipesServer.SendAsync(new FlashPipesMessage
                {
                    ErrorMessage = "Requested empty route"
                }, cancellationToken);
                throw new FlashPipesPipeRouterException(_flashPipesServer.PipeName, flashPipesMessage.Route,
                    "Requested empty route");
            }

            var parts = flashPipesMessage.Route
                .Split('/')
                .Where(x => !string.IsNullOrEmpty(x))
                .Select(x => x.ToLowerInvariant())
                .ToArray();

            if (parts.Length < 2)
            {
                await _flashPipesServer.SendAsync(new FlashPipesMessage
                {
                    ErrorMessage = "Requested invalid route"
                }, cancellationToken);
                throw new FlashPipesPipeRouterException(_flashPipesServer.PipeName, flashPipesMessage.Route,
                    "Invalid path (#1)");
            }

            if (!_baseRoutePaths.TryGetValue(parts[0], out var controllerType))
            {
                await _flashPipesServer.SendAsync(new FlashPipesMessage
                {
                    ErrorMessage = "Requested invalid route"
                }, cancellationToken);
                throw new FlashPipesPipeRouterException(_flashPipesServer.PipeName, flashPipesMessage.Route,
                    "Invalid path (#2)");
            }

            using var scope = _serviceProvider.CreateScope();
            var controller = scope.ServiceProvider.GetRequiredService(controllerType) as FlashPipesController
                             ?? throw new FlashPipesException("Controller was not found");
            var middlewareProvider = scope.ServiceProvider.GetRequiredService<FlashPipesMiddlewareProvider>()
                                     ?? throw new FlashPipesException("Middleware provider is not registered");
            foreach (var middlewareType in _middlewareTypes) middlewareProvider.Add(scope, middlewareType);
            var response = await middlewareProvider.RunMiddlewareChain(controller, flashPipesMessage);

            if (!flashPipesMessage.IsPost)
                await _flashPipesServer.SendAsync(response, cancellationToken);
            if (controller.Callback is not null)
                await controller.Callback();
        }

        public void Dispose()
        {
            _flashPipesServer.Dispose();
            Close();
        }

        public ValueTask DisposeAsync()
        {
            Close();
            return _flashPipesServer.DisposeAsync();
        }
    }
}