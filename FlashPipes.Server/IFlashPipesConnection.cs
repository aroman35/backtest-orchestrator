﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace FlashPipes.Server
{
    public interface IFlashPipesConnection : IDisposable, IAsyncDisposable
    {
        void StartListening(Action<Exception, string> errorHandler, CancellationToken cancellationToken = default, int responseTimeoutMs = 30_000);
        Task StartListeningAsync(Action<Exception, string> errorHandler, CancellationToken cancellationToken = default, int responseTimeoutMs = 30_000);

        IFlashPipesConnection WithMiddleware<TMiddleware>()
            where TMiddleware : FlashPipesMiddleware;
        void Close();
    }
}