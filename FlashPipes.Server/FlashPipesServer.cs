﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Threading;
using System.Threading.Tasks;
using FlashPipes.Common;
using FlashPipes.Common.Messaging;

namespace FlashPipes.Server
{
    public class FlashPipesServer : IFlashPipesServer
    {
        private readonly NamedPipeServerStream _pipeServer;
        public Guid PipeName { get; }
        public bool IsConnected => _pipeServer.IsConnected;
        public bool IsTerminated { get; private set; }

        public bool IsDisposed { get; private set; }

        private Action<Exception, string> _errorHandler;

        public FlashPipesServer(Guid pipeName)
        {
            _pipeServer = new NamedPipeServerStream(
                pipeName.ToString("D"),
                PipeDirection.InOut,
                NamedPipeServerStream.MaxAllowedServerInstances,
                PipeTransmissionMode.Byte,
                PipeOptions.Asynchronous);

            PipeName = pipeName;
        }

        public async Task StartListeningAsync(Func<FlashPipesMessage, CancellationToken, Task> payloadHandler, CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    if (!_pipeServer.IsConnected)
                        await _pipeServer.WaitForConnectionAsync(cancellationToken);
                
                    var message = FlashPipesMessage.Default;

                    if (_pipeServer.ReadAsMessage(ref message))
                        await payloadHandler(message, cancellationToken);
                }
                catch (Exception exception) when (exception is TaskCanceledException or ObjectDisposedException or IOException)
                {
                    IsTerminated = true;
                    return;
                }
                catch (Exception exception)
                {
                    _errorHandler?.Invoke(exception, $"Error listening pipe {PipeName}");
                    await Task.Delay(1000, cancellationToken);
                }
                finally
                {
                    await Task.Delay(10, cancellationToken);
                }
            }
        }

        public async Task SendAsync(FlashPipesMessage message, CancellationToken cancellationToken = default)
        {
            if (_pipeServer.CanWrite)
                await _pipeServer.WriteMessageAsync(message, cancellationToken);
        }

        public void Close()
        {
            _pipeServer?.Close();
        }

        public void SetErrorHandler(Action<Exception, string> handler)
        {
            _errorHandler = handler;
        }

        public void Dispose()
        {
            IsDisposed = true;
            _pipeServer.Dispose();
        }

        public async ValueTask DisposeAsync()
        {
            IsDisposed = true;
            await _pipeServer.DisposeAsync();
        }
    }
}